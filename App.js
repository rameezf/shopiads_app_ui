import React from 'react';
import Setup from './src/boot';
import {LogBox} from 'react-native';

const App = () => {
  LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
  LogBox.ignoreAllLogs();
  return <Setup />;
};

export default App;
