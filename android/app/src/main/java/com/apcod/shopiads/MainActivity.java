package com.apcod.shopiads;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends ReactActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    printHashKey(this);

  }
  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */

  public static void printHashKey(Context pContext) {
    try {
      PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
      for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        String hashKey = new String(Base64.encode(md.digest(), 0));
        Log.e("TAG", hashKey);
      }
    } catch (NoSuchAlgorithmException e) {
      Log.e("TAG", "printHashKey()", e);
    } catch (Exception e) {
      Log.e("TAG", "printHashKey()", e);
    }
  }

  @Override
  public void onBackPressed() {
    Intent intent=getIntent();
    intent.setData(null);
    super.onBackPressed();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override
  protected String getMainComponentName() {
    return "OfferDhamaka";
  }
}