import axios from "axios/index";
import humps from "humps";

import { } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { isStagging, Api } from "../../app.json";
import auth from "../Reducer/auth";
import {store, persistor} from "./ConfigureStore";
import * as RNLocalize from "react-native-localize";
import DeviceCountry from 'react-native-device-country';

const apikey = "RXKSIbnjklOp19PIKNnsmkOrosxkWO==";


let authClient = (countryUrls) => {
   let client = axios.create({
    baseURL: countryUrls.baseUrl,
    headers: {'Content-Type': 'application/json'},
    timeout: 0,
    cache: 'no-cache',
    mode: 'cors',
    redirect: 'follow',
    referrer: 'no-referrer',
    transformResponse: axios.defaults.transformResponse.concat(data => {
      if (typeof data === 'string') {
          return false;
      }
      return data;
    }),
  })
  client.interceptors.request.use(function (config) {
      let countrySelected = null;
      // DeviceCountry.getCountryCode().then((result) => {
      //   console.log("Device Country Code :::::",result);
      //   countrySelected = result.code
      // })
      // .catch((e) => {
      //   console.log(e);
      // });
      countrySelected = RNLocalize.getCountry();
      console.log("Country RNLocalize.getCountry() store ::::::",store && store.getState());
      if(store && store && store.getState() && store.getState().location.locationCountry != null) {
        countrySelected = store.getState().location.locationCountry != null && store.getState().location.locationCountry['countryId'];
      }
      console.log("Country Selected client js ::::::",countrySelected);
      config.baseURL = countrySelected && countrySelected.toUpperCase() == "US" ? "https://bkend.shopiads.com/Shopiads" : "https://www.shopiads.ca/Shopiads"
      console.log("Base url :::::",config)
      return config;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });
  return client;
};




const clients = {
  default: {
    client: authClient(Api),
  },
  none: {
    client : authClient(''),
  }
  //metric: perfClient(AUTH_URL)
};



export default clients;