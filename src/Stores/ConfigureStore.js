import { createStore, applyMiddleware, compose } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import { multiClientMiddleware } from "redux-axios-middleware";
import rootReducer from "../Reducer";
import AsyncStorage from "@react-native-async-storage/async-storage";
import thunkMiddleware from "redux-thunk";
import { name as appName } from "../../app.json";
import clients from "./Client";
import thunk from "redux-thunk";

const persistConfig = {
  key: "root",
  blacklist: [],
  whitelist: ['productTab','location'],
  keyPrefix: appName,
  storage: AsyncStorage
};

const middlewareConfig = {
  interceptors: {
    request: [{
      success: function ({ getState, dispatch, getSourceAction }, req) {
        console.log(req); //contains information about request object
        //...
      },
      error: function ({ getState, dispatch, getSourceAction }, error) {
        //...
      }
    }
    ],
    response: [{
      success: function ({ getState, dispatch, getSourceAction }, req) {
        console.log(req); //contains information about request object
        //...
      },
      error: function ({ getState, dispatch, getSourceAction }, error) {
        //...
      }
    }
    ]
  }
};

const middlewares = [
  thunkMiddleware,
  thunk,
  multiClientMiddleware(clients),
];
const persistedReducer = persistReducer(persistConfig, rootReducer);

let middleware = composeWithDevTools(
  applyMiddleware(...middlewares),
)

let store = createStore(persistedReducer, middleware);

let persistor = persistStore(store);

export { store, persistor };