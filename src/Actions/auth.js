import AsyncStorage from "@react-native-async-storage/async-storage";
import * as ACTION_TYPES from "../Constants/ActionTypes";

export const setUserData = (data) => ({
    type: ACTION_TYPES.SAVE_USER_DATA,
    payload: data
})

export const userUnRegistered = (userId) =>({
    types: [ACTION_TYPES.USER_UNREGISTERED,
        ACTION_TYPES.USER_UNREGISTERED_SUCCESS,
        ACTION_TYPES.USER_UNREGISTERED_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/stores/sendUserDeleteMail/"+userId 
    
            }
        } 
})