import * as ACTION_TYPES from "../Constants/ActionTypes";

export const getCity = (userid) => ({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
    ACTION_TYPES.GET_CITY_SUCCESS,
    ACTION_TYPES.GET_CITY_FAILED],

    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/states/" + userid 

        }
    }
})

export const getArea = (userid, cityid) => ({
    types: [ACTION_TYPES.GET_AREA_REQUESTED,
    ACTION_TYPES.GET_AREA_SUCCESS,
    ACTION_TYPES.GET_AREA_FAILED],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/cities/"+ userid + "/" + cityid

        }
    }
})

export const saveUserLocationCity = (data) => ({
    type: ACTION_TYPES.SAVE_USER_LOCATION_CITY,
    payload: data
});


export const saveUserLocationArea = (data) => ({
    type: ACTION_TYPES.SAVE_USER_LOCATION_AREA,
    payload: data
});


export const saveUserEmail = (data) => ({
    type: ACTION_TYPES.SAVE_EMAIL,
    payload: data
});


export const saveUserName  = (data) =>({
    type:  ACTION_TYPES.SAVE_NAME,
    payload:data
})


export const saveUserLocationCountry = (data) =>({
    type: ACTION_TYPES.SAVE_USER_LOCATION_COUNTRY,
    payload: data
});

export const saveUserMarketPlace = (data) =>({
    type: ACTION_TYPES.SAVE_USER_MARKET_PLACE,
    payload: data
});
