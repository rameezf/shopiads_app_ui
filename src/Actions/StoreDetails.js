import * as ACTION_TYPES from "../Constants/ActionTypes";


export const getStoreDetails = (userId) => ({
    types: [ACTION_TYPES.GET_STORE_DETAILS,
    ACTION_TYPES.GET_STORE_DETAILS_SUCCESS,
    ACTION_TYPES.GET_STORE_DETAILS_FAILED],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/stores/getStoreById/"+userId 
        }
    }
})

export const getOffersProducts = (offerId) => ({
    types: [ACTION_TYPES.GET_STORE_DETAILS,
    ACTION_TYPES.GET_STORE_DETAILS_SUCCESS,
    ACTION_TYPES.GET_STORE_DETAILS_FAILED],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/getOfferProducts/"+offerId
        }
    }
})

export const getOffersStoreId = (storeId) => ({
    types: [ACTION_TYPES.GET_STORE_DETAILS,
    ACTION_TYPES.GET_STORE_DETAILS_SUCCESS,
    ACTION_TYPES.GET_STORE_DETAILS_FAILED],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/getOffersById/bmFyZXNoQHlvcG1haWwuY29t/"+storeId
        }
    }
})


export const addUserSnips =(userId,data)=>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
    
       
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/snippets/add/"+userId ,
           }
          }
       
})


export const uploadBillImage = (storeName,data,billId) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/uploadBillImage/companyName/"+storeName+"/"+billId ,
           }
          }
})

export const uploadBillDetails = (data,billId) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/bills/"+billId ,
           }
          }
})

export const uploadRatingStoreImage = (storeName,data,imageid) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/uploadRatingImage/companyName/"+storeName+"/"+imageid ,
           }
          }
})


export const uploadRatingDetails = (data,userId) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/rating/"+ userId,
           }
          }
})


export const addFollowingApi = (data,userId) =>({

    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "GET",
              url: "/offers/makeFollow/"+userId+"/"+data+"/follow" ,
           }
          }
})
export const addLikeProducts = (data,userId,type="product") =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "GET",
              url: "/offers/makeLikes/"+userId+"/"+data+"/"+type ,
           }
          }
})


export const removeLikeProducts = (userId,item) =>({

    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "GET",
              url: "/offers/makeUnLikes/"+userId+"/"+(item.offerId?item.offerId :(item.storeId?item.storeId:item.productId))+"/"+item.type.toString().toLocaleLowerCase()+"/"+item.id,
           }
          }
   
})



export const makeUnlikeProducts =(userId, offerId,type,likeId) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "GET",
              url: "/offers/makeUnLikes/"+userId+"/"+offerId+"/"+type+"/"+likeId,
           }
          }
})


export const addStoreFollowingApi = (data) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_SUCCESS,
        ACTION_TYPES.UPLOAD_BILL_IMAGE_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              data:data,
              url: "/uploadBillImage/companyName/Mississauga2084/c5c5fb69816aada0dc5a1647ebaccc0f" ,
           }
          }
})




export const getHomeTabProducts =  (userid) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "stores/OffersBYStore/"+userid 
    
            }
        }
})

export const getAllTabs = (userid) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
    
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "stores/CollectionBYStore/"+userid 
    
            }
        }

})

export const getProductsTabsProducts = (userid) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
    
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/stores/productByStore/"+userid
    
            }
        }
})


export const getCollectionsByStoreTabProducts  = (data) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/getOfferProducts/"+data 
            }
        }

})

export const getStoreFollowList  = (storeId) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/getStoreFollowList/"+storeId 
            }
        }

})