import * as ACTION_TYPES from "../Constants/ActionTypes";

export const getOffersAll = (user, city, area) => ({
  types: [
    ACTION_TYPES.GET_OFFERS_ALL_REQUESTED,
    ACTION_TYPES.GET_OFFERS_ALL_SUCCESS,
    ACTION_TYPES.GET_OFFERS_ALL_FAILED,

  ],
  payload: {
    client: "default",
    request: {
      method: "GET",
      url: "/offers/all/" + user + "/" + city + "/" + area

    }
  }
});





export const getAllCity = (user, state) => ({
  types: [
    ACTION_TYPES.GET_OFFERS_ALL_REQUESTED,
    ACTION_TYPES.GET_OFFERS_ALL_SUCCESS,
    ACTION_TYPES.GET_OFFERS_ALL_FAILED,

  ],
  payload: {
    client: "default",
    request: {
      method: "GET",
      url: "/cities/" + user + "/" + state
    }
  }
});

export const getOfferCategory = (user, id, state, city,viewType,marketPlace) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],
  payload: {
    client: "default",
    request: {
      method: "GET",
      url:  "/stores/AllActiveSectionItem/bWUubWVkZHkzMTNAZ21haWwuY29t/"+id+"/"+city+"/"+state+"/"+viewType+"/"+marketPlace
    }
  }
});





export const getAllStoreList = (marketPlace) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],

  payload: {
    client: "default",
    request: {
      method: "GET",
      url:  "/stores/getAllStoreList/"+marketPlace
    }
  }
});


export const getAllSearchStoreList = (text,catId,marketPlace) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],

  payload: {
    client: "default",
    request: {
      method: "GET",
      url:  "/stores/searchStore/?text="+text+"&catId="+catId+"&marketPlace="+marketPlace
    }
  }
});




/*
*    
*  Offer Section
*/



export const getMainCategory =(user) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/mainCategoriesList/"+user
    }
  }
})



export const getSearchCategory =  (path) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/stores/searchProduct/"+path
    }
  }
})

export const getSubCategoriesList =(user) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/subCategoriesListByName/bmFyZXNoQHlvcG1haWwuY29t/All"
    }
  }
})



/**
 * 
 * Get Json All Keys
 */


export const getAllKeysFromJsonObject = (data) =>{

  const newData = Object.keys(data).reduce((result, currentKey) => {
    result.push(currentKey) ;
    return result;
  }, []);
  return newData;
}


export const getOfferBanner = (user, id, state, city,viewType,marketPlace) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],

  payload: {
    client: "default",
    request: {
      method: "GET",
      url: id=='Trending'? "/stores/AllActiveOffers/Z3VzdA==/"+city+"/"+state+"/"+viewType+"/"+marketPlace:"/stores/category/activeOffers/"+user+"/"+id+"/"+city+"/"+state+"/"+viewType+"/"+marketPlace
    }
  }
});



export const getOfferProducts = (user, id, state, city,marketPlace) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],

  payload: {
    client: "default",
    request: {
      method: "GET",
      url:id=='Trending'? "/stores/AllActiveProducts/Z3VzdA==/"+city+"/"+state+"/offer/"+marketPlace:"/stores/category/activeProducts/"+user+"/"+id+"/"+city+"/"+state+"/"+"offers/"+marketPlace
    }
  }
});




export const getAllStoresBasedonCategoryStores =(id,user, city, state)=>({
  
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url : id == 'Trending'?'/stores/AllActiveOffers/'+user+'/'+city+'/'+state+'/Store': (id =='All'? "/stores/getAllStoreList": "/stores/category/activeOffers/"+user+"/"+id+"/"+city+"/"+state+"/store")
    }
  }

})



export const  getOnlineStores = (user,cityId,stateId,categoryId,cityName,marketPlace) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/stores/OnLineStores/'+user+'/'+cityId+'/'+cityName+'/'+stateId+'/'+categoryId+"/"+marketPlace
    }
  }
})

export const  getInStores = (user,cityId,stateId,categoryId,cityName,marketPlace) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/stores/InStores/'+user+'/'+cityId+'/'+cityName+'/'+stateId+'/'+categoryId+"/"+marketPlace
    }
  }
})

export const checkInOffers  =(userId) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/getUserOfferRating/'+userId
    }
  }

})

export const checkInProducts  =(userId) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/getUserProductRating/'+userId
    }
  }

})
export const getNearByStores = (user,cityId,cityName,stateId,categoryId,marketPlace) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/stores/nearByStores/'+user+'/'+cityId+'/'+cityName+'/'+stateId+'/'+categoryId+"/"+marketPlace
    }
  }
})


export const getAllProductsBasedOnCategories =(cityId,categoryId,marketPlace)=>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  '/products/categories/cmFtYW5Ac2hvcGlhZHMuY29t/'+categoryId+'/'+cityId+'/'+marketPlace
    }
  }

})




export const userLogin =(userId,params)=>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "POST",
      data:params,
      
      url :  "users/"+userId
    }
  }

})



export const checkUserExist =(userId)=>({
  
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      
      url :  "getUserById/"+userId
    }
  }

})
export const getUserInfoByAppleId = (appleId) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",  
      url :  "/getUserByAppleId/"+appleId
    }
  }
})


export const getAllFavoriteProduct =(userId) =>({

  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  "getUserSnippets/"+userId+"/like"
    }
  }
})



export const updateFirebaseToken =(userId,firebaseToken, device) =>({

  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  "/updateUserTokenDevice/"+userId+"/"+firebaseToken+"/"+device
    }
  }
})



export const getAllStoreProducts =(storeId) =>({

  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  "getCompanyBranchProduct/"+storeId+"/active"
    }
  }
})

export const getAllFollowStore =(userId) =>({

  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :  "getUserSnippets/"+userId+"/follow"
    }
  }
})


export const getOffersAllStores =(user,city,state,viewType) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url :"/stores/AllActiveOffers/"+user+"/"+city+"/"+state+"/"+viewType
    }
  }
})

export const getOffersAllProducts = (user, city,state, viewType) =>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,

  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/stores/AllActiveProducts/"+user+"/"+city+"/"+state+"/"+viewType
    }
  }
})

export const getOffersAllActiveSectionItems= (user,catId,city,state,viewType)=>({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/stores/AllActiveSectionItem/"+user+"/"+catId+"/"+city+"/"+state+"/"+viewType
    }
  }
})


export const reportContactUs = (userId,params) => ({
  types: [
    ACTION_TYPES.GET_OFFER_CATEGORY_REQUESTED,
    ACTION_TYPES.GET_OFFER_CATEGORY_SUCCESS,
    ACTION_TYPES.GET_OFFER_CATEGORY_FAILURE,
  ],

  payload:{
    client :'default',
    request: {
      method: "POST",
      data:params,
      url :  "contactUs/"+userId
    }
  }
 
});

export const getCategoriesById =(userId,id) =>({
  types: [
    ACTION_TYPES.GET_CATEORIES_BY_ID,
    ACTION_TYPES.GET_CATEORIES_BY_ID_SUCCESS,
    ACTION_TYPES.GET_CATEORIES_BY_ID_FAILED
  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:"/getCategoriesById/"+userId+"/"+id
    }
  }
})


export const getImageExit = (path) =>({
  types: [
    ACTION_TYPES.GET_IMAGE_EXIST,
    ACTION_TYPES.GET_IMAGE_EXIST_SUCCESS,
    ACTION_TYPES.GET_IMAGE_EXIST_FAILED
  ],
  payload:{
    client :'default',
    request: {
      method: "GET",
      url:path
    }
  }
})



