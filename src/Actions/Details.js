import * as ACTION_TYPES from "../Constants/ActionTypes";

export const getProductDetails = (userid) => ({
    types: [ACTION_TYPES.GET_PRODUCT_DETAILS_REQUESTED,
    ACTION_TYPES.GET_PRODUCT_DETAILS_SUCCESS,
    ACTION_TYPES.GET_PRODUCT_DETAILS_FAILED],

    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/products/Z3VzdA==/"+userid 

        }
    }
})

export const getRating = (productId) =>({
    types: [ACTION_TYPES.GET_RATINGS_PRODUCT_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
    
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/ratingByProductId/"+productId 
    
            }
        }
})

export const checkOfferRating  = (userId) =>({

    types: [ACTION_TYPES.GET_RATINGS_PRODUCT_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/ratingByProductId/"+userId 
    
            }
        }
})

export const checkProductRating = (userId) => ({
    types: [ACTION_TYPES.GET_RATINGS_PRODUCT_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/ratingByProductId/"+userId 
    
            }
        }
})

export const getRatingByOfferId = (productId) =>({
    types: [ACTION_TYPES.GET_RATINGS_PRODUCT_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
    
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/ratingByOfferId/"+productId 
    
            }
        }
})

export const getSimilarProducts = (userId,marketPlace) =>({
    types: [ACTION_TYPES.GET_SIMILAR_PRODUCTS_REQUESTED,
        ACTION_TYPES.GET_RATING_PRODDUCT_SUCCESS,
        ACTION_TYPES.GET_RATING_PRODUCT_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/products/categories/Z3VzdA==/"+userId+"/notuseparameter/"+marketPlace 
            }
        }
})

export const productBuyNowClick = (userId,productId) =>({
    types: [ACTION_TYPES.GET_SIMILAR_PRODUCTS_REQUESTED,
        ACTION_TYPES.GET_RATING_PRODDUCT_SUCCESS,
        ACTION_TYPES.GET_RATING_PRODUCT_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/productBuyNowClick/"+userId+"/"+productId 
    
            }
        }

})


export const offerBuyNowClick = (userId,offerId) =>({
    types: [ACTION_TYPES.GET_SIMILAR_PRODUCTS_REQUESTED,
        ACTION_TYPES.GET_RATING_PRODDUCT_SUCCESS,
        ACTION_TYPES.GET_RATING_PRODUCT_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/offerBuyNowClick/"+userId+"/"+offerId 
    
            }
        } 
})


export const getImageUrlToBase64 = (path) =>({
    types: [ACTION_TYPES.GET_SIMILAR_PRODUCTS_REQUESTED,
        ACTION_TYPES.GET_RATING_PRODDUCT_SUCCESS,
        ACTION_TYPES.GET_RATING_PRODUCT_FAILED],
        payload: {
            client: "none",
            request: {
                method: "GET",
                url:path
    
            }
        }
})


export const isUserShopifyOrder = (userId,productId) =>({
    types: [ACTION_TYPES.IS_USER_SHOPIFY_ORDER,
        ACTION_TYPES.IS_USER_SHOPIFY_ORDER_SUCCESS,
        ACTION_TYPES.IS_USER_SHOPIFY_ORDER_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/isUserShopifyOrder/"+userId+"/"+productId 
    
            }
        } 
});


export const getVariantList = (productId) =>({
    types: [ACTION_TYPES.GET_VARIANT_LIST,
        ACTION_TYPES.GET_VARIANT_LIST_SUCCESS,
        ACTION_TYPES.GET_VARIANT_LIST_FAILED],
        payload: {
            client: "default",
            request: {
                method: "GET",
                url: "/variantProducts/"+productId 
    
            }
        } 
});
