import * as ACTION_TYPES from "../Constants/ActionTypes";

export const getStoresAll = (user, city, area) => ({
    types: [
        ACTION_TYPES.GET_STORES_ALL_REQUESTED,
        ACTION_TYPES.GET_STORES_ALL_SUCCESS,
        ACTION_TYPES.GET_STORES_ALL_FAILED,

    ],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/stores/all/" + user + "/" + city + "/" + area
        }
    }
});

export const getStoresCategory = (user, id, city, area) => ({
    types: [
        ACTION_TYPES.GET_STORES_CATEGORY_REQUESTED,
        ACTION_TYPES.GET_STORES_CATEGORY_SUCCESS,
        ACTION_TYPES.GET_STORES_CATEGORY_FAILURE,

    ],
    payload: {
        client: "default",
        request: {
            method: "GET",
            url: "/stores/categories/" + user + "/" + id + "/" + city + "/" + area

        }
    }
});





