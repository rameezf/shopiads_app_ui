import * as ACTION_TYPES from "../Constants/ActionTypes";

export const setProductKey = (data) => ({
    type: ACTION_TYPES.SAVE_PRODUCT_KEY,
    payload: data
});


export const setStoreType = (data) => ({
    type: ACTION_TYPES.SAVE_STORE_TYPE,
    payload: data
});




export const setOfferId =(data) =>({
    type: ACTION_TYPES.SAVE_OFFER_ID,
    payload: data

})

export const setAllStore = (data)=>({
    type:ACTION_TYPES.SAVE_ALL_STORE,
    payload:data
})


export const setAllSubCategoires = (data)=>({
    type:ACTION_TYPES.SAVE_SUBCATEGORY_ID,
    payload:data
})
export const setDepartmentKey = (data) =>({
    type:ACTION_TYPES.SAVE_DEPARTMENT_KEY,
    payload:data
})

export const setProfileImage = (data) => ({
    type: ACTION_TYPES.SAVE_PROFILE_IMAGE,
    payload: data
});

export const onSnipsSelected = (data) => ({
    type: ACTION_TYPES.SNIPS_SELECTED,
    payload: data
});

export const onSnipsDeleted = (data) => ({
    type: ACTION_TYPES.SNIPS_DELETED,
    payload: data
});



export const addToZoomImages = (data) => ({
    type: ACTION_TYPES.ZOOM_IMAGES,
    payload: data
});


export const addToFavourite = (data) => ({
    type: ACTION_TYPES.ADD_TO_FAVOURITE,
    payload: data
});


export const addToCategories = (data) => ({
    type: ACTION_TYPES.ADD_TO_CATEGORIES,
    payload: data
});

export const addToSubCategories = (data) => ({
    type: ACTION_TYPES.ADD_TO_SUB_CATEGORIES,
    payload: data
});

export const addToViewAllProduct =  (data) =>({
  type:ACTION_TYPES.VIEW_ALL_PRODDUCT,
  payload:  data  
})

export const addToFollow = (data) =>({
    type: ACTION_TYPES.ADD_TO_FOLLOW,
    payload: data
});


export const deleteToFollow = (data) =>({
    type: ACTION_TYPES.ADD_TO_FOLLOW,
    payload: data
});


export const deleteToFavourite = (data) => ({
    type: ACTION_TYPES.DELETE_FAVOURITE,
    payload: data
});

export const deleteToFollowStore = (data) =>({
    type: ACTION_TYPES.DELETE_FOLLOW_STORE,
    payload: data
})
export const setStoreKey=  (data)=>(
    {
        type: ACTION_TYPES.SAVE_STORE_KEY,
        payload: data
    }
)

export const addToVideoUrl = (data) =>({
    type: ACTION_TYPES.ADD_TO_VIDEO,
    payload: data
})

export const addToSnips = (data) =>({
    type: ACTION_TYPES.ADD_TO_SNIPS,
    payload: data

})

export const addToSearch = (data) =>({
    type: ACTION_TYPES.ADD_TO_SEARCH,
    payload:data
})