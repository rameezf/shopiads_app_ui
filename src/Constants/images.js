export const logo = require('../../assets/logo.png')
export const user = require('../../assets/Auth/user.png')
export const password = require('../../assets/Auth/password.png')
export const beard = require('../../assets/beard.jpg')
export const facebook = require('../../assets/Auth/facebook.png')
export const google = require('../../assets/Auth/google.png')

export const apple = require('../../assets/apple.png')
export const fashionBanner = require('../../assets/Products/fashion.png')
export const electronicsBanner = require('../../assets/Products/electronics.png')
export const footwearBanner = require('../../assets/Products/footwear.jpeg')
//top wear
export const productTshirt = require('../../assets/Products/dummy/tshirt.png')
export const productCasualShirt = require('../../assets/Products/dummy/shirt.png')
export const productFormalShirt = require('../../assets/Products/dummy/formal.png')
export const productshorts = require('../../assets/Products/dummy/shorts.png')
export const productJeans = require('../../assets/Products/dummy/jeans.png')
export const productHoody = require('../../assets/Products/dummy/hoody.png')
export const productJacket = require('../../assets/Products/dummy/jacket.png')
export const productDress = require('../../assets/Products/dummy/dress.png')
export const productBlazer = require('../../assets/Products/dummy/blazer.png')
//electronics
export const mobile = require('../../assets/Products/electronics_dummy/mobile.png')
export const tv = require('../../assets/Products/electronics_dummy/tv.png')
export const radio = require('../../assets/Products/electronics_dummy/radio.png')
export const laptop = require('../../assets/Products/electronics_dummy/laptop.png')
export const speaker = require('../../assets/Products/electronics_dummy/speakers.png')
export const ac = require('../../assets/Products/electronics_dummy/ac.png')
export const electronics_access = require('../../assets/Products/electronics_dummy/electronics_access.png')
//footwear

export const formal = require('../../assets/Products/footwear_dummy/formal.png')
export const hills = require('../../assets/Products/footwear_dummy/hills.png')
export const sports = require('../../assets/Products/footwear_dummy/sports.png')
export const sneakers = require('../../assets/Products/footwear_dummy/sneakers.png')
export const sandal = require('../../assets/Products/footwear_dummy/sandal.png')
export const slipper = require('../../assets/Products/footwear_dummy/slipper.png')
export const hikers = require('../../assets/Products/footwear_dummy/hikers.png')
export const infoIc = require('../../assets/info_ic.png')

// chip items

export const all = require('../../assets/chips/all.png')
export const electronic = require('../../assets/chips/electronic.png')
export const footwear = require('../../assets/chips/footwear.png')
export const newI = require('../../assets/chips/new.png')
export const fashion = require('../../assets/chips/fashion.png')
export const jewellery = require('../../assets/chips/jewellery.png')

export const dropDown = require('../../assets/arrow.png')
// No record 

export const noRecord = require('../../assets/lottie/norecordfound.json')
export const progresLoading = require('../../assets/lottie/progress_circle.json')
export const close = require('../../assets/close.png')

export const plus = require('../../assets/plus.png')

export const BUYNOW = require('../../assets/buynow.png')

export const SNIP = require('../../assets/snip.png')
export const BACK = require('../../assets/back.png')
export const SHARE = require('../../assets/share.png')
export const DROPDOWNARROW = require('../../assets/dropdown.png')
export const STOREFOLLOW = require('../../assets/store_follow.png')
export const DOWNARRAY = require('../../assets/arrowdown.png')
export const MINUS = require('../../assets/minus.png')
export const FILTER = require('../../assets/filter.png')
export const DELETE_ICON = require('../../assets/delete_icon.png')

export const STORE_ICON = require('../../assets/store.png')
export const PLAY_ICON = require('../../assets/play.png')

export const LOADER = require('../../assets/lottie/loading.json')


export const LOCATION_ICON = require('../../assets/location_icon.png')
export const MAIL_ICON = require('../../assets/mail_icon.png')

export const MENU_ICON = require('../../assets/menu_icon.png')

export const PROFILE_ICON = require('../../assets/profile_icon.png')

export const MESSAGE_ICON = require('../../assets/message_icon.png')

export const FACEBOOK_ICON = require('../../assets/facebook.png')
export const INSTAGRAM = require('../../assets/instagram.png')
export const TWITTER = require('../../assets/twitter.png')
export const TRENDING = require('../../assets/trending.png')

export const US_FLAG = require('../../assets/usFlag.png')
export const CANADA_FLAG = require('../../assets/canada.png')

export const DISCOUNT_ICON = require('../../assets/discount_icon.png')
