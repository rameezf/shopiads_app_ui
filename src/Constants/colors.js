
export const BLUE = "#1776d3";
export const WHITE="#ffffff";
export const BGCOLOR="#F2F2F2";
export const BLACK ="black";
export const GREY="#666666";
export const LIGHTGREY="#CFCFCF";
export const DISABLED_COLOR ="#7E7E7E";