import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList,  StyleProp,Image, ImageStyle, Alert, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Avatar } from 'react-native-paper'
import { setProductKey } from '../../Actions/product'
import { AuthContext } from '../../Context/Context'
import { StackNavigationProp } from '@react-navigation/stack';
import Icons from '../Constants/Icons';
import {style as productStyle}  from '../Screens/GridProduct.style';
import { Rating } from 'react-native-elements'



import {
  logo
} from '../Constants/images';
import { ThemeImage } from './ThemeImage'
interface listViewProps {
    item: {},
    foundInFav:boolean,
    isTypeView:boolean,
    onItemPress: () => void,
    onAddToFavourite:()=>void,
}
export const GridProduct = ({  item, onItemPress,foundInFav,onAddToFavourite,isTypeView=true, ...props }: listViewProps) => {
    return (
     <View
      style={{...productStyle.container,width:'48%',alignSelf:'center',margin:5}}>
         <TouchableOpacity onPress={onItemPress} >

        <View style={productStyle.imgContainer}>
        <ThemeImage
            source={{uri: item.images.length > 0 ?  item.images[0]:''}}
            style={productStyle.img}
          />
        </View>
        <View style={{flexDirection:'row',marginTop:5,width:135}}>
      
        <Text style={productStyle.txtStoreName} numberOfLines={1}>
            {item.webStoreName}
        </Text>
        </View>
       
        <Text style={productStyle.txtProductName} numberOfLines={1}>
            {item.heading}
        </Text>

        <View style={productStyle.txtPriceContainer}>
        <Text style={productStyle.txtCurrentPrice}>
            {props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "US" ? "US $"+item.offerPrice:"CAD "+item.offerPrice}
        </Text>
       {item.actualPrice>item.offerPrice?<Text style={productStyle.txtMrpPrice}>
            {item.actualPrice}
        </Text>:false}
        </View>
      <TouchableOpacity
        onPress={ onAddToFavourite}
        style={productStyle.ratingLikeContainer}>
        <View style={productStyle.ratingContainer}>
        <Rating
  type='star'
  fractions={2}
  readonly={true}
  ratingCount={5}
  showRating={false}
  startingValue={item.avgRating}
  imageSize={14}
/>

<Text style={productStyle.txtRating}>
            {item.avgRating}
          </Text>
        </View>
        <View style={productStyle.likeContainer}>
          <Icons.AntDesign
            name={foundInFav ? 'heart' : 'hearto'}
            size={14}
            color={'#1776d3'}
          />
          <Text style={productStyle.txtLikes}>
            {item.totalLikes}
          </Text>
        </View>
      </TouchableOpacity>
      </TouchableOpacity>

    </View>
    )
}
