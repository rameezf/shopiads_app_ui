import React, { useState } from 'react'
import {
    View, Text, StyleProp, TextStyle, TextInput,
    ViewStyle, StyleSheet, Image, TouchableOpacity, Animated
} from 'react-native'

import { Badge } from 'react-native-paper'
import { Easing } from 'react-native-reanimated'
import { GREY } from '../Constants/colors'

import Icons from '../Constants/Icons'

import { dropDown, STOREFOLLOW, CANADA_FLAG, US_FLAG } from '../Constants/images'

interface headerProps {
    title?: String | React.ReactNode;
    imgPath?: String | React.ReactNode;
    color?: String | React.ReactNode;
    leftText?: String | React.ReactNode;
    rightText?: String | React.ReactNode;
    isSearch?: boolean;
    titleStyle?: StyleProp<TextStyle>
    rightTextStyle?: StyleProp<TextStyle>
    onFontPress?: () => void,
    onRightPress?: () => void,
    onRightPress1?: () => void,
    onRightPress2?: () => void,
    onSearchPress?: () => void,

    badgeValue?: Number
}

export const Heading = ({
    title,
    leftText,
    rightText,
    titleStyle,
    onRightPress,
    rightTextStyle,
    ...props
}: headerProps) => {
    return (
        <View style={styles.headerViewStyle}>
            <View style={styles.clickViewStyle}>
                <Text {...props} style={[styles.fixedRightTextStyle, rightTextStyle]}>{leftText}</Text>
            </View>
            <View style={{ flex: 0.8, alignItems: 'center' }}>
                <Text {...props} style={[styles.fixedheadStyle, titleStyle]}>{title}</Text>
            </View>

            <View style={styles.clickViewStyle}>
                <TouchableOpacity onPress={onRightPress} >
                    <Text {...props} style={[styles.fixedRightTextStyle, rightTextStyle]}>{rightText}</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}




export const BackHeader = ({
    title,
    fontName,
    fontSize,
    fontColor,
    onFontPress,
    rightTextStyle,
    ...props
}: headerProps) => {
    return (
        <View style={{ ...styles.headerViewStyle, flex: 0.09, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={onFontPress} style={styles.clickViewStyle}>
                <Icons.Entypo name={fontName} size={fontSize} color={'#ffffff'} />
            </TouchableOpacity>
            <Text {...props} style={{ ...[styles.fixedRightTextStyle, rightTextStyle], flex: 1, color: '#ffffff', textAlign: 'center' }}>{title}</Text>
            <TouchableOpacity style={styles.clickViewStyle}>
            </TouchableOpacity>

        </View>
    )
}

export const HomeHeader = ({
    title,
    leftText,
    rightText,
    titleStyle,
    onRightPress,
    rightTextStyle,
    imgPath,
    onRightPress1,
    onRightPress2,
    onFontPress,
    onSearchPress,
    fontName,
    fontNameRight1,
    fontNameRight2,
    fontSize,
    fontSizeRight1,
    fontSizeRight2,
    fontColor,
    badgeValue,
    isSearch,
    showSearch = true,
    search,
    setSearch,
    color,
    ...props
}: headerProps) => {

    let shakeAnimation = new Animated.Value(0);

    React.useEffect(() => {
        triggerAnimation
    })

    const interpolate = shakeAnimation.interpolate({
        inputRange: [0, 0.5, 1, 1.5, 2, 2.5, 3],
        outputRange: [0, 0.5, 1, 1.5, 2, 2.5, 3]
    })

    const triggerAnimation = () => {
        shakeAnimation.setValue(0);
        Animated.timing(shakeAnimation, {
            useNativeDriver: true,
            duration: 300,
            toValue: 3,

        }).start()
    }
    // console.log("Image Path ::::",title, title === "US");
    return (
        <View style={styles.headerViewStyle}>
            {
                fontName && (<TouchableOpacity onPress={onFontPress} style={styles.clickViewStyle}>
                    <Icons.Entypo name={fontName} size={fontSize} color={fontColor} />
                </TouchableOpacity>)}
            {/* <TouchableOpacity onPress={onFontPress}>
        <Text {...props} numberOfLines={2} style={[{ fontSize: 12,width:70, fontWeight: 'bold', color: '#fff', padding:10 }, titleStyle]}>{title}</Text>
        </TouchableOpacity> */}
            <TouchableOpacity onPress={onFontPress}>
                <Image source={title && title.toUpperCase() === "US" ? US_FLAG : CANADA_FLAG} style={{ width: 45, marginTop: 10, height: 45, marginLeft: 10 }} />
            </TouchableOpacity>
            {!fontName && (
                <TouchableOpacity onPress={onFontPress}>
                    <Image source={dropDown} style={{ width: 10, marginTop: 10, height: 10, marginLeft: 5, tintColor: '#ffffff' }} />
                </TouchableOpacity>)}
            {!showSearch && (<TouchableOpacity style={{ ...styles.inputStyle, backgroundColor: '#ffffff', padding: 0, flexDirection: 'row', flex: 1, marginHorizontal: 10, height: 45, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={onSearchPress}>
                    <Icons.AntDesign name={fontNameRight1} size={fontSizeRight1} color={'#545454'} />
                </TouchableOpacity>
                <TextInput style={{ ...styles.inputStyle, fontSize: 12 }} placeholder='Shop with incorporated sellers only'
                    placeholderTextColor={GREY}
                    value={search}
                    onSubmitEditing={onSearchPress}
                    onChangeText={setSearch}
                />
            </TouchableOpacity>)}
            <TouchableOpacity onPress={onRightPress} style={styles.clickRightViewStyle}>
                <Image
                    source={STOREFOLLOW}
                    style={{ resizeMode: 'cover', height: 30, width: 30 }}
                />
                <Text style={[{ fontSize: 12, fontWeight: 'bold', color: '#fff', paddingTop: 4 }, titleStyle]} >Follow</Text>
            </TouchableOpacity>
        </View>
    )

}

const styles = StyleSheet.create({
    headerViewStyle: {
        flex: 0.12,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1776d3'
    },
    clickViewStyle: {
        flex: 0.2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    fixedheadStyle: {
        fontSize: 20,
        color: '#fff',
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    fixedRightTextStyle: {
        fontSize: 18,
        color: '#fee136',
    },
    inputStyle: {
        paddingLeft: 5,
        padding: 5,
        width: '90%',
        borderRadius: 4,
        fontSize: 9
    },
    clickRightViewStyle: {
        flex: 0.2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
})




