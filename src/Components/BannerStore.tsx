import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList, StyleProp, ImageStyle, Alert, Image, Dimensions, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators'
import { NavigationContainer } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack';
import Icons from '../Constants/Icons';
import {
  logo
} from '../Constants/images';
import { style as storeStyle } from './BannerStore.style';
import { Rating } from 'react-native-elements'
import { LIGHTGREY } from '../Constants/colors'
import { ThemeImage } from './ThemeImage'


const { width } = Dimensions.get('window');
//you need to preview n items.
const previewCount = 1;
//to center items
//the screen will show `previewCount` + 1/4 firstItemWidth + 1/4 lastItemWidth 
//so for example if previewCount = 3
//itemWidth will be =>>> itemWidth = screenWidth / (3 + 1/4 + 1/4)
const itemWidth = width / (previewCount + .2);
//to center items you start from 3/4 firstItemWidth 
const startScroll = (itemWidth * 3 / 4);


interface listViewProps {
  item: {},
  foundInFav: boolean,
  type: boolean,
  onItemPress: () => void,
  onAddToFavourite: () => void,
}
export const BannerStore = ({ item, onItemPress, type, foundInFav, onAddToFavourite, ...props }: listViewProps) => {

  // const [showBanner, setShowBanner] = React.useState(true);
  // const [bannerId, setBannerId] = React.useState(null);

  // useEffect(() => {
  //   console.log("Item Full Image Use Effect condn :::::",item.fullImage !== null && item.fullImage !== undefined ,
  //    ":::: Data ::::",item.fullImage);
  //   if(item.fullImage !== null && item.fullImage !== undefined) {
  //     try {
  //       fetch(item.fullImage).then(response => {
  //         if (response.status == 404) {
  //           console.log("Image not exist Banner ::::");
  //           setShowBanner(false);
  //           setBannerId(item.id);
  //         } else if (response.status == 200) {
  //           console.log("Image exist Banner  ::::");
  //           setShowBanner(true);
  //           setBannerId(item.id);
  //         }
  //       }).catch(err => console.log("Error in Banner ::::",err)); 
  //     } catch(error) {
  //         console.log("Error in fetch image banner store::::",error);
  //     }
  //   } else {
  //     setShowBanner(false);
  //     setBannerId(null);
  //   }
  // },[bannerId]);


  return (
    <View
      key={item.id}
      collapsable={true}
      style={{ ...storeStyle.container, width: width - 80 }}>
      <TouchableOpacity onPress={onItemPress} >
        <ThemeImage
          source={{ uri: item.fullImage ? item.fullImage : item.companyLogo }}
          style={storeStyle.img}
        />
      </TouchableOpacity>
      <View style={storeStyle.txtStoreInfoContainer}>
        <View style={{ borderColor: LIGHTGREY, borderWidth: 1, borderRadius: 3, padding: 3, marginTop: 5 }}>
          <Image
            source={{ uri: item.logo ? item.logo : item.companyLogo }}
            style={storeStyle.imgLogo}
          />
        </View>
        <Text style={{ ...storeStyle.txtStoreName, fontSize: 12 }}>
          {item.webStoreName}
        </Text>
        <TouchableOpacity
          onPress={onAddToFavourite}
          style={storeStyle.likeContainer}>
          <View style={{ ...storeStyle.likeIcon, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 14 }}>
            <Icons.AntDesign
              name={foundInFav ? 'heart' : 'hearto'}
              size={14}
              color={'#1776d3'}
            />
            <Text style={{ fontSize: 14, alignSelf: 'center', color: '#000', paddingHorizontal: 5, }}>
              {item['totalLikes']}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* <View style={storeStyle.ratingContainer}>
            <Rating
              type='star'
              fractions={2}
              readonly={true}
              ratingCount={5}
              showRating={false}
              startingValue={item.avgRating}
              imageSize={14}
            />
            <Text style={storeStyle.txtRating}>
              {item.avgRating+' Ratings'}
            </Text>
        </View> */}
    </View>
  )
}
