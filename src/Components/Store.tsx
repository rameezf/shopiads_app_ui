import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList,  StyleProp, ImageStyle,Image, Alert, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators'
import { NavigationContainer } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack';

import Icons from '../Constants/Icons';

import {style as storeStyle}  from '../Screens/Store.style';
import {style as bannerStyle}  from './BannerStore.style';

import { Rating } from 'react-native-elements'
import { GREY, LIGHTGREY, WHITE } from '../Constants/colors'
import {
  logo
} from '../Constants/images';
import { ThemeImage } from './ThemeImage'
interface listViewProps {
    item: {},
    foundInFav:boolean,
    type:boolean,
    onItemPress: () => void,

    onAddToFavourite:()=>void,
}
export const Store = ({  item, onItemPress, type= true ,foundInFav,onAddToFavourite, ...props }: listViewProps) => {
    return (
        <TouchableOpacity
        onPress={onItemPress}
        style={{...(type?storeStyle.container:bannerStyle.container),}}>
        <ThemeImage
   source={{uri: item.fullImage?item.fullImage:item.companyLogo}}
  
   style={(type?storeStyle.img:bannerStyle.img)}
        />
        <View style={(type?storeStyle.txtStoreInfoContainer:bannerStyle.txtStoreInfoContainer)}>
        <View style={{borderColor:LIGHTGREY,borderWidth:1,borderRadius:3,padding:3}}>
            <Image
              source={{uri: item.logo?item.logo:item.companyLogo }}
              style={storeStyle.imgLogo}
            />
            </View>
           
            <Text style={(type?storeStyle.txtStoreName:bannerStyle.txtStoreName)}>
              {item.webStoreName}
            </Text>
        <TouchableOpacity
          onPress={() => onAddToFavourite}
          style={type?storeStyle.likeContainer:bannerStyle.likeContainer}>
          <View style={type?storeStyle.likeIcon:bannerStyle.likeIcon}>
            <Icons.AntDesign
              name={foundInFav ? 'heart' : 'hearto'}
              size={14}
              color={'#1776d3'}
            />
          </View>
        </TouchableOpacity>
        </View>
        <View style={(type?storeStyle.ratingContainer:bannerStyle.ratingContainer)}>
            <Rating
                type='star'
                fractions={2}
                readonly={true}
                ratingCount={5}
                showRating={false}
                startingValue={item.avgRating}
                imageSize={14}
            />
            <Text style={storeStyle.txtRating}>
                {item.avgRating+' Ratings'}
            </Text>
        </View>
      </TouchableOpacity>

    )
}
