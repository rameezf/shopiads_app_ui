import * as React from 'react';
import { Avatar, Chip } from 'react-native-paper';
import { ViewStyle, TextStyle, TouchableOpacity, Image } from 'react-native'

interface ReviewChipProps {

  containerStyle: ViewStyle;
  title: String,
  textStyle: TextStyle
  onPress: () => void,
  icon: any,
  imageStyle: any
}

const ReviewChip = ({ title, containerStyle, textStyle, onPress, icon, imageStyle, props }: ReviewChipProps) => (
  // <Chip {...props} onPress={onPress} style={containerStyle} textStyle={textStyle}>{title}</Chip>
  <TouchableOpacity onPress={onPress} chipItem {...props} style={containerStyle} >
    <Image source={icon} style={imageStyle} />
  </TouchableOpacity>

  // <TouchableOpacity onPress={onPress}>
  //   <Avatar.Image source={icon} size={50} chipItem {...props} style={containerStyle} textStyle={textStyle} />
  // </TouchableOpacity>

);

export default ReviewChip;
