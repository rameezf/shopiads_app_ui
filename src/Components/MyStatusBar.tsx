import React, {
  Component,
} from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  StatusBar,
  Platform,
  SafeAreaView
} from 'react-native';

interface statusBarType {
  backgroundColorType: String | React.ReactNode
}
export const MyStatusBar = ({ backgroundColorType, ...props }: statusBarType) => (
  <View style={[styles.statusBar, { backgroundColorType }]}>
    <SafeAreaView>
      <StatusBar translucent backgroundColor={backgroundColorType} {...props} />
    </SafeAreaView>
  </View>
);

const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  }
})