import { Container } from 'native-base'
import React from 'react'
import {StyleSheet,View, Text,TouchableOpacity,StyleProp,ViewStyle,TextStyle} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

interface ThemeButtonStyle{
    btnText?:String | React.ReactNode
    btnStyle?:StyleProp<ViewStyle>
    btnMainStyle?:StyleProp<any>
    btnThemeStyle?:StyleProp<any>
    
    onBtnPress?:()=>void,
    ContainerStyle?:StyleProp<ViewStyle>
    btnTextStyle?:StyleProp<TextStyle>
    btnDisable?:boolean

}

const ThemeButton = ({
    btnText,
    btnStyle,
    btnThemeStyle,
    onBtnPress,
    btnMainStyle,
    btnTextStyle,
    ContainerStyle,
    btnDisable,
    ...props
}:ThemeButtonStyle) =>{
    return (
        <TouchableOpacity disabled={btnDisable} onPress={onBtnPress} style={[{ width:'90%',alignItems:'center'},btnThemeStyle]}>
        {btnDisable?
        <LinearGradient {...props} style={[styles.buttonColorFixed,btnMainStyle]}
        start={{ x: 0, y: 0 }}
           end={{ x: 1, y: 1 }} locations={[0, 0.8]} 
         colors={['#989898','#989898']}>
              <Text {...props} style={[styles.btnText, btnTextStyle]} >{btnText}</Text>
             </LinearGradient>
             :
             <LinearGradient {...props} style={[styles.buttonColorFixed,btnMainStyle]}
        start={{ x: 0, y: 0 }}
           end={{ x: 1, y: 1 }} locations={[0, 0.8]} 
         colors={['#1776d3','#1776d3']}>
              <Text {...props} style={[styles.btnText, btnTextStyle]} >{btnText}</Text>
             </LinearGradient>
}
             </TouchableOpacity>
    )
}

export default ThemeButton

const styles = StyleSheet.create({
    container:{
        flex: 0.25,
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    buttonColorFixed:{
        height:50, 
        width:'90%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:4
    },
    btnText:{
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold' ,
    }
})