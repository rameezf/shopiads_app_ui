import React from 'react'
import {View,Text,StyleSheet,FlatList} from 'react-native'
import ViewPager from '@react-native-community/viewpager';


interface viewpagerProps{
    arr:[]
}

export const Viewpager =({arr,props}:viewpagerProps)=>{

    const renderData=(item,index)=>{
      return(
        <View key={index} >
        <Text>{item}</Text>
      </View>
      )
    }
    return(
        <ViewPager style={styles.viewPager} initialPage={0}>
        <FlatList
        {...props}
        data={arr}
        renderItem={({item,index})=>renderData(item,index)}
        />
    </ViewPager>
    )
}
const styles = StyleSheet.create({
    viewPager: {
      flex: 1,
    },
  });