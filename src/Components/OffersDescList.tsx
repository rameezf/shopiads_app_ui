import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList,  StyleProp, ImageStyle, Alert,Image, Dimensions, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { ThemeImage } from './ThemeImage';

interface listViewProps {
    item:{},
    renderBannerOnClick: () => void,
}
export const OffersDescList = ({item,renderBannerOnClick, ...props }:listViewProps) => {

  // const [showBanner, setShowBanner] = React.useState(true);
  // const [bannerId, setBannerId] = React.useState(null);

  // useEffect(() => {
  //   if(item.fullImage !== null && item.fullImage !== undefined){
  //     fetch(item.fullImage).then(response => {
  //       if (response.status == 404) {
  //         console.log("Image not exist Banner ::::");
  //         setShowBanner(false);
  //         setBannerId(item.id);
  //       } else if (response.status == 200) {
  //         console.log("Image exist Banner  ::::");
  //         setShowBanner(true);
  //         setBannerId(item.id);
  //       }
  //     }).catch(err => console.log("Error in Banner ::::",err)); 
  //   } else {
  //     setShowBanner(false);
  //     setBannerId(null)
  //   }
  // },[bannerId]);

     
    return (
      <TouchableOpacity style={{width:'100%',flexDirection:'column'}}  onPress={()=>{
                        renderBannerOnClick();
                    }} >
        <ThemeImage
          source={{uri: item.fullImage}}
          style={{resizeMode: 'contain', height: 280, width: "100%",marginTop:10,paddingHorizontal:5}}
        />
      </TouchableOpacity>
    )
}
 