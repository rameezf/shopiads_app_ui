
import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList,  StyleProp, ImageStyle,Image, Alert, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators'
import { NavigationContainer } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack';

import Icons from '../Constants/Icons';

import {style as storeStyle}  from '../Screens/Store.style';
import {style as bannerStyle}  from './BannerStore.style';

import { Rating } from 'react-native-elements'
import { GREY, LIGHTGREY, WHITE } from '../Constants/colors'
import {
  logo
} from '../Constants/images';

interface listViewProps {
    source : any ,
  style :any,
}
import FastImage from 'react-native-fast-image'

export const ThemeImage = ({  source,style, ...props }: listViewProps) => {
   
    const [image,setImage]= useState(source)
    const [showDefaultImage,setShowDefaultImage] = useState(false);
    return (
(  (image  &&  image.uri && image.uri != undefined &&  !showDefaultImage ) ?

    <FastImage
    style={style}
    source={{
        uri: image.uri,
        priority: FastImage.priority.low,
    }}
    resizeMode={FastImage.resizeMode.stretch}
/>
    
    :
    

    <FastImage
    style={style}
    resizeMode={FastImage.resizeMode.contain}
    source= { logo}
/>

)
     
    )
}

