import { StyleSheet } from "react-native";


export const style = StyleSheet.create({
    txtCategoryName:{
        fontSize:13,fontWeight:'bold',paddingHorizontal:10,flex:1
    },
    txtViewAll:{
        
        fontSize:13, color:'#1776d3',paddingHorizontal:10,
    }, 
    container: {
        backgroundColor: '#ffffff',
        elevation:5,
        flex:1,
        borderRadius:5,
        margin: 8,
    },
  
    img:{
        borderTopLeftRadius:5,borderTopRightRadius:5,
        resizeMode: 'contain', height: 215, width: '100%',paddingHorizontal:5},
   
    txtStoreInfoContainer:{
        paddingHorizontal:5,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingBottom:5
    },
    imgLogo:{
        resizeMode: 'contain', height: 15, width: 30
    },
    txtStoreName:{
        marginTop: 5,
        flex:1, color: '#000',paddingHorizontal:5,
    },
    likeContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginLeft:"15%"
    },
    likeIcon:{
        width:70,
        alignSelf:'flex-end',
        alignContent:'flex-end',
    },
    
    ratingContainer:{
        paddingHorizontal:5,
        marginTop:3,
        width:'100%',paddingBottom:10,flexDirection:'row',justifyContent:'center',alignItems:'center'
    },
    txtRating:{
        fontSize: 13,flex:1,alignSelf:'flex-end', color: '#000',paddingHorizontal:5,
    },
  
  })