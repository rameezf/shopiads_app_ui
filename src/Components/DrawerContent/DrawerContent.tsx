import React, { useContext } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { TouchableRipple, Drawer, Title, Avatar, Switch, Caption, Paragraph, } from 'react-native-paper'
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Icons from '../../Constants/Icons';
import { beard } from '../../Constants/images'
import LinearGradient from 'react-native-linear-gradient'
import { AuthContext } from '../../Context/Context'




const CustomDrawerContent = ({ navigation, props }) => {

    const { Logout } = useContext(AuthContext)
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient style={{ flex: 1 }} start={{ x: 0.1, y: 0.1 }} end={{ x: 0.5, y: 1.5 }}
                locations={[0.1, 0.3, 0.8]}
                colors={['#3c91e1', '#6663c3', '#688696']}>
                <DrawerContentScrollView {...props}>
                    <View style={styles.Container}>
                        <View>
                            <Avatar.Image source={beard} />
                        </View>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Title style={{ color: '#fff', fontWeight: 'bold', fontSize: 14 }}>Welcome, Kubail Khan</Title>
                                <Icons.MaterialIcons name={'edit'} color={'#fff'} size={16} />
                            </View>

                            <Caption style={{ color: '#fff', fontSize: 12 }}>KubailKhan@yupmail.com</Caption>

                        </View>



                    </View>
                    <View style={{ flex: 0.35, borderBottomColor: '#fff', borderBottomWidth: 1, }}>
                        <Drawer.Section>
                            <DrawerItem icon={({ color, size }) => (<Icons.Ionicons name={'location'} color={"#fff"} size={22} />)} label={"Change Location"} labelStyle={styles.labelStyle} />
                            <DrawerItem icon={({ color, size }) => (<Icons.Entypo name={'home'} color={"#fff"} size={22} />)} label={"Home"} labelStyle={styles.labelStyle} />
                            <DrawerItem onPress={() => navigation?.navigate('About')} icon={({ color, size }) => (<Icons.Entypo name={'info-with-circle'} color={"#fff"} size={22} />)} label={"About Us"} labelStyle={styles.labelStyle} />
                            <DrawerItem icon={({ color, size }) => (<Icons.FontAwesome name={'question-circle'} color={"#fff"} size={22} />)} label={"Faq's"} labelStyle={styles.labelStyle} />
                            <DrawerItem icon={({ color, size }) => (<Icons.Ionicons name={'settings-sharp'} color={"#fff"} size={22} />)} label={"Account Settings"} labelStyle={styles.labelStyle} />
                            <DrawerItem icon={({ color, size }) => (<Icons.SimpleLineIcons name={'logout'} color={"#fff"} size={22} />)} label={"Logout"} labelStyle={styles.labelStyle} onPress={() => Logout()} />
                        </Drawer.Section>
                    </View>
                    <View style={{ flex: 0.3 }}>

                        <Title style={{ color: '#fff', fontSize: 18, marginLeft: 20 }}>Follow us on</Title>

                        <View style={{ marginLeft: 40, width: '50%', flexDirection: 'row', justifyContent: 'space-around', }}>
                            <DrawerItem icon={({ color, size }) => (<Icons.MaterialCommunityIcons name={'facebook'} color={"#fff"} size={30} />)} label={""} />
                            <DrawerItem icon={({ color, size }) => (<Icons.Entypo name={'instagram-with-circle'} color={"#fff"} size={30} />)} label={""} />
                            <DrawerItem icon={({ color, size }) => (<Icons.Entypo name={'youtube-with-circle'} color={"#fff"} size={30} />)} label={""} />
                        </View>



                    </View>
                </DrawerContentScrollView>

            </LinearGradient>

        </View>
    )
}

const styles = StyleSheet.create({
    Container: {
        flex: 0.3, padding: 10,
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    labelStyle: { fontSize: 16, color: '#fff' }
})

export default CustomDrawerContent