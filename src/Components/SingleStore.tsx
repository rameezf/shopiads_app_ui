import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList,  StyleProp, ImageStyle,Image, Alert,TouchableOpacity, ActivityIndicator } from 'react-native'
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators'
import { NavigationContainer } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack';

import Icons from '../Constants/Icons';

import { style as storeStyle } from '../Screens/Store.style';
import { style as bannerStyle } from './BannerStore.style';

import {
  logo
} from '../Constants/images';

import { Rating } from 'react-native-elements'
import { GREY, LIGHTGREY, WHITE } from '../Constants/colors'
// import { ThemeImage } from './ThemeImage';
interface listViewProps {
  item: {},
  foundInFav: boolean,
  type: boolean,
  onItemPress: () => void,

  onAddToFavourite: () => void,
}
export const SingleStore = ({ item, onItemPress, type = true, foundInFav, onAddToFavourite, ...props }: listViewProps) => {
  console.log('Single Store Item :::::',item, ":::Type:::",type);
  return (
    <TouchableOpacity
       onPress={onItemPress}
      style={{ ...(type ? storeStyle.container : bannerStyle.container), padding: 10 }}>
      <Image
      source={{ uri: item.primaryOfferImage ? item.primaryOfferImage : item.logoMedium }}
        style={(type ? storeStyle.img : bannerStyle.img)}
      />
      <View style={{...(type ? storeStyle.txtStoreInfoContainer : bannerStyle.txtStoreInfoContainer),width: 230,marginTop:5}}>
        <View style={{flexDirection:'row'}}>
        <View style={{ borderColor: LIGHTGREY, borderWidth: 1, borderRadius: 3, padding: 3,height: 40, width: 60,marginTop:10 }}>
          <Image
            source={{ uri: item.logo ? item.logo : item.logoMedium }}
            style={storeStyle.imgLogo}
          />
        </View>
        <Text ellipsizeMode='tail' numberOfLines={1} style={{...(type ? storeStyle.txtStoreName : bannerStyle.txtStoreName),flex:1, fontSize: 15, marginTop:20, width:100}}>
          {item.storeName}
        </Text>
        </View>
        <TouchableOpacity
          onPress={onAddToFavourite}
          style={{ ...type ? storeStyle.likeContainer : {...bannerStyle.likeContainer,marginLeft:"20%"}, width: 50 }}>
          <View style={{ ...type ? storeStyle.likeIcon : bannerStyle.likeIcon, flexDirection: 'row', justifyContent: 'flex-end'}}>
            <Icons.AntDesign
              name={foundInFav ? 'heart' : 'hearto'}
              size={14}
              color={'#1776d3'}
            />
            <Text style={{ top: type ? 3 : -4, fontSize: 14,color: '#000', paddingHorizontal: 5 }}>
              {item['totalLikes']}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={(type ? storeStyle.ratingContainer : bannerStyle.ratingContainer)}>
        <Rating
          type='star'
          fractions={2}
          readonly={true}
          ratingCount={5}
          showRating={false}
          startingValue={item.avgRating}
          imageSize={14}
        />
        <Text style={storeStyle.txtRating}>
          {item.avgRating + ' Ratings'}
        </Text>
      </View>
    </TouchableOpacity>

  )
}
