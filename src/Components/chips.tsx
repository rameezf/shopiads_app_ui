import * as React from 'react';
import { Avatar, Chip } from 'react-native-paper';
import { ViewStyle, TextStyle, TouchableOpacity, Image } from 'react-native'
import AnimatedLottieView from 'lottie-react-native';

interface ChipProps {

  containerStyle: ViewStyle;
  title: String,
  textStyle: TextStyle
  onPress: () => void,
  icon: any,
  imageStyle: any
}

const MyChip = ({ title, containerStyle, textStyle, onPress, icon, imageStyle, props }: ChipProps) =>{

  return (
     <TouchableOpacity onPress={onPress} chipItem {...props} style={containerStyle} >        
          <Image source={icon} style={imageStyle} />
  
      
      </TouchableOpacity>
    );
  }


export default MyChip;