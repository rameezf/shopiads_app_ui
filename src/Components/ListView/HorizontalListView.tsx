import React, { useContext, useEffect, useState } from 'react'
import { View, Text, FlatList, Image, StyleProp, ImageStyle, Alert, Platform, Linking, Share } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import icons from '../../Constants/Icons'
import { Avatar } from 'react-native-paper'
import { setProductKey } from '../../Actions/product'
import { AuthContext } from '../../Context/Context'
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators'
import { NavigationContainer } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack';
import { LoginAlert } from '../../NavigationStack/LoginAlert'
import { BLACK } from '../../Constants/colors'
import * as RNLocalize from "react-native-localize";
import {
    FACEBOOK_ICON, INSTAGRAM, TWITTER
} from '../../Constants/images'
import { ThemeImage } from '../ThemeImage';
import DeviceCountry from 'react-native-device-country';


interface listViewProps {
    arr: [],
    onFocus: boolean,
    childItem: String | React.ReactNode
    name: String,
    onItemPress: () => void,
    rightProductStyle: StyleProp<ImageStyle>
}

export const HorizontalListView = ({ arr, name, onItemPress, onFocus, rightProductStyle, ...props }: listViewProps) => {

    const [categoryData, setCategoryData] = useState([]);

    const setProductData = (index) => {
        setIndex(index)
    }

    const [indexval, setIndex] = useState(0)


    console.log("Horizontal List View :::::", arr);
    const renderData = (childItem: any, index: any) => {
        return (
            <View {...props} onPress={() => setProductData(index)} style={{ width: '100%', flexDirection: 'column' }} >
                <Text style={{ color: 'black', padding: 5, fontSize: 12, fontWeight: 'bold' }}>{childItem.name}</Text>
                <FlatList
                    {...props}
                    numColumns={3}
                    style={{ width: '100%' }}
                    data={arr[childItem.name]}
                    renderItem={({ item, index }) => subCategoryData(item, index)}
                />
            </View>
        )
    }

    const subCategoryData = (childItem, index) => {
        return (<TouchableOpacity {...props} onPress={() => {
            console.log("IMAgE URL", childItem);
            props.setAllSubCategoires(childItem.id);
            props.navigation.push('CategoriesProducts')
        }} style={{ width: 80, padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }} >
            <ThemeImage source={{ uri: childItem.image }} style={{ width: 70, height: 70 }} />
            <Text style={{ color: 'black', padding: 5, fontSize: 12, textAlign: 'center' }}>{childItem.name}</Text>
        </TouchableOpacity>)
    }
    return (
        <View>
            {arr.length != 0 && (
                <FlatList
                    {...props}
                    style={{ width: '100%' }}
                    data={arr[name]}
                    renderItem={({ item, index }) => renderData(item, index)}
                />
            )
            }
        </View>
    )
}

interface HorizontalListViewIconInterface {
    fontName: String | React.ReactNode,
    fontName2: String | React.ReactNode,
    ListName: String | React.ReactNode,
    arr: [],
    fontSize: Int16Array,
    fontSize2: Int16Array,
    fontColor: String | React.ReactNode,
    navigate: () => void,
    email: String,
    fontColor2: String | React.ReactNode,
    navigation?: StackNavigationProp<any, any>
}

export const HorizontalListViewIcon = ({ fontName, ListName, fontSize,
    navigation,
    fontName2,
    fontSize2,
    fontColor2,
    navigate,
    email,
    fontColor, arr, ...props }: HorizontalListViewIconInterface) => {

    const { Logout } = useContext(AuthContext)

    const [showLoginAlert, setShowLoginAlert] = React.useState(false);
    const [country, setCountry] = useState('CA');

    useEffect(() => {
        // DeviceCountry.getCountryCode().then((result) => {
        //     console.log("Device Country Code :::::",result);
        //     setCountry(result.code);
        // }).catch((e) => {
        //     console.log(e);
        //   });
        setCountry(RNLocalize.getCountry);
    }, []);

    //console.log("my location>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",RNLocalize.getCountry());
    const onSharePress = () => Share.share(shareOptions);
    const shareOptions = {
        title: 'App link',
        message: country.toUpperCase() === "US" ? 'Hey\nI\'m inviting you to use shopiads, to check latest offers.\nAndroid Link: https://play.google.com/store/apps/details?id=com.apcod.shopiads \n IOS Link: https://apps.apple.com/us/app/shopiads/id1601835714 '
            : 'Hey\nI\'m inviting you to use shopiads, to check latest offers.\nAndroid Link: https://play.google.com/store/apps/details?id=com.apcod.shopiads \n IOS Link: https://apps.apple.com/ca/app/shopiads/id1601835714 '
    };

    const onClickNavigate = (type: string) => {
        console.log("LFFKVKVMVMVMVVMVMVM", type);
        switch (type) {
            case "Logout": {
                Logout(navigation);
                break;
            }
            case "Contact Us": {
                if (email) {
                    navigation?.push("ContactUs");
                } else {
                    setShowLoginAlert(true);
                }
                break;
            }
            case "Rate Us": {
                const url = country.toUpperCase() === "US" ? Platform.OS === 'android' ? 'https://play.google.com/store/apps/details?id=com.apcod.shopiads' : 'https://apps.apple.com/us/app/shopiads/id1601835714'
                    : Platform.OS === 'android' ? 'https://play.google.com/store/apps/details?id=com.apcod.shopiads' : 'https://apps.apple.com/ca/app/shopiads/id1601835714';
                Linking.openURL(url);
                break
            }
            case "Refer to friend": {
                onSharePress()

                break;
            }
            case "Faq's": {
                Linking.openURL(country.toUpperCase() === "US" ? "https://www.shopiads.com/faq" : "https://www.shopiads.ca/faq")
                break;
            }
            case "About Us": {
                Linking.openURL(country.toUpperCase() === "US" ? "https://www.shopiads.com/about" : "https://www.shopiads.ca/about")
                break;
            }
            default: {
                if (email) {
                } else {
                    // setShowLoginAlert(true);
                    navigation?.push("Login");
                }
                break;

            }
        }

    }


    const renderData = (item, index) => {
        console.log("Render Horiontal List ::::", item, index)
        if (item.fontName == "user" && item.title !== "Login") {
            return (
                <View style={{
                    flexDirection: 'row', marginHorizontal: 8, padding: 12, justifyContent: 'center', alignItems: 'center',
                    borderBottomColor: '#5d5d5d', borderBottomWidth: 0.4
                }}>
                    <View style={{ flex: 0.1, }}>
                        <Avatar.Icon size={35} style={{ backgroundColor: "#1776d3" }} icon={props => <icons.FontAwesome {...props} name={item.fontName} size={fontSize} color={fontColor} />} />
                    </View>
                    <View style={{ flex: 0.85, alignItems: 'flex-start', justifyContent: 'center', marginLeft: 15 }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#5d5d5d', }}>{item.title}</Text>
                    </View>
                </View>
            )
        } else {
            return (
                <TouchableOpacity onPress={() => onClickNavigate(item.title)} style={{
                    flexDirection: 'row', marginHorizontal: 8, padding: 12, justifyContent: 'center', alignItems: 'center',
                    borderBottomColor: '#5d5d5d', borderBottomWidth: 0.4
                }}>
                    <View style={{ flex: 0.1, }}>
                        <Avatar.Icon size={35} style={{ backgroundColor: "#1776d3" }} icon={props => <icons.FontAwesome {...props} name={item.fontName} size={fontSize} color={fontColor} />} />
                    </View>
                    <View style={{ flex: 0.85, alignItems: 'flex-start', justifyContent: 'center', marginLeft: 15 }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#5d5d5d', }}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            )
        }

    }
    return (
        <SafeAreaView style={{ flexDirection: 'column', flex: 1, width: '100%' }} edges={['top']} >
            <View style={{ flexDirection: 'column', flex: 1, width: '100%' }}>
                <FlatList
                    {...props}
                    data={arr}
                    style={{ flex: 1 }}
                    renderItem={({ item, index }) => renderData(item, index)}
                />
                <Text style={{ fontSize: 12, width: '100%', textAlign: 'center' }}>{'Follow us on'}</Text>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', marginTop: 10 }} >
                    <TouchableOpacity style={{ padding: 10 }} onPress={() => {
                        Linking.openURL("https://www.facebook.com/profile.php?id=100084814547567")
                    }}>
                        <Image source={FACEBOOK_ICON} style={{ width: 50, height: 50 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ padding: 10 }} onPress={() => {
                        Linking.openURL("https://www.instagram.com/shopiads/")
                    }}>
                        <Image source={INSTAGRAM} style={{ width: 50, height: 50 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ padding: 10 }} onPress={() => {
                        Linking.openURL("https://twitter.com/ShopiAds")
                    }}>
                        <Image source={TWITTER} style={{ width: 50, height: 50 }} />
                    </TouchableOpacity>
                </View>
                <View >
                    <Text style={{ fontSize: 16, width: '100%', textAlign: 'center', fontWeight: 'bold', color: BLACK, }}>{'Version 1.0'}</Text>
                </View>
                <View style={{ justifyContent: 'flex-end', alignItems: 'center', width: '100%' }}>
                    <View style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, width: '100%', flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => {
                            Linking.openURL("https://www.shopiads.ca/termsOfuse")
                        }}>
                            <Text style={{ fontSize: 12 }}>{'Terms of Use * '}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            Linking.openURL("https://www.shopiads.ca/privacyPolicy")
                        }}>
                            <Text style={{ fontSize: 12 }}>{'Privacy Policy'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <LoginAlert
                showAlert={showLoginAlert}
                onLoginPress={() => {
                    //  props.setUserData(null);
                    onClickNavigate("Logout")
                    // navigation?.push('login');
                    setShowLoginAlert(false);
                }} onCancelPress={() => {
                    setShowLoginAlert(false)
                }} />
        </SafeAreaView>
    )
}

