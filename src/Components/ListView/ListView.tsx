import React, { useEffect, useState } from 'react'
import { View, Text, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { List, titleStyle } from 'react-native-paper'
import { setDepartmentKey, setProductKey } from '../../Actions/product'
import { ProductContext } from '../../Context/Context'
import { connect } from "react-redux";

interface listViewProps {
    arr: [],
    onFocus: boolean,
    childItem: String | React.ReactNode
    pressindex: Int16Array,
    onItemPress: () => void
}

const SimpleListView = ({ arr, pressindex, item, onItemPress, onFocus, ...props }: listViewProps) => {

    const [indexval, setIndex] = useState(0)

    useEffect(() => {

    //    props.setProductKey(0)
    }, [])

    const setProductData = (index) => {
        props.setProductKey(index)
        setIndex(index)
    }

    const renderData = (childItem, index) => {
        return (

            <TouchableOpacity {...props} onPress={() => setProductData(index)}
                style={{ backgroundColor: (indexval == index ? '#1776d3' : null) }}>
                <List.Item
                    title={childItem.name}

                    titleStyle={{ color: (indexval == index ? '#fee136' : '#989898'), fontSize: 14, }}
                />
            </TouchableOpacity>

        )
    }

    return (
        <ProductContext.Provider value={indexval}>
            <View style={{backgroundColor:'white'}}>

                <FlatList
                    {...props}
                    data={arr}
                    renderItem={({ item, index }) => renderData(item, index)}
                />
            </View>
        </ProductContext.Provider>
    )
}

const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch => ({
    setProductKey: (key) => dispatch(setDepartmentKey(key))
});

export default connect(
    null,
    mapDispatchToProps
)(SimpleListView);