import * as React from 'react';

import AppContainer from './NavigationStack/RootStack'
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { Provider as PaperProvider } from 'react-native-paper';
import {store, persistor} from "./Stores/ConfigureStore";
const Setup = () => {

  // const { persistor, store } = configureStore();


  return (
    <PaperProvider>

      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppContainer />
        </PersistGate>
      </Provider>

    </PaperProvider>
  )
}

export default Setup

global.XMLHttpRequest = global.originalXMLHttpRequest ?
  global.originalXMLHttpRequest :
  global.XMLHttpRequest;
global.FormData = global.originalFormData ?
  global.originalFormData :
  global.FormData;

fetch; // Ensure to get the lazy property

if (window.__FETCH_SUPPORT__) { // it's RNDebugger only to have
  window.__FETCH_SUPPORT__.blob = false;
} else {
  global.Blob = global.originalBlob ?
    global.originalBlob :
    global.Blob;
  global.FileReader = global.originalFileReader ?
    global.originalFileReader :
    global.FileReader;
}