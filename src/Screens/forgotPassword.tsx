import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, StatusBar, TouchableOpacity, Alert,  KeyboardAvoidingView } from 'react-native'
import { Button } from 'native-base'
import { SafeAreaView } from 'react-native-safe-area-context';

import ThemeButton from '../Components/Button'
import ThemeInput from '../Components/Input'
import { Heading } from '../Components/Heading'
import auth from '@react-native-firebase/auth';
import { StackNavigationProp } from '@react-navigation/stack';

interface registerProps {
    navigation?: StackNavigationProp<any, any>
}
const ForgotPassword = ({ navigation }: registerProps) => {

    const [email, setEmail] = useState('')

    const onForgotPress = () => {

        auth().sendPasswordResetEmail(email).then(res => {
            Alert.alert("Check your inbox/spam folder for further instruction")
            navigation?.push("Login")
        }).catch(err => {
            Alert.alert(err.toString())
        })


    }
    return (
        
        <SafeAreaView style={{...styles.container}} edges={['top']}>
            <Heading title={"Forgot Password"} rightText={"Close"} onRightPress={() => navigation?.pop()} />
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
                <View style={{flex:1}}>
                <View style={{ flex: 1}}></View>
                <View style={styles.miniContainer}>
                    <Text style={styles.textStyle}>{`Please enter your registered email address and \n we will help you to reset password`}</Text>
                    <ThemeInput onChangeText={(text:string) => setEmail(text)} imgPath={require('../../assets/Auth/user.png')} placeHolderText={"Email Address*"} />
                    <ThemeButton onBtnPress={() => onForgotPress()} btnMainStyle={{ marginVertical: 15 }} btnText={'Submit'} />
                </View>
                </View>
            </KeyboardAvoidingView>


        </SafeAreaView>
    )
}

export default ForgotPassword

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#7c8cb6',
        justifyContent: 'flex-end'

    },
    miniContainer: {
        height:250,
        padding: 18,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },


    textStyle: {
        fontSize: 16,
        color: '#212121',
    }

})