import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Button, Icon, Item} from 'native-base';
import {getOffersAll, getOfferCategory,getMainCategory, getAllOffersStoresJsonList, getOfferBanner, getOfferProducts, getAllStoresBasedonCategoryStores, getAllProductsBasedOnCategories, getAllFavoriteProduct} from '../Actions/Offer';
import {
  setProductKey,
  addToFavourite,
  deleteToFavourite,
  setStoreKey,
} from '../Actions/product';
import {connect} from 'react-redux';
import MyChip from '../Components/chips';
import ViewPager from '@react-native-community/viewpager';
import {Avatar} from 'react-native-paper';
import {
  all,
  noRecord,
  progresLoading,
DOWNARRAY
} from '../Constants/images';
import { style as storeStyle } from './Store.style';
import { Overlay, Rating } from 'react-native-elements';
import { Buffer } from "buffer"


import Icons from '../Constants/Icons';
import { BLACK, BLUE, WHITE } from '../Constants/colors';
import { Product } from '../Components/Product';
import { SafeAreaView } from 'react-native-safe-area-context';
import { BackHeader } from '../Components/Heading';
import { GridProduct } from '../Components/GridProduct';
import { addLikeProducts, makeUnlikeProducts } from '../Actions/StoreDetails';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { AuthContext } from '../Context/Context';

const CategoriesProducts = ({navigation, ...props}) => {

    const [dataChange, setDataChange] = useState(false);
    const [normalArr, setNormalArr] = useState([]);
    const [loadingData, setLoadingData] = useState(false);  
    const { Logout } = useContext(AuthContext)
  

    const [showLoginAlert, setShowLoginAlert] = React.useState(false);
    const [showLoader, setShowLoader] = React.useState(false);
  
    useEffect(() => {
    });

    useEffect(() => {
      let user = 'Z3Vlc3Q=';
      let city = props.getLocation.locationDataCity
        ? props.getLocation.locationDataCity.value
        : '733deffa992d4fb7ba80a345c62ca612';   
        let area = props.getLocation.locationDataArea
        ? props.getLocation.locationDataArea.areaId
        : '27341519d24c392e385a1e679fd1363a';
        console.log('/offers/all/' + user + '/' + city + '/' + area);
        let newdata=[] as any
        setLoadingData(true);
        getAllActiveCateogriesStores(city);
    }, [ props.getLocation.locationDataArea]);
  
  


const getAllActiveCateogriesStores = (cityId)=>{
    console.log('FFFF_>',props.categoryId);
 //  console.log('URL', '/products/categories/cmFtYW5Ac2hvcGlhZHMuY29t/'+props.categoryId+'/'+cityId)
    props.getAllProductsBasedOnCategories(cityId,props.categoryId,props.getLocation.userMarketPlace).then((res) => {
        let result = res.payload.data;
        console.log('FFFF_>',result.length);
        for (let i =0 ; i<result.length ; i++){
            let storeList =   props.store
             for(let j = 0 ;  j < storeList.length ;  j++){
               if(storeList[j].id == result[i].storeId){
                result[i].storeName =  storeList[j].storeName;
                break
              }
           }
          }
      
        setNormalArr(result);
     });
  }

  const renderProductItem = (item: any, idx: number) => {
        return (
            <GridProduct {...props} item={item} onItemPress={()=>{
            props.setProductKey(item.id)
            navigation.navigate('ProductDesc',{ itemType : item })
          }}
          foundInFav={foundInFav(item)}
          onAddToFavourite={()=>{
            onAddToFavourite(item, idx)
          }}
             />
        );
    };
    





  const onAddToFavourite = (item, index, type = "product") => {
    if (props.getLocation.email) {
      console.log("XXJXCJJCVJVJVJVJV",item);
      if(!item['requestType']   ){
    
      if (props.getFavData.length == 0) {
        item.totalLikes = item.totalLikes + 1
        addLikeStoreProductApi(item, type)
      } else {
        const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
        if (found) {
          item.totalLikes = item.totalLikes - 1
          const data = props.getFavData.filter(data => data.productId === item.id || data.offerId === item.id)
          let index = props.getFavData.indexOf(data[0]);
          props.getFavData.splice(index, 1);
          props.deleteToFavourite([]);
          props.deleteToFavourite(props.getFavData);
          setDataChange(!dataChange);
          removeLikeStoreProductApi(data[0],type)
        } else {
        item.totalLikes = item.totalLikes + 1
        addLikeStoreProductApi(item, type)
        }
      }
    }
  } else {
      setShowLoginAlert(true);
    }
  };

  const addLikeStoreProductApi = (item, type) => {
  setShowLoader(true)
  let email = Buffer.from(props.getLocation.email).toString('base64')
    item['requestType']= true;
    props.addLikeProducts(item.id, email, type).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
    item['requestType']= false;
    props.deleteToFavourite(res.payload.data);
    setShowLoader(false)
   
  });
    });
  }

  const removeLikeStoreProductApi = (item,type) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.makeUnlikeProducts(email,  type == "product"? item.productId:item.offerId,type , item.id).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });
    });
  }


  const foundInFav = (item) => {
    let FavItem = item;
    const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
    console.log('FFFFFF', props.getFavData)
    if (found) {
      return true;
    } else {
      return false;
    }
  };


  
  
    return (
        <SafeAreaView style={{flex: 1,backgroundColor:BLUE}}
    edges={['top']} 
        
        >
        <BackHeader title={''}
            fontSize={28}
            onFontPress={() => navigation?.pop()}
            fontName={'chevron-thin-left'}
        />
    <View style={{flex: 1,
    backgroundColor:'#FFFFFF'
    }}>
        
        <View style={{width:'100%',height:1,backgroundColor:BLACK}}/>
        <View style={{width:'100%',height:'100%'}}> 
      <FlatList
            data={normalArr}
            renderItem={({item, index}) => renderProductItem(item, index)}
            numColumns={2}
          />
         </View>
      </View>
      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          setShowLoginAlert(false);
          Logout(navigation)
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />

      <LoaderAlert
        showAlert={showLoader}
      />



      </SafeAreaView>
    );
  };


const mapStateToProps = (state) => ({
    getLocation: state.location,
    getFavData: state.productTab.favourite,
    store: state.productTab.store,
    categoryId: state.productTab.subCategoryId,
  
  });
  const mapDispatchToProps = (dispatch) => ({
    addToFavourite: (data) => dispatch(addToFavourite(data)),
    deleteToFavourite: (data) => dispatch(deleteToFavourite(data)),
   setProductKey:(data) => dispatch(setProductKey(data)),
   getAllProductsBasedOnCategories:(cityId,categoryId,marketPlace) => dispatch(getAllProductsBasedOnCategories(cityId,categoryId,marketPlace)),
   getAllFavoriteProduct: (data) => dispatch(getAllFavoriteProduct(data)),
   addLikeProducts: (data, userId, type) => dispatch(addLikeProducts(data, userId, type)),
   makeUnlikeProducts: (userId, offerId,type,likeId) => dispatch(makeUnlikeProducts(userId, offerId,type,likeId)),
  
  });
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CategoriesProducts)