import React ,{useEffect, useState}from 'react';
import {View, Text, Alert, FlatList, Image, StyleSheet, Dimensions} from 'react-native';
import {Container, Icon} from 'native-base';
import {BackHeader} from '../Components/Heading';
import Modal from 'react-native-modalbox';
import { connect } from 'react-redux'
import ProductDesc from './ProductDesc';
import {
  productCasualShirt,
  productFormalShirt,
  productshorts,
  productTshirt,
  productJeans,
  productHoody,
  dropDown,
  productBlazer,infoIc,
  plus,
  fashionBanner
} from '../Constants/images';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icons from '../Constants/Icons';

import {style as productStyle}  from './Product.style'
import ThemeButton from '../Components/Button';
import ViewPager from '@react-native-community/viewpager';
import { Rating } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import {  getAllTabs, getCollectionsByStoreTabProducts, getHomeTabProducts, getProductsTabsProducts, getStoreDetails, getStoreFollowList } from '../Actions/StoreDetails';
import { addToFavourite, deleteToFavourite, setProductKey } from '../Actions/product';
import { BLACK, BLUE, GREY, WHITE } from '../Constants/colors';
import moment from 'moment';
import HTMLView from 'react-native-htmlview';


const homeCategory={
  heading:'Home',
  id:''
}

const productCategory={
  heading:'Products',
  id:''

}

const AboutStore = ({navigation, route, ...props}) => {
  const [dataChange, setDataChange] = useState(false);
  const [storeDetails,setStoreDetails] = useState({}); //to show the "Read more & Less Line"

    const [followingCount,setFollowingCount]= useState(0)
  
    useEffect(()=>{
      if(Object.keys(storeDetails).length == 0){
        props.getStoreDetails(props.storeId).then((res) => {
          let result = res.payload.data;
          setStoreDetails(result)
          props.getStoreFollowList(props.storeId).then((res) => {
            let result = res.payload.data;
            setFollowingCount(result.length);
          });
          
        });
      }
    },)

  const parseDateFormat = (time:string) =>{
    const arr = time.split(":"); // splitting the string by colon
    let date =new  Date()
    date.setHours(arr[0])
    date.setMinutes(arr[1])
    return moment(date).format('hh:mmA')
  }
  

  const htmlViewStyle= StyleSheet.create({ p: { marginBottom: -25,marginLeft:3 } })

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:BLUE}} edges={['top']} >
    <BackHeader title={'About Store'} fontSize={28} onFontPress={() => navigation?.pop()} fontName={'chevron-thin-left'}/>
        <ScrollView style={{width:'100%',flex:1,backgroundColor:WHITE}}>
        <View style={{width:'100%',flexDirection:'column',paddingHorizontal:10,paddingTop:10}}>
        <View style={{width:'100%',flexDirection:'column',paddingTop:10}}>
          <View style={{width:'100%',flexDirection:'column',justifyContent:'center',alignItems:'center',backgroundColor:'#607d8b',padding:8,borderRadius:5}} >
          <View style={{width:'100%',flexDirection:'row',justifyContent:'center',alignItems:'center',}} >
            <View  style={{borderColor:'grey',borderWidth:1,padding:2,borderRadius:4}}>
            <Image 
             source={{uri:storeDetails['companyLogo']}}
            style={{width:70,height:40,borderRadius:2,resizeMode:'stretch'}}>
            </Image>
            </View>

          <View style={{flexDirection:'column',flex:1}}>
            <Text style={{...styles.txtStoreName,paddingVertical:0,flex:0,fontSize:13,paddingHorizontal:10}}>{storeDetails['storeName'] + (storeDetails['code'] == "Primary"? "":" Branch: "+storeDetails['code'])} </Text>
            <Text style={{...styles.txtDesc,flex:0,fontSize:13,paddingHorizontal:10}}>{storeDetails['address2']+','+"Unit: "+storeDetails['address1']+', '+storeDetails['cityId']+' '+storeDetails['pincode'] }</Text> 
            </View>  
          </View>
         
          <View style={{
           width:'100%',
           marginTop:10,
           flexDirection:'column'
         }}>
        <View style={{width:'100%',flexDirection:'row',}} >
             <View  style={{flex:1,flexDirection:'row',}}> 
               <Text style={styles.txtStoreDetailsTitle}> {'Incorporated As:'} </Text> 
               <Text style={{...styles.txtStoreDetailsDesc,flex:1.7}} numberOfLines={1}> {storeDetails['companyBusinessName']} </Text>
              </View>
            <View style={{flex:0.4,flexDirection:'row',}} >
              <Text style={styles.txtStoreDetailsTitle}> {'Followers:'} </Text> 
               <Text style={{...styles.txtStoreDetailsDesc,textAlign:'right',flex:0.9}} numberOfLines={1}> {followingCount.toString()} </Text>
              </View>
        </View>
        <View style={{width:'100%',flexDirection:'row',}}>
             <View  style={{flex:1,flexDirection:'row',}}> 
               <Text style={styles.txtStoreDetailsTitle}>{' Presence:'}</Text> 
              <Text style={{...styles.txtStoreDetailsDesc,flex:1.7}} numberOfLines={1}> {storeDetails['isphysical']==="Y"?'Online & In-Store':'Online'} </Text>
              </View>
            <View style={{flex:0.4,flexDirection:'row',}} >
              <Text style={styles.txtStoreDetailsTitle}> {'Rating:'} </Text> 
               <Text style={{...styles.txtStoreDetailsDesc,textAlign:'right',flex:1.1}} numberOfLines={1}> {storeDetails['avgRating']} </Text>
              </View>
        </View>
       

        <View style={{width:'100%',flexDirection:'row',}} >
        <View  style={{flex:1,flexDirection:'row',}}> 
               <Text style={styles.txtStoreDetailsTitle}>{' Delivery:'}</Text> 
              <Text style={{...styles.txtStoreDetailsDesc,flex:1.7}} numberOfLines={1}> {storeDetails['deliveryNotes']!=""?"Free over $"+storeDetails['deliveryNotes']:storeDetails['deliveryType']!=""&& storeDetails['secondDeliveryType']!="" &&storeDetails['secondDeliveryType']!=undefined ? storeDetails['deliveryType']+"/"+storeDetails['secondDeliveryType']
                              :storeDetails['deliveryType']!=""?storeDetails['deliveryType']:storeDetails['secondDeliveryType']!="" &&storeDetails['secondDeliveryType']!=undefined?storeDetails['secondDeliveryType']:""} </Text>
              </View>
            <View style={{flex:0.4,}} >
            <TouchableOpacity
            onPress={()=>{
                console.log('VVVV',storeDetails['fromHrs']);
                Alert.alert("Timing",(  storeDetails['fromHrs'] &&  storeDetails['toHrs'] ?( parseDateFormat( storeDetails['fromHrs'].toString().replace('[',"").replace(']',"")) +"-"+ parseDateFormat(storeDetails['toHrs'].toString().replace('[',"").replace(']',""))):"")+"\n" +storeDetails["timeNote"]+"\n"+(storeDetails["weeklyOff"].trim().length >0 ? ("Closed: "+storeDetails["weeklyOff"]):"") ) 
              }}
              style={{flexDirection:'row',justifyContent:'space-between'}} >
              <Text style={{...styles.txtStoreDetailsTitle,flex:0,}}> {'Timing:'} </Text> 
              <Image  source={infoIc} style={{width:15,height:15,marginRight:-2}}/>
              </TouchableOpacity>
             </View>
        </View>
        </View>
        </View>
       </View>
       <View style={{padding:10}}>
       <Text style={{...styles.txtStoreDetailsDesc, color:BLACK, fontSize:16}}> {'About Store'} </Text>

       <Text style={{...styles.txtStoreDetailsDesc, color:BLACK,marginTop:10}}> {'Store Description:'} </Text>
        <HTMLView addLineBreaks={false} value={storeDetails['storeDescription']} stylesheet={htmlViewStyle}/>

       <Text style={{...styles.txtStoreDetailsDesc, color:BLACK,marginTop:10}}> {'Returns:'} </Text>
       <HTMLView addLineBreaks={false} value={storeDetails['returnPolicy']} stylesheet={htmlViewStyle}/>

       <Text style={{...styles.txtStoreDetailsDesc, color:BLACK,marginTop:10}}> {'Delivery:'} </Text>
       <HTMLView addLineBreaks={false} value={storeDetails['deliveryPolicy']} stylesheet={htmlViewStyle}/>

       <Text style={{...styles.txtStoreDetailsDesc, color:BLACK,marginTop:10}}> {'Warranty:'} </Text>
       <HTMLView addLineBreaks={false} value={storeDetails['warrantyPolicy']} stylesheet={htmlViewStyle}/>

       </View>
     </View>
     </ScrollView>
    </SafeAreaView>
  );
};


const mapStateToProps = (state) => ({
  getFavData: state.productTab.favourite,
  storeId: state.productTab.storeKey,

});
const mapDispatchToProps = dispatch => ({
  getStoreDetails:(userId) => dispatch(getStoreDetails(userId)),
  getHomeTabProducts:(userid) => dispatch(getHomeTabProducts(userid)),
  addToFavourite: (data) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data) => dispatch(deleteToFavourite(data)),
  getAllTabs:(data)=> dispatch(getAllTabs(data)),
  getProductsTabsProducts:(data) => dispatch(getProductsTabsProducts(data)),
  getCollectionsByStoreTabProducts:(data) => dispatch(getCollectionsByStoreTabProducts(data)),
  setProductKey: (data)=> dispatch(setProductKey(data)),
  getStoreFollowList:(storeId) => dispatch(getStoreFollowList(storeId))
 
  
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutStore)





const styles = StyleSheet.create({
 container: {
  flex: 1,
},
logo: {
  resizeMode: 'contain',
  height: 160,
  marginTop:10,
},

txtStoreName:{
  paddingVertical:10,
  color:'white',
  fontWeight:'bold'
},

txtStoreDetailsTitle:{
  color:'#CFCFCF',
  flex:0.9,
  fontSize:9,
},
txtStoreDetailsDesc:{  
  color:'white',
  fontWeight:'bold',
  fontSize:12,
},
txtDesc:{
color:'#CFCFCF',
},
socialMedia: {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
},
socialMediaInner: {
  resizeMode: 'cover',
  height: 50,
  width: 50,
  margin: 5

},
btnWhiteBorder:{
borderColor: 'white',
borderWidth:1,
marginLeft:5,
alignItems:'center',
flexDirection:'row',
justifyContent:'center',
borderRadius:3,
width:Dimensions.get('window').width/2-25,
height:35,

},btnSolid:{
backgroundColor: '#1776d3',
marginRight:5,
alignItems:'center',
justifyContent:'center',
borderRadius:3,
width:Dimensions.get('window').width/2-25,
height:35,

},
forgottext: {
  alignSelf: 'flex-end',
  fontSize: 16,
  marginTop: 10,
  marginEnd:25,
  marginBottom:10,
},
textStyle: {
  fontSize: 16,
  fontWeight: 'bold',
}
})