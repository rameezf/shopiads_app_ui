import React, { useState, useEffect, useContext } from 'react';
import { View, Text, Alert, FlatList, Image, StyleSheet, TextInput } from 'react-native';
import { Container, Icon } from 'native-base';
import { BackHeader } from '../Components/Heading';
import ProductDesc from './ProductDesc';
import {
  productCasualShirt,
  productFormalShirt,
  productshorts,
  productTshirt,
  productJeans,
  productHoody,
  dropDown,
  ARROW,
  plus,
  FILTER,
  fashionBanner
} from '../Constants/images';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from '../Constants/Icons';

import SideMenu from 'react-native-side-menu'

import { SafeAreaView } from 'react-native-safe-area-context';
import { Menu } from 'react-native-paper';

import { CheckBox, Overlay, Rating } from 'react-native-elements';
import { getAllFavoriteProduct, getMainCategory, getSearchCategory, getSubCategoriesList, getCategoriesById } from '../Actions/Offer';

import { connect } from 'react-redux';
import { BLUE, GREY, LIGHTGREY, WHITE } from '../Constants/colors';
import { Product } from '../Components/Product';
import { addToFavourite, deleteToFavourite, setProductKey } from '../Actions/product';
import { transform } from '@babel/core';
import { GridProduct } from '../Components/GridProduct';
import { addLikeProducts, removeLikeProducts } from '../Actions/StoreDetails';
import { Buffer } from "buffer"
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { AuthContext } from '../Context/Context';


const Search = ({ navigation, route, ...props }) => {
  const [categories, setCategories] = useState([]);
  const [selectedChipItem, setSelectedChipItem] = useState({});
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [ratingPos, setRatingPosition] = useState(0);
  const [search, setSearch] = useState('');

  const [product, setProduct] = useState([]);
  const [menu, setMenu] = useState(false);
  const [latest, setLatest] = useState(true);
  const [priceSort, setPriceSort] = useState(0);
  const [filter, setFilter] = useState({ minPrice: '', maxPrice: '', rating: 0, selectedCategory: {}, selectedSubCategories: [] })
  const [selectedCategoriesList, setSelectedCatoriesList] = useState([]);
  const [checkedSubCategories, setCheckedSubCategories] = useState([]);
  const [dataChange, setDataChange] = useState(false);
  const [showLoader, setShowLoader] = React.useState(false);

  const { Logout } = useContext(AuthContext)

  const [showLoginAlert, setShowLoginAlert] = React.useState(false);

  const [subcategories, setSubCategories] = useState([]);


  useEffect(() => {
    setSearch(props.search);
    let search = props.search;
    if (search.length != 0) {
      let path = search + "/" + (filter.selectedCategory.id ? filter.selectedCategory.id : "all") + "/";
      if (filter.selectedSubCategories && filter.selectedSubCategories.length != 0) {
        for (let i = 0; i < filter.selectedSubCategories.length; i++) {
          if (i != 0) {
            path += ",";
          }
          path += filter.selectedSubCategories[i].id;
        }
      } else {
        path += 'undefined';
      }
      path += "/product/" + (filter.rating != 0 ? filter.rating : 'undefined') + "/" + (filter.minPrice ? filter.minPrice : 'undefined') + "/" + (filter.maxPrice ? filter.maxPrice : 'undefined') + "/" + props.getLocation.userMarketPlace
      setPriceSort(0);
      console.log('VVKJDJDJDD', path)
      props.getSearchCategory(path).then((res: any) => {
        console.log('Search Product Result :::::', res);
        if (res.payload.status === 200 && res.payload.data.length > 0) {
          console.log('Search Product Call Data :::::', res);
          setProduct(res.payload.data);
          callAPIonLoad(res.payload.data);
        } else {
          console.log('Search Product Call No Data :::::', res);
          setProduct([]);
          callAPIonLoad([]);
        }
      })
    }
    console.log("Call When Search is set for firts time ::::");
  }, [props.search])


  const ascendingOrder = () => {
    console.log("Ascending Order Product :::");
    product.sort(function (obj1, obj2) {
      return obj1.offerPrice - obj2.offerPrice;
    });
    setProduct([...product]);
  };


  const descendingOrder = () => {
    console.log("Descending Order Product :::");
    product.sort(function (obj1, obj2) {
      return obj2.offerPrice - obj1.offerPrice;
    });
    setProduct([...product]);
  };

  useEffect(() => {
    console.log("Call Api on Load :::");
    // callAPIonLoad();
  }, [product])



  async function callAPIonLoad(product: any): Promise<void> {
    console.log("Call api on load call ::::: ", product.length);
    if (product.length > 0 && Object.keys(props.subCategories).length != 0) {
      console.log("Product List Data not null :::");
      let catList: any = [];
      let departmentList: any = [];
      if (product != undefined && product.length > 0) {
        console.log("resultDep>>>>>>>>>>>>>> 11111", product);
        for (let i = 0; i < product.length; i++) {
          if (product[i]['categories'] != undefined && product[i]['categories'].length > 0) {
            catList.push(product[i].categories[0]);
            try {
              console.log("User Id Search ::::", props.getLocation.email);
              let userId = props.getLocation.email !== null && props.getLocation.email !== undefined ?
                Buffer.from(props.getLocation.email).toString('base64') : "gust";
              console.log("User Id Search :::: 1111 ", userId);
              await props.getCategoriesById(userId, product[i].categories[0]).then((response: any) => {
                if (response.payload != undefined) {
                  departmentList.push(response.payload.data);
                }
              });
            } catch (e) {
              console.log(e);
            }
          }
        }
        const dataArr = departmentList.map((item: any) => {
          return [item.name, item]
        });
        const maparr = new Map(dataArr); // create key value pair from array of array
        const resultDep: any = [...maparr.values()];//converting back to array from mapobject
        console.log("resultDep>>>>>>>>>>>>>>", resultDep, "Product Data ::::::", product);
        setSelectedCatoriesList(resultDep);
      } else {
        setSelectedCatoriesList([]);
      }
    } else {
      setSelectedCatoriesList([]);
    }
    // for (let i = 0; i < product.length; i++) {
    //   let categoryId = product[i].mainCategory;
    //   let ctObjFilter = props.categories.filter(data => {
    //     console.log("Product Id ::::",product[i]);
    //     console.log("category Id ::::",categoryId);
    //     console.log("Main Category Id ::::",data.subcategories, data.name);
    //     console.log("props.subCategories ::::",props.subCategories);
    //     return data.id === categoryId
    //   });  //  MAIN Category 
    //   ctObj.push(ctObjFilter[0]);
    //   let subCategoriesData = product[i].categories; 
    //   console.log("SubCategories Data ::::",subCategoriesData);
    //   subCategories.push(subCategoriesData);
    //   //  Product link with  sub categories
    // }  
    // if (ctObj.length > 0) {  //  Checking main categoriy is greater than 0
    //   for (let k = 0; k < ctObj.length; k++) {
    //     if (subCategories.length > 0) {
    //       console.log("ctObj[k].name :::",ctObj[k].name);
    //       let subCategoryFilter: [] = props.subCategories[ctObj[k].name]
    //       console.log("Sub Category Filter ::::",subCategoryFilter);
    //       for (let j = 0; j < subCategoryFilter.length; j++) {
    //         let obj = subCategoryFilter[j]
    //         let data: any = []
    //         for (let m = 0; m < subCategories.length; m++) {
    //           let subCategorySubData: [] = props.subCategories[obj.name].filter((data:any) => data.id === subCategories[m])
    //           data = [...data, ...subCategorySubData];
    //           // console.log('dataVVVVVVVVVVV',data)
    //         }
    //        // obj['data'] = data;

    //         if (data.length > 0) {
    //           if (obj.id in subCatgoryHasg) {
    //             let dataObj = subCatgoryHasg[obj.id]
    //             let categories = [...dataObj['data'], ...data]
    //             categories = categories.filter(function (item, pos) {
    //               return categories.indexOf(item) == pos;
    //             })
    //             dataObj['data'] = categories
    //             subCatgoryHasg[obj.id] = dataObj
    //           } else {
    //             let dummyObj = obj;
    //             dummyObj['data'] = [...data]
    //             subCatgoryHasg[obj.id] = dummyObj
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
    // Object.keys(subCatgoryHasg).forEach(function (key) {
    //   var value = subCatgoryHasg[key];
    //   subCategoryArray.push(value);
    //   // ...
    // });
  }

  useEffect(() => {
    console.log("Use Effect Call Filter Product ::::", selectedCategoriesList);
    if (search.length != 0) {
      let path = search + "/" + (filter.selectedCategory.id ? filter.selectedCategory.id : "all") + "/";
      if (filter.selectedSubCategories && filter.selectedSubCategories.length != 0) {
        for (let i = 0; i < filter.selectedSubCategories.length; i++) {
          if (i != 0) {
            path += ",";
          }
          path += filter.selectedSubCategories[i].id;
        }
      } else {
        path += 'undefined';
      }
      path += "/product/" + (filter.rating != 0 ? filter.rating : 'undefined') + "/" + (filter.minPrice ? filter.minPrice : 'undefined') + "/" + (filter.maxPrice ? filter.maxPrice : 'undefined') + "/" + props.getLocation.userMarketPlace
      setPriceSort(0);
      console.log('VVKJDJDJDD', path)
      props.getSearchCategory(path).then((res: any) => {
        console.log('Search Product Result :::::', res);
        if (res.payload.status === 200 && res.payload.data.length > 0) {
          console.log('Search Product Call Data :::::', res);
          setProduct(res.payload.data)
        } else {
          console.log('Search Product Call No Data :::::', res);
          setProduct([]);
        }
      });
      console.log("Use Effect Call Filter Product 1111 ::::", selectedCategoriesList);
    }
  }, [filter])


  const searchProduct = () => {
    if (search.length != 0) {
      let path = search + "/" + (filter.selectedCategory.id ? filter.selectedCategory.id : "all") + "/";
      if (filter.selectedSubCategories && filter.selectedSubCategories.length != 0) {
        for (let i = 0; i < filter.selectedSubCategories.length; i++) {
          if (i != 0) {
            path += ",";
          }
          path += filter.selectedSubCategories[i].id;
        }
      } else {
        path += 'undefined';
      }
      path += "/product/" + (filter.rating != 0 ? filter.rating : 'undefined') + "/" + (filter.minPrice ? filter.minPrice : 'undefined') + "/" + (filter.maxPrice ? filter.maxPrice : 'undefined') + "/" + props.getLocation.userMarketPlace
      setPriceSort(0);
      console.log('VVKJDJDJDD', path)
      props.getSearchCategory(path).then((res: any) => {
        console.log('Search Product Result :::::', res);
        if (res.payload.status === 200 && res.payload.data.length > 0) {
          console.log('Search Product Call Data :::::', res);
          setProduct(res.payload.data)
          callAPIonLoad(res.payload.data);
        } else {
          console.log('Search Product Call No Data :::::', res);
          setProduct([]);
          callAPIonLoad([]);
        }
      })
    }
  }


  const foundInSubCategories = (item: any) => {
    let FavItem = item;
    const found = checkedSubCategories.some((el: any) => el.id === item.id);
    if (found) {
      return true;
    } else {
      return false;
    }
  };

  const onAddToInSubCategories = (item: any) => {
    if (checkedSubCategories.length == 0) {
      let data: any = [];
      data.push(item);
      setCheckedSubCategories(data);
    } else {
      const found = foundInSubCategories(item);
      if (found) {
        const data: any = [];
        for (let i = 0; i < checkedSubCategories.length; i++) {
          if (checkedSubCategories[i].id != item.id) {
            data.push(checkedSubCategories[i])
          }
        }
        setCheckedSubCategories(data);
      } else {
        const data: any = [];
        for (let i = 0; i < checkedSubCategories.length; i++) {
          data.push(checkedSubCategories[i])
        }
        data.push(item);
        setCheckedSubCategories(data);
      }
    }
  };

  const getKeyByValue = (object: any, value: any) => {
    for (var prop in object) {
      if (object.hasOwnProperty(prop)) {
        for (var i = 0; i <= object[prop].length; i++) {
          if (object[prop][i] != undefined && object[prop][i] != null && object[prop][i].id === value) {
            if (!containsNumbers(prop))
              return prop;
          }
        }
      }
    }
  }

  function containsNumbers(value: any) {
    return /\d/.test(value);
  }

  const renderSubCategoryItem = (item: any, idx: number) => {
    return (
      <View style={{ width: '100%' }}>
        <Text style={{ color: 'black', fontSize: 10, padding: 10 }}>{getKeyByValue(props.subCategories, item.id)}</Text>
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', }} onPress={() => { onAddToInSubCategories(item); }}>
          <CheckBox checked={foundInSubCategories(item)} style={{ width: 20, height: 20 }} />
          <View style={{ flex: 1, height: 50, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ color: 'black', fontSize: 10, }}>{item.name}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const onAddToFavourite = (item: any, type = "product") => {
    console.log("Add to favourite::::", item);
    if (props.getLocation.email) {
      if (!item['requestType']) {
        if (props.getFavData.length == 0) {
          item.totalLikes = item.totalLikes + 1;
          //props.addToFavourite(item);
          addLikeStoreProductApi(item, type);
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
          if (found) {
            item.totalLikes = item.totalLikes - 1;
            const data = props.getFavData.filter(data => data.productId === item.id || data.offerId === item.id)
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0]);
          } else {
            item.totalLikes = item.totalLikes + 1;
            addLikeStoreProductApi(item, type);
            //props.addToFavourite(item);
          }
        }
      }
    } else {
      setShowLoginAlert(true);
    }
  };




  const addLikeStoreProductApi = (item: string, type: any) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    item['requestType'] = true;
    setShowLoader(true);
    props.addLikeProducts(item.id, email, type).then((res: any) => {
      props.getAllFavoriteProduct(email).then((res: any) => {
        item['requestType'] = false;
        setShowLoader(false);
        props.deleteToFavourite(res.payload.data);
      });
    });
  }

  const removeLikeStoreProductApi = (item: {}) => {
    let email = Buffer.from(props.getLocation.email).toString('base64');
    props.removeLikeProducts(email, item).then((res: any) => {
      props.getAllFavoriteProduct(email).then((res: any) => {
        props.deleteToFavourite(res.payload.data);
      });
    });
  }


  const foundInFav = (item: {}) => {
    const found = props.getFavData.some((el: any) => el.productId === item.id || el.offerId === item.id);
    if (found) {
      return true;
    } else {
      return false;
    }
  };


  const renderProductItem = (item: any, idx: number) => {
    return (
      <GridProduct {...props} item={item} onItemPress={() => {
        props.setProductKey(item.id)
        navigation.navigate('ProductDesc', { itemType: item })
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item)
        }}
      />
    );
  };


  console.log("selectedCategoriesList:::::", selectedCategoriesList);
  return (
    <SideMenu
      isOpen={menu}
      onChange={(isOpen: any) => setMenu(isOpen)}
      scrollsToTop={true}
      menuPosition={'right'}
      bounceBackOnOverdraw={false}
      menu={
        <SafeAreaView
          edges={['top']}
          style={{ flex: 1, backgroundColor: BLUE }}>
          <View style={{
            height: '100%',
            backgroundColor: 'white',
            width: '100%',
            alignItems: 'center'
          }}
          >
            <View style={{
              flex: 0.0904,
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
              backgroundColor: '#1776d3'
            }}
            />
            <Text style={{ backgroundColor: '#F2F2F2', width: '100%', padding: 10, marginTop: 10 }} >{'Search Filter'}</Text>
            <View style={{ flex: 1, width: '100%' }}>
              <ScrollView style={{ width: '100%', flex: 1, flexDirection: 'column' }}>
                <View style={{ width: '100%', flex: 1, flexDirection: 'column' }}>
                  {
                    selectedCategoriesList.length != 0 && (<Text style={{ width: '100%', padding: 10 }} >{'By Department'}</Text>)
                  }
                  <FlatList
                    data={selectedCategoriesList}
                    renderItem={({ item, index }) => renderSubCategoryItem(item, index)}
                  />
                  <Text style={{ width: '100%', padding: 10 }} >{'Price Range'}</Text>
                  <View style={{ width: '100%', padding: 10, backgroundColor: '#F2F2F2', flexDirection: 'row' }}>
                    <TextInput
                      value={minPrice}
                      onChangeText={setMinPrice}
                      keyboardType='numeric'
                      style={{ flex: 1, height: 40, fontSize: 12, paddingHorizontal: 10, backgroundColor: 'white', marginEnd: 5 }}
                      placeholder='MIN Price'
                    />
                    <TextInput
                      value={maxPrice}
                      onChangeText={setMaxPrice}
                      keyboardType='numeric'
                      style={{ flex: 1, height: 40, fontSize: 12, paddingHorizontal: 10, backgroundColor: 'white', marginEnd: 5 }}
                      placeholder='Max Price'
                    />
                  </View>
                  <Text style={{ width: '100%', padding: 10 }} >{'Rating'}</Text>
                  <View style={{ width: '100%', padding: 10, backgroundColor: 'white', flexDirection: 'column' }}>
                    <TouchableOpacity style={{ ...(ratingPos == 5 ? styles.categorySelected : styles.categoryUnSelected), width: '100%', flexDirection: 'row', marginTop: 10 }}
                      onPress={() => {
                        setRatingPosition(5)
                      }}
                    >
                      <Rating
                        type='star'
                        fractions={2}
                        style={{ width: 150 }}
                        readonly={true}
                        ratingCount={5}
                        showRating={false}
                        startingValue={5}
                        imageSize={24}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...(ratingPos == 4 ? styles.categorySelected : styles.categoryUnSelected), width: '100%', flexDirection: 'row', marginTop: 10 }}
                      onPress={() => {
                        setRatingPosition(4)
                      }}>
                      <Rating
                        type='star'
                        fractions={2}
                        style={{ width: 150 }}
                        readonly={true}
                        ratingCount={5}
                        showRating={false}
                        startingValue={4}
                        imageSize={24}
                      />
                      <Text>{'& up'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...(ratingPos == 3 ? styles.categorySelected : styles.categoryUnSelected), width: '100%', flexDirection: 'row', marginTop: 10 }}
                      onPress={() => {
                        setRatingPosition(3)
                      }}>
                      <Rating
                        type='star'
                        fractions={2}
                        style={{ width: 150 }}
                        readonly={true}
                        ratingCount={5}
                        showRating={false}
                        startingValue={3}
                        imageSize={24}
                      />
                      <Text>{'& up'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...(ratingPos == 2 ? styles.categorySelected : styles.categoryUnSelected), width: '100%', flexDirection: 'row', marginTop: 10 }}
                      onPress={() => {
                        setRatingPosition(2)
                      }}>
                      <Rating
                        type='star'
                        fractions={2}
                        style={{ width: 150 }}
                        readonly={true}
                        ratingCount={5}
                        showRating={false}
                        startingValue={2}
                        imageSize={24}
                      />
                      <Text>{'& up'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...(ratingPos == 1 ? styles.categorySelected : styles.categoryUnSelected), width: '100%', flexDirection: 'row', marginTop: 10 }}
                      onPress={() => {
                        setRatingPosition(1)
                      }}>
                      <Rating
                        type='star'
                        fractions={2}
                        style={{ width: 150 }}
                        readonly={true}
                        ratingCount={5}
                        showRating={false}
                        startingValue={1}
                        imageSize={24}
                      />
                      <Text>{'& up'}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={{ width: '100%', padding: 10, backgroundColor: '#F2F2F2', flexDirection: 'row', height: 60 }}>
              <TouchableOpacity style={{ flex: 1, height: 40, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: WHITE, borderColor: BLUE, borderWidth: 1, borderRadius: 3, marginEnd: 5 }}
                onPress={() => {
                  let data = { minPrice: '', maxPrice: '', rating: 0, selectedCategory: {}, selectedSubCategories: [] }
                  setFilter(data);
                  setMenu(false); setCheckedSubCategories([])
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    color: BLUE,
                    paddingHorizontal: 20
                  }}
                >{'Reset'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, height: 40, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: BLUE, borderRadius: 3, marginEnd: 5 }}
                onPress={() => {
                  let data = { minPrice: minPrice, maxPrice: maxPrice, rating: ratingPos, selectedCategory: selectedChipItem, selectedSubCategories: checkedSubCategories }
                  setFilter(data);
                  setMenu(false)
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    color: 'white',
                    paddingHorizontal: 20
                  }}
                >{'Apply'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      } >
      <SafeAreaView style={{ flex: 1, backgroundColor: BLUE }}
        edges={['top']}
      >
        <View style={{ width: '100%', height: '100%', backgroundColor: WHITE }}>
          <BackHeader title={"Search"}
            fontSize={28}
            onFontPress={() => navigation?.pop()}
            fontName={'chevron-thin-left'}
          />
          <View style={{ ...styles.inputStyleBg, backgroundColor: '#f2f2f2', padding: 0, flexDirection: 'row', height: 45, margin: 10, justifyContent: 'center', alignItems: 'center' }}>
            <Icons.AntDesign name={'search1'} size={28} color={'#545454'} />
            <TextInput style={{ ...styles.inputStyle, fontSize: 12 }} placeholder='Shop with incorporated sellers only'
              placeholderTextColor={GREY}
              value={search}
              onChangeText={(value) => { setSearch(value) }}
              returnKeyType="search"
              onSubmitEditing={() => { searchProduct() }}
            />
            <TouchableOpacity onPress={() => {
              setMinPrice(filter.minPrice);
              setMaxPrice(filter.maxPrice);
              setRatingPosition(filter.rating);
              // setSelectedChipItem(filter.selectedCategory);
              // setCheckedSubCategories(filter.selectedSubCategories)
              setMenu(true);
            }}
              style={{ width: 28, height: 28, padding: 5 }}>
              <Image source={FILTER} style={{ width: 20, height: 20, padding: 5 }} />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={latest ? styles.buttonSelected : styles.buttonUnSelected}>
              <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={() => {
                setLatest(true);
              }}>
                <Text style={{ color: 'black', fontSize: 12, }}>
                  {'Latest'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ height: 30, width: 1, backgroundColor: LIGHTGREY }}></View>
            <View style={{ height: 30, width: 1, backgroundColor: LIGHTGREY }}></View>
            <View style={styles.buttonUnSelected}>
              <TouchableOpacity style={{ paddingHorizontal: 10, flexDirection: 'row' }}
                onPress={() => {
                  if (priceSort == 0 || priceSort == 2) {
                    setPriceSort(1);
                    ascendingOrder()
                  } else {
                    descendingOrder()
                    setPriceSort(2)
                  }
                }}
              >
                <Text style={{ color: 'black', fontSize: 12, }}>
                  {'Price'}
                </Text>
                <Image style={{ ...((priceSort != 0) ? styles.imgSelected : styles.imgUnSelected), transform: [{ rotate: priceSort != 2 ? '0deg' : '180deg' }] }} source={dropDown} >
                </Image>
              </TouchableOpacity>
            </View>
          </View>
          <FlatList
            data={product}
            numColumns={2}
            renderItem={({ item, index }) => renderProductItem(item, index)}
            style={{ flex: 1, paddingHorizontal: 10 }}
          />
          <LoginAlert
            showAlert={showLoginAlert}
            onLoginPress={() => {
              //  props.setUserData(null);
              // navigation?.push('login');
              setShowLoginAlert(false);
              Logout(navigation);
            }} onCancelPress={() => {
              setShowLoginAlert(false)
            }} />
          <LoaderAlert
            showAlert={showLoader}
          />
        </View>
      </SafeAreaView>
    </SideMenu>
  )
}


const mapStateToProps = (state: any) => ({
  getFavData: state.productTab.favourite,
  search: state.productTab.search,
  getLocation: state.location,
  categories: state.productTab.categories,
  subCategories: state.productTab.subCategories
})

const mapDispatchToProps = (dispatch: any) => ({
  getMainCategory: (user: any) => dispatch(getMainCategory(user)),
  getSearchCategory: (path: any) => dispatch(getSearchCategory(path)),
  setProductKey: (data: any) => dispatch(setProductKey(data)),
  getSubCategoriesList: (user: any) => dispatch(getSubCategoriesList(user)),
  getAllFavoriteProduct: (userId: string) => dispatch(getAllFavoriteProduct(userId)),
  addLikeProducts: (data: any, userId: string, type: string) => dispatch(addLikeProducts(data, userId, type)),
  removeLikeProducts: (userId: string, item: any) => dispatch(removeLikeProducts(userId, item)),
  addToFavourite: (data: {}) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data: {}) => dispatch(deleteToFavourite(data)),
  getCategoriesById: (userId: string, categoryId: string) => dispatch(getCategoriesById(userId, categoryId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)


const styles = StyleSheet.create({

  categorySelected: {
    backgroundColor: 'white',
    margin: 5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: BLUE,
    borderWidth: 2,
    height: 40,
  },
  categoryUnSelected: {
    backgroundColor: 'white',
    margin: 5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#F2F2F2',
    borderWidth: 2,
    height: 40,
  },

  buttonSelected: { backgroundColor: WHITE, borderWidth: 2, borderColor: WHITE, borderBottomColor: BLUE, flex: 1, height: 40, marginHorizontal: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' },
  buttonUnSelected: { backgroundColor: WHITE, borderWidth: 2, borderColor: WHITE, borderBottomColor: LIGHTGREY, flex: 1, height: 40, marginHorizontal: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' },

  txtStoreName: {
    paddingVertical: 10,
    color: 'black',
    fontWeight: 'bold'
  },
  inputStyleBg: {
    padding: 5,
    borderRadius: 4,
    fontSize: 13
  },
  inputStyle: {
    padding: 5,
    flex: 1,
    borderRadius: 4,
    fontSize: 13
  },
  imgSelected: {
    width: 15, height: 15, marginHorizontal: 10, tintColor: 'red'
  },
  imgUnSelected: {
    width: 15, height: 15, marginHorizontal: 10, tintColor: LIGHTGREY

  }
})
