import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, StatusBar, TouchableOpacity, Alert,  KeyboardAvoidingView } from 'react-native'
import { Button } from 'native-base'
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import ThemeButton from '../Components/Button'
import ThemeInput from '../Components/Input'
import { Heading } from '../Components/Heading'
import auth from '@react-native-firebase/auth';
import { StackNavigationProp } from '@react-navigation/stack';
import {userUnRegistered} from '../Actions/auth';
import { Buffer } from "buffer";

interface registerProps {
    navigation?: StackNavigationProp<any, any>
}
const UnRegisterUser = ({navigation, ...props }) => {

    const [email, setEmail] = useState('')
    const [btnDisable, setBtnDisable] = useState(false);

    const _handleUnRegisterUser = () => {
        let userId = Buffer.from(email).toString('base64');
        console.log("Email :::",email);
        props.userUnRegistered(userId).then((res:any) => {
           /* if(res.payload.data == "successfully"){
                Alert.alert("User Deleted Successfully");
                navigation?.push("Login")
            } else if (res.payload.data == "User can not delete") {
                Alert.alert("User can not be delete");
                navigation?.push("Login");
            } else {
                Alert.alert("User not exist");
                navigation?.push("Login");
            }*/
            Alert.alert("Please check your email Inbox/Spam to Unregister");
            setBtnDisable(true);
            console.log("Unregistered user resp :::",res);
        });
    }

    return (
        <SafeAreaView style={{...styles.container}} edges={['top']} >
            <Heading title={"UnRegister User"} rightText={"Close"} onRightPress={() => navigation?.pop()} />
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
                <View style={{flex:1}}>
                <View style={{ flex: 1}}></View>
                <View style={styles.miniContainer}>
                    <Text style={styles.textStyle}>{`Please enter your registered email address and \n we will unregister the user`}</Text>
                    <ThemeInput  onChangeText={(text: string) => setEmail(text)} imgPath={require('../../assets/Auth/user.png')} placeHolderText={"Email Address*"} />
                    <ThemeButton  btnDisable={btnDisable} onBtnPress={() => _handleUnRegisterUser()} btnMainStyle={{ marginVertical: 15 }} btnText={'Submit'} />
                </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = (state:any) => ({
    userData: state,
  });

const mapDispatchToProps = dispatch => ({
    userUnRegistered:(userId:any)=> dispatch(userUnRegistered(userId)),
  });

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(UnRegisterUser)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#7c8cb6',
        justifyContent: 'flex-end'

    },
    miniContainer: {
        height:250,
        padding: 18,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 16,
        color: '#212121',
    }
})