import React, { useContext, useState } from 'react'
import { View, Text, Image, StatusBar, StyleSheet, Alert, Platform, KeyboardAvoidingView } from 'react-native'
import { Button, Container, Content } from 'native-base'
import { logo, user, password, google, facebook, apple } from '../Constants/images'
import { Heading } from '../Components/Heading'
import ThemeButton from '../Components/Button'

import ThemeInput from '../Components/Input'
import { StackNavigationProp } from '@react-navigation/stack';

import { AuthContext } from '../Context/Context'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { appleAuth } from '@invertase/react-native-apple-authentication'

interface loginProps {
    navigation?: StackNavigationProp<any, any>
}


const Login = ({ navigation }: loginProps) => {

    const [loginData, setloginData] = useState('')
    const [passData, setPassData] = useState('')
    const { Login, guestLogin, facebookSignIn, googleSignIn, appleSignIn } = useContext(AuthContext)
    const onLoginCall = (skip) => {
        if (loginData && passData) {
            Login(loginData.toLocaleLowerCase().trim(), passData,navigation)
        } else if (skip) {
            guestLogin(navigation)
        }
        else {
            Alert.alert("please enter credentials")
        }
    }
   
    return (
        <Container style={styles.container}>
            <StatusBar barStyle={"default"} backgroundColor={'transparent'} />
            <Heading title={"Log in"} />
            <Content>
                <KeyboardAvoidingView style={styles.midContainer}>
                    <Image source={logo} style={styles.logo} />
                    <ThemeInput
                        imgPath={user}
                        placeHolderText={"Email Address*"}
                        onChangeText={(text) => setloginData(text)} />
                    <ThemeInput imgPath={password}
                        placeHolderText={"Password*"}
                        onChangeText={(text) => setPassData(text)}
                        secured={true} />
                    <Text onPress={() => navigation?.push('ForgotPassword')} style={styles.forgottext}>Forgot password?</Text>
                </KeyboardAvoidingView>
                <View style={styles.button}>
                    <ThemeButton
                        onBtnPress={() => onLoginCall()}
                        btnText={'Login'} />
                </View>
                <Text style={[styles.textStyle, { alignSelf: 'center', marginVertical: 20 }]}>---- or login with. ----</Text>
                <View style={[{ flex: 0.25, }, styles.socialMedia]}>
                    <TouchableOpacity
                        style={
                            { marginHorizontal: 10 }
                        }
                        onPress={() => googleSignIn(navigation).then(() => console.log('Signed in with Google!'))}>
                        <Image source={google} style={styles.socialMediaInner} />
                    </TouchableOpacity>
                    {appleAuth.isSupported && Platform.OS == 'ios' && (
                        <TouchableOpacity
                            style={
                                { marginHorizontal: 10 }
                            }
                            onPress={() => appleSignIn(navigation).then(() => console.log('Signed in with Apple!'))}>
                            <Image source={apple} style={{ ...styles.socialMediaInner, width: 60, height: 60 }} />
                        </TouchableOpacity>
                    )}
                    <TouchableOpacity
                        style={
                            { marginHorizontal: 10 }
                        }
                        onPress={() => facebookSignIn(navigation).then(() => console.log('Signed in with Facebook!'))}>
                        <Image source={facebook} style={styles.socialMediaInner} />
                    </TouchableOpacity>
                </View>
            </Content>
            <Text onPress={() => onLoginCall('skip')} style={{
                alignSelf: 'center',
                color: '#fbbc03', fontSize: 18, textDecorationLine: 'underline',
            }}>Skip</Text>
            <View style={[{ flex: 0.05}, styles.socialMedia]}>
                <Text style={{ fontSize: 20 }}>Don't have an account yet?</Text>
                <Text onPress={() => navigation?.push('Register')} style={[styles.textStyle, { color: '#1776d3', }]}> Sign Up</Text>
            </View>
            <View style={[{ flex: 0.05, marginBottom: 10 }, styles.socialMedia]}>
                <Text onPress={() => navigation?.push('UnRegisterUser')} style={[styles.textStyle, { color: '#1776d3',textAlign:'center' }]}>UnRegister</Text>
            </View>
        </Container>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        resizeMode: 'contain',
        height: 160,
        marginTop: 10,
    },
    button: {
        flex: 0.1,
        alignItems: 'center'
    },
    midContainer: {
        flex: 0.58,
        alignItems: 'center',
        justifyContent: 'center'

    },
    socialMedia: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    socialMediaInner: {
        resizeMode: 'cover',
        height: 50,
        width: 50,
        margin: 5

    },
    forgottext: {
        alignSelf: 'flex-end',
        fontSize: 16,
        marginTop: 10,
        marginEnd: 25,
        marginBottom: 10,
    },
    textStyle: {
        fontSize: 16,
        fontWeight: 'bold',
    }
})