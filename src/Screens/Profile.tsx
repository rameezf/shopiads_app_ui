import React from 'react'
import { View, Text } from 'react-native'
import { HorizontalListViewIcon } from '../Components/ListView/HorizontalListView'
import { connect } from 'react-redux';

const Profile = ({ navigation, ...props }) => {
    const arr = [
        {
            "fontName": "user",
            "title": props.getLocation.name ? props.getLocation.name : 'Login'
        }, {
            "fontName": "question",
            "title": "Faq's"
        },
        {
            "fontName": "slideshare",
            "title": "Refer to friend"
        }, {
            "fontName": "envelope",
            "title": "Contact Us"
        }, {
            "fontName": "star",
            "title": "Rate Us"
        }, {
            "fontName": "info",
            "title": "About Us"
        }, {
            "fontName": 'sign-out',
            "title": "Logout"
        },

    ]

    return (
        <HorizontalListViewIcon
            arr={arr}
            navigation={navigation}
            fontColor={"#fff"}
            fontSize={18}
            email={props.getLocation.email}
            fontSize2={18}
            fontName2={"right"}
        />


    )
}


const mapStateToProps = (state) => ({
    getLocation: state.location,
});

export default connect(mapStateToProps, null)(Profile);