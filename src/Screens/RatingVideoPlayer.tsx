import React, { useEffect, useRef, useState } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-elements';

import { connect } from 'react-redux';
import { Container } from 'native-base';
import { BackHeader } from '../Components/Heading';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ActivityIndicator } from 'react-native-paper';
import VideoPlayer from 'react-native-video-player';


const RatingVideoPlayer = ({navigation, ...props }) => {
     const [index,setIndex] = useState(0)
  
     useEffect(() => {
      
      console.log(props.videoUrl)

  }, [])
  




  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
    <BackHeader title={''}
    fontSize={28}
    onFontPress={() => navigation?.pop()}
    fontName={'chevron-thin-left'}
/>
    <View style={{flex:1,width:'100%'}}>


    <VideoPlayer
    style={{width:'100%',height:"100%"}}
    video={{ uri: props.videoUrl}}
    videoWidth={1600}
    videoHeight={900}
/>
    
</View>

  </SafeAreaView>
  );
};


const mapStateToProps = (state) => ({
  videoUrl:state.productTab.videoUrl
  });
 
  export default connect(
    mapStateToProps,
    null
  )(RatingVideoPlayer)


  // Later on in your styles..
var styles = StyleSheet.create({
  backgroundVideo: {
    width:'100%',
    flex:1,
    bottom:0,
    top:0
  },
});