import React, { useEffect, useRef, useState } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';

import { connect } from 'react-redux';
import { Container } from 'native-base';
import { BackHeader } from '../Components/Heading';
import ImageViewer from 'react-native-image-zoom-viewer';
import { SafeAreaView } from 'react-native-safe-area-context';

const Zoom = ({navigation, ...props }) => {
    const [data,setData] = useState(props.zoomImages);
    const [index,setIndex] = useState(0)
    useEffect(() => {
      for(let i =0; i< data.length; i++){
        if(data[i].index >= 0 ){
          setIndex(data[i].index);
        }
      }

  }, [data])
  
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}
    edges={['top']} 
    
    >
    <BackHeader title= {''}
    fontSize={28}
    onFontPress={() => navigation?.pop()}
    fontName={'chevron-thin-left'}
/>
   <ImageViewer imageUrls={data}
   index={index}
   saveToLocalByLongPress ={false}
   enableSwipeDown={true}/>
  </SafeAreaView>
  );
};


const mapStateToProps = (state) => ({
  zoomImages:state.productTab.zoomImages
   
  });
 
  export default connect(
    mapStateToProps,
    null
  )(Zoom)