import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { View, Text, Alert, FlatList, Image, StyleSheet, TextInput } from 'react-native'
import { Container } from "native-base";
import { BackHeader } from '../Components/Heading';
import { connect } from 'react-redux'
import Icons from '../Constants/Icons'
import {
    productCasualShirt,

} from '../Constants/images';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ThemeButton from '../Components/Button';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getAllFollowStore } from '../Actions/Offer';
import { Buffer } from "buffer"
import { getStoreDetails, removeLikeProducts } from '../Actions/StoreDetails';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { addToFollow, setStoreKey } from '../Actions/product';
import { BLUE } from '../Constants/colors';
import { deleteSnippets } from './Snip';
const favDesc = ({ navigation, ...props }) => {

    const [showLoader, setShowLoader] = React.useState(false);

  const [normalArr, setNormalArr] = useState([]);
  const [storeArr, setStoreArr] = useState([]);

  const firstUpdate = useRef(true);
  
  useLayoutEffect(() => {
        if(firstUpdate.current ){
            firstUpdate.current= false;
            if(props.getLocation.email){
            let userId = Buffer.from(props.getLocation.email).toString('base64')
            setShowLoader(true)
            console.log('USERvfdfdfd IDD', userId)
            props.getAllFollowStore(userId).then((res) => {
                console.log('DATAAAAAAAVVV',res.payload.data)
                if(res.payload.data.length != 0){
                    getStoreDetail(0,res.payload.data,[])
                    props.addToFollow(res.payload.data)
                }else{
                    props.addToFollow([])
                    setShowLoader(false)
                }
            },
            );
        }
    }
    });


    const getStoreDetail = (pos, followingArr,storeArr) =>{
        if(pos < followingArr.length){
            console.log("store id>>>>>>>>>>>>>>",followingArr[pos].storeId)
            props.getStoreDetails(followingArr[pos].storeId).then((res) => {
                if(res.payload!=undefined)
                {
                   
                let result = res.payload.data;
                console.log('ERRRRRORRR',result);
            //    props.addToFollow(result)
                storeArr.push(result);
                getStoreDetail(pos+1,followingArr,storeArr)
                }
                else
                {
                    getStoreDetail(pos+1,followingArr,storeArr)
                }
           
                });


        }else{
            setShowLoader(false)
            setNormalArr(followingArr)
            setStoreArr(storeArr);
       
        }
    }



    const removeUnfollow =(item)=>{
        setShowLoader(true);
        let email = Buffer.from(props.getLocation.email).toString('base64')
        console.log(email);
        //props.removeLikeProducts(email,item).then((res) => {
            props.deleteSnippets(email,item).then((res) => {
            console.log('DATTTAAAAA',res);
            props.getAllFollowStore(email).then((res) => {
                setNormalArr(res.payload.data)
                console.log('DATAAAA->',res.payload.data);
                if(res.payload.data.length != 0){
                    getStoreDetail(0,res.payload.data,[])
                }else{
                    setNormalArr([]);
                    setStoreArr([]);
                    props.addToFollow([])
                    setShowLoader(false)
                }
            });
    });
    }




    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:BLUE }}
    edges={['top']} 
    >
            <BackHeader title={"STORE FOLLOWING"}
                fontSize={28}
                onFontPress={() => navigation?.pop()}
                fontName={'chevron-thin-left'}
            />
            <View style={{ flex: 1,backgroundColor: '#ffffff' }}>
                <FlatList
                    contentContainerStyle={{ paddingTop: 10 }}
                    data={storeArr}
                    renderItem={({ item, index }) =>
                        <View style={{ flexDirection: 'column', width: '100%' }}>

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <View  style={{borderColor:'grey',borderWidth:1,padding:2,borderRadius:4,margin:5}}>
                        <TouchableOpacity  onPress={()=>{
                                console.log(item);
                           props.setStoreKey(normalArr[index].storeId)
                                 navigation.push('StoresDesc');
                            
                        }}>

                      <Image 
                            source={{uri:item['companyLogo']}}
                            style={{width:70,height:40,borderRadius:2,resizeMode:'stretch'}}>
                            </Image>
                            </TouchableOpacity>
                         </View> 
            
            <Text style={{ ...styles.txtStoreName, flex: 1,padding:10 }}>{item.storeName}</Text>
                                <View style={{ width: 80 }}>
                                    <ThemeButton
                                        btnMainStyle={{ height: 35, width: 70, }}
                                        onBtnPress={() => {
                                            removeUnfollow(normalArr[index]);
                                         }}
                                         btnTextStyle={{fontSize:10}}
                                        btnText={'Unfollow'} />
                                </View>
                            </View>
                            <View style={{ width: '100%', height: 1, backgroundColor: '#f4f4f4' }} />

                        </View>
                    } />
            </View>

            <LoaderAlert
        showAlert={showLoader}
        />
        </SafeAreaView>
    )
}

const mapStateToProps = state => ({
    getFavData: state.productTab.favourite,
  getLocation: state.location,

})

const mapDispatchToProps = (dispatch) => ({
    getAllFollowStore: (user) => dispatch(getAllFollowStore(user)),
  removeLikeProducts: (userId,item) => dispatch(removeLikeProducts(userId,item)),
  deleteSnippets: (userId, data) => dispatch(deleteSnippets(userId, data)),
  getStoreDetails:(userId) => dispatch(getStoreDetails(userId)),
  addToFollow:(data) => dispatch(addToFollow(data)),
  setStoreKey:(userId) => dispatch(setStoreKey(userId))
    

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(favDesc)


const styles = StyleSheet.create({


    txtStoreName: {
        paddingVertical: 10,
        color: 'black',
        fontWeight: 'bold'
    },
    inputStyleBg: {
        padding: 5,
        width: '100%',
        borderRadius: 4,
        fontSize: 13
    },
    inputStyle: {
        padding: 5,
        flex: 1,
        borderRadius: 4,
        fontSize: 13
    }
})