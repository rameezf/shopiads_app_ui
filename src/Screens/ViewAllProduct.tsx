import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import {Button, Icon, Item} from 'native-base';
import {getOffersAll, getOfferCategory,getMainCategory, getAllOffersStoresJsonList, getOfferBanner, getOfferProducts, getAllStoresBasedonCategoryStores, getAllProductsBasedOnCategories, getAllFavoriteProduct} from '../Actions/Offer';
import {
  setProductKey,
  addToFavourite,
  deleteToFavourite,
  setStoreKey,
  setOfferId,
} from '../Actions/product';
import {connect} from 'react-redux';
import MyChip from '../Components/chips';
import ViewPager from '@react-native-community/viewpager';
import {Avatar} from 'react-native-paper';
import {
  all,
  noRecord,
  progresLoading,
DOWNARRAY
} from '../Constants/images';
const { width } = Dimensions.get('window');

import { style as storeStyle } from './Store.style';
import { Overlay, Rating } from 'react-native-elements';
import { Buffer } from "buffer"

import Icons from '../Constants/Icons';
import { BLACK, BLUE, WHITE } from '../Constants/colors';
import { Product } from '../Components/Product';
import { SafeAreaView } from 'react-native-safe-area-context';
import { BackHeader } from '../Components/Heading';
import { GridProduct } from '../Components/GridProduct';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { addLikeProducts, removeLikeProducts } from '../Actions/StoreDetails';
import { Store } from '../Components/Store';
import { BannerStore } from '../Components/BannerStore';
import FlatListSlider from '../Components/FlatListSlider';
import { AuthContext } from '../Context/Context';

const ViewAllProduct = ({navigation, ...props}) => {

  const [product,setProduct] =useState ([])
  const [store,setStore] =useState ([])

  const [type,setType] =useState(false)
  const [showLoader, setShowLoader] = React.useState(false);
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);

  const { Logout } = useContext(AuthContext)

  useEffect(() => {
    setProduct(props.viewAllProduct.product);
    setStore(props.viewAllProduct.offer);
    setType(props.viewAllProduct.productType);
  }, [props.viewAllProduct])

  const renderProductItem = (item: any, idx: number) => {
        return (
            <GridProduct {...props} item={item} onItemPress={()=>{
            props.setProductKey(item.id)
            navigation.navigate('ProductDesc',{ itemType : item })
          }}
          foundInFav={foundInFav(item)}
          onAddToFavourite={()=>{
            onAddToFavourite(item, "product")
          }}
             />
        );
    };


    const onAddToFavourite = (item:any,type) => {
      if (props.getLocation.email) {
        if(!item['requestType']   ){
      
        if (props.getFavData.length == 0) {
          item.totalLikes=item.totalLikes+1
          addLikeStoreProductApi(item,type)
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id ||  el.offerId === item.id );
          if (found) {
            item.totalLikes=item.totalLikes-1
            const data = props.getFavData.filter(data => data.productId === item.id  || data.offerId === item.id )
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            removeLikeStoreProductApi(data[0])
          } else {
            item.totalLikes=item.totalLikes+1
            addLikeStoreProductApi(item,type)
          }
        }
     }
     } else {
        setShowLoginAlert(true);
      }
    };
  

  const addLikeStoreProductApi = (item:string,type:string) => {
    item['requestType']= true;
  setShowLoader(true);
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.addLikeProducts(item.id, email,type).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        item['requestType']= false;
        props.deleteToFavourite(res.payload.data);
  setShowLoader(false);
    
      });
    });
  }

  const removeLikeStoreProductApi = (item:{}) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.removeLikeProducts(email, item).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });
     });
  }
    const foundInFav = (item:{}) => {
      let FavItem = item;
      const found = props.getFavData.some((el) => el.productId === FavItem.id  || el.offerId === item.id );
      if (found) {
        return true;
      } else {
        return false;
      }
    };


  const renderStoreItem = (item: {}, idx: number) => {
    return (
      <Store {...props} item={item} onItemPress={() => {
        props.setStoreKey(item.storeId)
        props.setOfferId(item.id)
        navigation.push('OfferDesc');
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item,"offer")
        }}
      />
    );
  };

  const renderBannerItem = (item: {}, idx: number) => {
    return ( 
      <BannerStore {...props} key={item.id} item={item} onItemPress={() => {
        props.setStoreKey(item.storeId)
        props.setOfferId(item.id)
        navigation.push('OfferDesc');
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item,"offer")
        }}
      />
    );
  };

  
  return (
        <SafeAreaView style={{flex: 1,}} edges={['top']} >
        <BackHeader title={''}
            fontSize={28}
            onFontPress={() => navigation?.pop()}
            fontName={'chevron-thin-left'}
        />
      <ScrollView style={{flex: 1}}>
        <View style={{width:'100%',height:'100%'}}> 
        {store.length !=0 && (
          <FlatListSlider
          data={store}
          width={width-80}
          onItemPress = {(item)=>{
              props.setStoreKey(item.storeId)
              props.setOfferId(item.id)
              navigation.push('OfferDesc');
          }}
          onAddToFavourite = {(item) =>{
            onAddToFavourite(item,"offer")
          }}
          foundInFav ={(item)=>foundInFav(item)}
          contentStyle={{ paddingHorizontal: 16,width:'100%'}}
          indicatorActiveWidth={40}
          />
        )}
        {type && (
          <FlatList
          data={product}
          renderItem={({item, index}) =>   renderProductItem(item, index) }
          numColumns={2}
          />
        )}
        {!type && (
        <FlatList
        data={product}
        renderItem={({item, index}) =>   renderBannerItem(item, index) }
        numColumns={1}
        />
       )}
         </View>
      </ScrollView>
      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          Logout(navigation);
          setShowLoginAlert(false);
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />
      <LoaderAlert
        showAlert={showLoader}
      />
      </SafeAreaView>
    );
  };

const mapStateToProps = (state) => ({
    getLocation: state.location,
    getFavData: state.productTab.favourite,
    store: state.productTab.store,
    categoryId: state.productTab.subCategoryId,
    viewAllProduct:state.productTab.viewAllProduct
  });
  const mapDispatchToProps = (dispatch) => ({
    addToFavourite: (data:any) => dispatch(addToFavourite(data)),
    deleteToFavourite: (data:any) => dispatch(deleteToFavourite(data)),
   setProductKey:(productId:string) => dispatch(setProductKey(productId)),
   getAllProductsBasedOnCategories:(cityId:string,categoryId:string,marketPlace:string) => dispatch(getAllProductsBasedOnCategories(cityId,categoryId,marketPlace)),
   setStoreKey: (storeId:string) => dispatch(setStoreKey(storeId)),
   setOfferId: (offerId:string) => dispatch(setOfferId(offerId)),
  removeLikeProducts: (userId:string, item:any) => dispatch(removeLikeProducts(userId, item)),
  addLikeProducts: (data:any , userId:string,type:string) => dispatch(addLikeProducts(data, userId,type)),
  getAllFavoriteProduct: (userId:string ) => dispatch(getAllFavoriteProduct(userId)),
 
  
  
  });

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewAllProduct)