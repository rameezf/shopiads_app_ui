import { Dimensions, StyleSheet } from "react-native";
import { BLUE, DISABLED_COLOR, GREY } from "../Constants/colors";


export const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    txtStoreName: {
      paddingVertical: 10,
      color: 'black',
      fontWeight: 'bold'
    },
    txtReview: {
      paddingVertical: 10,
      color: 'black',
    },
    txtDesc: {
      color: 'black',
    },
  
    btnWhiteBorder: {
      borderColor: 'white',
      borderWidth: 1,
      marginLeft: 5,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'center',
      borderRadius: 3,
      width: Dimensions.get('window').width / 2 - 25,
      height: 35,
    },

    btnDisableWhite: {
        borderColor: DISABLED_COLOR,
        backgroundColor:GREY,
        borderWidth: 1,
        marginLeft: 5,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 3,
        width: Dimensions.get('window').width / 2 - 25,
        height: 35,
      },
    txtProductName: {
      paddingHorizontal: 10,
      fontSize: 14,
      color: 'black',
      fontWeight: 'bold'
    },
    txtPrice: {
      fontSize: 15,
      color: '#1776d3',
      fontWeight: 'bold'
    },
    txtActualPrice: {
      fontSize: 12,
      marginTop: 2,
      marginStart: 5,
      color: '#545454', textDecorationLine: 'line-through', textDecorationStyle: 'solid',
      fontWeight: 'bold'
    },
    btnBlueBorder: {
      borderColor: '#1776d3',
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 3,
      width: 130,
      height: 35,
    }, btnSolid: {
      backgroundColor: '#1776d3',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 3,
      width: 100,
      height: 35,
  
    }
    , txtStoreDetailsTitle: {
      color: 'grey',
      flex: 1.5,
      fontSize: 12
    },
    txtStoreDetailsDesc: {
      color: 'black',
      fontWeight: 'bold',
      flex: 2,
      fontSize: 12
    },
    btnOrange: {
      flexDirection: 'row',
  
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      backgroundColor: '#C96349',
      flex: 1
    },
    btnGrey: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        backgroundColor: GREY,
        flex: 1
      },
    btnBuyNow: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      backgroundColor: '#1776d3',
      flex: 2
    },
  
  
    btnSubmit: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      borderRadius: 5,
      marginTop: 5,
      backgroundColor: '#1776d3',
  
    },
    edtStyle: {
      marginTop: 10,
      width: Dimensions.get('window').width / 1.2,
      backgroundColor: 'white',
      borderColor: '#F2F2F2',
      borderWidth: 0.5,
      borderRadius: 5,
      padding: 10,
      height: 70,
      fontSize: 12,
    },
  
    containerRatingStyle: {
      marginTop: 10,
      width: Dimensions.get('window').width / 1.2,
      height: 35,
      borderRadius: 5,
      flexDirection: 'row',
      borderColor: '#F2F2F2',
      borderWidth: 0.5,
    },

    containerButton: {
      marginTop: 10,
      width: Dimensions.get('window').width / 1.2,
      height: 35,
      borderRadius: 5,
      flexDirection: 'row',
      backgroundColor: BLUE,
      borderWidth: 0.5,
    },

    txtBillStyle: {
      flex: 1,
      height: 34,
      padding: 10,
      fontSize: 12,
    },
    txtBrowseStyle: {
      width: 100,
      height: 34,
      textAlign: 'center',
      backgroundColor: '#F2F2F2'
    },
    qtyBlueBorder: {
      borderColor: '#1776d3',
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 3,
      width: 30,
      height: 30,
    }
  
  })
