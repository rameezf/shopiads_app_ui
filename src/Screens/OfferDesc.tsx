import React, { useEffect, useState, useRef, useCallback, useContext } from 'react';
import { View, Text, Alert, FlatList, Image, StyleSheet, Dimensions, Linking, TextInput, Share, TouchableOpacity, Platform, Modal } from 'react-native';
import { Container, Icon } from 'native-base';
import { BackHeader } from '../Components/Heading';
// import Modal from 'react-native-modalbox';
import { connect } from 'react-redux'
import ProductDesc from './ProductDesc';
import {
  BUYNOW,
  SNIP,
  PLAY_ICON,
  BACK,
  user,
  SHARE,
  plus,
  logo,
  STORE_ICON,
  DISCOUNT_ICON
} from '../Constants/images';
import { ScrollView, } from 'react-native-gesture-handler';
import Icons from '../Constants/Icons';

import { style as productStyle } from './Product.style'
import ThemeButton from '../Components/Button';
import ViewPager from '@react-native-community/viewpager';

import { Overlay, Rating } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import { addFollowingApi, addLikeProducts, addUserSnips, getAllTabs, getCollectionsByStoreTabProducts, getHomeTabProducts, getOffersProducts, getOffersStoreId, getProductsTabsProducts, getStoreDetails, makeUnlikeProducts, uploadBillDetails, uploadBillImage, uploadRatingDetails, uploadRatingStoreImage } from '../Actions/StoreDetails';
import { addToFavourite, addToFollow, addToSnips, addToVideoUrl, addToZoomImages, deleteToFavourite, setProductKey, setStoreKey } from '../Actions/product';
import { Product } from '../Components/Product';
import { BLACK, BLUE, DISABLED_COLOR, GREY, WHITE, LIGHTGREY } from '../Constants/colors';
import UUIDGenerator from 'react-native-uuid-generator';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { checkInOffers, getAllFavoriteProduct, getAllFollowStore } from '../Actions/Offer';

import { Buffer } from "buffer"
import { getUserSnippets } from './Snip';
import { getRatingByOfferId, offerBuyNowClick } from '../Actions/Details';
import VideoRecorder from 'react-native-beautiful-video-recorder'
import ImagePicker from './ImagePicker';



const homeCategory = {
  heading: 'Home',
  id: ''
}

const productCategory = {
  heading: 'Products',
  id: ''

}
import Moment from 'moment';
import { AuthContext } from '../Context/Context';
import { ThemeImage } from '../Components/ThemeImage';
import RNFetchBlob from 'rn-fetch-blob';

const OfferDesc = ({ navigation, route, ...props }) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  const [chipItem, setChipItem] = useState({});
  const [dataChange, setDataChange] = useState(false);
  const [storeDetails, setStoreDetails] = useState({}); //to show the "Read more & Less Line"
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);
  const [showLoader, setShowLoader] = React.useState(false);
  const [offerDetails, setOfferDetails] = useState({}); //to show the "Read more & Less Line"
  const [homeProducts, setHomeProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [categoryType, setCategoryTypes] = useState([]);
  const [showAlert, setShowAlert] = React.useState(false);
  const [showFile, setShowFiles] = React.useState(0);
  const [billPath, setBillPath] = React.useState("");
  const [billImagePath, setBillImagePath] = React.useState("");
  const [billImagePath2, setBillImagePath2] = React.useState("");
  const [billImagePath3, setBillImagePath3] = React.useState("");
  const [billImagePath4, setBillImagePath4] = React.useState("");
  const [comment, setComment] = React.useState("");
  const [ratingValue, setRatingValue] = useState(0);
  const videoRecorder = useRef(null)



  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"

  const toggleNumberOfLines = () => { //To toggle the show text or hide it
    setTextShown(!textShown);
  }

  const { Logout } = useContext(AuthContext)

  const onTextLayout = useCallback(e => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);


  const onChangeChipItem = (item) => {
    setChipItem(item);
  };

  const [rating, showRating] = React.useState(false);


  const [ratingInfo, setRatingInfo] = useState([]);

  useEffect(() => {
    if (Object.keys(storeDetails).length == 0) {
      props.getStoreDetails(props.storeId).then((res) => {
        let result = res.payload.data;
        console.log('DFKDDKfffDDK', result);

        setStoreDetails(result)
        props.getOffersProducts(props.offerId).then((res) => {
          let result = res.payload.data;
          setProducts(result)
        })
        props.getOffersStoreId(props.offerId).then((res) => {
          let result = res.payload.data;
          let storeList = props.store
          result['address'] = ""
          for (let j = 0; j < storeList.length; j++) {
            if (storeList[j].id == result.storeId) {
              result.storeName = storeList[j].storeName;
              result['address'] = storeList[j]['address2'] + ', Unit: ' + storeList[j]['address1'] + ', ' + storeList[j]['cityId'] + ' ' + storeList[j]['pincode'];
              break
            }
          }
          console.log('DATAAASSSS', result);
          setOfferDetails(result)
          getRatingInfo()
        })

      });
    }

  })

  const onAddToFavourite = (item, index, type = "product") => {
    if (props.getLocation.email) {
      if (!item['requestType']) {
        if (props.getFavData.length == 0) {
          item.totalLikes = item.totalLikes + 1
          addLikeStoreProductApi(item, type)
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
          if (found) {
            item.totalLikes = item.totalLikes - 1
            const data = props.getFavData.filter(data => data.productId === item.id || data.offerId === item.id)
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0], type)
          } else {
            item.totalLikes = item.totalLikes + 1
            addLikeStoreProductApi(item, type)
          }
        }
      }
    } else {
      setShowLoginAlert(true);
    }
  };

  const addLikeStoreProductApi = (item, type) => {
    item['requestType'] = true;
    setShowLoader(true)

    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.addLikeProducts(item.id, email, type).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        item['requestType'] = false;
        props.deleteToFavourite(res.payload.data);
        setShowLoader(false)
      });
    });
  }

  const removeLikeStoreProductApi = (item, type) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    console.log("ç", item);
    props.makeUnlikeProducts(email, type == "product" ? item.productId : item.offerId, type, item.id).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
        setShowLoader(false);
      });
    });
  }


  const foundInFav = (item) => {
    let FavItem = item;
    const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
    console.log('FFFFFF', props.getFavData)
    if (found) {
      return true;
    } else {
      return false;
    }
  };




  const getRatingInfo = () => {
    props.getRatingByOfferId(props.offerId).then((res) => {
      let result = res.payload.data;
      setRatingInfo(result)
    });
  }



  const checkInOffers = () => {

    if (props.getLocation.email) {

      let email = Buffer.from(props.getLocation.email).toString('base64')

      props.checkInOffers(email).then((res) => {
        let result = res.payload.data;
        const found = result.some((el: any) => props.offerId === el.id);
        //  console.log('fkffkfvvmvmvmdfkdkdkd',found);

        if (found) {
          Alert.alert('You already rated a offer.');

        } else {
          if (props.getLocation.email) {
            showRating(true);
          } else {
            setShowLoginAlert(true);
          }

        }


      });
    } else {
      setShowLoginAlert(true);
    }

  }




  const renderProductItem = (item: any, idx: number) => {
    return (
      <Product {...props} item={item} onItemPress={() => {
        props.setProductKey(item.id)
        navigation.navigate('ProductDesc', { itemType: item })
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item, idx)
        }}
      />

    );

  };




  const addSnips = () => {
    if (props.getLocation.email) {
      setShowLoader(true);
      UUIDGenerator.getRandomUUID((uuid) => {
        console.log(uuid);
        let data = {
          'id': uuid,
          'userId': props.getLocation.email,
          'offer': offerDetails,
          'offerId': props.offerId,
          'storeId': offerDetails.storeId,
          'storeName': offerDetails.webStoreName,
          'storeCode': storeDetails.code,
          'type': 'Offer',
          'activityType': 'snip'
        }
        console.log('DAHASHSSH', data);
        let email = Buffer.from(props.getLocation.email).toString('base64')
        props.addUserSnips(email, data).then((res) => {
          props.getUserSnippets(email).then((res) => {
            let result = res.payload.data;
            props.addToSnips(result);
            setShowLoader(false);
          });
          //  Alert.alert('Snips added successfully.');
        });
      });
    } else {
      setShowLoginAlert(true);
    }
  }





  const renderRatingItem = (item: any) => {
    console.log('ITEEMEMEMEMEEM', item);
    return (<View style={{ width: '100%', flexDirection: 'column' }}>
      <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
        <Image
          source={user}
          style={{ width: 50, height: 50, resizeMode: 'stretch' }}>
        </Image>
        <View style={{ marginTop: 5, paddingHorizontal: 10, flex: 1, flexDirection: 'column' }}>
          <Text style={{ ...styles.txtDesc, color: BLACK, fontWeight: 'bold', marginTop: 5, paddingHorizontal: 5 }}>{item['userName']}</Text>
          <Text style={{ ...styles.txtDesc, fontWeight: 'bold', marginTop: 5, paddingHorizontal: 5 }}>{'Reviewed on ' + Moment(item['createdOn']).format('DD MMM YYYY')}</Text>

          <View style={{ width: 60, paddingHorizontal: 5, marginTop: 5 }}>
            <Rating
              type='star'
              fractions={2}
              readonly={true}
              ratingCount={5}
              showRating={false}
              startingValue={item['rating']}
              imageSize={12}
            />
          </View>
        </View>

        <View style={{ marginTop: 5, paddingHorizontal: 10 }}>
          <Text style={{ ...styles.txtDesc, flex: 1, marginTop: 5, paddingHorizontal: 10 }}>{''}</Text>
        </View>
      </View>


      <Text style={{ ...styles.txtDesc, color: BLUE, fontWeight: 'bold', marginTop: 10 }}>{'Verified Purchase'}</Text>

      <Text style={{ ...styles.txtReview, width: '100%', paddingVertical: 10 }}>{item.comment}</Text>
      {item.ratingMedia && (<ScrollView horizontal style={{ width: '100%', height: 100 }}><View style={{ flexDirection: 'row', width: '100%' }}>
        {
          item.ratingMedia.map((data) => {
            return renderRatingImage(data, item.ratingMedia);
          })}
      </View></ScrollView>)}
      <View style={{ backgroundColor: '#F2F2F2', width: '100%', height: 1 }} />
    </View>)
  }

  const renderRatingImage = (item: any, ratingMedia: any) => {
    return (<TouchableOpacity
      style={{ marginRight: 5 }}
      onPress={() => {
        if (!checkImageisVideoFile(item)) {
          let data: any = [];
          for (let i = 0; i < ratingMedia.length; i++) {
            let obj = { freeHeight: false, url: ratingMedia[i], index: item === ratingMedia[i] ? i : -1 }
            data.push(obj)
          }
          props.addToZoomImages(data)
          //  console.log('DATAIMAMASMMddscfdsdSS', item);
          navigation.push('Zoom')


        }
      }}
    ><ThemeImage
        source={{ uri: item }}
        style={{ resizeMode: 'stretch', height: 100, width: 100, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5, backgroundColor: 'black', borderRadius: 5 }}
      />
      {checkImageisVideoFile(item) && (
        <TouchableOpacity
          onPress={() => {
            props.addToVideoUrl(item);
            navigation.push("RatingVideoPlayer")
          }}
          style={{ position: 'absolute', alignSelf: 'center', bottom: 0, top: 30, height: 40, width: 40, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5 }}
        >
          <Image
            source={PLAY_ICON}
            style={{ resizeMode: 'stretch', position: 'absolute', alignSelf: 'center', height: 40, width: 40, tintColor: 'white' }}
          />
        </TouchableOpacity>
      )}
    </TouchableOpacity>);
  };



  const checkImageisVideoFile = (image: string) => {

    var types = ['JPG', 'jpg', 'jpeg', 'tiff', 'png', 'gif', 'bmp'];

    //split the url into parts that has dots before them
    var parts = image.split('.');
    //get the last part 
    var extension = parts[parts.length - 1];

    //check if the extension matches list 
    if (types.indexOf(extension) != -1) {
      return false;
    } else {
      console.log(extension);

      return true
    }

  }

  const checkIsSnip = () => {

    console.log('SNIPSDATAAAA', props.snips);
    const found = props.snips.some((el: any) => props.offerId === el.offerId);
    console.log('SNIPSDATAAAA', found);

    return found
  }


  const saveRatingApi = () => {
    if (props.getLocation.email) {
      setShowLoader(true);
      UUIDGenerator.getRandomUUID((uuid) => {
        let postData = new FormData();
        postData.append('file', {
          name: 'newMyLooks.jpg',
          type: 'image/jpg',
          uri: billPath
        });
        props.uploadBillImage(offerDetails['storeName'], postData, uuid).then((res) => {
          let param = { "id": uuid, image: ["https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response], "storeId": offerDetails['storeId'], "storeName": offerDetails['storeName'], companyId: offerDetails['companyId'], status: "N", "userId": props.getLocation.email, "userName": props.name }
          props.uploadBillDetails(param, uuid).then((res) => {
            setBillPath("")
            saveRatingImage(res.payload.data, [], 0);
            setShowLoader(false);
          });
        });
      });
    } else {
      setShowLoginAlert(true);
    }
  }

  const saveRatingImage = (data, ratingArray, request) => {
    UUIDGenerator.getRandomUUID((uuid) => {
      console.log(uuid);
      if (request == 0) {
        if (billImagePath.length != 0) {
          let extension = billImagePath.split('.').pop();
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath
          });
          console.log(billImagePath)
          props.uploadRatingStoreImage(offerDetails['storeName'], postData, uuid).then((res) => {
            setBillImagePath("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 1);
          });
        } else {
          saveRatingImage(data, ratingArray, 1);
        }
      } else if (request == 1) {
        if (billImagePath2.length != 0) {
          let extension = billImagePath2.split('.').pop();

          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath2
          });
          props.uploadRatingStoreImage(offerDetails['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath2("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 2)
          });
        } else {
          saveRatingImage(data, ratingArray, 2)
        }
      } else if (request == 2) {
        if (billImagePath3.length != 0) {
          let extension = billImagePath3.split('.').pop();

          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath3
          });
          props.uploadRatingStoreImage(offerDetails['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath3("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 3)

          });
        } else {
          saveRatingImage(data, ratingArray, 3)
        }

      } else if (request == 3) {
        if (billImagePath4.length != 0) {
          let extension = billImagePath4.split('.').pop();
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath4
          });
          props.uploadRatingStoreImage(offerDetails['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath4("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 4)
          });
        } else {
          saveRatingImage(data, ratingArray, 4)

        }
      } else {
        let param = { "id": uuid, "billId": data.id, "rating": ratingValue, "comment": comment, "offerId": props.offerId, "productId": "", "storeId": offerDetails['storeId'], "companyId": offerDetails['companyId'], "status": "N", "ratingMedia": ratingArray, "ratingType": "Offer", "userId": props.getLocation.email, "userName": props.name }
        let email = Buffer.from(props.getLocation.email).toString('base64')
        console.log('DATAAAAVVV->', param);
        props.uploadRatingDetails(param, email).then((res) => {
          console.log("RESSSS", res)
          setShowLoader(false);
          Alert.alert('Your rating has been submitted, after review by admin it will reflect.');
        });
      }
    });
  }




  const addFollowingApi = () => {
    const found = props.getFavStore.some((el) => offerDetails['storeId'] === el.storeId);
    if (!found) {
      if (props.getLocation.email) {
        setShowLoader(true);
        let email = Buffer.from(props.getLocation.email).toString('base64')
        props.addFollowingApi(offerDetails['storeId'], email).then((res) => {
          props.getAllFollowStore(email).then((res) => {
            props.addToFollow(res.payload.data);
            setShowLoader(false);
          });
        });
      } else {
        setShowLoginAlert(true)
      }
    }


  }


  const shareOptions = (imageUrl: string) => {
    let data =
    {
      title: offerDetails['heading'],
      message: props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'] == "US" ? "https://share.shopiads.com/Shopiads/shareMobileOffer/" + props.offerId : "https://www.shopiads.ca/Shopiads/shareMobileOffer/" + props.offerId, // Note that according to the documentation at least one of "message" or "url" fields is required
      subject: offerDetails['heading']
    }
    return data
  };

  const shareImageUrl = () => {

    let imageUrl: string = offerDetails['fullImage']

    if (imageUrl) {
      console.log(imageUrl);
      RNFetchBlob.fetch('GET', imageUrl)
        .then((res) => {
          let status = res.info().status;

          if (status == 200) {
            // the conversion is done in native code
            let base64Str = res.base64()
            // the following conversions are done in js, it's SYNC


            Share.share(shareOptions(base64Str))
            console.log(base64Str);
          } else {
            // handle other status codes
          }
        })


    } else {
      Share.share(shareOptions(""))

    }


  }



  const checkIsUnfollow = () => {
    if (props.getFavStore) {
      const found = props.getFavStore.some((el) => offerDetails['storeId'] === el.storeId);
      return !found
    } else {
      return true
    }
  }


  const showDiscount = (storeId: any) => {
    console.log("Store Id ::::", storeId);
    props.getStoreDetails(storeId).then((res: any) => {
      console.log('Store Details Response ::::: ', res);
      let result = res.payload.data;
      console.log('Store Details in product desc ::::: ', result);
      setStoreDetails(result)
      setModalVisible(true);
    });
  }


  console.log("offerDetails::::", offerDetails)
  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }}
      edges={['top']}
    >

      <View style={{ flex: 1, backgroundColor: 'white' }}>

        {Object.keys(offerDetails).length != 0 && (
          <ScrollView style={{ width: '100%', flex: 1 }}>
            <View style={{ width: '100%', height: '100%', flexDirection: 'column' }}>
              <View style={{
                flexDirection: 'column'
              }}>
                <ThemeImage
                  source={{ uri: offerDetails['fullImage'] }}
                  style={{ resizeMode: 'stretch', height: 270, width: "100%", paddingHorizontal: 5 }}
                />
                <View style={{ flexDirection: 'row', width: '100%', position: 'absolute', padding: 10 }}>
                  <TouchableOpacity onPress={() => {
                    navigation?.pop();
                  }} style={{ borderColor: WHITE, borderWidth: 1, padding: 5 }}>
                    <Image
                      source={BACK}
                      style={{ resizeMode: 'stretch', height: 20, width: 20, }}
                    />
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-end' }}>
                    <TouchableOpacity
                      onPress={() => {
                        shareImageUrl()
                      }}
                    >
                      <Image
                        source={SHARE}
                        style={{ resizeMode: 'stretch', height: 20, width: 20, tintColor: BLUE }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{ margin: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ ...styles.txtProductName, paddingHorizontal: 0 }}>{offerDetails['heading']}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => {
                      onAddToFavourite(offerDetails, 0, "offer")
                    }}>
                      <Icons.AntDesign
                        name={foundInFav(offerDetails) ? 'heart' : 'hearto'}
                        size={20}
                        color={'#1776d3'}
                      />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 15, alignSelf: 'center', color: '#000', marginLeft: 10 }}>
                      Likes ({offerDetails['totalLikes']})
                </Text>
                  </View>
                </View>
              </View>


              {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10 }}>
                <Rating
                  type='star'
                  fractions={2}
                  readonly={true}
                  ratingCount={5}
                  showRating={false}
                  startingValue={offerDetails['avgRating']}
                  imageSize={20}
                />
                <Text style={{ fontSize: 15, alignSelf: 'center', color: '#000', paddingHorizontal: 5, }}>
                  {offerDetails['avgRating']}
                </Text>
                <TouchableOpacity onPress={() => {
                  onAddToFavourite(offerDetails, 0, "offer")
                }}>
                  <Icons.AntDesign
                    name={foundInFav(offerDetails) ? 'heart' : 'hearto'}
                    size={20}
                    color={'#1776d3'}
                  />
                </TouchableOpacity>
                <Text style={{ fontSize: 15, alignSelf: 'center', color: '#000', paddingHorizontal: 5, }}>
                  {offerDetails['totalLikes']}
                </Text>
                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                  <TouchableOpacity style={styles.btnBlueBorder} onPress={() => {

                 checkInOffers();
                  
                  }}>
                    <Text style={styles.txtProductName}>{'Rate Offer'}</Text>
                  </TouchableOpacity>
                </View>
              </View> */}
              <View>

              </View>
              <View style={{ width: '100%', marginTop: 10, height: 7, backgroundColor: '#f4f4f4' }} />

              <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }} >
                <Text style={{ ...styles.txtStoreName, flex: 1 }}>{'Description'}</Text>
              </View>

              <Text
                onTextLayout={onTextLayout}
                numberOfLines={textShown ? undefined : 4}
                style={{ ...styles.txtDesc, lineHeight: 21, padding: 10, color: 'black', marginTop: 10 }}>{offerDetails['description']}</Text>

              {
                lengthMore ? <Text
                  onPress={toggleNumberOfLines}
                  style={{ ...styles.txtDesc, lineHeight: 21, marginTop: 10, width: '100%', textAlign: 'center' }}>{textShown ? 'Read less...' : 'Read more...'}</Text>
                  : null
              }

              <View style={{ width: '100%', height: 7, marginTop: 10, backgroundColor: '#f4f4f4' }} />


              <Text style={{ ...styles.txtProductName, marginTop: 10 }}>{'Store'}</Text>
              <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#607d8b', padding: 8, borderRadius: 5, margin: 10 }} >
                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                  <View style={{ borderColor: 'grey', borderWidth: 1, padding: 2, borderRadius: 4 }}>
                    <Image
                      source={{ uri: offerDetails['logo'] }}
                      style={{ width: 70, height: 40, borderRadius: 2, resizeMode: 'stretch' }}>
                    </Image>
                  </View>
                  <View style={{ flexDirection: 'column', flex: 1 }}>
                    <Text style={{
                      ...styles.txtDesc, flex: 1, fontSize: 13, paddingHorizontal: 10,
                      color: '#CFCFCF',
                    }}>{offerDetails['storeName'] + (storeDetails && storeDetails['code'] == "Primary" ? "" : " Branch: " + storeDetails['code'])}</Text>
                    <Text style={{
                      ...styles.txtDesc, flex: 1, fontSize: 13, paddingHorizontal: 10,
                      color: '#CFCFCF',
                    }} >{offerDetails['address']}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10, }}>

                  <TouchableOpacity style={checkIsUnfollow() ? styles.btnWhiteBorder : styles.btnDisableWhite}
                    disabled={!checkIsUnfollow()}
                    onPress={() => {
                      if (props.getLocation.email) {
                        addFollowingApi()
                      } else {
                        setShowLoginAlert(true);
                      }
                    }}>
                    <Image source={plus} style={{ width: 20, height: 20, tintColor: checkIsUnfollow() ? WHITE : LIGHTGREY, marginRight: 10 }} ></Image>
                    <Text style={{ ...styles.txtStoreName, color: checkIsUnfollow() ? 'white' : LIGHTGREY }}>{checkIsUnfollow() ? 'Follow' : 'Following'}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{ ...styles.btnWhiteBorder }} onPress={() => {
                    props.setStoreKey(offerDetails['storeId']);
                    navigation.push('StoresDesc');
                  }}>
                    <Image source={STORE_ICON} style={{ width: 20, height: 20, tintColor: WHITE, marginRight: 10 }} ></Image>
                    <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'View Store'}</Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity style={{ ...styles.btnWhiteBorder, width: '98%', marginTop: 10 }} onPress={() => { showDiscount(offerDetails['storeId']) }}>
                  <Image source={DISCOUNT_ICON} style={{ width: 20, height: 20, tintColor: '#ffffff', marginRight: 10 }} ></Image>
                  <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'Additional Discounts'}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%', marginTop: 10, height: 7, backgroundColor: '#f4f4f4' }} />
              <Text style={{ ...styles.txtProductName, marginTop: 10, width: '100%' }}>{'Related Products'}</Text>
              <FlatList
                style={{ width: '100%', paddingHorizontal: 10, marginTop: 10 }}
                data={products}
                horizontal={true}
                renderItem={({ item, index }) => renderProductItem(item, index)}
              />
              <View style={{ width: '100%', height: 7, marginTop: 10, backgroundColor: '#f4f4f4' }} />
              {/* <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }} >
                <Text style={{ ...styles.txtStoreName, flex: 1, color: BLACK }}>{'Ratings'}</Text>
              </View>
              <View style={{ width: '100%', alignItems: 'center' }}>
                <Text style={{ ...styles.txtProductName,  }}>{'Overall Rating'}</Text>
                <Text style={{ ...styles.txtStoreName,  fontSize: 25 }}>{offerDetails['avgRating']}</Text>
                <Rating
                  type='star'
                  fractions={2}
                  readonly={true}
                  ratingCount={5}
                  showRating={false}
                  startingValue={offerDetails.avgRating}
                  imageSize={40}    />
                <Text style={{ ...styles.txtProductName, marginTop: 5 }}>{'Total ' + ratingInfo.length+ ' Reviews'}</Text>

                <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }} >
                </View>
                <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }} >
                  {ratingInfo.map((item) => {
                    return renderRatingItem(item);
                  })
                  }

                </View>
              </View> */}



            </View>
          </ScrollView>)}
      </View>


      <View style={{ height: 50, flexDirection: 'row' }} >
        <TouchableOpacity
          style={([checkIsSnip() ? styles.btnGrey : styles.btnOrange,
          { width: offerDetails['isShopifyCollection'] !== "Y" ? Dimensions.get('window').width / 2.5 : Dimensions.get('window').width }])} onPress={() => {
            if (Object.keys(offerDetails).length != 0) {
              if (!checkIsSnip()) {
                addSnips();
              }
            }

          }} disabled={checkIsSnip()}>
          <Image style={{ tintColor: '#ffffff', width: 30, height: 30 }} source={SNIP} ></Image>

          <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{(checkIsSnip() ? 'Snipped' : 'Snip')}</Text>
        </TouchableOpacity>

        {offerDetails['isShopifyCollection'] !== "Y" && <TouchableOpacity style={styles.btnBuyNow} onPress={() => {
          if (props.getLocation.email) {
            let email = Buffer.from(props.getLocation.email).toString('base64')
            props.offerBuyNowClick(email, props.offerId).then((res) => {
              console.log(res);
            });
            if (Object.keys(offerDetails).length != 0 && offerDetails['vendorUrl']) {
              Linking.openURL(offerDetails['vendorUrl'] + "?utm_source=shopiads").catch(err => console.error("Couldn't load page", err));
            } else {
              Alert.alert("URL not found")
            }
          } else {
            setShowLoginAlert(true);
          }
        }}>
          <Image style={{ tintColor: '#ffffff', width: 30, height: 30 }} source={BUYNOW} ></Image>
          <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Shop Now'}</Text>
        </TouchableOpacity>}
      </View>


      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          setShowLoginAlert(false);
          Logout(navigation);
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />

      <LoaderAlert
        showAlert={showLoader}
      />


      <Overlay isVisible={rating} onBackdropPress={() => {
        showRating(false);
      }} >
        <Rating
          type='star'
          fractions={0}
          readonly={false}
          startingValue={ratingValue}
          onFinishRating={setRatingValue}
          ratingCount={5}
          imageSize={40}
        />
        <TextInput
          value={comment}
          onChangeText={setComment}
          style={styles.edtStyle}
          placeholder='Enter comment'
        />

        <Text>{'Bill Image'}</Text>
        <TouchableOpacity
          onPress={() => {
            let options = {
              quality: 0.5,

              mediaType: 'photo',
            };
            ImagePicker.showImagePicker(options, response => {
              if (response.error) {
              } else if (response.uri) {
                setBillPath(response.uri)
              }
            });
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billPath}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>
        <Text>{'Upload Image (Optional)'}</Text>
        <TouchableOpacity
          onPress={() => {

            setShowAlert(true);
            setShowFiles(1);

          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(2);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath2}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>


        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(3);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath3}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(4);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath4}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btnSubmit} onPress={() => {
          let pass = true;
          if (billPath.length == 0) {
            Alert.alert("Please Choose Bill Image")
            pass = false;
          }
          else if (ratingValue == 0 || ratingValue == undefined) {
            Alert.alert("Please Choose rating star")
            pass = false;
          }
          else if (pass) {
            showRating(false);
            saveRatingApi();
          }
        }}>
          <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Submit'}</Text>
        </TouchableOpacity>

        <Overlay isVisible={showAlert} style={{ alignItems: 'center', justifyContent: 'center' }} onBackdropPress={() => {
          setShowAlert(false);
        }}  >
          <View style={{ width: 200, height: 200, alignItems: 'center', justifyContent: 'center' }}>

            <Text>{'Choose Files'}</Text>

            <TouchableOpacity style={styles.btnSubmit} onPress={() => {

              let options = {
                quality: 0.5,
                mediaType: 'photo',
              };
              ImagePicker.showImagePicker(options, response => {
                if (response.error) {

                } else if (response.uri) {
                  if (showFile == 1) {
                    setBillImagePath(response.uri)
                  } else if (showFile == 2) {
                    setBillImagePath2(response.uri)
                  } else if (showFile == 3) {
                    setBillImagePath3(response.uri)
                  } else if (showFile == 4) {
                    setBillImagePath4(response.uri)
                  }
                  setShowAlert(false);
                }
              });
            }}>
              <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Choose Image'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnSubmit} onPress={() => {
              let options = {
                quality: 0.5,
                mediaType: 'photo',
              };
              ImagePicker.openCamera(options, response => {
                if (response.error) {
                  Alert.alert(response.error);

                } else if (response.uri) {
                  if (showFile == 1) {
                    setBillImagePath(response.uri)
                  } else if (showFile == 2) {
                    setBillImagePath2(response.uri)
                  } else if (showFile == 3) {
                    setBillImagePath3(response.uri)
                  } else if (showFile == 4) {
                    setBillImagePath4(response.uri)
                  }
                  setShowAlert(false);
                }
              });
            }}>
              <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Take Snap'}</Text>
            </TouchableOpacity>
          </View>
        </Overlay>
      </Overlay>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
        style={{
          borderWidth: 0,
          width: '100%',
          borderRadius: 5,
          height: Platform.OS === 'android' ? Dimensions.get('screen').height - 300 : '60%',
          overflow: 'hidden',
          marginTop: 30,
          marginBottom: 30,
          backgroundColor: 'red'
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 22, backgroundColor: '#70757ab5' }}>
          <View style={{
            margin: 5, backgroundColor: 'white', borderRadius: 20, padding: 5, alignItems: 'center', shadowColor: '#000', shadowOffset: {
              width: 0,
              height: 2,
            }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, maxHeight: Dimensions.get('screen').height - 200
          }}>
            <ScrollView showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 17, width: '100%', textAlign: 'center', marginTop: 10 }}>
                  Apply Discount at Checkout
                </Text>
                <View style={{ marginTop: 15, paddingHorizontal: 10 }}>
                  {Object.keys(storeDetails).length != 0 && storeDetails['shopifyDiscount'].length > 0 ?
                    storeDetails['shopifyDiscount'].map((item: any) => {
                      return (
                        <View style={{ flexDirection: 'row', marginTop: 6, right: 10 }} key={Math.floor(Math.random() * 100000)}>
                          <Text style={{ fontSize: 15, marginLeft: 8 }}> {'->'}</Text>
                          {Platform.OS === 'ios' ?
                            <TextInput
                              value={item}
                              editable={false}
                              multiline
                            /> :
                            <Text style={{ fontSize: 15, marginLeft: 10, marginRight: 10 }} selectable={true} selectionColor='#c0c8cf'>{item}</Text>}
                        </View>
                      )
                    }) :
                    <View style={{ marginTop: 20, paddingHorizontal: 10, height: 100 }}>
                      <Text style={{ fontWeight: 'bold', fontSize: 13, textAlign: 'center', marginTop: 20 }}>
                        No additional discounts available at this moment
                  </Text>
                    </View>
                  }
                </View>
              </View>
            </ScrollView>
            <View style={{ alignItems: 'center', marginTop: 50 }}>
              <TouchableOpacity style={{ ...styles.btnWhiteBorder, width: 100, backgroundColor: '#007bff', marginBottom: 18, borderRadius: 5 }} onPress={() => {
                setModalVisible(false);
              }}>
                <Text style={{ ...styles.txtStoreName, color: 'white', fontSize: 15, marginTop: -3 }}>{'Close'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView >
  );
};



const mapStateToProps = (state) => ({
  getFavData: state.productTab.favourite,
  storeId: state.productTab.storeKey,
  storeType: state.productTab.storeType,
  offerId: state.productTab.offerId,
  getLocation: state.location,
  store: state.productTab.store,
  getFavStore: state.productTab.followStore,
  name: state.location.name,
  snips: state.productTab.snips


});
const mapDispatchToProps = dispatch => ({
  getStoreDetails: (userId) => dispatch(getStoreDetails(userId)),
  getHomeTabProducts: (userid) => dispatch(getHomeTabProducts(userid)),
  addToFavourite: (data) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data) => dispatch(deleteToFavourite(data)),
  getAllTabs: (data) => dispatch(getAllTabs(data)),
  getProductsTabsProducts: (data) => dispatch(getProductsTabsProducts(data)),
  getCollectionsByStoreTabProducts: (data) => dispatch(getCollectionsByStoreTabProducts(data)),
  setProductKey: (data) => dispatch(setProductKey(data)),
  getOffersProducts: (offerId) => dispatch(getOffersProducts(offerId)),
  getOffersStoreId: (storeId) => dispatch(getOffersStoreId(storeId)),
  getAllFavoriteProduct: (data) => dispatch(getAllFavoriteProduct(data)),
  addLikeProducts: (data, userId, type) => dispatch(addLikeProducts(data, userId, type)),
  makeUnlikeProducts: (userId, offerId, type, likeId) => dispatch(makeUnlikeProducts(userId, offerId, type, likeId)),
  addUserSnips: (userId, data) => dispatch(addUserSnips(userId, data)),
  getUserSnippets: (userId) => dispatch(getUserSnippets(userId)),
  addToSnips: (data) => dispatch(addToSnips(data)),
  getRatingByOfferId: (userid: any) => dispatch(getRatingByOfferId(userid)),
  uploadRatingDetails: (data, userId) => dispatch(uploadRatingDetails(data, userId)),
  uploadRatingStoreImage: (storeName, data, imageid) => dispatch(uploadRatingStoreImage(storeName, data, imageid)),
  uploadBillImage: (storeName, data, billId) => dispatch(uploadBillImage(storeName, data, billId)),
  uploadBillDetails: (data, billId) => dispatch(uploadBillDetails(data, billId)),
  addToZoomImages: (data) => dispatch(addToZoomImages(data)),
  addToVideoUrl: (data: string) => dispatch(addToVideoUrl(data)),
  addToFollow: (data) => dispatch(addToFollow(data)),
  addFollowingApi: (data, userId) => dispatch(addFollowingApi(data, userId)),
  getAllFollowStore: (userId) => dispatch(getAllFollowStore(userId)),
  setStoreKey: (data) => dispatch(setStoreKey(data)),
  checkInOffers: (userId) => dispatch(checkInOffers(userId)),
  offerBuyNowClick: (userId: string, offerId: string) => dispatch(offerBuyNowClick(userId, offerId)),



});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferDesc)





const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    resizeMode: 'contain',
    height: 160,
    marginTop: 10,
  },
  txtProductName: {
    paddingHorizontal: 10,
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold'
  },
  txtReview: {
    paddingVertical: 10,
    color: 'black',
  },

  txtStoreName: {
    paddingVertical: 10,
    color: 'black',
    fontWeight: 'bold'
  },

  txtStoreDetailsTitle: {
    color: 'grey',
    flex: 1,
    fontSize: 12
  },
  txtStoreDetailsDesc: {
    color: 'black',
    fontWeight: 'bold',
    flex: 1,
    fontSize: 12

  },
  txtDesc: {
    color: '#CFCFCF',
  },
  socialMedia: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  socialMediaInner: {
    resizeMode: 'cover',
    height: 50,
    width: 50,
    margin: 5

  },
  btnWhiteBorder: {
    borderColor: 'white',
    borderWidth: 1,
    marginLeft: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,

  },


  btnDisableWhite: {
    borderColor: DISABLED_COLOR,
    backgroundColor: GREY,
    borderWidth: 1,
    marginLeft: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,
  },
  btnSolidBorder: {
    borderColor: DISABLED_COLOR,
    backgroundColor: GREY,
    borderWidth: 1,
    marginLeft: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,
  },
  btnSolid: {
    backgroundColor: '#1776d3',
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,

  },
  forgottext: {
    alignSelf: 'flex-end',
    fontSize: 16,
    marginTop: 10,
    marginEnd: 25,
    marginBottom: 10,
  },
  textStyle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnBlueBorder: {
    borderColor: '#1776d3',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: 130,
    height: 35,

  },
  containerRatingStyle: {
    marginTop: 10,
    width: Dimensions.get('window').width / 1.2,
    height: 35,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#F2F2F2',
    borderWidth: 0.5,
  },
  txtBillStyle: {
    flex: 1,
    height: 34,
    padding: 10,
    fontSize: 12,
  },
  txtBrowseStyle: {
    width: 100,
    height: 34,
    textAlign: 'center',
    backgroundColor: '#F2F2F2'
  },
  edtStyle: {
    marginTop: 10,
    width: Dimensions.get('window').width / 1.2,
    backgroundColor: 'white',
    borderColor: '#F2F2F2',
    borderWidth: 0.5,
    borderRadius: 5,
    padding: 10,
    height: 70,
    fontSize: 12,
  },

  btnSubmit: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 5,
    marginTop: 5,
    backgroundColor: '#1776d3',

  },
  btnOrange: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#C96349',
  },
  btnGrey: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: GREY,
  },
  btnBuyNow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#1776d3',
    width: Dimensions.get('window').width / 1.5
  },

})
