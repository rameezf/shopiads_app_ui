import React, { useContext, useEffect, useState } from 'react'
import { View, Text, Image, Alert } from 'react-native'
import SimpleListView from '../Components/ListView/ListView'
import { HorizontalListView } from '../Components/ListView/HorizontalListView'
import {
    fashionBanner, electronicsBanner, productCasualShirt, productFormalShirt, productshorts, productTshirt,
    productJeans, productHoody, productJacket, productDress, productBlazer, mobile, tv, ac,
    speaker, laptop, electronics_access, radio, formal, sports, slipper, sneakers, sandal, hills, hikers, footwearBanner
} from '../Constants/images'
import { connect } from 'react-redux';
import { ProductContext } from '../Context/Context'
import { getMainCategory, getSubCategoriesList,  } from '../Actions/Offer'
import { setAllSubCategoires } from '../Actions/product'

const Product = (props) => {


    const [chipData,setChipData]=useState( []);

    const [chipsSubCategries,setChipSubCategoriesData]=useState( []);

    const productKey = props.productType.departmentKey
    const topHeadTitle =""
    const [subCategoryData,setSubCategoryData] = useState([]);

    const arrRight :any= []
    const [onFocus, setFocus] = useState(false)

    const [selectedCategory,setSelectedCateogory] =useState("")

    useEffect(()=>{
      //  console.log('DARA',productKey);
        if(chipData.length==0){
            setChipData(props.categories)
            setChipSubCategoriesData(props.subCategories)
          //  getMainCategories()
            if(props.categories){
                setSelectedCateogory(props.categories[0].name)
         
                //  productKey= props.categories[0].id

            }
        }
      })
      
      useEffect(() => {
        if(chipData.length > 0){
            setSelectedCateogory(chipData[productKey].name)

        }
       
       }, [productKey]);
    



    return (
        <View style={{ flex: 1, flexDirection: 'row', borderTopColor: '#fff', borderTopWidth: 1 }}>
            <View style={{ flex: 0.35, height: '100%', }}>
                <SimpleListView {...props} arr={chipData} onFocus={onFocus} />
            </View>
            <View style={{ flex: 0.65, marginHorizontal: 10 }}>
                <HorizontalListView {...props} arr={chipsSubCategries} name={selectedCategory} onFocus={onFocus} rightProductStyle={{ resizeMode: 'contain', height: 70, width: 70 }} />
            </View>
        </View>
    )
}
const mapStateToProps = state => ({
    productType: state.productTab,
    categories:state.productTab.categories,
    subCategories:state.productTab.subCategories
  
});
const mapDispatchToProps = dispatch => ({
    getMainCategory: (user) => dispatch(getMainCategory(user)),
    getSubCategoriesList: (user) => dispatch(getSubCategoriesList(user)),
    setAllSubCategoires:(id) => dispatch(setAllSubCategoires(id))


});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Product)
