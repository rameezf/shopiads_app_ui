
import * as ACTION_TYPES from "../Constants/ActionTypes";


export const getUserSnippets =(userId)=>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload:{
            client :'default',
            request: {
              method: "GET",
              url: "/getUserSnippets/"+userId+"/snip" ,
           }
          }
})


export const deleteSnippets = (userId, data) =>({
    types: [ACTION_TYPES.GET_CITY_REQUESTED,
        ACTION_TYPES.GET_CITY_SUCCESS,
        ACTION_TYPES.GET_CITY_FAILED],
        payload:{
            client :'default',
            request: {
              method: "POST",
              url: "/snippets/delete/"+userId ,
              data: data
           }
          }
})

