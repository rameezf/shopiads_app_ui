import React, { createRef, useContext, useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Platform
} from 'react-native';
import { getOffersAll, getOfferCategory, getMainCategory, getAllStoresBasedonCategoryStores, getOnlineStores, getInStores, getNearByStores, getAllStoreList, getAllSearchStoreList, getAllFavoriteProduct } from '../Actions/Offer';
import {
  addToFavourite,
  addToViewAllProduct,
  deleteToFavourite,
  setStoreKey,
} from '../Actions/product';
import { connect } from 'react-redux';
import MyChip from '../Components/chips';
import {
  all,TRENDING,
  DOWNARRAY,logo
} from '../Constants/images';

import { BLACK, BLUE, GREY, LIGHTGREY, WHITE, } from '../Constants/colors';
import { Store } from '../Components/Store';
import { style as storeStyle } from './Store.style';


const trendingObj = {
  "id": "trending",
  "name": "Trending",
  "image": "",
  "root": "Y",
  "priority": 1,
  "subcategories": [
  ],
  "maincategories": [
  ],
  "rootImage":TRENDING,
 "createdOn": "2021-11-01T06:51:14.974+0000",
  "updatedOn": "2021-11-01T06:51:14.142+0000",
  "createdBy": "dharmveer.siet@gmail.com",
  "updatedBy": "dharmveer.siet@gmail.com"
};

const allStoresObj = {

  "id": "all",
  "name": "All Stores",
  "image": "",
  "root": "Y",
  "priority": 1,
  "subcategories": [
  ],
  "maincategories": [
  ],
  "rootImage":all,

  "createdOn": "2021-11-01T06:51:14.974+0000",
  "updatedOn": "2021-11-01T06:51:14.142+0000",
  "createdBy": "dharmveer.siet@gmail.com",
  "updatedBy": "dharmveer.siet@gmail.com"
};
const allObj = {
  "id": "All",
  "name": "All Stores",
  "image": "",
  "rootImage":all,
  "root": "Y",
  "priority": 1,
  "subcategories": [
  ],
  "maincategories": [
  ],
  "createdOn": "2021-11-01T06:51:14.974+0000",
  "updatedOn": "2021-11-01T06:51:14.142+0000",
  "createdBy": "dharmveer.siet@gmail.com",
  "updatedBy": "dharmveer.siet@gmail.com"
}

const storesAlphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
import { Buffer } from "buffer"

import StickyHeaderFlatlist from 'react-native-sticky-header-flatlist';
import { Overlay } from 'react-native-elements';
import { SingleStore } from '../Components/SingleStore';
import { addLikeProducts, makeUnlikeProducts, removeLikeProducts } from '../Actions/StoreDetails';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { AuthContext } from '../Context/Context';
import { ThemeImage } from '../Components/ThemeImage';

const DATA =
  [
    {
      title: "Family",
      contactList: [
        { title: "Armani Snider" },
        { title: "Macauly Downs" },
        //... More name
      ]
    },
    {
      title: "Company",
      contactList: [
        { title: "Armani Snider" },
        { title: "Macauly Downs" },
        //... More name
      ]
    },
    {
      title: "Club",
      contactList: [
        { title: "Armani Snider" },
        { title: "Macauly Downs" },
        //... More name
      ]
    },

  ]


const Stores = ({ navigation, ...props }) => {
  const { Logout } = useContext(AuthContext)

  const [showLoginAlert, setShowLoginAlert] = React.useState(false);
  const [showLoader, setShowLoader] = React.useState(false);
 

  const [chipData, setChipData] = useState([]);
  const [chipDataAll, setAllChipData] = useState([]);

  const [noData, setNoData] = useState(false);
  const [chipItem, setChipItem] = useState({});
  const [dataChange, setDataChange] = useState(false);
  const [normalArr, setNormalArr] = useState([]);
  const [loadingData, setLoadingData] = useState(false);
  const [showAllCateogories, setShowAllCategories] = useState(false);

  const [onlineStore, setOnlineStores] = useState([]);
  const [inStore, setInStores] = useState([]);
  const [nearByStore, setNearByStores] = useState([]);
  const [allStores, setAllStore] = useState([]);
  const [filterAllStores, setFilterAllStore] = useState([]);
  const [scrollPosition,setScrollPosition]= useState([]);

  const flatListRefAllStores = useRef<FlatList>(null)
  const flatListRefCategory = useRef<FlatList>(null)

  const [search, setSearch] = useState('');
  const [count,setCount] = useState(0)
  const [indexCategory,setIndexCategory] = useState(0)

  const [loader, setLoader] = useState(false);

  useEffect(() => {
    if (chipData.length == 0) {
      getMainCategories()
    }
  })
  useEffect(() => {
    let user = 'Z3Vlc3Q=';
    let state = props.getLocation.locationDataCity
      ? props.getLocation.locationDataCity.cityId
      : '';

    let stateName = props.getLocation.locationDataArea
      ? props.getLocation.locationDataArea.areaName
      : '';

    let city = props.getLocation.locationDataArea
      ? props.getLocation.locationDataArea.areaId
      : '';
    if (state && city) {
      setInStores([]);
      setNearByStores([]);
      setOnlineStores([]);
      let categoryId: string = chipItem['id'];
      getNearByStores(user, city, stateName, state, categoryId)
      setLoadingData(true);
      setLoader(true)
    }
  }, [chipItem, props.getLocation.locationDataArea]);

  const getMainCategories = () => {
    let userId = "Z3Vlc3Q=";
    props.getMainCategory(userId).then(res => {
      if (res.payload.status === 200) {
        let data = [] as any;
        data = [trendingObj, allObj, ...res.payload.data]
        setChipData(data);
        let dataAll = [] as any;
        dataAll = [allStoresObj, ...res.payload.data]
        setAllChipData(dataAll);
        setChipItem(data[0])
      }
    })
  }

  useEffect(() => {
    if (search.length != 0) {
      getAllSearchStoresList(search, value.id)

    } else {
      getAllSearchStoresList(search, value.id)
    }
  }, [search])


  
  const getAllSearchStoresList = (text: string, catId: string) => {

    setAllStore([])
    setFilterAllStore([]);

    console.log("URLLLLLL->>>>>", "/stores/searchStore/" + text + "/" + catId)
    props.getAllSearchStoreList(text, catId,props.getLocation.userMarketPlace).then((res) => {
      let result = res.payload.data;
      setCount(result.length);
      let count= 0
      let data: any = [];
      let scrollPos=-1
      for (let i = 0; i < storesAlphabets.length; i++) {
        let stores = result.filter(data => (data.storeName.length > 0 && data.storeName.charAt(0) == storesAlphabets[i]));
        count+=stores.length;
        let obj = { title: storesAlphabets[i], contactList: stores }
        data.push(obj)
        if(scrollPos<0 &&stores.length >0 ){
          scrollPos=i
        }
      }
      setCount(count);
      setAllStore(data)
      if(scrollPos>=0 && result.length ){
          setTimeout(() =>  {
            try{
              flatListRefAllStores.current?.scrollToIndex({ index: scrollPos })
            }catch(e){
            }
            }, 500);
      }
    });
  }

  const onChangeChipItem = (item) => {
    setChipItem(item);
  };
 

  const getOnlineStore = (user: string, cityId: string, stateId: string, cityName: string) => {
    let categoryId: string = chipItem['id'];
    props.getOnlineStores(user, cityId, stateId, categoryId, cityName,props.getLocation.userMarketPlace).then((res) => {
      setOnlineStores(res.payload.data);
      setLoader(false)
    });
  }

  const getInStores = (user: string, cityId: string, stateId: string, categoryId: string, cityName: string) => {
    props.getInStores(user, cityId, stateId, categoryId, cityName,props.getLocation.userMarketPlace).then((res) => {
      setInStores(res.payload.data);
      console.log('DATADDJJDJD', res.payload.data);
      getOnlineStore(user, cityId, stateId, cityName)
    });
  }

  const getNearByStores = (user: string, cityId: string, cityName: string, stateId: string, categoryId: string) => {
    console.log('VVV', cityName)
    props.getNearByStores(user, cityId, cityName, stateId, categoryId,props.getLocation.userMarketPlace).then((res) => {
      setLoadingData(false);
      console.log(res.payload.data);
      setNearByStores(res.payload.data);
      getInStores(user, cityId, stateId, categoryId, cityName);

    });
  }



  const onAddToFavourite = (item:any,type="store") => {
    console.log('VJVJVVJVJVJVJ',item);
    if (props.getLocation.email) {
      if(!item['requestType']   ){
      if (props.getFavData.length == 0) {
        item.totalLikes=item.totalLikes+1
        addLikeStoreProductApi( item,type)
      } else {
        const found = props.getFavData.some((el) => el.storeId ===item.id  && el.type == "Store")   ;
        if (found) {
          item.totalLikes=item.totalLikes-1
          const data = props.getFavData.filter(data => data.storeId === item.id  && data.type == "Store" )
          let index = props.getFavData.indexOf(data[0]);
          props.getFavData.splice(index, 1);
          props.deleteToFavourite([]);
          props.deleteToFavourite(props.getFavData);
          setDataChange(!dataChange);
          removeLikeStoreProductApi(data[0])
        } else {
          item.totalLikes=item.totalLikes+1
          addLikeStoreProductApi( item,type)
        }
      }
   }
   } else {
      setShowLoginAlert(true);
    }
  };

  const addLikeStoreProductApi = (item:string,type) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    item['requestType']= true;
    setShowLoader(true)
  
    props.addLikeProducts(item.id, email,type).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        item['requestType']= false;
  setShowLoader(false)
    
        props.deleteToFavourite(res.payload.data);
      });
    });
  }

  const removeLikeStoreProductApi = (item:{}) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.removeLikeProducts(email, item).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });
    });
  }



  const foundInFav = (item:{}) => {
    const found = props.getFavData.some((el) => el.storeId === item.id && el.type == "Store"  );
    if (found) {
      return true;
    } else {
      return false;
    }
  };





  const renderStoreItem = (item: any, idx: number, type: boolean) => {
    return (
      <SingleStore {...props} item={item} onItemPress={() => {
        props.setStoreKey(item.storeId ? item.storeId : item.id)
        navigation.push('StoresDesc');
      }}
        type={type}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
         onAddToFavourite(item)
        }}
      />
    );
  };
 
  const renderStore = (item: any, index: number) => {
    return (
      <View
        style={{ width: "28%", flexDirection: 'column', margin: 10 }}
      >
        <TouchableOpacity
          onPress={() => {
            console.log("ITEMCCCCCCCCC",item);
            props.setStoreKey(item.storeId ? item.storeId : item.id)
            navigation.push('StoresDesc');
          }}
          style={{ marginLeft:20,width: 52, height: 55, borderColor: LIGHTGREY,backgroundColor:WHITE, borderWidth: 1, borderRadius: 3 }}>
          <Image
            source={{ uri: item.fullImage ? item.fullImage : item.companyLogo }}
            style={{ height: 50, resizeMode: 'contain',width:50 }}
          /></TouchableOpacity>
        <Text style={{ marginTop: 5,textAlign:'center',fontSize:11, marginRight:Platform.OS == 'ios' ? 5 : 12 }} >{item.storeName+(item.code == "Primary" ? "":("\n"+item['code']))}</Text>
      </View>
    )

  }

  const renderAlphaName = (item: any, index: number) => {
    return (

      <TouchableOpacity
        onPress={() => {
          console.log(index);
          setTimeout(() => flatListRefAllStores.current?.scrollToIndex({ index: index }), 500);
        }}
        style={{ width: 50, height: 50, }}>
        <Text style={{ width: 50, marginTop: 5, textAlign: 'center' }}>{item}</Text>
      </TouchableOpacity>
    )
  }

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(allStoresObj);

  const renderStoreTitle = (item: any, index: number) => {
    return (
      <View style={{ flexDirection: 'column', width: '100%' }}>
        <Text style={{ width: "100%", marginTop: 5, padding: 10, textAlign:'center'}}>{item.title}</Text>
        <FlatList
          data={item.contactList}
          numColumns={3}
          renderItem={({ item, index }) => renderStore(item, index)}
          style={{ flex: 1, paddingHorizontal: 10 }}
        />

        <View style={{backgroundColor:LIGHTGREY,height:1,width:'100%',marginTop:10}}/>
      </View>
    )

  }



  const renderChipItem = (item: any, index: number,type:Boolean) => {
    return (
      <View style={{ flex: 1, alignItems: 'center', marginVertical: 10, justifyContent: 'center' }}>
        <MyChip
          icon={item.image ? { uri: item.image } :  item.rootImage}
          onPress={() => {
            setShowAllCategories(false);
            onChangeChipItem(item);
            setIndexCategory(index);
            if(type){
              setTimeout(() => flatListRefCategory.current?.scrollToIndex({ index: index }), 500);
            }
          }}
          imageStyle={{ width: 55, height: 55, resizeMode: 'stretch' }}
          containerStyle={{
            marginHorizontal: 5,
            justifyContent: 'center',
            width: 65,
            height: 65,
            elevation: 4,
            borderRadius: 65 / 2,
            alignItems: 'center',
            backgroundColor: '#FFFFFF',
            borderColor:'#1776d3' ,
            borderWidth: 1,
          }}
        />
        <Text
          numberOfLines={1}
          style={{
            width: 70,
            textAlign: 'center',
            color: item.name == chipItem['name'] ? '#1776d3' : '#000',
            fontSize: 11,
            fontWeight: 'bold',
          }}>
          {item.name}
        </Text>
      </View>
    );
  };


  const renderChipDataItem = (item: any, index: number) => {
    return (
      <TouchableOpacity
        onPress={() => {
          setOpen(false)
          setShowAllCategories(false);
          setValue(item);
          getAllSearchStoresList(search, item.id)

        }}
        style={{ flex: 1, alignItems: 'center', flexDirection: 'row', marginVertical: 10, width: 200, justifyContent: 'center' }}>
        <MyChip
          icon={item.image ? { uri: item.image } : all}
          onPress={() => {
            setOpen(false)
            setShowAllCategories(false);
            setValue(item);
            getAllSearchStoresList(search, item.id)
          }}
          imageStyle={{ width: 55, height: 55, resizeMode: 'stretch' }}
          containerStyle={{
            marginHorizontal: 5,
            justifyContent: 'center',
            width: 65,
            height: 65,
            elevation: 4,
            borderRadius: 65 / 2,
            alignItems: 'center',
            backgroundColor: '#FFFFFF',
            borderColor: value && item.name == value['name'] ? '#1776d3' : '#FFFFFF',
            borderWidth: 1,
          }}
        />
        <Text
          numberOfLines={1}
          style={{
            width: 135,
            textAlign: 'center',
            color: value && item.name == value['name'] ? '#1776d3' : '#000',
            fontSize: 11,
            fontWeight: 'bold',
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  
  const handleScroll = ({viewableItems, changed}) => {

    //  console.log("Visible items are", viewableItems);
 
  //    console.log("Changed in this iteration", changed);
   
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ width: '100%', height: 100, flexDirection: 'row', }}>
        <FlatList
          data={chipData}
          horizontal
          ref={flatListRefCategory}
          scrollEnabled
          renderItem={({ item, index }) => renderChipItem(item, index,false)}
          style={{ flex: 1,}}
        />
        {
          (chipData.length > 0 && <TouchableOpacity onPress={() => {
            setShowAllCategories(true);
          }} style={{ width: 30, height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Image source={DOWNARRAY} style={{ width: 14, height: 14, tintColor: BLUE }} />
          </TouchableOpacity>)
        }

      </View>

      {
        loader ? (<View style={{ width: '100%', height: 200, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ width: '100%', textAlign: 'center', fontSize: 14, marginTop: 20 }}>{'Loading...'}</Text>
        </View>)
          : (chipItem.id === "All" ? (
            <View style={{ width: '100%', height: "100%", flexDirection: 'column' }}>
              <View style={{ flexDirection: 'row',borderColor:BLUE,borderWidth:1,borderRadius:5,marginHorizontal:20 }}>
                <TouchableOpacity
                  style={{ width: 100, height: 45, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}
                  onPress={() => {
                    setOpen(true);
                  }}
                >
                  <Text style={{ color: BLACK, flex: 1, padding: 5, textAlign: 'center' }}>{value ? value.name : "All Stores"}</Text>
                  <Image style={{ width: 10, height: 10 }} source={DOWNARRAY}></Image>
                </TouchableOpacity>
                <View style={{width:1,height:45,marginStart:10,backgroundColor:BLUE}}/>
                <TextInput style={{
                  padding: 10,
                  flex: 1,
                  borderRadius: 4,
                  fontSize: 13,
                }} placeholder={'Search from '+count+' Stores'}
                  value={search}
                  onChangeText={setSearch}
                />
              </View>
              <View style={{backgroundColor:BLACK,height:1,width:'100%',marginTop:10}}/>
              <FlatList
                data={storesAlphabets}
                horizontal

                renderItem={({ item, index }) => renderAlphaName(item, index)}
                style={{ paddingHorizontal: 10,marginTop:10 }}
              />
                   <View style={{backgroundColor:BLACK,height:1,width:'100%',marginTop:5}}/>
         
              <FlatList
                data={allStores}
                viewabilityConfig={{
                  itemVisiblePercentThreshold: 50
                }}
                ref={flatListRefAllStores}
                renderItem={({ item, index }) => renderStoreTitle(item, index)}
                style={{ paddingHorizontal: 10, height: '100%', marginBottom: 100 }}
              />
            </View>

          ) : (
            <ScrollView style={{ width: '100%', height: '100%' }}>
              <View style={{ width: '100%', height: '100%' }}>
                <View style={{ width: '100%', flexDirection: 'row', padding: 10 }}>
                  <Text style={storeStyle.txtCategoryName}>{'Nearby stores'}</Text>
                  <TouchableOpacity onPress={() => {
                    let data = { productType: false, product: [], offer: nearByStore }
                    props.addToViewAllProduct(data)
                    navigation.push('StoreViewAllProduct');
                  }}>
                    <Text style={storeStyle.txtViewAll}>{'View All >'}</Text>
                  </TouchableOpacity>
                </View>

                {nearByStore.length == 0 && (
                  <Text style={{ padding: 10, width: '100%', textAlign: 'center' }}>
                    {"No Record Found "}
                  </Text>
                )}
                <FlatList
                  data={nearByStore}
                  keyExtractor={item => item.id}
                  stickyHeaderIndices={[0]}
                  horizontal
                  renderItem={({ item, index }) => renderStoreItem(item, index, true)}
                  style={{ flex: 1, paddingHorizontal: 10 }}
                />
                <View style={{ width: '100%', flexDirection: 'row', padding: 10 }}>
                  <Text style={storeStyle.txtCategoryName}>{props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "US" ? 'US-wide stores':'Canada-wide stores'}</Text>
                  <TouchableOpacity onPress={() => {
                    let data = { productType: false, product: [], offer: inStore }
                    props.addToViewAllProduct(data)
                    navigation.push('StoreViewAllProduct');
                  }}>
                    <Text style={storeStyle.txtViewAll}>{'View All >'}</Text>
                  </TouchableOpacity>
                </View>

                {inStore.length == 0 && (
                  <Text style={{ padding: 10, width: '100%', textAlign: 'center' }}>
                    {"No Record Found "}
                  </Text>
                )}

              <View style={{ width: '100%', flexDirection: 'row' }}>
               {inStore.length > 0 && (
                  <View style={{ flexDirection: 'row', width: '100%' }}>
                    <FlatList
                      data={inStore}
                      keyExtractor={item => item.id}
                      stickyHeaderIndices={[0]}
                      horizontal
                      renderItem={({ item, index }) => renderStoreItem(item, index, true)}
                      style={{ flex: 1, paddingHorizontal: 10 }}
                    />
                  </View>
                )
                }
               </View>

               {/* <View style={{ width: '100%', flexDirection: 'row', padding: 10 }}>
                  <Text style={storeStyle.txtCategoryName}>{'Canada-wide online-stores'}</Text>
                  <TouchableOpacity onPress={() => {
                    let data = { productType: false, product: [], offer: onlineStore }
                    props.addToViewAllProduct(data)
                    navigation.push('StoreViewAllProduct');
                  }}>
                    <Text style={storeStyle.txtViewAll}>{'View All >'}</Text>
                  </TouchableOpacity>
                </View>


                {onlineStore.length == 0 && (
                  <Text style={{ padding: 10, width: '100%', textAlign: 'center' }}>
                    {"No Record Found "}
                  </Text>
                )}
                {onlineStore.length > 0 && (
                  <View style={{ flexDirection: 'column', width: '100%' }}>

                    <FlatList
                      data={onlineStore}
                      keyExtractor={item => item.id}
                      stickyHeaderIndices={[0]}
                      horizontal
                      renderItem={({ item, index }) => renderStoreItem(item, index, true)}
                      style={{ flex: 1, paddingHorizontal: 10 }}
                    />
                  </View>
                )
                */}
              </View>
            </ScrollView> ))
      }
      <Overlay isVisible={open} style={{ width: 200, height: 200 }} onBackdropPress={() => {
        setOpen(false);
      }} >

        <View
          style={{ width: 300, height: 400 }}
        >
          <FlatList
            data={chipDataAll}
            renderItem={({ item, index }) => renderChipDataItem(item, index)}
          />
        </View>
      </Overlay>
      {
        showAllCateogories && (

          <ScrollView style={{
            width: '100%', flex: 1, position: 'absolute', top: 0,
            bottom: 0,
            left: 0,
            backgroundColor: '#f2f2f2',
            right: 0,
          }}>
            <View style={{ backgroundColor: '#ffffff', width: '100%', flexDirection: 'column', }}>
              <View style={{ width: '100%', flexDirection: 'row', paddingHorizontal: 10 }}>
                <Text style={{ fontSize: 15, width: 120, padding: 10, color: '000', flex: 1, paddingHorizontal: 5, }}>
                  {'All Categories'}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    setShowAllCategories(false);
                  }} style={{
                    width: 40, height: 40, padding: 10, backgroundColor: '#ffffff', justifyContent: 'center', alignItems: 'center',
                    transform: [{ rotate: '180deg' }]
                  }}>
                  <Image source={DOWNARRAY} style={{ width: 14, height: 14 }} />
                </TouchableOpacity>

              </View>
              <FlatList
                data={chipData}
                numColumns={4}
                renderItem={({ item, index }) => renderChipItem(item, index,true)}
                style={{ width: '100%', }}
              />
            </View>
          </ScrollView>
        )
      }
         <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          setShowLoginAlert(false);
          Logout(navigation);
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />
      <LoaderAlert
        showAlert={showLoader}
      />
    </View>
  );
};
const mapStateToProps = (state) => ({
  getLocation: state.location,
  getFavData: state.productTab.favourite,

});
const mapDispatchToProps = (dispatch) => ({
  getOffersAll: (user, city, area) => dispatch(getOffersAll(user, city, area)),
  addToFavourite: (data) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data) => dispatch(deleteToFavourite(data)),
  getOfferCategory: (user, id, city, area, viewType,marketPlace) => dispatch(getOfferCategory(user, id, city, area, viewType,marketPlace)),
  getMainCategory: (user) => dispatch(getMainCategory(user)),
  getAllStoresBasedonCategoryStores: (id, user, city, state) => dispatch(getAllStoresBasedonCategoryStores(id, user, city, state)),
  setStoreKey: (data) => dispatch(setStoreKey(data)),
  getOnlineStores: (user: string, cityId: string, stateId: string, categoryId: string, cityName: string,marketPlace:string) => dispatch(getOnlineStores(user, cityId, stateId, categoryId, cityName,marketPlace)),
  getInStores: (user: string, cityId: string, stateId: string, categoryId: string, cityName: string,marketPlace:string) => dispatch(getInStores(user, cityId, stateId, categoryId, cityName,marketPlace)),
  getNearByStores: (user: string, cityId: string, cityName: string, stateId: string, categoryId: string,marketPlace:string) => dispatch(getNearByStores(user, cityId, cityName, stateId, categoryId,marketPlace)),
  getAllStoreList: (marketPlace:string) => dispatch(getAllStoreList(marketPlace)),
  getAllSearchStoreList: (text: string, catId: string,marketPlace:string) => dispatch(getAllSearchStoreList(text, catId,marketPlace)),
  addToViewAllProduct: (data: any) => dispatch(addToViewAllProduct(data)),
  addLikeProducts: (data:any , userId:string,type:string) => dispatch(addLikeProducts(data, userId,type)),
  makeUnlikeProducts: (userId,storeId,likeId) => dispatch(makeUnlikeProducts(userId,storeId,likeId)),
  getAllFavoriteProduct: (userId:string ) => dispatch(getAllFavoriteProduct(userId)),
  removeLikeProducts: (userId:string, item:any) => dispatch(removeLikeProducts(userId, item)),
  

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Stores)