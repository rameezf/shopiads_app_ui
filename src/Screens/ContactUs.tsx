import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { View, Text, Alert, FlatList, Image, StyleSheet, TextInput, Dimensions, Keyboard } from 'react-native'
import { Container } from "native-base";
import { BackHeader } from '../Components/Heading';
import { connect } from 'react-redux'
import Icons from '../Constants/Icons'
import {
    PROFILE_ICON,MAIL_ICON,MENU_ICON,LOCATION_ICON,MESSAGE_ICON

} from '../Constants/images';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import ThemeButton from '../Components/Button';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getAllFollowStore, reportContactUs } from '../Actions/Offer';
import { Buffer } from "buffer"
import { getStoreDetails, removeLikeProducts } from '../Actions/StoreDetails';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { addToFollow, setStoreKey } from '../Actions/product';
import { BLUE, GREY, LIGHTGREY, WHITE } from '../Constants/colors';
import ThemeInput from '../Components/Input';
import { logo, user, password, google, facebook, apple } from '../Constants/images'

const ContactUs = ({ navigation, ...props }) => {

    const [message, setMessage] = useState('')

    const [feedbackType,setFeedbackType] = useState(false);

  

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: BLUE }}
    edges={['top']} 
        
        >
            <BackHeader title={"Contact Us"}
                fontSize={28}
                onFontPress={() => navigation?.pop()}
                fontName={'chevron-thin-left'}
            />
            <ScrollView  style={{flex:1, backgroundColor:WHITE}}>
                <View style={{flex:1}}>

                   <Text style={{fontSize:12,padding:10,marginTop:10,marginStart:10}}>{'Location'}</Text>
            <View style={{borderRadius:45,height:45,alignItems:'center',paddingHorizontal:10,marginHorizontal:10,flexDirection:'row'}}>
            <Image
               style={{width:20,height:17,resizeMode:'stretch'}}
                source={LOCATION_ICON}
               />   
                 <Text style={{fontSize:12,marginStart:10}}>{props.getLocation.locationDataCity.cityName +', '+ props.getLocation.locationDataArea.areaName }</Text>
            </View>
            <View style={{backgroundColor:LIGHTGREY,height:1,marginTop:10}} />

       
            <Text style={{fontSize:12,padding:10,marginTop:10,marginStart:10}}>{'Feedback Type'}</Text>
         
            <View style={{flexDirection:'row',alignItems:'center'}} >
            <Image

               style={{width:20,height:17,resizeMode:'stretch',marginStart:20}}
                source={MESSAGE_ICON}
               />   
            <TouchableOpacity onPress={()=>{
              setFeedbackType(false)
          }}>
            <Text    style={{
              marginStart:10,
            height:40,
            borderRadius:5,
            padding:10,
            textAlign:'left',
            borderWidth:1,
            borderColor:feedbackType ?LIGHTGREY:BLUE,
            fontSize: 12,
          }}>{'Application'}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{
              setFeedbackType(true)
          }}>
  <Text    style={{
              marginStart:10,
            height:40,
            borderRadius:5,
            padding:10,
            textAlign:'left',
            borderWidth:1,
            borderColor:feedbackType ?BLUE:LIGHTGREY,
            fontSize: 12,
          }}>{'Bussiness inquiry'}</Text>              
          </TouchableOpacity>
            
            </View>

            <View style={{backgroundColor:LIGHTGREY,height:1,marginTop:20}} />
            <Text style={{fontSize:12,padding:10,marginTop:10,marginStart:10}}>{'Details'}</Text>
            <View style={{borderRadius:45,height:45,marginTop:10,alignItems:'center',paddingHorizontal:10,marginHorizontal:10,flexDirection:'row'}}>
          <Image
               style={{width:20,height:17,resizeMode:'stretch'}}
                source={PROFILE_ICON}
               />  
            <Text    style={{
              marginStart:10,
            height:40,
            borderRadius:5,
            padding:10,
            textAlign:'left',
            flex:1,
            borderWidth:1,
            borderColor:LIGHTGREY,
            fontSize: 12,
          }}>{props.getLocation.name}</Text>
            </View>
           
            <View style={{borderRadius:45,height:45,marginTop:20,alignItems:'center',paddingHorizontal:10,marginHorizontal:10,flexDirection:'row'}}>
          <Image
               style={{width:20,height:17,resizeMode:'stretch'}}
                source={MAIL_ICON}
               />  
            <Text    style={{
              marginStart:10,
            height:40,
            borderRadius:5,
            padding:10,
            textAlign:'left',
            flex:1,
            borderWidth:1,
            borderColor:LIGHTGREY,
            fontSize: 12,
          }}>{props.getLocation.email}</Text>
            </View>
           
            <View style={{borderRadius:45,height:45,marginTop:20,alignItems:'center',paddingHorizontal:10,marginHorizontal:10,flexDirection:'row'}}>
            <Image
               style={{width:20,height:20,resizeMode:'stretch'}}
                source={MENU_ICON}
               />  
            <TextInput
                value={message}
                keyboardType='default'
                multiline={true}
                onSubmitEditing={() => {
                    Keyboard.dismiss()
              }}
              returnKeyType={'done'} 
              onChangeText={setMessage}
              style={{
                marginStart:10,
                height:70,
                borderRadius:5,
                padding:10,
                textAlign:'left',
                flex:1,
                borderWidth:1,
                borderColor:LIGHTGREY,
                fontSize: 12,
              }}
              placeholder='Messagge'
        />
        </View>
        <ThemeButton
            btnTextStyle={{fontSize:12}}
            btnThemeStyle={{width:200,alignSelf:'center'}}
            btnMainStyle={{
              width:200,
              alignSelf:'center',
              margin:40,
              flex:0,
              height:40
            }}
        onBtnPress={() => {

                    if(message.trim()){
                        let param ={'email':props.getLocation.email,'name':props.getLocation.name, 'type':feedbackType?'Bussiness inquiry':'Application','comment':message}
                        let email = Buffer.from(props.getLocation.email).toString('base64')
                        props.reportContactUs(email,param).then((res) => {
                            if (res.payload.status == 200) {
                                Alert.alert(
                                  '',
                                  "Thank you for getting in touch! We appreciate you contacting us. One of our colleagues will get back in touch with you soon!Have a great day!"
                                  )
                            setMessage('')
                         }
                         });
                    }else{
                        Alert.alert("please enter message")
                
                    }

                   
                
                }}
                        btnText={'Submit'} />
            </View>

            </ScrollView>
          
                

        </SafeAreaView>
    )
}

const mapStateToProps = state => ({
  getLocation: state.location,

})

const mapDispatchToProps = (dispatch) => ({
    reportContactUs: (userId:string,params:any) => dispatch(reportContactUs(userId,params)),
  
  });

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactUs)


const styles = StyleSheet.create({


    txtStoreName: {
        paddingVertical: 10,
        color: 'black',
        fontWeight: 'bold'
    },
    inputStyleBg: {
        padding: 5,
        width: '100%',
        borderRadius: 4,
        fontSize: 13
    },
    inputStyle: {
        padding: 5,
        flex: 1,
        borderRadius: 4,
        fontSize: 13
    }
})