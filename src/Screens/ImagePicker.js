import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

class ImagePickerClass {

    showImagePicker(options,callback) {
        launchImageLibrary({mediaType:'photo', maxWidth:800, durationLimit:40, ...options},callback);
    }

    openCamera(options,callback) {
        return launchCamera({...options},callback);
    }
}

const ImagePicker = new ImagePickerClass();

export default ImagePicker;