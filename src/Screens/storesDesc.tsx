import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react';
import { View, Text, Alert, FlatList, Image, StyleSheet, Dimensions, Platform, ScrollView, TouchableOpacity, Modal, TextInput } from 'react-native';
import { Container, Icon } from 'native-base';
import { BackHeader } from '../Components/Heading';
// import Modal from 'react-native-modalbox';
import { connect } from 'react-redux'
import ProductDesc from './ProductDesc';
import {
  STORE_ICON,
  plus,
  logo, infoIc, DISCOUNT_ICON
} from '../Constants/images';
// import { } from 'react-native-gesture-handler';
import Icons from '../Constants/Icons';
import { style as productStyle } from './Product.style'
import ThemeButton from '../Components/Button';
import ViewPager from '@react-native-community/viewpager';
import { Rating } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import { addFollowingApi, addLikeProducts, getAllTabs, getCollectionsByStoreTabProducts, getHomeTabProducts, getOffersProducts, getOffersStoreId, getProductsTabsProducts, getStoreDetails, getStoreFollowList, removeLikeProducts } from '../Actions/StoreDetails';
import { addToFavourite, addToFollow, deleteToFavourite, setOfferId, setProductKey, setStoreKey } from '../Actions/product';
import { Product } from '../Components/Product';
import { BLUE, DISABLED_COLOR, GREY, WHITE, LIGHTGREY } from '../Constants/colors';
import { getAllFavoriteProduct, getAllFollowStore } from '../Actions/Offer';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { Buffer } from "buffer"
import { GridProduct } from '../Components/GridProduct';
import { AuthContext } from '../Context/Context';
import { ThemeImage } from '../Components/ThemeImage';
import moment from 'moment';
import { OffersDescList } from '../Components/OffersDescList';

const homeCategory = {
  heading: 'Home',
  id: ''
}

const productCategory = {
  heading: 'Products',
  id: ''

}

const StoresDes = ({ navigation, route, ...props }) => {
  const [showLoader, setShowLoader] = React.useState(false);
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);
  const { Logout } = useContext(AuthContext)

  const [modalVisible, setModalVisible] = React.useState(false);
  const [chipItem, setChipItem] = useState({});
  const [dataChange, setDataChange] = useState(false);
  const [storeDetails, setStoreDetails] = useState({}); //to show the "Read more & Less Line"
  const [homeProducts, setHomeProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [categoryType, setCategoryTypes] = useState([]);

  const [followingCount, setFollowingCount] = useState(0)

  const onChangeChipItem = (item) => {
    setChipItem(item);
  };



  const onAddToFavourite = (item, index) => {
    if (props.getLocation.email) {
      if (!item['requestType']) {

        if (props.getFavData.length == 0) {
          item.totalLikes = item.totalLikes + 1
          addLikeStoreProductApi(item)
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id);
          if (found) {
            const data = props.getFavData.filter(data => data.productId === item.id)
            item.totalLikes = item.totalLikes - 1
            let index = props.getFavData.indexOf(data[0]);
            console.log('YYYYY', index)
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0])
          } else {
            item.totalLikes = item.totalLikes + 1
            addLikeStoreProductApi(item)
          }
        }
      }
    } else {
      setShowLoginAlert(true);
    }
  };

  const addLikeStoreProductApi = (item) => {
    item['requestType'] = true;
    setShowLoader(true)
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.addLikeProducts(item.id, email).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        setShowLoader(false);

        item['requestType'] = false;
        props.deleteToFavourite(res.payload.data);
      });
    });
  }

  const parseDateFormat = (time: string) => {

    const arr = time.split(":"); // splitting the string by colon

    const seconds = arr[0] * 3600 + arr[1] * 60 + (+arr[2]); // converting

    let date = new Date()
    date.setHours(arr[0]);
    date.setMinutes(arr[1]);

    return moment(date).format('hh:mmA')

  }

  const removeLikeStoreProductApi = (item) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.removeLikeProducts(email, item).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });

    });
  }



  const firstUpdate = useRef(true);

  useLayoutEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      props.getStoreDetails(props.storeId).then((res) => {
        let result = res.payload.data;
        console.log('RESULTDATAAAA', result);
        setStoreDetails(result)

        props.getStoreFollowList(props.storeId).then((res) => {
          let result = res.payload.data;
          setFollowingCount(result.length);
        });
        getCategories();


      });
    }
  });


  const foundInFav = (item) => {
    let FavItem = item;
    const found = props.getFavData.some((el) => el.productId === FavItem.id);
    if (found) {
      return true;
    } else {
      return false;
    }
  };



  const getCategories = () => {
    props.getAllTabs(props.storeId).then((res) => {
      let result = res.payload.data;
      if (result.length > 0) {
        result.splice(0, 0, productCategory);
        result.splice(0, 0, homeCategory);
        setChipItem(result[0])
        setCategoryTypes(result);
      }
      else {
        //let result1=[];
        result.splice(0, 0, productCategory);
        result.splice(0, 0, homeCategory);
        setChipItem(result[0])
        setCategoryTypes(result);
      }
      getHomeTabProducts();
    });
  }

  const getHomeTabProducts = () => {

    props.getHomeTabProducts(props.storeId).then((res) => {
      let result = res.payload.data;
      setHomeProducts(result);
      console.log('DATAVVVV', result.length);
      if (result.length == 0) {
        onChangeChipItem(productCategory)
        setHomeProducts([]);
        setProducts([]);
        getProductsTabsProducts()
      }
    });
  }

  const getProductsTabsProducts = () => {
    props.getProductsTabsProducts(props.storeId).then((res) => {
      let result = res.payload.data;
      console.log('DATAVVVV', result.length);

      setProducts(result);
    });
  }

  const getCollectionsByStoreTabProducts = (item) => {
    props.getCollectionsByStoreTabProducts(item.id).then((res) => {
      let result = res.payload.data;
      setProducts(result);
    });
  }




  const addFollowingApi = () => {
    const found = props.getFavStore.some((el) => props.storeId === el.storeId);
    if (!found) {
      if (props.getLocation.email) {
        setShowLoader(true);
        let email = Buffer.from(props.getLocation.email).toString('base64')
        props.addFollowingApi(props.storeId, email).then((res) => {
          props.getAllFollowStore(email).then((res) => {
            props.addToFollow(res.payload.data);
            setShowLoader(false);
          });
        });
      } else {
        setShowLoginAlert(true)
      }
    }


  }



  const checkIsUnfollow = () => {
    if (props.getFavStore) {
      const found = props.getFavStore.some((el) => props.storeId === el.storeId);
      return !found
    } else {
      return true
    }

  }

  const renderBannerOnClick = (item: any) => {
    props.setStoreKey(item.storeId ? item.storeId : item.id);
    props.setOfferId(item.id);
    navigation.push('OfferDesc');
  }


  const renderBannerItem = (item: any, idx: number) => {
    return (
      <OffersDescList item={item} renderBannerOnClick={() => renderBannerOnClick(item)} />
    )
  }


  const renderProductItem = (item: any, idx: number) => {
    return (
      <GridProduct {...props} item={item} onItemPress={() => {
        props.setProductKey(item.id)
        console.log('REJJDJDJDJD', item.id)
        navigation.navigate('ProductDesc', { itemType: item })
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item, idx)
        }}
      />

    );

  };

  const renderChipItem = (item: any, index: number) => {
    return (
      <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 4 }} onPress={() => {
        onChangeChipItem(item);

        if (index == 0) {
          setHomeProducts([]);
          setProducts([]);
          getHomeTabProducts()
        } else if (index == 1) {
          setHomeProducts([]);
          setProducts([]);
          getProductsTabsProducts()
        } else {
          setHomeProducts([]);
          setProducts([]);
          getCollectionsByStoreTabProducts(item);
        }
      }}>
        <Text
          numberOfLines={2}
          style={{
            height: 40,
            minWidth: 100,
            padding: 10,
            width: '100%',
            textAlign: 'center',
            borderColor: item == chipItem ? '#1776d3' : '#1776d3',
            borderBottomWidth: 2,
            borderRadius: 3,

            backgroundColor: item == chipItem ? '#1776d3' : 'transparent',
            color: item == chipItem ? '#FFFFFF' : '#000',

            fontSize: 12,
            fontWeight: 'bold',
          }}>
          {item.heading}
        </Text>
      </TouchableOpacity>
    );
  };


  const closeModal = () => {
    console.log("CLose MOdal Call :::::::::::")
    setModalVisible(false);
  }

  console.log("storeDetails ::::", storeDetails, Object.keys(storeDetails).length)
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: BLUE }}
      edges={['top']}
    >
      <View style={{ flex: 1, backgroundColor: WHITE }} >
        <BackHeader title={storeDetails['storeName']}
          fontSize={28}
          onFontPress={() => navigation?.pop()}
          fontName={'chevron-thin-left'}
        />
        {Object.keys(storeDetails).length != 0 && (
          <ScrollView style={{ width: '100%', flex: 1 }} showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false} >
            <View style={{ width: '100%', height: '100%' }}>
              <View style={{ width: '100%', flexDirection: 'column', paddingHorizontal: 10, paddingTop: 10 }}>
                <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#607d8b', padding: 8, borderRadius: 5 }} >
                  <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }} >
                    <View style={{ borderColor: 'grey', borderWidth: 1, padding: 2, borderRadius: 4 }}>
                      <Image
                        source={{ uri: storeDetails['logoMedium'] }}
                        style={{ width: 50, height: 40, borderRadius: 2, resizeMode: 'contain' }} />
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, marginLeft: 10 }}>
                      <Text style={{ ...styles.txtStoreName, paddingVertical: 0, flex: 0, fontSize: 13, paddingHorizontal: 10 }}>{storeDetails['storeName'] + (storeDetails['code'] == "Primary" ? "" : " Branch: " + storeDetails['code'])} </Text>
                      <Text style={{ ...styles.txtDesc, flex: 0, fontSize: 13, paddingHorizontal: 10 }}>{storeDetails['address2'] + ',' + "Unit: " + storeDetails['address1'] + ', ' + storeDetails['cityId'] + ' ' + storeDetails['pincode']}</Text>
                    </View>
                  </View>
                  <View style={{
                    width: '100%',
                    marginTop: 10,
                    flexDirection: 'column'
                  }}>
                    <View style={{ width: '100%', flexDirection: 'row', }} >
                      <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Text style={[styles.txtStoreDetailsTitle, { marginLeft: -2 }]}> {'Incorporated As:'} </Text>
                        <Text style={{ ...styles.txtStoreDetailsDesc, flex: 1.6, marginLeft: -4 }} numberOfLines={1}> {storeDetails['companyBusinessName']} </Text>
                      </View>
                      <View style={{ flex: 0.4, flexDirection: 'row', }} >
                        <Text style={styles.txtStoreDetailsTitle}> {'Followers:'} </Text>
                        <Text style={{ ...styles.txtStoreDetailsDesc, textAlign: 'right', flex: 0.9 }} numberOfLines={1}> {followingCount.toString()} </Text>
                      </View>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', }} >
                      <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Text style={styles.txtStoreDetailsTitle}>{'Presence:'}</Text>
                        <Text style={{ ...styles.txtStoreDetailsDesc, flex: 1.7 }} numberOfLines={1}> {storeDetails['isphysical'] === "Y" ? 'Online & In-Store' : 'Online'} </Text>
                      </View>
                      <View style={{ flex: 0.4, flexDirection: 'row', }} >
                        <Text style={styles.txtStoreDetailsTitle}> {'Rating:'} </Text>
                        <Text style={{ ...styles.txtStoreDetailsDesc, textAlign: 'right', flex: 1.1 }} numberOfLines={1}> {storeDetails['avgRating']} </Text>
                      </View>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', }} >
                      <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Text style={styles.txtStoreDetailsTitle}>{'Delivery:'}</Text>
                        <Text style={{ ...styles.txtStoreDetailsDesc, flex: 1.7 }} numberOfLines={1}> {storeDetails['deliveryNotes'] != "" ? "Free over $" + storeDetails['deliveryNotes'] : storeDetails['deliveryType'] != "" && storeDetails['secondDeliveryType'] != "" && storeDetails['secondDeliveryType'] != undefined ? storeDetails['deliveryType'] + "/" + storeDetails['secondDeliveryType']
                          : storeDetails['deliveryType'] != "" ? storeDetails['deliveryType'] : storeDetails['secondDeliveryType'] != "" && storeDetails['secondDeliveryType'] != undefined ? storeDetails['secondDeliveryType'] : ""} </Text>
                      </View>
                      <View style={{ flex: 0.4, justifyContent: 'center' }}>
                        <TouchableOpacity
                          onPress={() => {
                            Alert.alert("Timing", (storeDetails['fromHrs'] && storeDetails['toHrs'] ? (parseDateFormat(storeDetails['fromHrs'].toString().replace('[', "").replace(']', "")) + "-" + parseDateFormat(storeDetails['toHrs'].toString().replace('[', "").replace(']', ""))) : "") + "\n" + storeDetails["timeNote"] + "\n" + (storeDetails["weeklyOff"].trim().length > 0 ? ("Closed: " + storeDetails["weeklyOff"]) : ""))
                          }}
                          style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                          <Text style={{ ...styles.txtStoreDetailsTitle, flex: 0, }}> {'Timing:'} </Text>
                          <Image source={infoIc} style={{ width: 15, height: 15, marginRight: -2 }} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', width: '100%', marginTop: 10, }}>
                      <TouchableOpacity style={checkIsUnfollow() ? styles.btnWhiteBorder : styles.btnDisableWhite}
                        disabled={!checkIsUnfollow()}
                        onPress={() => {
                          if (props.getLocation.email) {
                            addFollowingApi()
                          } else {
                            setShowLoginAlert(true);
                          }
                        }}>
                        <Image source={plus} style={{ width: 20, height: 20, tintColor: checkIsUnfollow() ? WHITE : LIGHTGREY, marginRight: 10 }} ></Image>
                        <Text style={{ ...styles.txtStoreName, color: checkIsUnfollow() ? 'white' : LIGHTGREY }}>{checkIsUnfollow() ? 'Follow' : 'Following'}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ ...styles.btnWhiteBorder }} onPress={() => {
                        navigation.push('AboutStore')
                      }}>
                        <Image source={STORE_ICON} style={{ width: 20, height: 20, tintColor: WHITE, marginRight: 10 }} ></Image>
                        <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'About Store'}</Text>
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{ ...styles.btnWhiteBorder, marginTop: 10, width: undefined, marginRight: 3 }} onPress={() => {
                      setModalVisible(true)
                    }}>
                      <Image source={DISCOUNT_ICON} style={{ width: 20, height: 20, tintColor: '#ffffff', marginRight: 10 }} ></Image>
                      <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'Additional Discounts'}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <FlatList
                style={{ width: '100%', height: 40, paddingHorizontal: 10, marginTop: 10 }}
                data={categoryType}
                horizontal
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => renderChipItem(item, index)}
              />
              <FlatList
                style={{ width: '100%', paddingHorizontal: 10, marginTop: 10 }}
                data={homeProducts}
                horizontal={false}
                renderItem={({ item, index }) => renderBannerItem(item, index)}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
              />
              <FlatList
                style={{ width: '100%', paddingHorizontal: 10, marginTop: 10 }}
                data={products}
                numColumns={2}
                renderItem={({ item, index }) => renderProductItem(item, index)}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </ScrollView>)}
        <LoginAlert
          showAlert={showLoginAlert}
          onLoginPress={() => {
            //  props.setUserData(null);
            Logout(navigation);
            setShowLoginAlert(false);
          }} onCancelPress={() => {
            setShowLoginAlert(false)
          }} />
        <LoaderAlert
          showAlert={showLoader}
        />
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
        style={{
          borderWidth: 0,
          width: '100%',
          borderRadius: 5,
          overflow: 'hidden',
          marginTop: 30,
          marginBottom: 30,
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 22, backgroundColor: '#70757ab5' }}>
          <View style={{
            margin: 5, backgroundColor: 'white', borderRadius: 20, padding: 5, alignItems: 'center', shadowColor: '#000', shadowOffset: {
              width: 0,
              height: 2,
            }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, maxHeight: Dimensions.get('screen').height - 200
          }}>
            <ScrollView showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 17, width: '100%', textAlign: 'center', marginTop: 10 }}>
                  Apply Discount at Checkout
                </Text>
                <View style={{ marginTop: 15, paddingHorizontal: 10 }}>
                  {Object.keys(storeDetails).length != 0 && storeDetails['shopifyDiscount'].length > 0 ?
                    storeDetails['shopifyDiscount'].map((item: any) => {
                      return (
                        <View style={{ flexDirection: 'row', marginTop: 6, right: 10 }} key={Math.floor(Math.random() * 100000)}>
                          <Text style={{ fontSize: 15, marginLeft: 8 }}> {'->'}</Text>
                          {Platform.OS === 'ios' ?
                            <TextInput
                              value={item}
                              editable={false}
                              multiline
                            /> :
                            <Text style={{ fontSize: 15, marginLeft: 10, marginRight: 10 }} selectable={true} selectionColor='#c0c8cf'>{item}</Text>}
                        </View>
                      )
                    }) :
                    <View style={{ marginTop: 20, paddingHorizontal: 10, height: 100 }}>
                      <Text style={{ fontWeight: 'bold', fontSize: 13, textAlign: 'center', marginTop: 20 }}>
                        No additional discounts available at this moment
                  </Text>
                    </View>
                  }
                </View>
              </View>
            </ScrollView>
            <View style={{ alignItems: 'center', marginTop: 50 }}>
              <TouchableOpacity style={{ ...styles.btnWhiteBorder, width: 100, backgroundColor: '#007bff', marginBottom: 18, borderRadius: 5 }} onPress={() => {
                closeModal();
              }}>
                <Text style={{ ...styles.txtStoreName, color: 'white', fontSize: 15, marginTop: -3 }}>{'Close'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};



const mapStateToProps = (state) => ({
  getFavData: state.productTab.favourite,
  storeId: state.productTab.storeKey,

  offerId: state.productTab.offerId,
  getLocation: state.location,
  getFavStore: state.productTab.followStore


});
const mapDispatchToProps = dispatch => ({
  getStoreDetails: (userId) => dispatch(getStoreDetails(userId)),
  getHomeTabProducts: (userid) => dispatch(getHomeTabProducts(userid)),
  addToFavourite: (data) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data) => dispatch(deleteToFavourite(data)),
  getAllTabs: (data) => dispatch(getAllTabs(data)),
  getProductsTabsProducts: (data) => dispatch(getProductsTabsProducts(data)),
  getCollectionsByStoreTabProducts: (data) => dispatch(getCollectionsByStoreTabProducts(data)),
  setProductKey: (data) => dispatch(setProductKey(data)),
  getOffersProducts: (offerId) => dispatch(getOffersProducts(offerId)),
  getOffersStoreId: (storeId) => dispatch(getOffersStoreId(storeId)),
  addFollowingApi: (data, userId) => dispatch(addFollowingApi(data, userId)),
  setStoreKey: (data) => dispatch(setStoreKey(data)),
  setOfferId: (data) => dispatch(setOfferId(data)),
  getAllFollowStore: (userId) => dispatch(getAllFollowStore(userId)),
  addToFollow: (data) => dispatch(addToFollow(data)),
  addLikeProducts: (data, userId) => dispatch(addLikeProducts(data, userId)),
  removeLikeProducts: (userId, item) => dispatch(removeLikeProducts(userId, item)),
  getAllFavoriteProduct: (userId) => dispatch(getAllFavoriteProduct(userId)),
  getStoreFollowList: (storeId) => dispatch(getStoreFollowList(storeId))



});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoresDes)


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    resizeMode: 'contain',
    height: 160,
    marginTop: 10,
  },

  txtStoreName: {
    paddingVertical: 10,
    color: 'white',
    fontWeight: 'bold'
  },

  txtStoreDetailsTitle: {
    color: 'white',
    flex: 0.9,
    fontWeight: 'normal',
    marginVertical: 1,
    fontSize: 9,
  },
  txtStoreDetailsDesc: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 12,
  },
  txtDesc: {
    color: 'white',
  },
  socialMedia: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  socialMediaInner: {
    resizeMode: 'cover',
    height: 50,
    width: 50,
    margin: 5

  },
  btnWhiteBorder: {
    borderColor: 'white',
    borderWidth: 1,
    marginLeft: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,

  },

  btnDisableWhite: {
    borderColor: DISABLED_COLOR,
    backgroundColor: GREY,
    borderWidth: 1,
    marginLeft: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,
  },
  btnSolid: {
    backgroundColor: '#1776d3',
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: Dimensions.get('window').width / 2 - 25,
    height: 35,

  },
  forgottext: {
    alignSelf: 'flex-end',
    fontSize: 16,
    marginTop: 10,
    marginEnd: 25,
    marginBottom: 10,
  },
  textStyle: {
    fontSize: 16,
    fontWeight: 'bold',
  }
})