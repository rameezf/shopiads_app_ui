import React, { useEffect, useRef, useState } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';

import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { BLUE, WHITE } from '../Constants/colors';

const CameraRecord = ({ ...props }) => {

  const videoRecorder = useRef(null)

 
 const start = () => {
  if (videoRecorder && videoRecorder.current) {
    videoRecorder.current.open({ maxLength: 30 }, (data) => {
      console.log('captured data', data);
    });
  }
 }

  
  return (
    <View style={{alignItems:'center',justifyContent:'center',width:'100%',height:'100%'}}>
  <TouchableOpacity style={{
    backgroundColor:BLUE,borderRadius:5,width:150,alignItems:'center',justifyContent:'center'
  }} onPress={()=>{
    start();
  }}>
		  	<Text style={{padding:10,color:WHITE,textAlign:'center'}}>{'Start Video Recording'}</Text>
		  </TouchableOpacity>
   </View>
  );
};


const mapStateToProps = (state) => ({
  
  });
  const mapDispatchToProps = (dispatch) => ({
   
  });
  export default connect(
    mapStateToProps,mapDispatchToProps
  )(CameraRecord)