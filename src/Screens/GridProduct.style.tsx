import { Dimensions, StyleSheet } from "react-native";
import { BLACK, GREY } from "../Constants/colors";


export const style = StyleSheet.create({
    container: {
        borderRadius:5,
        backgroundColor: '#ffffff',
        elevation:3,

        margin: 4,
    },
    imgContainer:{
        height: Dimensions.get('window').height / 2 -200, width: '100%'
    },
    img:{
        borderTopLeftRadius:5,borderTopRightRadius:5,
        height: Dimensions.get('window').height / 2-200, width: '100%',resizeMode: 'stretch'
    },
   
    txtStoreName:{
        paddingHorizontal:5,
        marginTop:4,
        fontSize: 13,width:'100%', color: GREY,
    },
    imgLogo:{
        resizeMode: 'contain', height: 15, width: 30
    },
    txtProductName:{
        fontWeight:'bold',
      
        paddingHorizontal:5,
        marginTop:2,
        fontSize: 13,width:'100%', color: BLACK
    },
    txtPriceContainer:{
        paddingHorizontal:5,
        flexDirection:'row',width:'100%',alignItems:'center'
    }
    ,
    txtCurrentPrice:{
        fontSize: 15, color: '#1776d3'
    },
    txtMrpPrice:{
        paddingLeft:5,
        marginTop:3,
        fontSize: 11, color: '#545454',textDecorationLine: 'line-through', textDecorationStyle: 'solid'
    },
    ratingLikeContainer:{
        width:'100%',
        marginBottom:10,
        paddingHorizontal:5,
        flexDirection: 'row',
    },
    ratingContainer:{
        flex: 1,flexDirection:'row',alignItems:'center',justifyContent:'center',
    },
    txtRating:{
        paddingLeft:5,
        fontSize: 15,flex:1,alignSelf:'flex-end', color: '#000',    },
    likeContainer:{flexDirection:'row',alignItems:'center',justifyContent:'center',},
    txtLikes:{fontSize: 15, color: '#000',paddingLeft:5}
  })