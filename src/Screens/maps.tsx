import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, PermissionsAndroid, Alert } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker, Circle } from "react-native-maps";
import { Heading } from '../Components/Heading'
import Slider from 'react-native-slider';

interface ILocation {

  latitude: number;
  longitude: number;
}

export const Maps = ({ navigation, route }) => {

  const [value, setValue] = useState(100)
  const [longitude, setLongitude] = useState(.0);
  const [latitude, setLatitude] = useState(.0);


  useEffect(() => {
    requestLocationPermission()

  }, [])

  const geoEncoding = (latt, long) => {
   
  }

  const requestLocationPermission = async () => {
    const chckLocationPermission = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    if (chckLocationPermission === PermissionsAndroid.RESULTS.GRANTED) {
      alert("You've access for the location");
    } else {
      try {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'Cool Location App required Location permission',
            'message': 'We required Location permission in order to get device location ' +
              'Please grant us.'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        
        } else {
          alert("You don't have access for the location");
        }
      } catch (err) {
        alert(err)
      }
    }
  };



  const save = () => {
    alert('location saved successfully')
    navigation?.navigate('home')
  }

  return (
    <View style={{ flex: 1 }}>
      <Heading title={"Your Location"} rightText={"save"} onRightPress={save} />
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.0100,
          longitudeDelta: 0.0100,
        }}
      >
        <Circle center={{ latitude: latitude, longitude: longitude, }}
          radius={value}
          fillColor={'rgba(200,300,200,0.5)'} />
        <Marker
          draggable={true}
          coordinate={{ latitude: latitude, longitude: longitude, }} />
      </MapView>
      <View style={{ flex: 0.1, padding: 10 }}>
        <Text style={{ fontSize: 16, alignSelf: 'center' }}>Set your radius</Text>
        <Text>{'Radius: ' + parseInt(value) + " Kms"}</Text>
        <Slider
          minimumValue={1}
          maximumValue={200}
          onValueChange={(val) => setValue(val)}
          value={value}
        />


      </View>
    </View>



  )
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    flex: 0.75
  },
});