import { StyleSheet } from "react-native";


export const style = StyleSheet.create({
   
   
    txtCategoryName:{
        fontSize:13,fontWeight:'bold',paddingHorizontal:10,flex:1
    },
    txtViewAll:{
        
        fontSize:13, color:'#1776d3',paddingHorizontal:10,
    }, 
    container: {
        backgroundColor: '#ffffff',
        elevation:5,
        marginRight:5,
        borderRadius:5,
    },
  
    img:{
        borderTopLeftRadius:5,borderTopRightRadius:5,
        resizeMode: 'contain',
        height: 210, 
        width: 300,
        paddingHorizontal:5
    },
   
    txtStoreInfoContainer:{
        marginTop:5,
        width:190,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    imgLogo:{
        resizeMode:'contain', height: 30, width: 50
    },
    txtStoreName:{
       width:120, color: '#000',paddingHorizontal:5
    },
    likeContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginLeft:"10%"
    },
    likeIcon:{
        width:70,
        alignSelf:'flex-end',
        alignContent:'flex-end',
        alignItems:'flex-end',
        justifyContent:'flex-end'
    },
    
    ratingContainer:{
        marginTop:3,
        paddingHorizontal:0,
        width:190,paddingBottom:10,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    txtRating:{
        fontSize: 13,flex:1,alignSelf:'flex-end', color: '#000',paddingHorizontal:5,
    },
  
  })