import React from 'react'
import {View,Text,Image,Animated,StyleProp, ViewStyle} from 'react-native'
import {logo} from '../Constants/images'


interface splashprops{
    splashStyle:StyleProp<ViewStyle>
}
 const Splash =({splashStyle}:splashprops)=>{

    React.useEffect(()=>{
      Animated.timing(opacity,{
          useNativeDriver:true,
          toValue:0,
          duration:1000,
      }).start()
    },[])
    const opacity = React.useState(new Animated.Value(1))[0]
    return (
     <Animated.View style={[{flex:1, alignItems:'center',opacity, justifyContent:'center',}]}>
         <Image source={logo} style={{resizeMode:'contain',height:200}}/>
     </Animated.View>
    )
}

export default Splash