import React, { useEffect, useState, useRef, useLayoutEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image,
  ScrollView,
  Alert,
  SectionList,
  StyleSheet,
  Linking,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { onSnipsSelected, onSnipsDeleted, setProductKey, addToSnips, setStoreKey, setOfferId } from '../Actions/product';
import Icons from '../Constants/Icons';
import Modal from 'react-native-modalbox';

import {
  productCasualShirt,
  DELETE_ICON,
} from '../Constants/images';
import { CheckBox } from 'react-native-elements';
import { Buffer } from "buffer"

import { deleteSnippets, getUserSnippets } from './Snip';
const DELETE_EXPIRED = "Expired Items";
const DELETE_ALL = "All Items";
const DELETE_CHECKED_ITEM = "Checked Items";

import { BLACK, GREY } from '../Constants/colors';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import ThemeButton from '../Components/Button';
import { ThemeImage } from '../Components/ThemeImage';
import { offerBuyNowClick, productBuyNowClick } from '../Actions/Details';
const Snips = ({ navigation, ...props }) => {
  const [dataDelete, setDataDelete] = React.useState([]);
  const [snipsData, setSnipsData] = useState([]);
  const [modalVisible, setModalVisible] = React.useState(false);
  const deleteArr = [DELETE_EXPIRED, DELETE_ALL, DELETE_CHECKED_ITEM];
  const deleteArr1 = [DELETE_EXPIRED, DELETE_ALL];
  const [quantityValue, setQuantityValue] = useState(0);

  const [deletePos, setDeletePos] = React.useState("");
  const [deleteChecked,setDelecteChecked] = React.useState("");
  const [showLoader, setShowLoader] = React.useState(false);
  
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUserSnippets();
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const onAddDeleteToSnips = (item: any) => {

    if (dataDelete.length == 0) {
      let data: any = [];
      data.push(item);
      setDataDelete(data);
    } else {
      const found = dataDelete.some((el) => el.id === item.id);
      if (found) {
        const data: any = [];
        for (let i = 0; i < dataDelete.length; i++) {
          if (dataDelete[i].id != item.id) {
            data.push(dataDelete[i])
          }
        }
        setDataDelete(data);
      } else {
        const data: any = [];
        for (let i = 0; i < dataDelete.length; i++) {
          data.push(dataDelete[i])
        }
        data.push(item);
        setDataDelete(data);
      }
    }
  };

  const foundOnDeleteToSnips = (item) => {
    const found = dataDelete.some((el) => el.id === item.id);
    return found
  }
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (props) => (
        <TouchableOpacity onPress={() => setDeleteItem(true)}>
          <Icons.Ionicons name={'trash'} size={25} color={'#1776d3'} />
        </TouchableOpacity>
      ),
    });
  });


  const firstUpdate = useRef(true);

  useLayoutEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
    }
  });


  const getUserSnippets = () => {
    if (props.getLocation.email) {
      setShowLoader(true);
      let email = Buffer.from(props.getLocation.email).toString('base64')
      props.getUserSnippets(email).then((res) => {
        let result = res.payload.data;
        console.log('DAKSKSSKS',result);
        const cats = result.map(q => q.storeId);
        let filterUniqueStoreID = cats.filter((q, idx) => cats.indexOf(q) === idx)
        let enumData = [] as any;
        for (let i = 0; i < filterUniqueStoreID.length; i++) {
          let data = result.filter(data => data.storeId == filterUniqueStoreID[i])
          console.log('VKLDFKFKFKFKF',data[0]);
          let obj = { 'data': data, 'storeName': data[0].storeName +(data[0].storeCode && data[0].storeCode == "Primary"?"":", Branch: "+data[0].storeCode)+"" }
          enumData.push(obj);
        }
        props.addToSnips(result)
        setSnipsData(enumData);
        setShowLoader(false);
      });
    }
  }




  const removeSnippets = (items, pos) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.deleteSnippets(email, items[pos]).then((res) => {
      setShowLoader(true);
      console.log('ITEMS', items[0]);
      if (items.length - 1 > pos) {
        removeSnippets(items, pos + 1)
      } else {
        getUserSnippets();
        setDataDelete([]);

      }
    });
  };

  const checkIsExpired = (date:string) =>{
    console.log('VKVKKFKFKFKFKFKFKF',date)
    if (date ==  "N") {
      return false;
    }else{
      return true;
    }
  }
  const onPressDelete = () => {
    if (deletePos == DELETE_CHECKED_ITEM) {
      if (dataDelete.length == 0) {
        Alert.alert("Please checked atleast single items");
      } else {
        removeSnippets(dataDelete, 0);
      }
    } else if (deletePos == DELETE_ALL) {
      let data: any = [];

      for (let i = 0; i < snipsData.length; i++) {
        let obj = snipsData[i].data;
        for (let j = 0; j < obj.length; j++) {
          data.push(obj[j]);
        }

      }
      removeSnippets(data, 0);
    } else if (deletePos == DELETE_EXPIRED) {
      let data: any = [];
      for (let i = 0; i < snipsData.length; i++) {
        let obj = snipsData[i].data;
        if(deleteChecked){
          for(let i=0; i<dataDelete.length;i++){
            data.push(dataDelete[i])
          }
        }
        for (let j = 0; j < obj.length; j++) {
          let innerObj = obj[j]
          if (innerObj.type == 'Product') {
            try {
              let obj= data.filter(data => data.id === innerObj.id);    
              if(obj.length==0 ){
                if (innerObj.isExpire != "N") {
                  data.push(innerObj);
                }
              }
              
            } catch (e) {

            }

          } else {
            let offerDate = new Date(innerObj.offer.validTill);
            console.log(offerDate);
            let todayDate = new Date();
            if (todayDate.getTime() < offerDate.getTime()) {
              data.push(innerObj);
            }
          }
        }
      }
      if (data.length != 0) {
        removeSnippets(data, 0);
      } else {
        Alert.alert("Expired products not found.");
      }
    }else if(deleteChecked && dataDelete.length>0){
      removeSnippets(dataDelete, 0);
    }

  }


  const renderTitleName = (item: any) => {
    return (<View style={{ width: '100%', flexDirection: 'column' }}>
      <Text style={{ ...styles.txtStoreName, marginTop: 5, paddingHorizontal: 10 }}>{item.storeName}</Text>
      <FlatList
        style={{ width: '100%', paddingHorizontal: 10, marginTop: 10 }}
        data={item.data}
        renderItem={({ item, index }) => item.type == "Offer" ? renderOfferItem(item) : renderProductItem(item)}
      />
      <View style={{ width: '100%', marginTop: 10, height: 7, backgroundColor: '#f4f4f4' }} />

    </View>
    );
  };


  const renderProductItem = (item: any) => {
    return (<TouchableOpacity style={{ width: "100%", marginVertical: 10, flexDirection: 'column' }}
      onPress={() => {
        props.setProductKey(item.product.id)
        navigation.navigate('ProductDesc', { itemType: item })
        //   onAddDeleteToSnips(item);
      }}
    >
      <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }} >
        <CheckBox
          onIconPress={() => {
            onAddDeleteToSnips(item);
          }}
          checked={foundOnDeleteToSnips(item)}
        />
        <ThemeImage
          source={item.product.images.length == 0 ? productCasualShirt : { uri: item.product.images[0] }}
          style={{ width: 100, height: 80, borderRadius: 5 }}/>
        <View style={{ flexDirection: 'column', flex: 1 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ ...styles.txtDesc, flex: 1, paddingHorizontal: 10 }}>{item.product.name}</Text>
            <Text style={{ ...styles.txtCurrentPrice, paddingHorizontal: 10 }}>{"$"  + item.product.offerPrice}</Text>
          </View>
          <View style={{
            flexDirection:'row',
            marginTop:10,
            flex:1,
          }}>
         <Text style={{flex:1,color:'red',fontWeight:'600',paddingStart:10 }}>{ (checkIsExpired(item.isExpire)?'Expired':'')}</Text>
         { item.product['isShopifyProduct'] !== "Y" ? <ThemeButton  
          btnTextStyle={{ fontSize:12 }}
          btnThemeStyle={{ width:70 }}
          btnMainStyle={{ width:70,margin:0,flex:0,height:30 }}
            onBtnPress={() => {
              if(props.getLocation.email){
                let email = Buffer.from(props.getLocation.email).toString('base64')
                props.productBuyNowClick(email,item.product.id).then((res) => {
                  console.log('CCCCCCCDDDKDDJDJDJDJDJD',res.payload.data);
                });
                if (item.product['isShopifyProduct'] === "Y") {
                  let url = props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId']=="US"?"https://www.shopiads.com/redirectToCart/"+item.product['id']+"/"+props.getLocation.email+"/1":"https://www.shopiads.ca/redirectToCart/"+item.product['id']+"/"+props.getLocation.email+"/1";
                  // let url = "https://"+storeInfo['websiteUrl']+"/cart/"+productInfo['shopifyVarientId']+":1?checkout[email]="+props.getLocation.email+"&attributes[utm_source]=shopiads"
                  console.log("Product Url :::",url);
                  Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
                }
                  else if (item.product['isShopifyProduct'] !== "Y"&& item.product['vendorUrl']) {
                  Linking.openURL(item.product['vendorUrl']+"?utm_source=shopiads").catch(err => console.error("Couldn't load page", err));
                  //let url = "https://www.shopiads.ca/redirectToCart/"+item.product['id']+"/"+props.getLocation.email;
                  // let url = "https://"+storeInfo['websiteUrl']+"/cart/"+productInfo['shopifyVarientId']+":1?checkout[email]="+props.getLocation.email+"&attributes[utm_source]=shopiads"
                  //console.log("Product Url :::",url);
                  // Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
                  }else{
                  Alert.alert("URL not found")
                }
              }
              }}
            btnText={'Buy Now'} /> : 
                <View style={{flexDirection:'row'}}>
                  <TouchableOpacity style={{...styles.qtyBlueBorder,marginLeft: 2}} onPress={() => {
                      item["quantity"] = item.quantity ? item.quantity - 1 : 1;
                      setQuantityValue(item.quantity);
                    }}>
                      <Text style={styles.txtProductName}>-</Text>
                  </TouchableOpacity>
                  <View style={{paddingRight:8,paddingLeft:8,marginTop:Platform.OS == 'ios' ? 1 : -3 }}>
                      <Text style={{fontSize:20}}>{item.quantity ? item.quantity : 1}</Text>
                  </View>
                  <TouchableOpacity style={{...styles.qtyBlueBorder}} onPress={() => {
                       item["quantity"] = item.quantity ? item.quantity + 1 : 2;
                       setQuantityValue(item.quantity);
                    }}>
                    <Text style={styles.txtProductName}>+</Text>
                  </TouchableOpacity>
                </View>
            }
          </View>
        </View>
      </View>
      <View style={{ width: '100%', height: 1, backgroundColor: '#f4f4f4', marginTop: 5 }} />
    </TouchableOpacity>
    );
  }



  const renderOfferItem = (item: any) => {
    return (<TouchableOpacity style={{ width: "100%", marginVertical: 10, flexDirection: 'column' }}
      onPress={() => {
        props.setStoreKey(item.storeId)
        props.setOfferId(item.offerId)
        navigation.push('OfferDesc');
      }}
    >
      <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }} >
        <CheckBox
          checked={foundOnDeleteToSnips(item)}
          onIconPress={() => {
            onAddDeleteToSnips(item);
          }}
      />
        <ThemeImage
          source={{ uri: item.offer.fullImage }}
          
          style={{ width: 100, height: 80, borderRadius: 5 ,resizeMode: 'stretch',}}/>
        <View style={{ flexDirection: 'column', flex: 1 }}>
        <Text style={{ ...styles.txtDesc,  paddingHorizontal: 10 }}>{item.offer.heading}</Text>
       
          <View style={{
            flexDirection: 'row',
            marginTop: 10,
            flex: 1,
          }}>
            <Text style={{ flex: 1, color: 'red', fontWeight: '600', paddingStart: 10 }}>{(checkIsExpired(item.isExpire) ? 'Expired' : '')}</Text>
            <ThemeButton
              btnTextStyle={{ fontSize: 12 }}
              btnThemeStyle={{ width: 70 }}
              btnMainStyle={{
                width: 70,
                margin: 0,
                flex: 0,
                height: 30
              }}
              onBtnPress={() => {
                if(props.getLocation.email){
                  let email = Buffer.from(props.getLocation.email).toString('base64')
                  props.offerBuyNowClick(email,item.offerId).then((res) => {
                  });
                   
                  if (item.offer['vendorUrl']) {
                    Linking.openURL(item.offer['vendorUrl']+"?utm_source=shopiads").catch(err => console.error("Couldn't load page", err));
                  } else {
                    Alert.alert("URL not found")
                  }
                }
              }}
              btnText={'Shop Now'} />
          </View>

        </View>
      </View>
      <View style={{ width: '100%', height: 1, backgroundColor: '#f4f4f4', marginTop: 5 }} />
        </TouchableOpacity>
    );
  }


  const buyMoreProduct = () => {
      const buyProduct = dataDelete;
      if(buyProduct.length > 0){
        console.log("Buy Product ::::",buyProduct);
        let offerSnip = buyProduct.filter(item => {
          return item['type'] == "Offer";
         });
        if(offerSnip != undefined && offerSnip !=null && offerSnip.length > 0) {
          Alert.alert("Offers are for Ads purposes, to buy select products")
        } else {
          const storeId = buyProduct[0]['product']['storeId'];
          let varientIds = buyProduct[0]['product']['shopifyVarientId']+":1";
          let isPass = true;
          let productIdsAndQuantity:any = [];
          for (let i = 0; i < buyProduct.length; i++) {
              let selectedProduct = buyProduct[i];
              let qty = selectedProduct['quantity'] ? selectedProduct['quantity'] : 1;
              productIdsAndQuantity.push(`${selectedProduct['product']['shopifyVarientId']}:${qty}`);
              if(selectedProduct['product']['isShopifyProduct'] == "Y") {
                  if(storeId == selectedProduct['storeId']) {
                    varientIds = varientIds+","+selectedProduct['product']['shopifyVarientId']+":1";
                  } else {
                    isPass=false;
                    Alert.alert("You can buy multiple products from the same store at a time")
                    break; 
                  }
              } else {
                  isPass=false;
                  Alert.alert("You can buy multiple products from the same store at a time")
                  break;
              }
          } 
          if(isPass) {
            console.warn("Product Ids ::::",productIdsAndQuantity);
            
            const arrayToString = productIdsAndQuantity.join();
          let url = props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId']=="US"?"https://shopiads.com/redirectToBulkCart/"+arrayToString+"/"+props.getLocation.email+"/"+storeId+ "/1":"https://shopiads.ca/redirectToBulkCart/"+arrayToString+"/"+props.getLocation.email+"/"+storeId+ "/1";
            // let url = "https://"+storeInfo['websiteUrl']+"/cart/"+productInfo['shopifyVarientId']+":1?checkout[email]="+props.getLocation.email+"&attributes[utm_source]=shopiads"
            console.warn("Product Url :::",url);
            Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
          }
        }
      }  
   };

   
  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ width: '100%', flexDirection: 'row', padding: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
          <Text style={{ ...styles.txtStoreName, marginTop: 5, paddingHorizontal: 5, flex: 1 }}>{'MY LIST'}</Text>
          <ThemeButton  
            btnTextStyle={{fontSize:12}}
            btnThemeStyle={{width:70,marginRight:20}}
            btnMainStyle={{ width:70,margin:0,flex:0,height:30}}
            onBtnPress={() => { buyMoreProduct()}}
            btnDisable={dataDelete.length > 0 ? false : true}
            btnText={'Buy Now'} 
          />
          <TouchableOpacity onPress={() => {
            setDeletePos("")
            setDelecteChecked("")
            setModalVisible(true);
          }} style={{marginRight:10}}>
            <Image source={DELETE_ICON} style={{ width: 20, height: 20 }}></Image>
          </TouchableOpacity>
        </View>
      <View style={{ width: '100%', height: 1, backgroundColor: '#f4f4f4',  }} />

      <ScrollView style={{ flex: 1 }}>
       
        {
          snipsData.map((item) => {
            return renderTitleName(item);
          })}


      </ScrollView>
      <Modal
        isOpen={modalVisible}
        position={'bottom'}
        backdropPressToClose={true}
        onClosed={() => setModalVisible(false)}
        useNativeDriver={true}
        animationDuration={100}
        style={{
          borderWidth: 0,
          width: '100%',
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          height: '40%',
          overflow: 'hidden',
          margingTop: 30,
        }}>
        <View style={{ margin: 10, flex: 1 }}>
          <View style={{ flex: 0.1 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 15, width: '100%', textAlign: 'center' }}>
              Delete Items
            </Text>
          </View>

          <View style={{ flex: 0.8, marginTop: 10 }}>

            {(dataDelete.length == 0 ? deleteArr1 : deleteArr).map((i) => {
              return (
                <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 8 }}
                  onPress={() => {
                    if(i!=DELETE_CHECKED_ITEM){
                      setDeletePos(i);
                    }else{
                      if(deleteChecked){
                        setDelecteChecked("");

                      }else{
                        setDelecteChecked(i);
                      }
                    }
                  }}
                >
                  <View style={{ flex: 0.1 }}>
                    {i == deletePos || i == deleteChecked ? (
                      <Icons.Feather
                        name={'check'}
                        size={22}
                        color={'#1776d3'}
                      />
                    ) : null}
                  </View>

                  <Text style={{ paddingLeft: 10 }}>{i}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <View
            style={{
              flex: 0.1,
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}>
            <TouchableOpacity onPress={() => setModalVisible(false)}>
              <Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 10 }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                onPressDelete()
              }}>
              <Text
                style={{
                  color: '#FF0000',
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginLeft: 10,
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <LoaderAlert
        showAlert={showLoader}
      />

    </View>
  );
};
const mapStateToProps = (state) => ({
  getLocation: state.location,
  snipsData: state.productTab,
});
const mapDispatchToProps = (dispatch) => ({
  onSnipsSelected: (data) => dispatch(onSnipsSelected(data)),
  onSnipsDeleted: (data) => dispatch(onSnipsDeleted(data)),
  getUserSnippets: (data) => dispatch(getUserSnippets(data)),
  deleteSnippets: (userId, data) => dispatch(deleteSnippets(userId, data)),
  setProductKey: (productId:string) => dispatch(setProductKey(productId)),
  addToSnips:(data:any) => dispatch(addToSnips(data)),
  setStoreKey: (storeId:string) => dispatch(setStoreKey(storeId)),
  setOfferId: (offerId:string) => dispatch(setOfferId(offerId)),
  productBuyNowClick: (email:string,productId:string) => dispatch(productBuyNowClick(email,productId)),
  offerBuyNowClick: (email:string,offerId:string) => dispatch(offerBuyNowClick(email,offerId))
});
export default connect(mapStateToProps, mapDispatchToProps)(Snips);




const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  txtStoreName: {
    paddingVertical: 10,
    color: 'black',
    fontWeight: 'bold'
  },
  txtReview: {
    paddingVertical: 10,
    color: 'black',
    fontWeight: 'bold'
  },
  txtDesc: {
    color: 'black',
  },

  txtCurrentPrice: {
    fontSize: 15, color: '#1776d3'
  },
  txtPrice: {
    fontSize: 14,
    color: '#1776d3',
    fontWeight: 'bold'
  },
  txtActualPrice: {
    fontSize: 13,
    color: '#545454', textDecorationLine: 'line-through', textDecorationStyle: 'solid',
    fontWeight: 'bold'
  },
  btnBlueBorder: {
    borderColor: '#1776d3',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: 130,
    height: 35,

  }, btnSolid: {
    backgroundColor: '#1776d3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: 100,
    height: 35,

  }
  , txtStoreDetailsTitle: {
    color: 'grey',
    width: '100%',
    fontSize: 12
  },
  txtStoreDetailsDesc: {
    color: 'black',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'right',
    fontSize: 12
  },
  btnOrange: {
    flexDirection: 'row',

    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#C96349',
    flex: 1
  },
  btnBuyNow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#1776d3',
    flex: 2
  },
  qtyBlueBorder: {
    borderColor: '#1776d3',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    width: 25,
    height: 25,
  },
  txtProductName: {
    paddingHorizontal: 5,
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold'
  },

})