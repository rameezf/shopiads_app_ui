import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { View, Text, Alert, FlatList, Image, StyleSheet, TextInput, Linking } from 'react-native'
import { Container } from "native-base";
import { BackHeader } from '../Components/Heading';
import { connect } from 'react-redux'
import Icons from '../Constants/Icons'
import {
    productCasualShirt,
} from '../Constants/images';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ThemeButton from '../Components/Button';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getAllFollowStore } from '../Actions/Offer';
import { Buffer } from "buffer"
import { getStoreDetails, removeLikeProducts } from '../Actions/StoreDetails';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { addToFollow, setStoreKey } from '../Actions/product';
import { GREY, LIGHTGREY } from '../Constants/colors';
import * as RNLocalize from "react-native-localize";
import DeviceCountry from 'react-native-device-country';

const MyProfile = ({ navigation, ...props }) => {

    const [ country, setCountry ]= useState('CA');

    useEffect(()=> {
        // DeviceCountry.getCountryCode().then((result) => {
        //     console.log("Device Country Code :::::",result);
        //     setCountry(result.code);
        //   }).catch((e) => {
        //     console.log(e);
        //   });
        setCountry(RNLocalize.getCountry());
      },[]);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff',flex:1 }} edges={['top']}>
            <BackHeader title={"My Profile"}
                fontSize={28}
                onFontPress={() => navigation?.pop()}
                fontName={'chevron-thin-left'}
            />
            <Text style={{fontSize:12,padding:10,marginTop:10,marginStart:10}}>{'User Name'}</Text>
            <View style={{backgroundColor:LIGHTGREY,borderRadius:45,height:45,justifyContent:'center',paddingHorizontal:10,marginHorizontal:10}}>
            <Text style={{fontSize:12}}>{props.getLocation.name}</Text>
            </View>
            <View style={{flex:1,justifyContent:'flex-end',alignItems:'center',width:'100%'}} >
                <View style={{alignItems:'center',paddingHorizontal:10,marginHorizontal:10,width:'100%',flexDirection:'column'}}>
                    <TouchableOpacity style={{padding:10}} onPress={()=>{
                        Linking.openURL( country.toUpperCase() == "US" ? "https://www.shopiads.com/privacyPolicy" : "https://www.shopiads.ca/privacyPolicy" ) 
                    }}>
                        <Text style={{fontSize:12}}>{'Terms of Use * Privacy Policy'}</Text>
                    </TouchableOpacity>
                </View>
            </View> 
        </SafeAreaView>
    )
}

const mapStateToProps = state => ({
  getLocation: state.location,

})


export default connect(
    mapStateToProps,
    null
)(MyProfile)


const styles = StyleSheet.create({


    txtStoreName: {
        paddingVertical: 10,
        color: 'black',
        fontWeight: 'bold'
    },
    inputStyleBg: {
        padding: 5,
        width: '100%',
        borderRadius: 4,
        fontSize: 13
    },
    inputStyle: {
        padding: 5,
        flex: 1,
        borderRadius: 4,
        fontSize: 13
    }
})