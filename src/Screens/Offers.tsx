import React, { useContext, useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  RefreshControl,
  Linking,
} from 'react-native';
import { Button, Icon, Item } from 'native-base';
import { getOffersAll, getOfferCategory, getMainCategory, getOfferBanner, getOfferProducts, getAllStoreList, getAllFavoriteProduct, getAllFollowStore, getOffersAllActiveSectionItems, getSubCategoriesList } from '../Actions/Offer';
import {
  setProductKey,
  addToFavourite,
  deleteToFavourite,
  setStoreKey,
  setAllStore,
  setOfferId,
  addToFollow,
  addToViewAllProduct,
  addToSnips,
  addToCategories,
  addToSubCategories,
} from '../Actions/product';
import { connect } from 'react-redux';
import MyChip from '../Components/chips';
import ViewPager from '@react-native-community/viewpager';
import { Avatar } from 'react-native-paper';
import {
  TRENDING,
  DOWNARRAY
} from '../Constants/images';
import { Overlay, Rating } from 'react-native-elements';
import RNRestart from 'react-native-restart';
const AnimatedPagerView = Animated.createAnimatedComponent(ViewPager);


import { BGCOLOR, BLACK, BLUE, WHITE } from '../Constants/colors'
import Icons from '../Constants/Icons';
import { style as storeStyle } from './Store.style';
import { Product } from '../Components/Product';
import { Store } from '../Components/Store';
import { AuthContext } from '../Context/Context';
import { Buffer } from "buffer"
import { addLikeProducts, removeLikeProducts } from '../Actions/StoreDetails';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { getUserSnippets } from './Snip';
import { BannerStore } from '../Components/BannerStore';
import Animated from 'react-native-reanimated';
import FlatListSlider from '../Components/FlatListSlider';
import { MarketPlaceAlert } from '../NavigationStack/MarketPlaceAlert';
import { saveUserMarketPlace } from '../Actions/location';

let trendingObj = {
  "id": "Trending",
  "name": "Trending",
  "image": "",
  "root": "Y",
  "priority": 1,
  "subcategories": [
  ],
  "maincategories": [
  ],
  "createdOn": "2021-11-01T06:51:14.974+0000",
  "updatedOn": "2021-11-01T06:51:14.142+0000",
  "createdBy": "dharmveer.siet@gmail.com",
  "updatedBy": "dharmveer.siet@gmail.com"
};

const { width } = Dimensions.get('window');
//you need to preview n items.
const previewCount = 3;
//to center items
//the screen will show `previewCount` + 1/4 firstItemWidth + 1/4 lastItemWidth 
//so for example if previewCount = 3
//itemWidth will be =>>> itemWidth = screenWidth / (3 + 1/4 + 1/4)
const itemWidth = width / (previewCount + .2);

//to center items you start from 3/4 firstItemWidth 
const startScroll = (itemWidth * 3 / 4);

const Offers = ({ navigation, ...props }) => {
  const [chipData, setChipData] = useState([
  ])
  const [chipItem, setChipItem] = useState({});
  const [dataChange, setDataChange] = useState(false);
  const [normalArr, setNormalArr] = useState([]);
  const [loadingData, setLoadingData] = useState(false);
  const [showAllCateogories, setShowAllCategories] = useState(false);
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);
  const [showMarketPlaceAlert, setShowMarketPlaceAlert] = React.useState(false);
  const [showLoader, setShowLoader] = React.useState(false);
  const { Logout } = useContext(AuthContext)

  const flatListRefCategory = useRef<FlatList>(null)


  const [isFocuused, setFocused] = useState(true);

  const [viewPagerCurrentIndex, setViewPagerCurrentIndex] = useState(0);


  useEffect(() => {
    if (chipData.length == 0) {
      getMainCategories()
    }
  })


  useEffect(() => {
    if (props.getLocation.userMarketPlace != undefined && props.getLocation.userMarketPlace != "default" && props.getLocation.locationDataCity && props.getLocation.locationDataArea) {
      setShowMarketPlaceAlert(true);
    }
    else {
      setShowMarketPlaceAlert(false);
    }
  }, [props.getLocation.locationDataArea])

  useEffect(() => {
    let isFocused = true

    //console.log("market place on offer page>>>>>>>>>>>>>>>>>>",props.getLocation.userMarketPlace)


    let data = Linking.getInitialURL()
      .then(url => {
        console.log('VDKDKDD', url);

        console.log("user market place>>>>>>>>>>>", props.getLocation.userMarketPlace)

        if (url) {
          let data = url.toString().split("/");
          if (data[4] == "shareOffer" || data[4] == "shareMobileOffer") {
            props.setOfferId(data[5])
            navigation.navigate('OfferDesc')
          } else if (data[4] == "shareProduct" || data[4] == "shareMobileProduct") {
            props.setProductKey(data[5])
            navigation.navigate('ProductDesc')
          } else
            console.log('VVVKFKfdFKF', data[3]);
        }
      })
      .catch(err => {
        console.warn('Deeplinking error', err)
      })


    Linking.addEventListener('url', ({ url }) => {
      console.log('VDKDKDD', url);
      if (url) {
        let data = url.toString().split("/");
        if (data[4] == "shareOffer" || data[4] == "shareMobileOffer") {
          props.setOfferId(data[5])
          navigation.navigate('OfferDesc')
        } else if (data[4] == "shareProduct" || data[4] == "shareMobileProduct") {
          props.setProductKey(data[5])
          navigation.navigate('ProductDesc')
        } else
          console.log('VVVKFKfdFKF', data[3]);
      }
    })
    refreshData();

  }, [chipItem, props.getLocation.locationDataArea]);

  const refreshData = () => {
    let user = 'Z3Vlc3Q=';

    let state = props.getLocation.locationDataCity
      ? props.getLocation.locationDataCity.cityId
      : '';
    let city = props.getLocation.locationDataArea
      ? props.getLocation.locationDataArea.areaId
      : '';
    if (state && city) {
      if (props.getLocation.email) {
        console.log('TESTTTTT', props.getLocation.email);
        user = Buffer.from(props.getLocation.email).toString('base64')
        props.getAllFavoriteProduct(user).then((res) => {
          console.log('DATAFAFAFAVVVYYVYY', res.payload.data);
          props.deleteToFavourite(res.payload.data);
          setLoadingData(true);
          getAllStore(user, state, city)
        });
        props.getAllFollowStore(user).then((res) => {
          props.addToFollow(res.payload.data);
        });
        props.getUserSnippets(user).then((res) => {
          let result = res.payload.data;
          props.addToSnips(result);

        });

      } else {
        props.deleteToFavourite([]);
        props.addToFollow([]);
        setLoadingData(true);
        getAllStore(user, state, city)
      }

    }


  }
  /*
  *    Offers Categories 
  */
  const getMainCategories = () => {
    let userId = "Z3Vlc3Q=";

    props.getMainCategory(userId).then(res => {
      if (res.payload.status === 200) {
        let data = [] as any;
        data = [trendingObj, ...res.payload.data]
        props.addToCategores(res.payload.data)
        setChipData(data);
        setChipItem(data[0])
        getSubCategires()
      }
    })
  }

  /**
 *  Get all store 
 * @param user  
 * @param state 
 * @param city 
 */
  const getAllStore = (user: string, state: string, city: string) => {
    props.getAllStoreList(props.getLocation.userMarketPlace).then((res) => {
      let result = res.payload.data;
      props.setAllStore(result);
      getAllActiveBanner(user, state, city)
    });
  }

  /**
   * Banner Offers  
   * @param user 
   * @param state 
   * @param city 
   */

  const getAllActiveBanner = (user: string, state: string, city: string) => {
    props.getOfferBanner(user, chipItem['id'], state, city, 'offers', props.getLocation.userMarketPlace).then((res: any) => {
      let result = res.payload.data;
      console.log("Offer Banner Data Result ::::", result);
      let data: any = []
      if (result.length > 0) {
        let type = 'Banner'
        let newObj = { 'name': "FEATURED", 'viewType': type, 'viewAll': false, 'data': result };
        //data.push(newObj);// for hide FEATURED DEALS offers
      } else {
        let type = 'Banner'
        let newObj = { 'name': "FEATURED", 'viewType': type, 'viewAll': false, 'data': [] };
        //data.push(newObj);// for hide FEATURED DEALS offers
      }
      getAllActiveProducts(data, user, state, city)
    });
  }

  /**
   *  
   * @param data // -> Sending data from Banner Offers.
   * @param user 
   * @param state 
   * @param city 
   * @param isNoRecord 
   */
  const getAllActiveProducts = (data: any, user: string, state: string, city: string) => {
    props.getOfferProducts(user, chipItem['id'], state, city, props.getLocation.userMarketPlace).then((res) => {
      let result = res.payload.data;
      console.log()
      if (result.length > 0) {
        let type = 'Products'
        let newObj = { 'name': "", 'viewType': type, 'viewAll': true, 'data': result };
        data.push(newObj);
      }
      getAllActiveCateogriesStores(data, user, state, city);
    });
  }


  /**
   * 
   * @param data ->  Sending from getAllActiveProducts
   * @param user 
   * @param state 
   * @param city 
   */

  const getAllActiveCateogriesStores = (data: any, user: string, state: string, city: string) => {
    props.getOfferCategory(user, chipItem['id'], state, city, 'Offer', props.getLocation.userMarketPlace).then((res) => {
      let result = res.payload.data;
      let resultData = [] as any;
      resultData = getAllOffersStoresJsonList(result, data)
      setNormalArr(resultData);
      setLoadingData(false);
    });
  }
  /**
   * Get All Json keys from JsonObject.
   * @param data -> Sending from getAllActiveCategoriesStore
   * @returns -> All Json Keys
   */
  const getAllKeysFromJsonObject = (data: any) => {
    const newData = Object.keys(data).reduce((result, currentKey) => {
      result.push(currentKey);
      return result;
    }, []);
    return newData;
  }

  /**
   * 
   * @param data 
   * @param newData 
   * @returns 
   */
  const getAllOffersStoresJsonList = (data: any, newData: any) => {
    let keys = getAllKeysFromJsonObject(data);
    let isRecord = true
    for (let i = 0; i < keys.length; i++) {
      console.log("Offer_First_Carousel :::::", keys);
      if (keys[i] != "Offer_First_Carousel") {
        let dataObj = data[keys[i]];
        let type = ((i % 2 == 0) ? 'Banner' : 'Products')
        if (dataObj.length == 0) {
          if (isRecord) {
            isRecord = false
          }
        }
        let newObj = { 'name': ((i % 2 != 0) ? keys[i] : keys[i]), 'viewType': type, 'viewAll': false, 'data': dataObj };
        newData.push(newObj);
      }
    }
    return newData
  }



  const onChangeChipItem = (item: any) => {
    setChipItem(item);
    setShowAllCategories(false);
    setNormalArr([]);
  };

  const onAddToFavourite = (item: any, type = "product") => {
    console.log(item);
    if (props.getLocation.email) {
      if (!item['requestType']) {

        if (props.getFavData.length == 0) {
          item.totalLikes = item.totalLikes + 1
          addLikeStoreProductApi(item, type)
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
          if (found) {
            item.totalLikes = item.totalLikes - 1
            const data = props.getFavData.filter(data => data.productId === item.id || data.offerId === item.id)
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0])
          } else {
            item.totalLikes = item.totalLikes + 1
            addLikeStoreProductApi(item, type)
          }
        }
      }
    } else {
      setShowLoginAlert(true);
    }
  };


  const foundInFav = (item: {}) => {
    const found = props.getFavData.some((el) => el.productId === item.id || el.offerId === item.id);
    if (found) {
      return true;
    } else {
      return false;
    }
  };


  const getSubCategires = () => {
    let userId = "Z3Vlc3Q=";
    props.getSubCategoriesList(userId).then(res => {
      if (res.payload.status === 200) {
        props.addToSubCategories(res.payload.data)
      }
    })
  }


  const addLikeStoreProductApi = (item: string, type) => {
    setShowLoader(true);
    let email = Buffer.from(props.getLocation.email).toString('base64')
    item['requestType'] = true;
    props.addLikeProducts(item.id, email, type).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        setShowLoader(false)
        item['requestType'] = false;
        props.deleteToFavourite(res.payload.data);
      });
    });
  }

  const removeLikeStoreProductApi = (item: {}) => {
    // setShowLoader(true);
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.removeLikeProducts(email, item).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });
    });
  }

  const snapToOffsets = () => {
    return ((0 * (itemWidth) * previewCount) + startScroll)
  }

  const renderProductItem = (item: {}, idx: number) => {
    return (
      <Product {...props} item={item} key={item.id} onItemPress={() => {
        props.setProductKey(item.id)
        navigation.navigate('ProductDesc', { itemType: item })
      }}
        isLogo={true}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item)
        }}
      />
    );
  };

  const renderChipItem = (item: {}, index: number, type: Boolean) => {
    return (
      <View style={{ flex: 1, alignItems: 'center', marginVertical: 2, justifyContent: 'center' }}>
        <MyChip
          icon={item.image ? { uri: item.image } : TRENDING}
          onPress={() => {
            onChangeChipItem(item);
            if (type) {
              setTimeout(() => flatListRefCategory.current?.scrollToIndex({ index: index }), 500);
            }
          }}
          imageStyle={{ width: 55, height: 55, resizeMode: 'stretch' }}
          containerStyle={{
            marginHorizontal: 5,
            justifyContent: 'center',
            width: 65,
            height: 65,
            elevation: 4,
            borderRadius: 65 / 2,
            alignItems: 'center',
            backgroundColor: '#FFFFFF',
            borderColor: '#1776d3',
            borderWidth: 1,
          }}
        />
        <Text
          numberOfLines={1}
          style={{
            width: 65,
            textAlign: 'center',
            color: item.name == chipItem['name'] ? '#1776d3' : '#000',
            fontSize: 11,

            fontWeight: 'bold',
          }}>
          {item.name}
        </Text>
      </View>
    );
  };


  return (
    <View style={{ flex: 1, backgroundColor: BGCOLOR }}>
      <View style={{ width: '100%', height: 90, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <FlatList
          data={chipData}
          ref={flatListRefCategory}
          horizontal
          renderItem={({ item, index }) => renderChipItem(item, index, false)}
          style={{ flex: 1, borderColor: "red", }}
        />
        {
          (chipData.length > 0 && <TouchableOpacity onPress={() => {
            setShowAllCategories(true);
          }} style={{ width: 30, height: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Image source={DOWNARRAY} style={{ width: 14, height: 14, tintColor: BLUE }} />
          </TouchableOpacity>)
        }
      </View>
      <ScrollView style={{ width: '100%', flex: 1, marginTop: 10 }}>
        {
          loadingData ? (<View style={{ width: '100%', height: 200, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ width: '100%', textAlign: 'center', fontSize: 14, marginTop: 20 }}>{'Loading...'}</Text>
          </View>)
            : <View style={{ width: '100%', height: '100%' }}>
              {normalArr.map((item, index) => {
                // console.log("Normal Arr ::::",item,"::View Type::",item['viewType']);
                return (item['viewType'] == 'Store' ? <View style={{ width: '100%' }}>
                  <View style={{ width: '100%', flexDirection: 'row' }}>
                    <Text style={storeStyle.txtCategoryName}>{item['name']}</Text>
                    {normalArr[(index)]['data'].length != 0 && (
                      <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                        <Text style={storeStyle.txtCategoryName}></Text>
                        <TouchableOpacity onPress={() => {
                          let data = { productType: true, product: [], offer: normalArr[(index)]['data'] }
                          props.addToViewAllProduct(data)
                          navigation.push('ViewAllProduct');
                        }}>
                          <Text style={storeStyle.txtViewAll}>{'View All >'}</Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                  <FlatListSlider
                    data={item['data']}
                    width={width - 80}
                    onItemPress={(item) => {
                      props.setStoreKey(item.storeId)
                      props.setOfferId(item.id)
                      navigation.push('OfferDesc');
                    }}
                    onAddToFavourite={(item) => {
                      onAddToFavourite(item, "offer")
                    }}
                    foundInFav={(item: any) => foundInFav(item)}
                    contentStyle={{ paddingHorizontal: 16, width: '100%' }}
                    indicatorActiveWidth={40}
                  />
                </View> : (item['viewType'] == 'Banner' && !item['name'].includes("_NOFFERSECTION") ? <View>
                  <View style={{ width: '100%', flexDirection: 'row' }}>
                    <Text style={storeStyle.txtCategoryName}>{item['name'].split("_")[0].includes("FEATURED") ?
                      item['name'].split("_")[0].toUpperCase() + " DEALS" :
                      item['name'].split("_")[0].toUpperCase() + " OFFERS"}</Text>
                    {!item['name'].includes("_NOFFERSECTION") ?
                      <TouchableOpacity onPress={() => {
                        let data = { productType: false, product: item['data'], offer: [] }
                        props.addToViewAllProduct(data)
                        navigation.push('ViewAllProduct');
                      }}>
                        {item['data'].length > 0 && <Text style={{ ...storeStyle.txtViewAll }}>{'View All >'}</Text>}
                      </TouchableOpacity> : null}
                  </View>
                  {item['data'].length > 0 ? <FlatListSlider
                    data={item['data']}
                    width={width - 80}
                    onItemPress={(item) => {
                      props.setStoreKey(item.storeId)
                      props.setOfferId(item.id)
                      navigation.push('OfferDesc');
                    }}
                    onAddToFavourite={(item: any) => {
                      onAddToFavourite(item, "offer")
                    }}
                    foundInFav={(item: any) => foundInFav(item)}
                    contentStyle={{ paddingHorizontal: 16, width: '100%' }}
                    indicatorActiveWidth={40}
                  /> : <View>
                      {!item['name'].split("_")[0].includes("FEATURED") && <Text style={{ marginLeft: 10, fontSize: 12, padding: 8 }}>No offers available in this section</Text>}
                    </View>}
                </View>
                  : ((item['name'].includes("_Product") || item['name'] === "") && <View style={{ flexDirection: 'column', width: '100%' }}>
                    <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 5 }}>
                      <Text style={storeStyle.txtCategoryName}>{item['name'] === "" ? "FEATURED DEALS" : item['name'].split("_")[0].toUpperCase() + " PRODUCTS"}</Text>
                      <TouchableOpacity onPress={() => {
                        let data = { productType: true, product: item['data'], offer: [] }
                        props.addToViewAllProduct(data)
                        navigation.push('ViewAllProduct');
                      }}>
                        {item['data'].length > 0 && <Text style={{ ...storeStyle.txtViewAll }}>{'View All >'}</Text>}
                      </TouchableOpacity>
                    </View>
                    {item['data'].length > 0 ? <FlatList
                      data={item['data']}
                      horizontal={true}
                      keyExtractor={(item, index) => item.toString() + index}
                      renderItem={({ item, index }) => renderProductItem(item, index)}
                      // contentContainerStyle={{ paddingHorizontal: 16,width:'100%'}}
                      style={{ margin: 5, marginRight: 10 }}
                    // windowSize={1}
                    // initialNumToRender={1}
                    // maxToRenderPerBatch={1}
                    // removeClippedSubviews={true}
                    /> : <View>
                        <Text style={{ marginLeft: 10, fontSize: 12, padding: 8 }}>No products available in this section</Text>
                      </View>}
                  </View>))
                )
              })
              }
            </View>
        }
      </ScrollView>
      {
        showAllCateogories && (
          <ScrollView style={{
            width: '100%', flex: 1, position: 'absolute', top: 0,
            backgroundColor: '#f2f2f2',
            bottom: 0,
            left: 0,
            right: 0,
          }}>
            <View style={{ backgroundColor: '#ffffff', width: '100%', flexDirection: 'column', paddingBottom: 20 }}>
              <View style={{ width: '100%', flexDirection: 'row', paddingHorizontal: 10 }}>
                <Text style={{ fontSize: 15, width: 120, padding: 10, color: '#000', flex: 1, paddingHorizontal: 5, }}>
                  {'All Categories'}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    setShowAllCategories(false);
                  }} style={{
                    width: 40, height: 40, padding: 10, backgroundColor: '#ffffff', justifyContent: 'center', alignItems: 'center',
                    transform: [{ rotate: '180deg' }]
                  }}>
                  <Image source={DOWNARRAY} style={{ width: 14, height: 14 }} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={chipData}
                numColumns={4}
                renderItem={({ item, index }) => renderChipItem(item, index, true)}
                style={{ width: '100%' }}
              />
            </View>
          </ScrollView>
        )
      }


      <MarketPlaceAlert
        showAlert={showMarketPlaceAlert}
        marketPlace={props.getLocation.userMarketPlace}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          setShowMarketPlaceAlert(false);
          props.saveUserMarketPlace("default");
          setTimeout(function () {
            RNRestart.Restart();
          }, 2000);
        }} onCancelPress={() => {
          setShowMarketPlaceAlert(false)
        }} />

      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          // navigation?.push('login');
          setShowLoginAlert(false);
          Logout(navigation);
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />
      <LoaderAlert
        showAlert={showLoader}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  getLocation: state.location,
  getFavData: state.productTab.favourite,
  store: state.productTab.store,
  userData: state.auth.userData
});

const mapDispatchToProps = (dispatch) => ({
  getOffersAll: (user: string, city: string, area: string) => dispatch(getOffersAll(user, city, area)),
  addToFavourite: (data: {}) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data: {}) => dispatch(deleteToFavourite(data)),
  addToFollow: (data: {}) => dispatch(addToFollow(data)),
  getOfferCategory: (user: string, id: string, state: string, city: string, viewType: string, marketPlace: string) => dispatch(getOfferCategory(user, id, state, city, viewType, marketPlace)),
  getOfferBanner: (user: string, id: string, state: string, city: string, viewType: string, marketPlace: string) => dispatch(getOfferBanner(user, id, state, city, viewType, marketPlace)),
  getOfferProducts: (user: string, id: string, state: string, city: string, marketPlace: string) => dispatch(getOfferProducts(user, id, state, city, marketPlace)),
  getAllStoreList: (marketPlace: string) => dispatch(getAllStoreList(marketPlace)),
  getMainCategory: (user: string) => dispatch(getMainCategory(user)),
  setProductKey: (productId: string) => dispatch(setProductKey(productId)),
  setStoreKey: (storeId: string) => dispatch(setStoreKey(storeId)),
  setAllStore: (data: any) => dispatch(setAllStore(data)),
  setOfferId: (offerId: string) => dispatch(setOfferId(offerId)),
  getAllFavoriteProduct: (userId: string) => dispatch(getAllFavoriteProduct(userId)),
  addLikeProducts: (data: any, userId: string, type: string) => dispatch(addLikeProducts(data, userId, type)),
  removeLikeProducts: (userId: string, item: any) => dispatch(removeLikeProducts(userId, item)),
  getAllFollowStore: (userId: string) => dispatch(getAllFollowStore(userId)),
  addToViewAllProduct: (data: any) => dispatch(addToViewAllProduct(data)),
  getUserSnippets: (userId: string) => dispatch(getUserSnippets(userId)),
  addToSnips: (data: any) => dispatch(addToSnips(data)),
  addToCategores: (data: any) => dispatch(addToCategories(data)),
  addToSubCategories: (data: any) => dispatch(addToSubCategories(data)),
  getSubCategoriesList: (user) => dispatch(getSubCategoriesList(user)),
  saveUserMarketPlace: (data) => dispatch(saveUserMarketPlace(data)),


});
export default connect(mapStateToProps, mapDispatchToProps)(Offers);