import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import {Button, Container, Content} from 'native-base';
import {Heading} from '../Components/Heading';
import ThemeButton from '../Components/Button';
import {AuthContext} from '../Context/Context';
import ThemeInput from '../Components/Input';
import {StackNavigationProp} from '@react-navigation/stack';
import ImagePicker from 'react-native-image-crop-picker';
import {Avatar} from 'react-native-paper';
import Icons from '../Constants/Icons';
import {connect} from 'react-redux';
import {setProfileImage} from '../Actions/product';
import { SafeAreaView } from 'react-native-safe-area-context';
import { BLUE, WHITE } from '../Constants/colors';

interface registerProps {
  navigation?: StackNavigationProp<any, any>;
}
const Register = ({navigation, props}: registerProps) => {
  const {setRegister} = useContext(AuthContext);
  const [profile, setProfile] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [visible, setVisible] = useState(false);
  const [gender, setGender] = useState('');
  const [username, setUsername] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  const registerdata = () => {
    if (email && password) {
      setRegister(email.toLocaleLowerCase().trim(), password, username, mobileNumber, gender, "",navigation);
      setVisible(true);
    } else {
      Alert.alert('Please enter mandatory fields');
    }
  };

  const chooseImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      //props.setProfileImage(image.path)
      setProfile(image.path);
    });
  };

  return (
    <SafeAreaView style={{...styles.container,backgroundColor:BLUE}}
    edges={['top']} 
    
    >
      <StatusBar barStyle={'default'}  />
      <Heading
        title={'Register'}
        rightText={'Close'}
        onRightPress={() => navigation?.goBack()}
      />

      <Content style={{backgroundColor:WHITE}}>
        <View style={styles.midContainer}>
        

          <ThemeInput
            imgPath={require('../../assets/Auth/id_card.png')}
            onChangeText={(txt) => setUsername(txt)}
            placeHolderText={'Name'}
          />
          <ThemeInput
            imgPath={require('../../assets/Auth/user.png')}
            placeHolderText={'Email Address*'}
            onChangeText={(txt) => setEmail(txt)}
          />
          <ThemeInput
            imgPath={require('../../assets/Auth/password.png')}
            secured={true}
            placeHolderText={'Password*'}
            onChangeText={(pass) => setPassword(pass)}
          />
          <ThemeInput
            imgPath={require('../../assets/Auth/smartphone.png')}
            maxlength={10}
            placeHolderText={'Phone Number (Optional)'}
            keyboardType={'numeric'}
            onChangeText={(txt) => setMobileNumber(txt)}
          />
        </View>
       
      </Content>
      <View style={styles.button}>
        <ThemeButton onBtnPress={() => registerdata()} btnText={'Register'} />
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  profileImg: state.productTab.profileImg,
});
const mapDispatchToProps = (dispatch) => ({
  setProfileImage: (data) => dispatch(setProfileImage(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  viewMale: {
    flex: 0.5,
    flexDirection: 'row',
    borderLeftColor: '#989898',
    borderTopColor: '#989898',
    borderBottomColor: '#989898',
    borderBottomWidth: 0.8,
    borderTopWidth: 0.8,
    borderLeftWidth: 0.8,
    alignItems: 'center',
    height: 50,
  },
  viewFemale: {
    flex: 0.5,
    flexDirection: 'row',
    borderLeftColor: '#989898',
    borderTopColor: '#989898',
    borderBottomColor: '#989898',
    borderBottomWidth: 0.8,
    borderTopWidth: 0.8,
    borderLeftWidth: 0.8,
    borderRightColor: '#989898',
    borderRightWidth: 0.8,

    alignItems: 'center',
    height: 50,
  },
  logo: {
    resizeMode: 'contain',
    height: 40,
    width: 40,
  },
  checkboxmain: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 15,
  },

  checkbox: {
    width: '45%',
    borderRadius: 0,
    backgroundColor: 'transparent',
    borderColor: '#919191',
    borderWidth: 1,
  },
  checkboxInner: {
    flexDirection: 'row',
    alignSelf: 'center',
    borderColor: '#989898',
    borderWidth: 0.8,
  },
  button: {
    flex: 0.2,
    backgroundColor:WHITE,
    alignItems: 'center',
  },
  midContainer: {
    flex: 0.58,
    marginTop: 20,
    backgroundColor:WHITE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  socialMedia: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  socialMediaInner: {
    resizeMode: 'cover',
    height: 50,
    width: 50,
    margin: 5,
  },
  textStyle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});
