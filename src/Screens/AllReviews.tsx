import React ,{useEffect, useState} from 'react';
import {View, Text, Alert, FlatList, Image, StyleSheet} from 'react-native';
import {Container, Icon} from 'native-base';
import {BackHeader} from '../Components/Heading';
import ProductDesc from './ProductDesc';
import {
  PLAY_ICON,
  user,
} from '../Constants/images';
import { connect } from 'react-redux'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icons from '../Constants/Icons';
import ThemeButton from '../Components/Button';
import ViewPager from '@react-native-community/viewpager';

import { Rating } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import MyChip from '../Components/chips';
import { getRating } from '../Actions/Details';
import { addToVideoUrl, addToZoomImages } from '../Actions/product';
import { BLACK, BLUE, GREY } from '../Constants/colors';
import moment from 'moment';
const arrRight = [
  {
      "name": 'ALL ',
      "value": 'All'
  }, {
    "name": '5 Star ',
    "value": "5"
  }, {
      "name": '4 Star ',
      "value": "4"
  }, {
      "name": '3 Star ',
      "value": "3"
  },
  {
      "name": '2 Star ',
      "value": "2"
  }, {
      "name": '1 Star ',
      "value": "1"
  }
  ]

const AllReviews = ({navigation, route, ...props}) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  const [chipItem, setChipItem] = useState({
    "name": 'ALL ',
});
  
  const [filterData, setFilter] = useState([]);
  const [ratingInfo,setRatingInfo] = useState([]);
  const [rattingCount, setRattingCount] = useState([]);

  const onChangeChipItem = (item) => {
    setChipItem(item);
  };

 
  const filterIndex =(index)=>{
      if(index == 0){
        setFilter(ratingInfo)
      }else if(index == 1){
        let array=ratingInfo.filter(data => data.rating == "5")
        setFilter(array)
      }else if(index == 2){
        let array=ratingInfo.filter(data => data.rating == "4")
        setFilter(array)
      }else if(index == 3){
        let array=ratingInfo.filter(data => data.rating == "3")
        setFilter(array)
      }else if(index ==  4){
        let array=ratingInfo.filter(data => data.rating == "2")
        setFilter(array)
      }else if(index == 5){
        let array=ratingInfo.filter(data => data.rating == "1")
        setFilter(array)
      }else if(index == 6){
        let array=ratingInfo.filter(data => data.comment.length != 0)
        setFilter(array)
      }else if(index == 7){
        let array=ratingInfo.filter(data => data.ratingMedia )
        setFilter(array)
      }
  }
  useEffect(()=>{
    if(ratingInfo.length==0){
      getRatingInfo()
    }
  })
const getRatingInfo =()=>{
  props.getRating(props.productId).then((res) => {
    let result = res.payload.data;
    console.log("Result in Ratting filter ::::",result);
    setRatingInfo(result);
    setFilter(result);
    setRattingCount(result);
  });
}


const renderRatingItem = (item: any) => {
  return (<View style={{ width: '100%', flexDirection: 'column',padding:10 }}>
     <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
      <Image
        source={user}
        style={{ width: 50, height: 50, resizeMode: 'stretch' }}>
      </Image>
      <View style={{ marginTop: 5, paddingHorizontal: 10, flex: 1, flexDirection: 'column' }}>
        <Text style={{ ...styles.txtDesc,color:BLACK, fontWeight: 'bold', marginTop: 5, paddingHorizontal: 5 }}>{item['userName']}</Text>
        <Text style={{ ...styles.txtDesc,  color:GREY, marginTop: 5, paddingHorizontal: 5 }}>{'Reviewed on '+ moment(item['createdOn']).format('DD MMM YYYY')}</Text>
        <View style={{ width: 60, paddingHorizontal: 5, marginTop: 5 }}>
          <Rating
            type='star'
            fractions={2}
            readonly={true}
            ratingCount={5}
            showRating={false}
            startingValue={item['rating']}
            imageSize={12}
          />
        </View>
      </View>
      <View style={{ marginTop: 5, paddingHorizontal: 10 }}>
        <Text style={{ ...styles.txtDesc, flex: 1, marginTop: 5, paddingHorizontal: 10 }}>{''}</Text>
      </View>
    </View>
    <Text style={{ ...styles.txtDesc,color:BLUE, fontWeight: 'bold',marginTop:10 }}>{'Verified Purchase'}</Text>
    <Text style={{ ...styles.txtReview, width: '100%', paddingVertical: 10 }}>{item.comment}</Text>
    {item.ratingMedia && (<ScrollView horizontal style={{ width: '100%'}}><View style={{ flexDirection: 'row', width: '100%' }}>
      {
        item.ratingMedia.map((data) => {
          return renderRatingImage(data,item.ratingMedia);
        })}
    </View></ScrollView>)}
    <View style={{ backgroundColor: '#F2F2F2', width: '100%', height: 1 }} />
  </View>)
}

const renderRatingImage = (item: any,ratingMedia:[]) => {
  return (<TouchableOpacity
  style={{marginRight:5}}
    onPress={()=>{
      if(!checkImageisVideoFile(item))  {
        let data: any = [];
        for (let i = 0; i < ratingMedia.length; i++) {
          let obj = { freeHeight: false, url: ratingMedia[i], index: item ===ratingMedia[i] ? i : -1 }
          data.push(obj)
        }
        props.addToZoomImages(data)
        //  console.log('DATAIMAMASMMddscfdsdSS', item);
        navigation.push('Zoom')
      }     
    }}
    ><Image
    source={{ uri: item }}
    style={{ resizeMode: 'stretch', height: 100, width: 100, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5, backgroundColor: 'black', borderRadius: 5 }}
  />
    {checkImageisVideoFile(item) && (
      <TouchableOpacity
        onPress={() => {
          props.addToVideoUrl(item);
          navigation.push("RatingVideoPlayer")
         }}
        style={{ position: 'absolute', alignSelf: 'center', bottom: 0, top: 30, height: 40, width: 40, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5 }}
      >
        <Image
          source={PLAY_ICON}
          style={{ resizeMode: 'stretch', position: 'absolute', alignSelf: 'center', height: 40, width: 40, tintColor: 'white' }}
        />
      </TouchableOpacity>
    )}
  </TouchableOpacity>);
};

 
const checkImageisVideoFile = (image: string) => {
  var types = ['JPG', 'jpg', 'jpeg', 'tiff', 'png', 'gif', 'bmp'];
  //split the url into parts that has dots before them
  var parts = image.split('.');
  //get the last part 
  var extension = parts[parts.length - 1];

  //check if the extension matches list 
  if (types.indexOf(extension) != -1) {
    return false;
  } else {
    console.log(extension);

    return true
  }

}
 
  const renderChipItem = (item: any, index: number) => {
    const totalRatting = rattingCount.filter((data:any) => data.rating === item.value);
    return (
      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center',padding:4}} onPress={()=>{
            onChangeChipItem(item);
            filterIndex(index);
      }}>
        <Text
            numberOfLines={2}
            style={{
                height:40,
                minWidth:100,
                padding:10,
                width:'100%',
                textAlign:'center',
                borderColor:item == chipItem?'transparent' : '#1776d3',
                borderWidth:1,
                borderRadius:3,
                backgroundColor: item == chipItem ? '#1776d3' : 'transparent',
                color: item == chipItem ? '#FFFFFF' : '#000',
                fontSize: 12,
                fontWeight: 'bold',
            }}>
          {item.name}{item.value !== "All" && ("("+totalRatting.length+")")}
        </Text>
      </TouchableOpacity>
    );
  };


  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#ffffff'}}
      edges={['top']} 
      >
      <BackHeader title={"Rating"}
          fontSize={28}
          onFontPress={() => navigation?.pop()}
          fontName={'chevron-thin-left'}
      />
      <ScrollView style={{width:'100%',flex:1}}>
        <FlatList
          style={{width:'100%',height:40,paddingHorizontal:10,marginTop:10}}
              data={arrRight}
              horizontal
              renderItem={({item, index}) => renderChipItem(item, index)}
            />
          { filterData.map((item) => {
            return renderRatingItem(item);
          } )
        }
      </ScrollView>
    </SafeAreaView>
  );
};


const mapStateToProps = (state) => ({
  getFavData: state.productTab.favourite,
  productId: state.productTab.productKey,
 
});

const mapDispatchToProps = dispatch => ({
  getRating : (userid) =>dispatch(getRating(userid)),
  addToZoomImages: (data) => dispatch(addToZoomImages(data)),
  addToVideoUrl: (data: string) => dispatch(addToVideoUrl(data)),

});



export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllReviews)


const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  logo: {
      resizeMode: 'contain',
      height: 160,
      marginTop:10,
  },

  txtReview:{
    paddingVertical:10,
    color:'black',
},
  txtStoreName:{
      paddingVertical:10,
      color:'black',
      fontWeight:'bold'
  },

  txtStoreDetailsTitle:{
      color:'grey',
    width:'100%',

      fontSize:12
  },
  txtStoreDetailsDesc:{
    color:'black',
    fontWeight:'bold',
    width:'100%',
    textAlign:'right',
    fontSize:12

},
  txtDesc:{
    color:'black',
 },
  socialMedia: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
  },
  socialMediaInner: {
      resizeMode: 'cover',
      height: 50,
      width: 50,
      margin: 5

  },
  forgottext: {
      alignSelf: 'flex-end',
      fontSize: 16,
      marginTop: 10,
      marginEnd:25,
      marginBottom:10,
  },
  textStyle: {
      fontSize: 16,
      fontWeight: 'bold',
  }
})