import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import {Button, Icon, Item} from 'native-base';
import {getOffersAll, getOfferCategory,getMainCategory, getAllOffersStoresJsonList, getOfferBanner, getOfferProducts, getAllStoresBasedonCategoryStores, getAllProductsBasedOnCategories, getAllFavoriteProduct} from '../Actions/Offer';
import {
  setProductKey,
  addToFavourite,
  deleteToFavourite,
  setStoreKey,
  setOfferId,
} from '../Actions/product';
import {connect} from 'react-redux';
import MyChip from '../Components/chips';
import ViewPager from '@react-native-community/viewpager';
import {Avatar} from 'react-native-paper';
import {
  all,
  noRecord,
  progresLoading,
DOWNARRAY
} from '../Constants/images';
const { width } = Dimensions.get('window');

import { style as storeStyle } from './Store.style';
import { Overlay, Rating } from 'react-native-elements';
import { Buffer } from "buffer"

import Icons from '../Constants/Icons';
import { BLACK, BLUE, WHITE } from '../Constants/colors';
import { Product } from '../Components/Product';
import { SafeAreaView } from 'react-native-safe-area-context';
import { BackHeader } from '../Components/Heading';
import { GridProduct } from '../Components/GridProduct';
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { addLikeProducts, removeLikeProducts } from '../Actions/StoreDetails';
import { Store } from '../Components/Store';
import { BannerStore } from '../Components/BannerStore';
import FlatListSlider from '../Components/FlatListSlider';
import { SingleStore } from '../Components/SingleStore';
import { AuthContext } from '../Context/Context';

const StoreViewAllProduct = ({navigation, ...props}) => {

const { Logout } = useContext(AuthContext)


  const [product,setProduct] =useState ([])
  const [store,setStore] =useState ([])

  const [type,setType] =useState(false)
  const [showLoader, setShowLoader] = React.useState(false);
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);

  const [dataChange, setDataChange] = useState(false);

  useEffect(() => {
    setProduct(props.viewAllProduct.product);
    setStore(props.viewAllProduct.offer);
    setType(props.viewAllProduct.productType);
  }, [props.viewAllProduct])


    const onAddToFavourite = (item:any,type="store") => {
      if (props.getLocation.email) {
        if(!item['requestType']   ){
        if (props.getFavData.length == 0) {
          item.totalLikes=item.totalLikes+1
          addLikeStoreProductApi( item,type)
        } else {
          const found = props.getFavData.some((el) => el.storeId ===item.id  && el.type == "Store")   ;
          if (found) {
            item.totalLikes=item.totalLikes-1
          console.log('VJVJVVJVJVJVJMMM',"NOT FOUND");
            const data = props.getFavData.filter(data => data.storeId === item.id  && data.type == "Store" )
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0]);
          } else {
            item.totalLikes=item.totalLikes+1
            addLikeStoreProductApi( item,type)
          }
        }
   }
     } else {
        setShowLoginAlert(true);
      }
    };
  
    const addLikeStoreProductApi = (item:string,type) => {
      item['requestType']= true;
  setShowLoader(true)
    
      let email = Buffer.from(props.getLocation.email).toString('base64')
      props.addLikeProducts(item.id, email,type).then((res) => {
        props.getAllFavoriteProduct(email).then((res) => {
         item['requestType']= false;
  setShowLoader(false);

          props.deleteToFavourite(res.payload.data);
        });
      });
    }
  
    const removeLikeStoreProductApi = (item:{}) => {
      let email = Buffer.from(props.getLocation.email).toString('base64')
      props.removeLikeProducts(email, item).then((res) => {
        props.getAllFavoriteProduct(email).then((res) => {
          props.deleteToFavourite(res.payload.data);
        });
      });
    }
    const foundInFav = (item:{}) => {
      const found = props.getFavData.some((el) => el.storeId === item.id && el.type == "Store"  );
      if (found) {
        return true;
      } else {
        return false;
      }
    };
  
  

  const renderStoreItem = (item: {}, idx: number) => {
    return (
      <SingleStore {...props} item={item} onItemPress={() => {
        props.setStoreKey(item.storeId ? item.storeId : item.id)
        navigation.push('StoresDesc');
      }}
        type={type}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
         onAddToFavourite(item)
        }}
      />
    );
  };

  
  return (
        <SafeAreaView style={{flex: 1,}} edges={['top']} >
        <BackHeader title={''}
            fontSize={28}
            onFontPress={() => navigation?.pop()}
            fontName={'chevron-thin-left'}
        />
    <ScrollView style={{flex: 1}}>
        <View style={{width:'100%',height:'100%'}}> 
          { store.length !=0 && (
            <FlatList
              style={{padding:10}}
              data={store}
              renderItem={({item, index}) => renderStoreItem(item, index) }
            />
          ) }
          { !type && (
            <FlatList
              data={product}
              renderItem={({item, index}) => renderStoreItem(item, index) }
              numColumns={1}
            />
          )}
        </View>
      </ScrollView>
      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //props.setUserData(null);
          //navigation?.push('login');
            Logout(navigation);
            setShowLoginAlert(false);
        }} 
        onCancelPress={() => {
          setShowLoginAlert(false)
        }} />
      <LoaderAlert
        showAlert={showLoader}
      />
      </SafeAreaView>
    );
  };

const mapStateToProps = (state) => ({
    getLocation: state.location,
    getFavData: state.productTab.favourite,
    store: state.productTab.store,
    categoryId: state.productTab.subCategoryId,
    viewAllProduct:state.productTab.viewAllProduct
  });
  const mapDispatchToProps = (dispatch) => ({
    addToFavourite: (data:any) => dispatch(addToFavourite(data)),
    deleteToFavourite: (data:any) => dispatch(deleteToFavourite(data)),
   setProductKey:(productId:string) => dispatch(setProductKey(productId)),
   getAllProductsBasedOnCategories:(cityId:string,categoryId:string,marketPlace:string) => dispatch(getAllProductsBasedOnCategories(cityId,categoryId,marketPlace)),
   setStoreKey: (storeId:string) => dispatch(setStoreKey(storeId)),
   setOfferId: (offerId:string) => dispatch(setOfferId(offerId)),
  removeLikeProducts: (userId:string, item:any) => dispatch(removeLikeProducts(userId, item)),
  addLikeProducts: (data:any , userId:string,type:string) => dispatch(addLikeProducts(data, userId,type)),
  getAllFavoriteProduct: (userId:string ) => dispatch(getAllFavoriteProduct(userId)),
 
  
  
  });

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StoreViewAllProduct)