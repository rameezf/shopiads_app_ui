import React, { useCallback, useContext, useEffect, useRef, useState } from 'react'
import { View, Text, Image, Share, TouchableOpacity, Alert, Linking, StyleSheet, ScrollView, Dimensions, PermissionsAndroid, Platform, Modal, TextInput } from 'react-native'
import { Button, } from 'react-native-paper';
import { connect } from 'react-redux'
import { addToFavourite, addToFollow, addToSnips, addToVideoUrl, addToZoomImages, deleteToFavourite, onSnipsSelected, setProductKey, setStoreKey, } from '../Actions/product'
import Icons from '../Constants/Icons'
import ViewPager from '@react-native-community/viewpager';
import { style as productStyle } from './Product.style'
import {
  plus,
  productBlazer,
  BUYNOW,
  SNIP,
  BACK,
  STORE_ICON,
  SHARE, user,
  PLAY_ICON,
  logo,
  infoIc,
  DISCOUNT_ICON
} from '../Constants/images';
import { Overlay, Rating, BottomSheet } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getImageUrlToBase64, getProductDetails, getRating, getSimilarProducts, productBuyNowClick, isUserShopifyOrder, getVariantList } from '../Actions/Details';
import UUIDGenerator from 'react-native-uuid-generator';
import { addFollowingApi, addLikeProducts, addUserSnips, getStoreDetails, removeLikeProducts, uploadBillDetails, uploadBillImage, uploadRatingDetails, uploadRatingStoreImage } from '../Actions/StoreDetails';
import { BLACK, BLUE, DISABLED_COLOR, GREY, LIGHTGREY, WHITE } from '../Constants/colors';
import ImagePicker from './ImagePicker';
import { Buffer } from "buffer";
import { LoginAlert } from '../NavigationStack/LoginAlert';
import { checkInOffers, checkInProducts, getAllFavoriteProduct, getAllFollowStore, getAllStoreProducts } from '../Actions/Offer';
import { setUserData } from '../Actions/auth';
import { LoaderAlert } from '../NavigationStack/LoaderAlert';
import { Product } from '../Components/Product';
import { Picker } from 'native-base';
import { styles as styles } from './ProductDesc.style';
import { getUserSnippets } from './Snip';
import { style } from './Store.style';
import moment from 'moment';
import { AuthContext } from '../Context/Context';
import { ThemeImage } from '../Components/ThemeImage';
import RNFetchBlob from 'rn-fetch-blob';
import { Dropdown } from 'react-native-element-dropdown';
import HTMLView from 'react-native-htmlview';

const ProductDesc = ({ navigation, route, itemType, ...props }) => {
  const BRAND = { 'column': 'Brand', 'value': '' };
  const SoldBy = { 'column': 'Sold & Fulfilled By', 'value': '' }
  const Shipping = { 'column': 'Shipping', 'value': '' }
  const ReturnPolicy = { 'column': 'Return Policy', 'value': '', "type": 1 }

  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => { //To toggle the show text or hide it
    setTextShown(!textShown);
  }

  const [attributes, setAttributes] = useState([]);
  const [selectedAttributes, setSelectedAttributes] = useState([]);
  const [showLoginAlert, setShowLoginAlert] = React.useState(false);
  const [showLoader, setShowLoader] = React.useState(false);
  const [rating, showRating] = React.useState(false);
  const [billPath, setBillPath] = React.useState("");
  const [billImagePath, setBillImagePath] = React.useState("");
  const [billImagePath2, setBillImagePath2] = React.useState("");
  const [billImagePath3, setBillImagePath3] = React.useState("");
  const [billImagePath4, setBillImagePath4] = React.useState("");

  const [showAlert, setShowAlert] = React.useState(false);
  const [showFile, setShowFiles] = React.useState(0);
  const [comment, setComment] = React.useState("");
  const [viewPagerPosition, setViewPagerPosition] = React.useState(0)
  const [productInfo, setProductInfo] = useState({}); //to show the "Read more & Less Line"
  const [storeInfo, setStoreInfo] = useState({});
  const [ratingInfo, setRatingInfo] = useState([]);
  const [similarProducts, setSimilarProducts] = useState([]);
  const [storeProducts, setStoreProducts] = useState([]);
  const [subAttributes, setSubAttributes] = useState([]);

  const [snipped, setSnipped] = useState(false);
  const [ratingValue, setRatingValue] = useState(0);
  const [shopifyOrder, setShopifyOrder] = useState(false);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [selectedVariantOption1, setSelectedVariantOption1] = useState("");
  const [selectedVariantOption2, setSelectedVariantOption2] = useState("");
  const [selectedVariantOption3, setSelectedVariantOption3] = useState("");
  const [quantityValue, setQuantityValue] = useState(1);
  const [variantList, setVariantList] = useState([]);
  const { Logout } = useContext(AuthContext)
  const [storeDetails, setStoreDetails] = useState({}); //to show the "Read more & Less Line"
  const [modalVisible, setModalVisible] = React.useState(false);


  const onTextLayout = useCallback(e => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);

  useEffect(() => {
    if (Object.keys(productInfo).length == 0) {
      props.getProductDetails(props.productId).then((res) => {
        let result = res.payload.data;
        console.log('VVVVINFOFOF', result);
        let storeList = props.store
        for (let j = 0; j < storeList.length; j++) {
          if (storeList[j].id == result.storeId) {
            result.storeName = storeList[j].storeName;
            result['address'] = storeList[j]['address2'] + ', Unit:' + storeList[j]['address1'] + ', ' + storeList[j]['cityId'] + ' ' + storeList[j]['pincode'];
            break
          }
        }
        let soldBy = SoldBy;
        soldBy.value = result['storeName'];
        let attributes = [];
        let selectedAttributes = [];
        if (result.isShopifyProduct === "Y") {
          // let brand=BRAND;
          // brand.value=result['brand'];
          // let shipping=Shipping;
          // shipping.value=result['shippingType'];
          attributes = [soldBy];
        } else {
          let brand = BRAND;
          brand.value = result['brand'];
          let shipping = Shipping;
          shipping.value = result['shippingType'];
          attributes = [brand, soldBy, shipping];
        }
        try {
          for (let j = 0; j < result.attribute.length; j++) {
            if (result.attribute[j].value == "Default Title") {
              // attributes.push(result.attribute[j]);
            } else {
              selectedAttributes.push(result.attribute[j]);
            }
          }
          attributes.push(ReturnPolicy);
        } catch (e) {
        }
        setAttributes(attributes);
        setSelectedAttributes(selectedAttributes);
        setProductInfo(result);
        setSelectedVariantOption1(result['variantOption1']);
        setSelectedVariantOption2(result['variantOption2']);
        setSelectedVariantOption3(result['variantOption3']);
        getStoreInfo(result.storeId);
        getRatingInfo();
        variantListFunc(props.productId);
        if (result.categories.length > 0) {
          getSimilarProducts(result.categories[0])
        }
      });
    }
  })


  const variantListFunc = (productId: any) => {
    props.getVariantList(productId).then((res: any) => {
      console.log("Variant List Data ::::", res.payload.data);
      setVariantList(res.payload.data);
    })
  }

  useEffect(() => {
    props.getAllStoreProducts(productInfo['storeId']).then((res) => {
      let result = res.payload.data;
      setStoreProducts(result)
    });
  }, [productInfo]);

  const getRatingInfo = () => {
    props.getRating(props.productId).then((res) => {
      let result = res.payload.data;
      setRatingInfo(result)
    });
  }

  const getStoreInfo = (storeId) => {
    props.getStoreDetails(storeId).then((res) => {
      let result = res.payload.data;
      setStoreInfo(result)
    });
  }

  const getSimilarProducts = (catgoriesId) => {
    props.getSimilarProducts(catgoriesId, props.getLocation.userMarketPlace).then((res) => {
      let result = res.payload.data;
      result = result.filter(function (item) {
        return item.id != props.productId;
      })
      setSimilarProducts(result);
    });
  }


  const addFollowingApi = () => {
    const found = props.getFavStore.some((el) => productInfo.storeId === el.storeId);
    if (!found) {
      setShowLoader(true);
      let email = Buffer.from(props.getLocation.email).toString('base64')
      console.log(productInfo);
      props.addFollowingApi(productInfo['storeId'], email).then((res) => {
        props.getAllFollowStore(email).then((res) => {
          props.addToFollow(res.payload.data);
          setShowLoader(false);
        });
      });
    } else {
      //  Alert.alert('Already Following the store.');
    }
  }

  const checkIsSnip = () => {
    const found = props.snips.some((el) => props.productId === el.productId);
    return found
  }

  const checkIsUnfollow = (productInfo) => {
    if (props.getFavStore) {
      const found = props.getFavStore.some((el) => productInfo.storeId === el.storeId);
      return !found
    } else {
      return true
    }
  }


  const foundInFav = (item) => {
    let FavItem = item;
    const found = props.getFavData.some((el) => el.productId === FavItem.id);
    if (found) {
      return true;
    } else {
      return false;
    }
  };

  const addSnips = () => {
    if (props.getLocation.email) {
      setShowLoader(true);
      UUIDGenerator.getRandomUUID((uuid) => {
        let data = {
          'id': uuid,
          'userId': props.getLocation.email,
          'product': productInfo,
          'productId': productInfo.id,
          'storeId': productInfo.storeId,
          'storeName': productInfo.storeName,
          'type': 'Product',
          'storeCode': storeInfo['code'],
          'activityType': 'snip'
        }
        let email = Buffer.from(props.getLocation.email).toString('base64')
        props.addUserSnips(email, data).then((res) => {
          props.getUserSnippets(email).then((res) => {
            let result = res.payload.data;
            props.addToSnips(result);
            setShowLoader(false);
          });
        });
      });
    } else {
      setShowLoginAlert(true);
    }
  }

  const renderRatingItem = (item: any) => {
    return (<View style={{ width: '100%', flexDirection: 'column' }}>
      <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
        <Image
          source={user}
          style={{ width: 50, height: 50, resizeMode: 'stretch' }}>
        </Image>
        <View style={{ marginTop: 5, paddingHorizontal: 10, flex: 1, flexDirection: 'column' }}>
          <Text style={{ ...styles.txtDesc, color: BLACK, fontWeight: 'bold', marginTop: 5, paddingHorizontal: 5 }}>{item['userName']}</Text>
          <Text style={{ ...styles.txtDesc, color: GREY, marginTop: 5, paddingHorizontal: 5 }}>{'Reviewed on ' + moment(item['createdOn']).format('DD MMM YYYY')}</Text>

          <View style={{ width: 60, paddingHorizontal: 5, marginTop: 5 }}>
            <Rating
              type='star'
              fractions={2}
              readonly={true}
              ratingCount={5}
              showRating={false}
              startingValue={item['rating']}
              imageSize={12}
            />
          </View>
        </View>

        <View style={{ marginTop: 5, paddingHorizontal: 10 }}>
          <Text style={{ ...styles.txtDesc, flex: 1, marginTop: 5, paddingHorizontal: 10 }}>{''}</Text>
        </View>
      </View>
      <Text style={{ ...styles.txtDesc, color: BLUE, fontWeight: 'bold', marginTop: 10 }}>{'Verified Purchase'}</Text>
      <Text style={{ ...styles.txtReview, width: '100%', paddingVertical: 10 }}>{item.comment}</Text>
      {item.ratingMedia && (<ScrollView horizontal style={{ width: '100%' }}><View style={{ flexDirection: 'row', width: '100%' }}>
        {
          item.ratingMedia.map((data) => {
            return renderRatingImage(data, item.ratingMedia);
          })}
      </View></ScrollView>)}
      <View style={{ backgroundColor: '#F2F2F2', width: '100%', height: 1 }} />
    </View>)
  }

  const renderRatingImage = (item: any, ratingMedia: []) => {
    return (<TouchableOpacity
      style={{ marginRight: 5 }}
      onPress={() => {
        if (!checkImageisVideoFile(item)) {
          let data: any = [];
          for (let i = 0; i < ratingMedia.length; i++) {
            let obj = { freeHeight: false, url: ratingMedia[i], index: item === ratingMedia[i] ? i : -1 }
            data.push(obj)
          }
          props.addToZoomImages(data)
          //  console.log('DATAIMAMASMMddscfdsdSS', item);
          navigation.push('Zoom')


        }
      }}
    ><ThemeImage
        source={{ uri: item }}
        style={{ resizeMode: 'stretch', height: 100, width: 100, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5, backgroundColor: 'black', borderRadius: 5 }}
      />


      {checkImageisVideoFile(item) && (
        <TouchableOpacity
          onPress={() => {
            props.addToVideoUrl(item);
            navigation.push("RatingVideoPlayer")
          }}
          style={{ position: 'absolute', alignSelf: 'center', bottom: 0, top: 30, height: 40, width: 40, borderColor: 'black', borderWidth: 1, paddingHorizontal: 5 }}
        >
          <Image
            source={PLAY_ICON}
            style={{ resizeMode: 'stretch', position: 'absolute', alignSelf: 'center', height: 40, width: 40, tintColor: 'white' }}
          />
        </TouchableOpacity>
      )}



    </TouchableOpacity>);
  };

  const checkImageisVideoFile = (image: string) => {

    var types = ['JPG', 'jpg', 'jpeg', 'tiff', 'png', 'gif', 'bmp'];

    //split the url into parts that has dots before them
    var parts = image.split('.');
    //get the last part 
    var extension = parts[parts.length - 1];

    //check if the extension matches list 
    if (types.indexOf(extension) != -1) {
      return false;
    } else {
      console.log(extension);
      return true
    }
  }


  const checkInProducts = (productInfo: any) => {
    if (props.getLocation.email) {
      let email = Buffer.from(props.getLocation.email).toString('base64');
      props.checkInProducts(email).then((res: any) => {
        let result = res.payload.data;
        const found = result.some((el: any) => props.productId === el.id);
        if (found) {
          Alert.alert('You already rated a Product.');
        } else {
          if (props.getLocation.email) {
            showRating(true);
            let email = Buffer.from(props.getLocation.email).toString('base64');
            props.isUserShopifyOrder(email, productInfo['id']).then((res: any) => {
              setShopifyOrder(res.payload.data);
            });
          } else {
            setShowLoginAlert(true);
          }
        }
      });
    } else {
      setShowLoginAlert(true);
    }
  }


  const [dataChange, setDataChange] = useState(false);

  const onAddToFavourite = (item, index) => {
    if (props.getLocation.email) {
      if (!item['requestType']) {

        if (props.getFavData.length == 0) {
          item.totalLikes = item.totalLikes + 1;

          addLikeStoreProductApi(item)
        } else {
          const found = props.getFavData.some((el) => el.productId === item.id);
          if (found) {
            item.totalLikes = item.totalLikes - 1;
            const data = props.getFavData.filter(data => data.productId === item.id)
            let index = props.getFavData.indexOf(data[0]);
            props.getFavData.splice(index, 1);
            props.deleteToFavourite([]);
            props.deleteToFavourite(props.getFavData);
            setDataChange(!dataChange);
            removeLikeStoreProductApi(data[0])
          } else {
            item.totalLikes = item.totalLikes + 1
            addLikeStoreProductApi(item)
          }
        }
      }
    } else {
      setShowLoginAlert(true);
    }
  };

  const addLikeStoreProductApi = (item) => {
    setShowLoader(true)
    item['requestType'] = true;

    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.addLikeProducts(item.id, email).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        item['requestType'] = false;
        props.deleteToFavourite(res.payload.data);
        setShowLoader(false)

      });
    });
  }

  const removeLikeStoreProductApi = (item) => {
    let email = Buffer.from(props.getLocation.email).toString('base64')
    props.removeLikeProducts(email, item).then((res) => {
      props.getAllFavoriteProduct(email).then((res) => {
        props.deleteToFavourite(res.payload.data);
      });
    });
  }



  const renderProductItem = (item: any, idx: number) => {
    return (
      <Product {...props} item={item} onItemPress={() => {
        props.setProductKey(item.id)
        navigation.push('ProductDesc', { item: item })
      }}
        foundInFav={foundInFav(item)}
        onAddToFavourite={() => {
          onAddToFavourite(item, idx)
        }}
      />
    );
  };

  const renderAttribute = (item: any) => {
    return (
      item.type && item.type == 1 ? (
        <View style={{ flex: 0.5, flexDirection: 'row' }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'center' }} onPress={
            () => {
              let message = storeInfo['returnPolicy'] + "\n" + storeInfo['deliveryPolicy'] + "\n" + storeInfo["deliveryPolicy"] + "\n" + storeInfo["warrantyPolicy"]
              message = message.replace(/<[^>]+>/g, '');
              message = message.replace(/_/g, "&nbsp;")  //replaceAll("&nbsp;","."); is removed due to not support in android
              if (message.toString().length > 100) {
                // Maximum exceeded
                message = message.toString().substring(0, 100) + "....";
              }
              Alert.alert("Return Policy", message, [
                {
                  text: 'Cancel',
                  onPress: () => { },
                  style: 'cancel',
                },
                {
                  text: 'Read More', onPress: () => {
                    props.setStoreKey(productInfo['storeId']);
                    navigation.push('AboutStore')
                  }
                },
              ])
            }
          }>
            <Text style={{ ...styles.txtStoreDetailsDesc, marginLeft: 15 }}> {'Return Policy'} </Text>
            <Image source={infoIc} style={{ width: 15, height: 15, marginRight: 20 }} />
          </TouchableOpacity>
        </View>
      ) :
        (
          <View style={{ flex: 0.8, flexDirection: 'row', marginLeft: 5 }}>
            <Text style={{ ...styles.txtStoreDetailsTitle, flex: 1.2 }}> {item['column'] + ':'} </Text>
            <Text style={{ ...styles.txtStoreDetailsDesc, flex: 1.2 }} numberOfLines={1}> {item['value']} </Text>
          </View>
        )
    )
  }

  const setSubAttributesFun = (selectedData: any, index: any, itemData: any) => {
    let tempObj = {
      label: itemData.column,
      value: selectedData.value
    }
    let tempArray = subAttributes;
    let selectedDataArr = subAttributes;
    if (tempArray.length <= 0) {
      selectedDataArr.push(tempObj);
    } else {
      for (let i = 0; i < tempArray.length; i++) {
        if (tempArray[i].label === itemData.column) {
          selectedDataArr[i].value = selectedData.value;
        }
        let isTrue = tempArray.some(item => item.label === itemData.column);
        if (!isTrue) {
          selectedDataArr.push(tempObj);
        }
      }
    }
    setSubAttributes(selectedDataArr);
    handleVariantShopifyChanage(selectedData, index);
  }


  const handleVariantShopifyChanage = (selectedData: any, index: any) => {
    console.log("handleVariantShopifyChanage index>>>>>>>>>>>>", selectedData.value)
    if (selectedData.value != undefined && selectedData.value != null && selectedData.value != "-1") {
      let op1 = selectedVariantOption1
      let op2 = selectedVariantOption2
      let op3 = selectedVariantOption3
      let productData = [];

      if (index == 0) {
        setSelectedVariantOption1(selectedData.value);
        op1 = selectedData.value;

      }
      if (index == 1) {
        setSelectedVariantOption2(selectedData.value);
        op2 = selectedData.value;
      }
      if (index == 2) {
        setSelectedVariantOption3(selectedData.value);
        op3 = selectedData.value;
      }

      console.log("selectedVariantOption3 >>>>>>>>>>>>", selectedVariantOption3)

      console.log("op1 >>>>>>>>>>>>", op1)
      console.log("op2 >>>>>>>>>>>>", op2)
      console.log("op3 >>>>>>>>>>>>", op3)
      productData = variantList.filter(function (item) {
        return item.variantOption1 == op1 && item.variantOption2 == op2 && item.variantOption3 == op3;
      });
      console.log("handleVariantShopifyChanage filter productData>>>>>>>>>>>>", productData)
      if (productData != undefined && productData != null && productData.length > 0) {
        console.log("Product Data Id ::::", productData[0].id, props.productId);
        navigation.push('ProductVariantDesc', {
          "storeId": productInfo['storeId'],
          "productId": productData[0].id,
          "productVariantId": props.productId
        })
      }
    }
  }

  const renderSelectedAttributes = (itemData: any, index: any) => {
    let tempData = itemData.value && itemData.value.split(",").map((item: any) => {
      return {
        label: item, value: item
      }
    });
    let selectedDefaultVariantOption = tempData.filter((item: any) => {
      if (index == 0)
        return selectedVariantOption1 === item.value;
      if (index == 1)
        return selectedVariantOption2 === item.value;
      if (index == 2)
        return selectedVariantOption3 === item.value;

    });
    return itemData.value != "Default Title" ?
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', left: 10, right: 20 }}>
        <Text style={{ fontSize: 15, color: 'black', marginTop: 10 }}>{itemData.column}</Text>
        <Dropdown
          style={{ width: 180, margin: 8, height: 30, borderBottomColor: 'gray', borderWidth: 0, right: 15 }}
          selectedTextStyle={{ fontSize: 16, color: "#1776d3" }}
          data={tempData}
          maxHeight={300}
          labelField="label"
          valueField="value"
          value={selectedDefaultVariantOption[0]}
          // activeColor="#1776d3"
          onChange={item => {
            setSubAttributesFun(item, index, itemData);
          }}
          dropdownPosition="auto"
        />
      </View>
      : <></>
  }

  const shareOptions = (imageUrl: string) => {
    let data =
    {
      title: productInfo['heading'],
      message: props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'] == "US" ? "https://share.shopiads.com/Shopiads/shareMobileProduct/" + props.productId : "https://www.shopiads.ca/Shopiads/shareMobileProduct/" + props.productId, // Note that according to the documentation at least one of "message" or "url" fields is required
    }
    return data
  };

  const shareImageUrl = () => {
    Share.share(shareOptions(""))
  }


  const saveRatingApi = () => {
    if (props.getLocation.email) {
      setShowLoader(true);
      if (!shopifyOrder) {
        UUIDGenerator.getRandomUUID((uuid) => {
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.jpg',
            type: 'image/jpg',
            uri: billPath
          });
          props.uploadBillImage(productInfo['storeName'], postData, uuid).then((res) => {
            let param = {
              "id": uuid,
              "image": ["https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response],
              "storeId": productInfo['storeId'],
              "storeName": productInfo['storeName'],
              "companyId": productInfo['companyId'],
              "status": "N",
              "userId": props.getLocation.email,
              "userName": props.name
            }
            setShowLoader(false);
            props.uploadBillDetails(param, uuid).then((res) => {
              setBillPath("");
              saveRatingImage(res.payload.data, [], 0);
              setShowLoader(false);
            });
          });
        });
      }
      else {
        UUIDGenerator.getRandomUUID((uuid) => {
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.jpg',
            type: 'image/jpg',
            uri: billPath
          });
          console.log("bill data upload>>>>>>>>>>>");
          let param = {
            "id": uuid,
            "image": ["https://storage.googleapis.com/assets.shopiads.ca"],
            "storeId": productInfo['storeId'],
            "storeName": productInfo['storeName'],
            "companyId": productInfo['companyId'],
            "status": "A",
            "userId": props.getLocation.email,
            "userName": props.name
          }
          setShowLoader(false);
          props.uploadBillDetails(param, uuid).then((res) => {
            console.log("bill data response>>>>>>>>>>>", res);
            setBillPath("");
            saveRatingImage(res.payload.data, [], 0);
            setShowLoader(false);
          });

        });
      }
    } else {
      setShowLoginAlert(true);
    }
  }



  const saveRatingImage = (data, ratingArray, request) => {
    UUIDGenerator.getRandomUUID((uuid) => {
      console.log(uuid);
      if (request == 0) {
        if (billImagePath.length != 0) {
          let extension = billImagePath.split('.').pop();
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath
          });
          console.log(billImagePath)
          props.uploadRatingStoreImage(productInfo['storeName'], postData, uuid).then((res) => {
            setBillImagePath("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 1);
          });
        } else {
          saveRatingImage(data, ratingArray, 1);
        }
      } else if (request == 1) {
        if (billImagePath2.length != 0) {
          let extension = billImagePath2.split('.').pop();

          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath2
          });
          props.uploadRatingStoreImage(productInfo['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath2("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 2)
          });
        } else {
          saveRatingImage(data, ratingArray, 2)
        }
      } else if (request == 2) {
        if (billImagePath3.length != 0) {
          let extension = billImagePath3.split('.').pop();

          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath3
          });
          props.uploadRatingStoreImage(productInfo['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath3("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 3)

          });
        } else {
          saveRatingImage(data, ratingArray, 3)
        }

      } else if (request == 3) {
        if (billImagePath4.length != 0) {
          let extension = billImagePath4.split('.').pop();
          let postData = new FormData();
          postData.append('file', {
            name: 'newMyLooks.' + extension,
            type: 'image/' + extension,
            uri: billImagePath4
          });
          props.uploadRatingStoreImage(productInfo['storeName'], postData, uuid).then((res) => {
            console.log(res.payload.request._response);
            setBillImagePath4("")
            let path = "https://storage.googleapis.com/assets.shopiads.ca" + res.payload.request._response
            let imgArray = ratingArray;
            imgArray.push(path)
            saveRatingImage(data, imgArray, 4)
          });
        } else {
          saveRatingImage(data, ratingArray, 4)

        }
      } else {

        let param = { "id": uuid, "billId": data.id, "rating": ratingValue, "comment": comment, "offerId": "", "productId": props.productId, "storeId": productInfo['storeId'], "companyId": productInfo['companyId'], "status": !shopifyOrder ? "N" : "A", "ratingMedia": ratingArray, "ratingType": "Product", "userId": props.getLocation.email, "isShopifyOrderReview": shopifyOrder ? "Y" : "N", "userName": props.name }

        let email = Buffer.from(props.getLocation.email).toString('base64')
        console.log('DATAAAA->', param);
        props.uploadRatingDetails(param, email).then((res) => {
          console.log("RESSSS", res)
          setShowLoader(false);
          if (!shopifyOrder) {
            Alert.alert('Your rating has been submitted, after review by admin it will reflect.');
          }
          else {
            Alert.alert('Your rating has been submitted.');
          }

        });
      }
    });
  }

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");
        let options = {
          quality: 0.5,
          mediaType: 'photo',
        };

        ImagePicker.openCamera(options, response => {
          if (response.error) {

          } else if (response.uri) {
            if (showFile == 1) {
              setBillImagePath(response.uri)
            } else if (showFile == 2) {
              setBillImagePath2(response.uri)
            } else if (showFile == 3) {
              setBillImagePath3(response.uri)
            } else if (showFile == 4) {
              setBillImagePath4(response.uri)
            }
            setShowAlert(false);
          }
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };


  const showDiscount = (storeId: any) => {
    console.log("Store Id ::::", storeId);
    props.getStoreDetails(storeId).then((res: any) => {
      console.log('Store Details Response ::::: ', res);
      let result = res.payload.data;
      console.log('Store Details in product desc ::::: ', result);
      setStoreDetails(result)
      setModalVisible(true);
    });
  }

  console.log("productInfo Navigation:::::", productInfo);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}
      edges={['top']}>
      <View style={{ width: '100%', flex: 1 }}>
        {Object.keys(productInfo).length != 0 && (
          <ScrollView style={{ width: '100%', flex: 1 }}>
            <View style={{ width: '100%', height: '100%' }}>
              <View style={{ width: '100%', height: 350 }}>
                <ViewPager
                  onPageSelected={(event) => {
                    setViewPagerPosition(event.nativeEvent.position);
                  }}
                  transitionStyle='scroll'
                  orientation='horizontal'
                  showPageIndicator={false} style={{ height: 350, marginHorizontal: 10 }} initialPage={0}>
                  {productInfo['images'] ? productInfo['images'].map((item) => {
                    return (
                      <View style={{ width: '100%', height: 350 }}>
                        <TouchableOpacity onPress={() => {
                          let data: any = [];
                          for (let i = 0; i < productInfo['images'].length; i++) {
                            let obj = { freeHeight: false, url: productInfo['images'][i], index: item === productInfo['images'][i] ? i : -1 }
                            data.push(obj)
                          }
                          props.addToZoomImages(data)

                          //  console.log('DATAIMAMASMMddscfdsdSS', item);
                          navigation.push('Zoom')
                        }}>
                          <ThemeImage
                            source={{ uri: item }}
                            style={{ resizeMode: 'center', height: 350, width: "100%", paddingHorizontal: 5 }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  }) : <View style={{ width: '100%', height: 350 }}>
                    </View>}
                </ViewPager>
                {
                  productInfo['images'].length != 0 && (
                    <Text style={{ alignSelf: 'flex-end', bottom: 0, end: 0, margin: 10, fontSize: 14, position: 'absolute', color: 'black', paddingHorizontal: 10, paddingVertical: 5, borderColor: 'grey', borderWidth: 0.5, borderRadius: 2 }}>{(viewPagerPosition + 1) + '/' + productInfo['images'].length}</Text>
                  )
                }
                <View style={{ flexDirection: 'row', width: '100%', position: 'absolute', padding: 1 }}>
                  <TouchableOpacity onPress={() => {
                    navigation?.pop();
                  }} style={{ backgroundColor: WHITE, padding: 8, borderRadius: 3 }}>
                    <Image
                      source={BACK}
                      style={{ resizeMode: 'stretch', height: 20, width: 20, }}
                    />
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-end' }}>
                    <TouchableOpacity style={{ padding: 10, marginRight: 3 }} onPress={() => {
                      shareImageUrl()
                    }}
                    >
                      <Image
                        source={SHARE}
                        style={{ resizeMode: 'stretch', height: 20, width: 20, tintColor: BLUE }}
                      />
                    </TouchableOpacity>

                  </View>
                </View>
              </View>

              <View style={{ margin: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ ...styles.txtProductName, paddingHorizontal: 0 }}>{productInfo['heading']}</Text>
                </View>
                <View style={{ flexDirection: 'row', }}>
                  <Text style={styles.txtPrice}>{props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "US" ? "US $" + productInfo['offerPrice'] : "CAD " + productInfo['offerPrice']}</Text>
                  {productInfo['actualPrice'] > productInfo['offerPrice'] ? <Text style={styles.txtActualPrice}>{productInfo['actualPrice']}</Text> : false}
                </View>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10 }}>

                <Rating
                  type='star'
                  fractions={2}
                  readonly={true}
                  ratingCount={5}
                  showRating={false}
                  startingValue={productInfo['avgRating']}
                  imageSize={20}
                />

                <Text style={{ fontSize: 15, alignSelf: 'center', color: '#000', paddingHorizontal: 5, }}>
                  {productInfo['avgRating']}
                </Text>
                <TouchableOpacity onPress={() => {
                  onAddToFavourite(productInfo, 0)
                }}>
                  <Icons.AntDesign
                    name={foundInFav(productInfo) ? 'heart' : 'hearto'}

                    size={20}
                    color={'#1776d3'}
                  />
                </TouchableOpacity>

                <Text style={{ fontSize: 15, alignSelf: 'center', color: '#000', paddingHorizontal: 5, }}>
                  Likes ({productInfo['totalLikes']})
                </Text>
                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                  <TouchableOpacity style={styles.btnBlueBorder} onPress={() => {
                    checkInProducts(productInfo)
                  }}>
                    <Text style={styles.txtProductName}>{'Rate Product'}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <FlatList
                style={{ paddingHorizontal: 5, width: '100%', marginTop: 10 }}
                data={attributes}
                numColumns={2}
                renderItem={({ item, index }) => renderAttribute(item)}
              />
              {/* productInfo['isShopifyProduct'] !== "Y" && variantList != undefined && variantList.length > 0 && <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <Text style={{fontSize:15,marginTop:14,marginLeft:12}}>Select Other Variant </Text>
                <Dropdown
                  style={{ width:180,margin: 8,height: 30,borderBottomColor: 'gray',borderWidth: 0,right:10}}
                  selectedTextStyle={{fontSize: 16}}
                  data={variantList}
                  maxHeight={300}
                  labelField="heading"
                  valueField="id"
                  value={variantList[0]}
                  activeColor="#1776d3"
                  onChange={item => {
                    // setSubAttributesFun(item,index,itemData);
                  }}
                  dropdownPosition="auto"
                />
                </View>*/}
              {productInfo['isShopifyProduct'] == "Y" && variantList != undefined && variantList.length > 0 ? <FlatList
                style={{ paddingHorizontal: 5, width: '100%', marginTop: 10 }}
                data={selectedAttributes}
                keyExtractor={item => item}
                renderItem={({ item, index }) => renderSelectedAttributes(item, index)}
              /> : <></>}
              {productInfo['isShopifyProduct'] == "Y" && <View style={{ flexDirection: 'row', left: 15, marginTop: 10, right: 10 }}>
                <Text style={{ fontSize: 16, marginTop: 5, color: "#000000", flex: Platform.OS == 'ios' ? 0.4 : 0.5 }}>Quantity </Text>
                <View style={{ flexDirection: 'row', flex: Platform.OS == 'ios' ? 0.6 : 0.5 }}>
                  <TouchableOpacity style={{ ...styles.qtyBlueBorder, marginLeft: Platform.OS == 'ios' ? 10 : 2 }} onPress={() => {
                    quantityValue > 1 ? setQuantityValue(quantityValue - 1) : setQuantityValue(1)
                  }}>
                    <Text style={styles.txtProductName}>-</Text>
                  </TouchableOpacity>
                  <View style={{ paddingRight: 8, paddingLeft: 8 }}>
                    <Text style={{ fontSize: 20 }}>{quantityValue}</Text>
                  </View>
                  <TouchableOpacity style={{ ...styles.qtyBlueBorder }} onPress={() => {
                    setQuantityValue(quantityValue + 1)
                  }}>
                    <Text style={styles.txtProductName}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>}
              <View style={{ width: '100%', height: 7, marginTop: 10, backgroundColor: '#f4f4f4' }} />
              <Text style={{ ...styles.txtProductName, marginTop: 10 }}>{'Store'}</Text>
              <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#607d8b', padding: 8, borderRadius: 5, margin: 10 }} >
                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ borderColor: 'grey', borderWidth: 1, padding: 2, borderRadius: 4 }}>
                    <Image
                      source={{ uri: productInfo['logo'] }}
                      style={{ width: 60, height: 40, borderRadius: 2, resizeMode: 'stretch' }}>
                    </Image>
                  </View>
                  <View style={{ flexDirection: 'column', flex: 1 }}>
                    <Text style={{
                      ...styles.txtDesc, flex: 1, fontSize: 13, paddingHorizontal: 10,
                      color: '#CFCFCF',
                    }}>{productInfo['storeName'] + (storeInfo && storeInfo['code'] == "Primary" ? "" : " Branch: " + storeInfo['code'])}</Text>
                    <Text style={{
                      ...styles.txtDesc, flex: 1, fontSize: 13, paddingHorizontal: 10,
                      color: '#CFCFCF',
                    }} >{productInfo['address']}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', width: '100%', marginTop: 10, }}>
                  <TouchableOpacity style={checkIsUnfollow(productInfo) ? styles.btnWhiteBorder : styles.btnDisableWhite}
                    disabled={!checkIsUnfollow(productInfo)}
                    onPress={() => {
                      if (props.getLocation.email) {
                        addFollowingApi()
                      } else {
                        setShowLoginAlert(true);
                      }
                    }}>
                    <Image source={plus} style={{ width: 20, height: 20, tintColor: checkIsUnfollow(productInfo) ? WHITE : LIGHTGREY, marginRight: 10 }} ></Image>
                    <Text style={{ ...styles.txtStoreName, color: checkIsUnfollow(productInfo) ? 'white' : LIGHTGREY }}>{checkIsUnfollow(productInfo) ? 'Follow' : 'Following'}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{ ...styles.btnWhiteBorder }} onPress={() => {
                    props.setStoreKey(productInfo['storeId']);
                    navigation.push('StoresDesc');
                  }}>
                    <Image source={STORE_ICON} style={{ width: 20, height: 20, tintColor: WHITE, marginRight: 10 }} ></Image>
                    <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'View Store'}</Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity style={{ ...styles.btnWhiteBorder, width: '98%', marginTop: 10 }} onPress={() => { showDiscount(productInfo['storeId']) }}>
                  <Image source={DISCOUNT_ICON} style={{ width: 20, height: 20, tintColor: '#ffffff', marginRight: 10 }} ></Image>
                  <Text style={{ ...styles.txtStoreName, color: 'white' }}>{'Additional Discounts'}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%', marginTop: 10, height: 7, backgroundColor: '#f4f4f4' }} />
              <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }} >
                <Text style={{ ...styles.txtStoreName, flex: 1 }}>{'Description'}</Text>
              </View>
              <Text
                onTextLayout={onTextLayout}
                numberOfLines={textShown ? undefined : 4}
                style={{ ...styles.txtDesc, lineHeight: 21, padding: 10, marginTop: 10 }}>{productInfo['description']}</Text>
              {
                lengthMore ? <Text
                  onPress={toggleNumberOfLines}
                  style={{ ...styles.txtDesc, lineHeight: 21, marginTop: 10, width: '100%', textAlign: 'center' }}>{textShown ? 'Read less...' : 'Read more...'}</Text>
                  : null
              }
              <View style={{ width: '100%', height: 7, backgroundColor: '#f4f4f4' }} />
              <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }} >
                <Text style={{ ...styles.txtStoreName, flex: 1 }}>{'Ratings'}</Text>
              </View>
              <View style={{ width: '100%', alignItems: 'center' }}>
                <Text style={{ ...styles.txtProductName, marginTop: 5 }}>{'Overall Rating'}</Text>
                <Text style={{ ...styles.txtStoreName, fontSize: 25 }}>{productInfo['avgRating']}</Text>
                <Rating
                  type='star'
                  fractions={2}
                  readonly={true}
                  ratingCount={5}
                  showRating={false}
                  startingValue={productInfo.avgRating}

                  imageSize={40}
                />
                <Text style={{ ...styles.txtProductName, marginTop: 5 }}>{'Total ' + ratingInfo.length + ' Reviews'}</Text>
                <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }} >
                  {ratingInfo.map((item) => {
                    return renderRatingItem(item);
                  })
                  }
                  {ratingInfo.length != 0 && (
                    <TouchableOpacity style={{ width: '100%', padding: 10, }} onPress={() => {
                      props.setStoreKey(productInfo['storeId']);
                      navigation.push('AllReviews')
                    }}>
                      <Text style={{ ...styles.txtReview, width: '100%', textAlign: 'center', padding: 10 }}>{'All reviews ->'}</Text>
                    </TouchableOpacity>
                  )}
                </View>
                <View style={{ width: '100%', height: 7, marginTop: 10, backgroundColor: '#f4f4f4' }} />
                <Text style={{ ...styles.txtProductName, marginTop: 10, width: '100%' }}>{'Related Products'}</Text>
                <FlatList
                  style={{ marginTop: 10 }}
                  data={similarProducts}
                  horizontal={true}
                  renderItem={({ item, index }) => renderProductItem(item, index)}
                />
                <View style={{ width: '100%', height: 7, backgroundColor: '#f4f4f4', marginTop: 10 }} />
                <Text style={{ ...styles.txtProductName, marginTop: 10, width: '100%' }}>{'Store Products'}</Text>
                <FlatList
                  style={{ marginTop: 10 }}
                  data={storeProducts}
                  horizontal={true}
                  renderItem={({ item, index }) => renderProductItem(item, index)}
                />
              </View>
            </View>
          </ScrollView>)}
      </View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={(checkIsSnip() ? styles.btnGrey : styles.btnOrange)} onPress={() => {
          if (!checkIsSnip()) {
            addSnips();
          }
        }} disabled={checkIsSnip()}>
          <Image style={{ tintColor: '#ffffff', width: 30, height: 30 }} source={SNIP} ></Image>
          <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{(checkIsSnip() ? 'Snipped' : 'Snip')}</Text>
        </TouchableOpacity>
        {productInfo['isShopifyProduct'] != "Y" || productInfo['shopifyInventory'] > 0 ?
          <TouchableOpacity style={styles.btnBuyNow} onPress={() => {
            if (props.getLocation.email) {
              let email = Buffer.from(props.getLocation.email).toString('base64')
              props.productBuyNowClick(email, props.productId).then((res) => {
                console.log('VVVVVDDJKDKDKDD', res.payload.data);
              });
              if (productInfo['isShopifyProduct'] === "Y") {
                let url = props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'] == "US" ? "https://www.shopiads.com/redirectToCart/" + props.productId + "/" + props.getLocation.email + "/" + quantityValue : "https://www.shopiads.ca/redirectToCart/" + props.productId + "/" + props.getLocation.email + "/" + quantityValue;
                // let url = "https://"+storeInfo['websiteUrl']+"/cart/"+productInfo['shopifyVarientId']+":1?checkout[email]="+props.getLocation.email+"&attributes[utm_source]=shopiads"
                console.log("Product Url :::", url);
                Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
              }
              else if (productInfo['isShopifyProduct'] !== "Y" && productInfo['vendorUrl']) {
                Linking.openURL(productInfo['vendorUrl'] + "?utm_source=shopiads").catch(err => console.error("Couldn't load page", err));

              } else {
                Alert.alert("URL not found")
              }
            } else {
              setShowLoginAlert(true);
            }
          }}>
            <Image style={{ tintColor: '#ffffff', width: 30, height: 30 }} source={BUYNOW} ></Image>
            <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Buy Now'}</Text>
          </TouchableOpacity> :
          <TouchableOpacity style={styles.btnBuyNow}>
            <Image style={{ tintColor: '#ffffff', width: 30, height: 30 }} source={BUYNOW} ></Image>
            <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Sold Out'}</Text>
          </TouchableOpacity>
        }
      </View>
      <Overlay isVisible={rating} onBackdropPress={() => {
        showRating(false);
      }} >
        <Rating
          type='star'
          fractions={0}
          readonly={false}
          startingValue={ratingValue}
          onFinishRating={setRatingValue}
          ratingCount={5}
          imageSize={40}
        />
        <TextInput
          value={comment}
          onChangeText={setComment}
          style={styles.edtStyle}
          placeholder='Enter comment'
        />

        {!shopifyOrder && <Text>{'Bill Image'}</Text>}
        {!shopifyOrder && <TouchableOpacity
          onPress={() => {
            let options = {
              mediaType: 'photo',
              quality: 0.5,
            };
            ImagePicker.showImagePicker(options, response => {
              if (response.error) {
              } else if (response.uri) {
                setBillPath(response.uri)
              }
            });
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billPath}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>}
        <Text>{'Upload Image (Optional)'}</Text>
        <TouchableOpacity
          onPress={() => {
            //navigation.push('CameraRecord')

            setShowAlert(true);
            setShowFiles(1);

          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(2);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath2}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(3);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath3}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setShowAlert(true);
            setShowFiles(4);
          }}
          style={styles.containerRatingStyle}>
          <Text style={styles.txtBillStyle}>
            {billImagePath4}
          </Text>
          <Text style={styles.txtBrowseStyle}>
            {'Browse'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btnSubmit} onPress={() => {
          let pass = true;
          if (billPath.length == 0 && !shopifyOrder) {
            Alert.alert("Please Choose Bill Image")
            pass = false;
          }
          else if (ratingValue == 0 || ratingValue == undefined) {
            Alert.alert("Please Choose rating star")
            pass = false;
          }
          else if (pass) {
            showRating(false);
            saveRatingApi();
          }
        }}>
          <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Submit'}</Text>
        </TouchableOpacity>

        <Overlay isVisible={showAlert} style={{ alignItems: 'center', justifyContent: 'center' }} onBackdropPress={() => {
          setShowAlert(false);
        }}  >
          <View style={{ width: 200, height: 200, alignItems: 'center', justifyContent: 'center' }}>

            <Text>{'Choose Files'}</Text>

            <TouchableOpacity style={styles.btnSubmit} onPress={() => {
              let options = {
                quality: 0.5,
                mediaType: 'photo',
              };
              ImagePicker.showImagePicker(options, response => {
                if (response.error) {
                } else if (response.uri) {
                  if (showFile == 1) {
                    setBillImagePath(response.uri)
                  } else if (showFile == 2) {
                    setBillImagePath2(response.uri)
                  } else if (showFile == 3) {
                    setBillImagePath3(response.uri)
                  } else if (showFile == 4) {
                    setBillImagePath4(response.uri)
                  }
                  setShowAlert(false);
                }
              });
            }}>
              <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Choose Image'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnSubmit} onPress={() => { requestCameraPermission() }}>
              <Text style={{ ...styles.txtProductName, color: '#ffffff' }}>{'Take Snap'}</Text>
            </TouchableOpacity>
          </View>
        </Overlay>
      </Overlay>
      <LoginAlert
        showAlert={showLoginAlert}
        onLoginPress={() => {
          //  props.setUserData(null);
          Logout(navigation);

          setShowLoginAlert(false);
        }} onCancelPress={() => {
          setShowLoginAlert(false)
        }} />
      <LoaderAlert
        showAlert={showLoader}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
        style={{
          borderWidth: 0,
          width: '100%',
          borderRadius: 5,
          height: Platform.OS === 'android' ? Dimensions.get('screen').height - 300 : '60%',
          overflow: 'hidden',
          marginTop: 30,
          marginBottom: 30,
          backgroundColor: 'red'
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 22, backgroundColor: '#70757ab5' }}>
          <View style={{
            margin: 5, backgroundColor: 'white', borderRadius: 20, padding: 5, alignItems: 'center', shadowColor: '#000', shadowOffset: {
              width: 0,
              height: 2,
            }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, maxHeight: Dimensions.get('screen').height - 200
          }}>
            <ScrollView showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 17, width: '100%', textAlign: 'center', marginTop: 10 }}>
                  Apply Discount at Checkout
                </Text>
                <View style={{ marginTop: 15, paddingHorizontal: 10 }}>
                  {Object.keys(storeDetails).length != 0 && storeDetails['shopifyDiscount'].length > 0 ?
                    storeDetails['shopifyDiscount'].map((item: any) => {
                      return (
                        <View style={{ flexDirection: 'row', marginTop: 6, right: 10 }} key={Math.floor(Math.random() * 100000)}>
                          <Text style={{ fontSize: 15, marginLeft: 8 }}> {'->'}</Text>
                          {Platform.OS === 'ios' ?
                            <TextInput
                              value={item}
                              editable={false}
                              multiline
                            /> :
                            <Text style={{ fontSize: 15, marginLeft: 10, marginRight: 10 }} selectable={true} selectionColor='#c0c8cf'>{item}</Text>}
                        </View>
                      )
                    }) :
                    <View style={{ marginTop: 20, paddingHorizontal: 10, height: 100 }}>
                      <Text style={{ fontWeight: 'bold', fontSize: 13, textAlign: 'center', marginTop: 20 }}>
                        No additional discounts available at this moment
                  </Text>
                    </View>
                  }
                </View>
              </View>
            </ScrollView>
            <View style={{ alignItems: 'center', marginTop: 50 }}>
              <TouchableOpacity style={{ ...styles.btnWhiteBorder, width: 100, backgroundColor: '#007bff', marginBottom: 18, borderRadius: 5 }} onPress={() => {
                setModalVisible(false);
              }}>
                <Text style={{ ...styles.txtStoreName, color: 'white', fontSize: 15, marginTop: -3 }}>{'Close'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView >
  )
}


const mapStateToProps = (state: any) => ({
  getFavData: state.productTab.favourite,
  productId: state.productTab.productKey,
  store: state.productTab.store,
  getLocation: state.location,
  getFavStore: state.productTab.followStore,
  snips: state.productTab.snips,
  name: state.location.name,
  storeId: state.productTab.storeKey,
  userData: state,
});
const mapDispatchToProps = dispatch => ({
  onSnipsSelected: (data: any) => dispatch(onSnipsSelected(data)),
  getProductDetails: (userid: string) => dispatch(getProductDetails(userid)),
  addToFavourite: (data: any) => dispatch(addToFavourite(data)),
  deleteToFavourite: (data: any) => dispatch(deleteToFavourite(data)),
  setProductKey: (data: any) => dispatch(setProductKey(data)),
  getRating: (userid: any) => dispatch(getRating(userid)),
  getSimilarProducts: (userId: any, marketPlace: string) => dispatch(getSimilarProducts(userId, marketPlace)),
  setStoreKey: (data) => dispatch(setStoreKey(data)),
  setUserData: (data) => dispatch(setUserData(data)),
  getStoreDetails: (userId) => dispatch(getStoreDetails(userId)),
  addUserSnips: (userId, data) => dispatch(addUserSnips(userId, data)),
  uploadBillImage: (storeName, data, billId) => dispatch(uploadBillImage(storeName, data, billId)),
  uploadBillDetails: (data, billId) => dispatch(uploadBillDetails(data, billId)),
  uploadRatingStoreImage: (storeName, data, imageid) => dispatch(uploadRatingStoreImage(storeName, data, imageid)),
  addFollowingApi: (data, userId) => dispatch(addFollowingApi(data, userId)),
  uploadRatingDetails: (data, userId) => dispatch(uploadRatingDetails(data, userId)),
  addLikeProducts: (data, userId) => dispatch(addLikeProducts(data, userId)),
  removeLikeProducts: (userId, item) => dispatch(removeLikeProducts(userId, item)),
  getAllFavoriteProduct: (userId) => dispatch(getAllFavoriteProduct(userId)),
  getAllStoreProducts: (storeId) => dispatch(getAllStoreProducts(storeId)),
  getAllFollowStore: (userId) => dispatch(getAllFollowStore(userId)),
  addToFollow: (data) => dispatch(addToFollow(data)),
  addToZoomImages: (data) => dispatch(addToZoomImages(data)),
  getUserSnippets: (userId: string) => dispatch(getUserSnippets(userId)),
  addToSnips: (data: any) => dispatch(addToSnips(data)),
  addToVideoUrl: (data: string) => dispatch(addToVideoUrl(data)),
  checkInProducts: (userId) => dispatch(checkInProducts(userId)),
  getImageUrlToBase64: (path) => dispatch(getImageUrlToBase64(path)),
  productBuyNowClick: (userId, productId) => dispatch(productBuyNowClick(userId, productId)),
  isUserShopifyOrder: (userId, productId) => dispatch(isUserShopifyOrder(userId, productId)),
  getVariantList: (productId: any) => dispatch(getVariantList(productId)),

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDesc)