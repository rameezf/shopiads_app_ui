import React, { useEffect } from 'react'

import { NavigationContainer, StackActions } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import AuthNavigation from './AuthNavigation'
import { View, Text, useWindowDimensions, Button, Platform, Alert, Linking, TouchableOpacity } from 'react-native'
import BottomTab from './BottomTabNavigation'
import auth, { firebase } from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import DrawerNavigation from './DrawerNavigation'
import { screensEnabled } from 'react-native-screens'
import Splash from '../Screens/Splash'
import { AuthContext } from '../Context/Context'
import Animated from 'react-native-reanimated'
import Register from '../Screens/Register'
import { GoogleSignin } from '@react-native-google-signin/google-signin';

import { Maps } from '../Screens/maps';
import { setUserData } from '../Actions/auth';
import { connect } from 'react-redux'
import FavDesc from '../Screens/favDesc'
import StoresDes from '../Screens/storesDesc'
import { Buffer } from "buffer"

import jwt_decode from 'jwt-decode';

import {
    AccessToken,
    GraphRequest,
    GraphRequestManager,
    LoginManager,
} from 'react-native-fbsdk';
import ProductDesc from '../Screens/ProductDesc'
import { checkUserExist, getAllFavoriteProduct, getUserInfoByAppleId, updateFirebaseToken, userLogin } from '../Actions/Offer'
import { parse } from 'react-native-svg'
import AllReviews from '../Screens/AllReviews'
import Search from '../Screens/Search'
import AboutStore from '../Screens/AboutStore'
import CategoriesProducts from '../Screens/CategoriesProducts'
import OfferDesc from '../Screens/OfferDesc'
import { saveUserEmail, saveUserName } from '../Actions/location'
import ViewAllProducts from '../Screens/ViewAllProduct'
import CameraRecord from '../Screens/CameraRecord'
import Zoom from '../Screens/Zoom'
import {
    appleAuth,
    AppleButton,
} from '@invertase/react-native-apple-authentication';
import RatingVideoPlayer from '../Screens/RatingVideoPlayer'
import StoreViewAllProduct from '../Screens/StoreViewAllProduct'
import MyProfile from '../Screens/MyProfile'
import ContactUs from '../Screens/ContactUs'

const RootStack = createStackNavigator()
import messaging from '@react-native-firebase/messaging';
import { BLUE, WHITE } from '../Constants/colors'
import ThemeButton from '../Components/Button'
import { Overlay } from 'react-native-elements'
import { setOfferId, setProductKey, setStoreKey } from '../Actions/product'
// import { TouchableOpacity } from 'react-native-gesture-handler'
import { ThemeImage } from '../Components/ThemeImage'
import ProductVariantDesc from '../Screens/ProductVariantDesc';
import VersionCheck from 'react-native-version-check';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { store } from "../Stores/ConfigureStore";




const AppContainer = (props) => {
    const [isLoading, setLoading] = React.useState(true)
    const [user, setUser] = React.useState(props.userData)
    const [notificationData, setNotification] = React.useState(null);
    const [notificationCountry, setNotificationCountry] = React.useState(null);

    const randmath = () => {
        return Math.floor(Math.random() * 1000000000)
    }

    const navigationRef = React.createRef();

    const notificationConfigure = async () => {
        // check if we have permissions
        let enabled = await messaging().hasPermission();

        if (enabled === messaging.AuthorizationStatus.AUTHORIZED) {
            const fcmToken = await messaging().getToken();
            //   console.log("TOKEN", fcmToken);

            if (fcmToken) {
                // console.log(fcmToken);
            } else {
                // user doesn't have a device token yet
                // console.warn("no token");
            }
        } else {
            await messaging().requestPermission();
            console.log("requested");

            enabled = await messaging().hasPermission();
            console.log("done", enabled);
            if (!enabled) {
                return false;
            }
        }

        return true;
    };

    useEffect(() => {
        console.log("Check Update needed use effect call :::")
        checkVersion();
    }, []);

    const checkVersion = async () => {
        try {
            let versionConfig = {};
            const currentVersion = VersionCheck.getCurrentVersion();
            if (Platform.OS === "ios") {
                const packageName = 'com.apcod.shopiads';   // await VersionCheck.getPackageame();
                const countryName = await VersionCheck.getCountry();
                console.log("Country Name :::", countryName);
                versionConfig["provider"] = () =>
                    fetch(`https://itunes.apple.com/ca/lookup?bundleId=${packageName}`)
                        .then(r => r.json())
                        .then((res) => res?.results[0]?.version || currentVersion);
            }
            console.log("Version Config ::::::", versionConfig);
            const latestVersion = await VersionCheck.getLatestVersion(versionConfig);
            console.log("Latest Version ::::::", latestVersion, "::::: currentVersion :::", currentVersion);
            const updateNeeded = await VersionCheck.needUpdate({
                latestVersion,
                currentVersion,
                provider: Platform.OS === "ios" ? "appStore" : "playStore",
            });
            console.log("Update Needed Data Version ::::::::", updateNeeded);
            if (updateNeeded && updateNeeded?.isNeeded) {
                //Alert the user and direct to the app url
                Alert.alert('Please Update',
                    'You will have to update your app to the latest version to continue using',
                    [{
                        text: 'Update',
                        onPress: () => {
                            Linking.canOpenURL('https://play.google.com/store/apps/details?id=com.apcod.shopiads').then(supported => {
                                if (supported) {
                                    if (Platform.OS == 'android') {
                                        Linking.openURL('https://play.google.com/store/apps/details?id=com.apcod.shopiads');
                                    } else {
                                        Linking.openURL('https://apps.apple.com/ca/app/shopiads/id1601835714');
                                    }
                                } else {
                                    console.log("Don't know how to open URI: ");
                                }
                            });
                        }
                    }])
            }
        } catch (error) {
            console.log("Error in check version ::::", error);
        }
    }


    const requestPermissionNotification = async () => {

        const token = await messaging().getToken();
        //  console.log('TOKENN',token);

        messaging().onMessage(async (remoteMessage) => {
            if (remoteMessage) {
                let data = { ...remoteMessage.data, 'title': remoteMessage.notification.title, "message": remoteMessage.notification.body }
                console.log('FCM Message Data Remoted onMessaging  onMessage :', data);
                setNotification(data)
                if (data?.country_origin == "Canada") {
                    setNotificationCountry("CA");
                } else {
                    setNotificationCountry('US');
                }
                storeMessagingId(remoteMessage.data.product_id);
            }
        });
        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then(async remoteMessage => {
                console.log('FCM Message Data: getInitialNotification ', remoteMessage);
                if (remoteMessage) {
                    const storeMessageId = await getStoreMessagingId();
                    console.log("FCM Message Data: getInitialNotification ::: store Message ID ::::", storeMessageId, "::Product Id ::", remoteMessage.data.product_id)
                    if (storeMessageId == remoteMessage.data.product_id) {
                        // setNotification(null);
                        return false;
                    } else {
                        let data = { ...remoteMessage.data, 'title': remoteMessage.notification.title, "message": remoteMessage.notification.body }
                        setNotification(data)
                        if (data?.country_origin == "Canada") {
                            setNotificationCountry("CA");
                        } else {
                            setNotificationCountry('US');
                        }
                        storeMessagingId(remoteMessage.data.product_id);
                    }
                }
                //console.log('VVJFJFJFJFJFJFJFJF',remoteMessage.notification.title);
            });

        messaging().onNotificationOpenedApp(remoteMessage => {
            if (remoteMessage) {
                console.log('FCM Message Data  onNotificationOpenedApp ', remoteMessage);
                let data = { ...remoteMessage.data, 'title': remoteMessage.notification.title, "message": remoteMessage.notification.body }
                setNotification(data);
                if (data?.country_origin == "Canada") {
                    setNotificationCountry("CA");
                } else {
                    setNotificationCountry('US');
                }
                storeMessagingId(remoteMessage.data.product_id);
            }
            // navigation.navigate(remoteMessage.data.type);
        });
    }


    const storeMessagingId = async (value) => {
        try {
            await AsyncStorage.setItem('NOTIFICATION_ID', value)
        } catch (e) {
            console.log("Error in store messaging Id ");
        }
    }


    const getStoreMessagingId = async () => {
        try {
            const value = await AsyncStorage.getItem('NOTIFICATION_ID')
            if (value !== null) {
                return value;
            }
        } catch (e) {
            console.log("Error in get messaging Id ");
        }
    }


    const updateFirebaseToken = async (userId) => {
        const firebaseToken = await messaging().getToken();
        props.updateFirebaseToken(userId, firebaseToken, Platform.OS).then((res) => {
            console.log('RESPSOSSNNCNCNCNCNCNCNCNC', res)
        })
    }
    useEffect(() => {
        notificationConfigure();
        requestPermissionNotification();
        if (user && props.getLocation && props.getLocation.email) {
            setUser(props.getLocation.email)
            let email = Buffer.from(props.getLocation.email).toString('base64');
            updateFirebaseToken(email);
        }
    })


    const authContext = React.useMemo(() => {

        return {
            Login: async (email, password, navigation) => {
                await auth().signInWithEmailAndPassword(email, password).then(res => {
                    console.log('EMAILLLLgbbkb ---> ffff', res);
                    setUser(email)
                    props.setUserData(email)
                    props.saveUserEmail(email);
                    navigation.push("home")

                }).catch(err => {
                    var errorMessage = err.message.replace(err.code, '').replace('[]', '');

                    alert(errorMessage)
                })
            },
            setRegister: async (email, password, username, mobileNumber, gender, photo, navigation) => {
                setLoading(false)
                try {
                    await auth().createUserWithEmailAndPassword(email, password).then(res => {
                        res.user.updateProfile({
                            displayName: username
                        }).then((s) => {
                            props.saveUserName(username)
                        })
                        props.registerApi(res.user.uid, {
                            'email': res.user.email, 'emailOk': 'Y', 'emailSubcription': 'Y',
                            'name': username, 'mob': mobileNumber, 'mobOk': 'Y',
                            'createdBy': res.user.email,
                            'updatedBy': res.user.email,
                            'authType': 'AU', 'password': '', 'country': '', 'gender': gender,
                            'companyId': '', 'roleId': ''
                        }).then((res) => {
                            alert("Registered Successfully")
                            setUser(email)
                            props.saveUserName(username)
                            props.setUserData(email)
                            props.saveUserEmail(email);
                            navigation.push("home")
                        });
                    }).catch(err => {
                        alert(err)
                    })

                } catch (e) {
                    var errorMessage = err.message.replace(err.code, '').replace('[]', '');
                    alert(errorMessage)

                }
            },
            guestLogin: (navigation) => {
                props.saveUserEmail(null);
                setUser("Guest")
                setLoading(false)
                navigation.push("home")
            },
            googleSignIn: async (navigation) => {
                // Get the users ID token
                try {
                    let isLogin = await GoogleSignin.isSignedIn()
                    if (isLogin) {
                        await GoogleSignin.signOut()
                    }
                    const { idToken } = await GoogleSignin.signIn();
                    // Create a Google credential with the tokenx
                    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
                    // Sign-in the user with the credential
                    await auth().signInWithCredential(googleCredential);
                    // const userInfo = await GoogleSignin.signIn();
                    //setUser("google")
                    navigation.push("home")

                } catch (error) {
                    console.log('TTOTKFKFKFEEOEOE', error.code);
                    //    setUser("google")
                }

            },
            facebookSignIn: async (navigation) => {
                LoginManager.logOut();
                LoginManager.logInWithPermissions(['public_profile', 'email']).then(
                    login => {
                        if (login.isCancelled) {
                            console.log("Login Dat   a FB ::::", login);
                        } else {
                            AccessToken.getCurrentAccessToken().then(async (data) => {
                                console.log("Data Access Token ::::", data);
                                const token = data.accessToken.toString();
                                const PROFILE_REQUEST_PARAMS = {
                                    fields: {
                                        string: 'id,email,name,picture,first_name,last_name',
                                    },
                                };
                                const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
                                console.log("FaceBook Credential ::::", facebookCredential);
                                await auth().signInWithCredential(facebookCredential).then(res => {
                                    console.log("Facebook Login Credential ::::", res.additionalUserInfo.profile.email);
                                    if (res && res.additionalUserInfo.profile.email != null && res.additionalUserInfo.profile.email != undefined && res.additionalUserInfo.profile.email != "") {
                                        navigation.push('home')
                                    } else {
                                        Alert.alert('Facebook SignIn',
                                            "Your email is not verified on Facebook. Please use another login option",
                                            [
                                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                                            ])
                                    }
                                }).catch(async (err) => {
                                    var errorMessage = err.message.replace(err.code, '').replace('[]', '');
                                    alert(errorMessage)
                                });
                            });
                        }
                    },
                    error => {
                        console.log('Login fail with error FB : ' + error);
                    },
                );


            },
            appleSignIn: async (navigation) => {
                try {
                    const appleAuthRequestResponse = await appleAuth.performRequest({
                        requestedOperation: appleAuth.Operation.LOGIN,
                        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
                    });
                    //console.log('appleAuthRequestResponse', appleAuthRequestResponse);
                    const {
                        user: newUser,
                        //   email,
                        nonce,
                        fullName,
                        identityToken,
                        realUserStatus /* etc */,
                    } = appleAuthRequestResponse;


                    //console.log(fullName);

                    const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce);
                    auth()
                        .signInWithCredential(appleCredential)
                        .then((user) => {
                            let givenName = fullName.givenName;
                            let lastName = fullName.familyName;
                            if ((givenName && lastName) || (user.email)) {
                                user.user.updateProfile({
                                    displayName: givenName && lastName ? givenName + " " + lastName : user.email
                                }).then((s) => {
                                    let userName = user.displayName ? user.displayName : user.email;
                                    props.saveUserName(userName)

                                    props.setUserData(user.email)
                                    props.saveUserEmail(user.email);
                                })

                            }

                            navigation.push('home')
                            // Succeed fully user logs in
                        })
                        .catch((err) => {
                            var errorMessage = err.message.replace(err.code, '').replace('[]', '');
                            alert(errorMessage)
                        });

                    //console.log(params);
                    //onSuccess(params);
                } catch (error) {
                    if (error.code === appleAuth.Error.CANCELED) {
                        //  console.warn('User canceled Apple Sign in.');
                    } else {
                        //console.warn('ERROR canceled Apple Sign in.');
                        // console.error(error);
                    }
                }

            },
            Logout: async (navigation) => {
                setLoading(false)
                // setUser(null)
                navigation.push('authStack')
                if (user == "Guest") {
                    setUser(null)
                    props.saveUserEmail(null);

                } else {
                    props.saveUserEmail(null);

                    try {
                        await auth().signOut().then(res => {
                            alert("SignOut Successfully")
                            props.saveUserEmail(null);
                            props.saveUserName(null);


                            setUser(null)
                        }).catch(err => {
                            setUser(null)
                        })
                    } catch (e) {
                        console.error(e);
                    }
                }
            },
        }
    }, [])

    React.useEffect(() => {
        if (Platform.OS == 'android') {
            GoogleSignin.configure({
                webClientId: '15194957769-p5gs59bq6cppljkrgd02hse3p9v9f1qo.apps.googleusercontent.com'
            });
        } else {
            GoogleSignin.configure({
            });
        }
        if (firebase.auth().onAuthStateChanged(user => {
            if (user && user.email !== null) {
                console.log("User Data ::::::", user)
                let email = Buffer.from(user.email).toString('base64');
                console.log("VKFKFKFKKFVFKKFKFK", email);
                props.checkUserExist(email).then((res) => {
                    if (res && res.payload && res.payload.status == 200 && res.payload.data != '') {
                        setUser(user.email)
                        let userName = user.displayName ? user.displayName : user.email;
                        props.saveUserName(userName)
                        props.setUserData(user.email)
                        props.saveUserEmail(user.email);
                    } else {
                        let phoneNumber = '';
                        let email = user.email;
                        let name = user.displayName ? user.displayName : email;
                        let uid = user.uid
                        let data = {
                            email: email,
                            createdBy: email,
                            updatedBy: email,

                            emailOk: "Y",
                            emailSubcription: "Y",
                            name: name,
                            mob: phoneNumber,
                            mobOk: "Y",
                            authType: "S",
                            password: "",
                            country: "",
                            gender: "",
                            companyId: "",
                            roleId: "",
                            createdBy: email,
                            updatedBy: email

                        }
                        //console.log('VVKCVFKD', data);
                        //console.log(uid);
                        props.registerApi(uid,
                            data
                        ).then((res) => {
                            if (res.payload.status == 200) {
                                setUser(user.email)
                                console.log("VVKKFFKFKFKF", "REGISTERR");
                                props.saveUserName(user.displayName)
                                props.setUserData(user.email)
                                props.saveUserEmail(user.email);
                            }
                        });
                    }
                });
            } else {
                console.log("Facebook Data user not available :::::");
            }
        }))
            setTimeout(() => {
                setLoading(false)
            }, 1000)
    }, [])

    if (isLoading)
        return <Splash />

    return (
        <View style={{ width: '100%', height: "100%" }}>
            <AuthContext.Provider value={authContext}>

                <NavigationContainer ref={navigationRef} >

                    {/* (<DrawerNavigation />) : */}

                    <RootStack.Navigator initialRouteName={props.getLocation.email ? "home" : "authStack"} screenOptions={{ headerShown: false }}>
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"home"} component={BottomTab} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"authStack"} component={AuthNavigation} options={{ headerShown: false }} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"StoresDesc"} component={StoresDes} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"ProductDesc"} component={ProductDesc} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"ProductVariantDesc"} component={ProductVariantDesc} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"FavScreen"} component={FavDesc} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"AllReviews"} component={AllReviews} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"Maps"} component={Maps} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"AboutStore"} component={AboutStore} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"CategoriesProducts"} component={CategoriesProducts} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"Login"} component={AuthNavigation} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"OfferDesc"} component={OfferDesc} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"Search"} component={Search} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"ViewAllProduct"} component={ViewAllProducts} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"StoreViewAllProduct"} component={StoreViewAllProduct} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"CameraRecord"} component={CameraRecord} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"Zoom"} component={Zoom} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"RatingVideoPlayer"} component={RatingVideoPlayer} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"MyProfile"} component={MyProfile} />
                        <RootStack.Screen options={{ gestureEnabled: false }} name={"ContactUs"} component={ContactUs} />
                    </RootStack.Navigator>
                </NavigationContainer>
            </AuthContext.Provider>


            <Overlay isVisible={notificationData != null}>
                <View style={{ width: 290, borderRadius: 20, paddingBottom: 10, backgroundColor: WHITE }}>
                    <Text style={{ margin: 10, width: '100%', textAlign: 'center', fontSize: 17, fontWeight: 'bold', width: '100%' }}>{'Notification for'} {notificationData?.country_origin} {'Store'}</Text>
                    <ThemeImage
                        source={{ uri: notificationData ? notificationData.imagePath : '' }}
                        style={{ width: '100%', height: 100 }}
                    />
                    <Text style={{ margin: 10, fontSize: 14, fontWeight: 'bold', width: '100%' }}>{notificationData ? notificationData.title : ''}</Text>
                    <Text style={{ margin: 10, fontSize: 14, flex: 1, width: '100%' }}>{notificationData ? notificationData.message : ''}</Text>

                    {store && store.getState() && store.getState().location.locationCountry != null && notificationCountry != null
                        && store.getState().location.locationCountry['countryId'].toUpperCase() == notificationCountry.toUpperCase() ? <ThemeButton
                            btnThemeStyle={{ width: '100%' }}
                            onBtnPress={() => {
                                if (notificationData.type == "product") {
                                    props.setProductKey(notificationData.product_id)
                                    if (navigationRef.current.getCurrentRoute().name == "ProductDesc") {
                                        navigationRef.current.dispatch(
                                            StackActions.replace("ProductDesc"))
                                    } else {
                                        navigationRef.current?.navigate("ProductDesc");
                                    }
                                } else {
                                    props.setStoreKey(notificationData.store_id)
                                    props.setOfferId(notificationData.offer_id)
                                    if (navigationRef.current.getCurrentRoute().name == "OfferDesc") {
                                        navigationRef.current.dispatch(
                                            StackActions.replace("OfferDesc"))

                                    } else {
                                        navigationRef.current?.navigate("OfferDesc");
                                    }
                                }
                                setNotification(null)
                            }}
                            btnText={'Detail'} /> : false}
                    <TouchableOpacity
                        onPress={() => {
                            setNotification(null)
                        }}
                    >
                        <Text style={{ width: '100%', padding: 10, marginTop: 10, fontWeight: 'bold', textAlign: 'center', color: BLUE }}>{'Dismiss'}</Text>
                    </TouchableOpacity>
                </View>
            </Overlay>
        </View>

    )
}

const mapStateToProps = state => ({
    userData: state.auth.userData,
    getLocation: state.location,
})
const mapDispatchToProps = dispatch => ({
    setUserData: (data) => dispatch(setUserData(data)),
    getUserInfoByAppleId: (id) => dispatch(getUserInfoByAppleId(id)),
    registerApi: (id, params) => dispatch(userLogin(id, params)),
    saveUserEmail: (email) => dispatch(saveUserEmail(email)),
    getAllFavoriteProduct: (userId) => dispatch(getAllFavoriteProduct(userId)),
    checkUserExist: (userId) => dispatch(checkUserExist(userId)),
    saveUserName: (name) => dispatch(saveUserName(name)),
    setProductKey: (id) => dispatch(setProductKey(id)),
    setStoreKey: (id) => dispatch(setStoreKey(id)),
    setOfferId: (id) => dispatch(setOfferId(id)),

    updateFirebaseToken: (userId, firebaseToken, device) => dispatch(updateFirebaseToken(userId, firebaseToken, device))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppContainer)

