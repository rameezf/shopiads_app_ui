import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, StyleSheet, Alert, TouchableOpacity, FlatList, NativeModules, KeyboardAvoidingView, ScrollView } from 'react-native';
import { HomeHeader } from '../Components/Heading';
import { MyStatusBar } from '../Components/MyStatusBar';
import { connect } from 'react-redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icons from '../Constants/Icons';
import Icon from '../Constants/Icons'
import Offers from '../Screens/Offers';
import Product from '../Screens/Product';
import Stores from '../Screens/Stores';
import Snips from '../Screens/Snips';
import Profile from '../Screens/Profile';
import { ChangeLocation } from '../Screens/ChangeLocation';
import { getArea, getCity, saveUserLocationArea, saveUserLocationCity, saveUserLocationCountry, saveUserMarketPlace } from '../Actions/location';
import Modal from 'react-native-modalbox'
import { checkUserExist, userLogin } from '../Actions/Offer'

import { Image, Overlay } from 'react-native-elements';
import ThemeButton from '../Components/Button';
import { setLocationState } from '../Actions/auth';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { SAVE_USER_LOCATION_AREA } from '../Constants/ActionTypes';
import { SafeAreaView } from 'react-native-safe-area-context';
import {
    all,
    close, CANADA_FLAG, US_FLAG
} from '../Constants/images';
import DropDownPicker from 'react-native-dropdown-picker';
import CameraRecord from '../Screens/CameraRecord';
import { addToSearch } from '../Actions/product';
import { BLUE } from '../Constants/colors';
import * as RNLocalize from "react-native-localize";
import RNRestart from 'react-native-restart';
import { Buffer } from "buffer";
import DeviceCountry from 'react-native-device-country';


const BottomTabStack = createBottomTabNavigator();

const BottomTab = ({ navigation, route, ...props }) => {
    const [isSearch, setSearch] = React.useState(true);
    const [tabName, setTabName] = React.useState('');
    const [showUserLocation, setUserLocation] = React.useState(false);
    const [text, setText] = useState('');
    const [focus, setFocus] = useState(false);
    const [valueCity, getValueCity] = useState({ cityName: (props.getLocation && props.getLocation.locationDataCity && props.getLocation.locationDataCity.cityName ? props.getLocation.locationDataCity.cityName : ''), cityId: (props.getLocation.locationDataCity ? props.getLocation && props.getLocation.locationDataCity.cityId : "") });
    const [valueArea, getValueArea] = useState({ areaName: (props.getLocation.locationDataArea && props.getLocation.locationDataArea.areaName ? props.getLocation.locationDataArea.areaName : ''), areaId: (props.getLocation && props.getLocation.locationDataArea && props.getLocation.locationDataArea.areaId ? props.getLocation.locationDataArea.areaId : "") });
    const [location, setlocation] = useState(false);
    const [cityArr, setCityArr] = useState([]);
    const [AreaArr, setAreaArr] = useState([]);
    const [modalVisible, setModalVisible] = useState(false)
    const [modalVisible2, setModalVisible2] = useState(false)
    const [user, setUser] = React.useState(null)
    const [valueCountry, getValueCountry] = useState();
    const [countryArr, setCountryArr] = useState([{ "countryId": 'US', "countryName": 'US' }, { "countryId": 'CA', "countryName": 'Canada' }]);

    const onFocus = () => {
        setFocus(true)
    }


    // useEffect(() => {
    //     getAreabasedonCity()
    //     console.log('AREA OPEN', areaOpen)
    // }, [valueCity])

    useEffect(() => {
        console.log('------> User Info change City', props.userData);
        let userId = "Z3Vlc3Q=";
        props.getCity(userId).then(res => {
            console.log("Props get State user data :::::", res);
            if (res.payload.status == 200) {
                res.payload.data.map(item => {
                    setCityArr(oldArr => [...oldArr, { cityId: item.id, cityName: item.name }])
                })
                if (!props.getLocation.locationDataCity && !props.getLocation.locationDataArea) {
                    setUserLocation(true)
                }
                getAreabasedonCity(valueCity);
            }
        });
    }, [])


    const getCityVal = (i) => {
        props.saveUserLocationCity(i)
        console.log('i->', i);
        getValueCity(i)
        setModalVisible(false)
        setAreaArr([])
        setAreaId(null);
        getValueArea({ "areaId": "", "areaName": "" })
        getAreabasedonCity(i);
    }

    const getAreabasedonCity = (value) => {
        console.log("Area base city call ::::")
        let userId = "Z3Vlc3Q=";
        if (value) {
            props.getArea(userId, value.cityId).then(res => {
                if (res.payload.status == 200) {
                    console.log(res)
                    tempArray = [];
                    res.payload.data.map(item => {
                        tempArray.push({ areaId: item.id, areaName: item.name })
                    });
                    setAreaArr(tempArray);
                }
            })
        }
    }

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(props.getLocation && props.getLocation.locationDataCity && props.getLocation.locationDataCity.cityId ? props.getLocation.locationDataCity.cityId : null);
    const [areaOpen, setAreaOpen] = useState(false);
    const [areaId, setAreaId] = useState(props.getLocation && props.getLocation.locationDataArea && props.getLocation.locationDataArea.areaId ? props.getLocation.locationDataArea.areaId : null);

    const [countryOpen, setCountryOpen] = useState(false);
    const [countryId, setCountryId] = useState("CA");

    const [search, setSearchValue] = useState('')
    const toggleOverlay = () => {
        setUserLocation(false);
    };


    useEffect(() => {
        //   DeviceCountry.getCountryCode().then((result) => {
        //     if(props.getLocation && props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "US") {
        //       getValueCountry({"countryId":'US',"countryName":'US'});
        //       setCountryId("US");
        //     } else if (props.getLocation && props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "CA") {
        //       getValueCountry({"countryId":'CA',"countryName":'Canada'});
        //       setCountryId("CA");
        //     } else if (result.code.toUpperCase() == "US") {
        //       getValueCountry({"countryId":'US',"countryName":'US'});
        //       setCountryId("US");
        //     } else {
        //       getValueCountry({"countryId":'CA',"countryName":'Canada'});
        //       setCountryId("CA");
        //     }
        //     console.log("::: Country Id :::",countryId);
        //   })
        // .catch((e) => {
        //   console.log(e);
        // });
        if (props.getLocation && props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "US") {
            getValueCountry({ "countryId": 'US', "countryName": 'US' });
            setCountryId("US");
        } else if (props.getLocation && props.getLocation.locationCountry != null && props.getLocation.locationCountry['countryId'].toUpperCase() == "CA") {
            getValueCountry({ "countryId": 'CA', "countryName": 'Canada' });
            setCountryId("CA");
        } else if (RNLocalize.getCountry().toUpperCase() == "US") {
            getValueCountry({ "countryId": 'US', "countryName": 'US' });
            setCountryId("US");
            props.saveUserLocationCountry({ "countryId": 'US', "countryName": 'US' });
        } else {
            getValueCountry({ "countryId": 'CA', "countryName": 'Canada' });
            setCountryId("CA");
            props.saveUserLocationCountry({ "countryId": 'CA', "countryName": 'Canada' });
        }
    }, [])


    const getAreaVal = (i) => {
        getValueArea(i);
        setModalVisible2(false);
    }

    const getCountryVal = (i) => {
        console.log('Country Val ->', i, countryId);
        getValueCountry(i);
        props.saveUserLocationCountry(i);
        props.saveUserLocationCity(null);
        props.saveUserLocationArea(null);
        DeviceCountry.getCountryCode().then((result) => {
            console.log("Device Country ::::", result);
            if (countryId && result.code.toUpperCase() != countryId.toUpperCase()) {
                if (countryId.toUpperCase() == "CA") {
                    props.saveUserMarketPlace("United States");
                } else if (countryId.toUpperCase() == "US") {
                    props.saveUserMarketPlace("Canada");
                } else {
                    props.saveUserMarketPlace("default");
                }
            } else {
                props.saveUserMarketPlace("default"); s
            }
        }).catch((e) => {
            console.log(e);
            props.saveUserMarketPlace("default");
        });
        console.log("user Data ::::", props.userData);
        if (props?.userData !== null) {
            //console.log("user Data111111111 ::::",props.userData);
            let emailId = props.userData;
            let emailCrypted = Buffer.from(emailId).toString('base64');
            console.log("VKFKFKFKKFVFKKFKFK", emailCrypted);
            getRegisterUser(emailCrypted, emailId);
            setTimeout(function () {
                RNRestart.Restart();
            }, 300);
        } else {
            setTimeout(function () {
                RNRestart.Restart();
            }, 300);
        }
    }

    const getRegisterUser = (emailCrypted, emailId) => {
        props.checkUserExist(emailCrypted).then((res) => {
            console.log("Check user exist res ::::", res)
            if (res && res.payload && res.payload.status == 200 && res.payload.data != '') {
                console.log("User Exist ::::");
            } else {
                console.log("User Exist not ::::");
                let phoneNumber = '';
                let email = emailId;
                let name = props.getLocation && props.getLocation.name ? props.getLocation.name : email;
                let uid = emailId;
                let data = {
                    email: email,
                    createdBy: email,
                    updatedBy: email,
                    emailOk: "Y",
                    emailSubcription: "Y",
                    name: name,
                    mob: phoneNumber,
                    mobOk: "Y",
                    authType: "AU",
                    password: "",
                    country: "",
                    gender: "",
                    companyId: "",
                    roleId: "",
                    createdBy: email,
                    updatedBy: email
                }
                console.log("User Exist not ::::", data);
                props.registerApi(emailCrypted, data);
            }
        });
    }

    const [routeHead, setRouteHead] = React.useState('');
    console.log("setAreaId ::::", areaId, value)
    return (
        <View style={{ flex: 1 }}>
            <SafeAreaView style={{ flex: 1, backgroundColor: BLUE }}
                edges={['top']}
            >
                <HomeHeader
                    title={
                        (props.getLocation && props.getLocation.locationCountry != null) ?
                            // ( props.getLocation.locationDataCity.cityName.length>5? props.getLocation.locationDataCity.cityName.substring(0,5)+'..,'+props.getLocation.locationDataArea.areaName:props.getLocation.locationDataCity.cityName+',\n'+props.getLocation.locationDataArea.areaName)
                            countryId
                            : countryId
                    }
                    fontColor={'#fff'}
                    search={search}
                    setSearch={setSearchValue}
                    fontSize={28}
                    showSearch={false}

                    onSearchPress={() => {
                        props.addToSearch(search);
                        navigation?.push('Search');
                    }}
                    badgeValue={props.favData.length > 0 ? props.favData.length : null}
                    onFontPress={() => {
                        setUserLocation(true);
                    }}
                    onRightPress={() => navigation?.push('FavScreen')}
                    // onFontPress={()=>navigation.toggleDrawer()}
                    fontSizeRight1={28}
                    fontSizeRight2={28}
                    isSearch={isSearch}
                    onRightPress1={() => setSearch(!isSearch)}
                    fontNameRight1={'search1'}
                    fontNameRight2={routeHead == 'snips' ? 'delete' : 'hearto'}
                />
                <BottomTabStack.Navigator
                    tabBarOptions={{
                        activeTintColor: '#dcd02e',
                        style: {
                            backgroundColor: BLUE,
                        },
                        inactiveBackgroundColor: '#1776d3',
                        activeBackgroundColor: '#1776d3',
                        inactiveTintColor: '#fff',
                        labelStyle: {
                            fontSize: 10,
                            margin: 0,

                            padding: 0,
                        },
                    }}
                    tabPress={() => setRouteHead('snips')}
                    screenOptions={({ route }) => ({
                        tabBarIcon: ({ focus, color, size }) => {
                            if (route.name === 'offers') {
                                return (
                                    <Icons.MaterialIcons name={'home'} size={20} color={color} />
                                );
                            } else if (route.name === 'Stores') {
                                return <Icons.AntDesign name={'earth'} size={20} color={color} />;
                            } else if (route.name === 'Department') {
                                return (
                                    <Icons.AntDesign name={'appstore-o'} size={20} color={color} />
                                );
                            } else if (route.name === 'Profile') {
                                return (
                                    <Icons.FontAwesome name={'user'} size={20} color={color} />
                                );
                            } else if (route.name === 'My List') {
                                return <Icons.Entypo name={'scissors'} size={20} color={color} />;
                            }
                            return (
                                <Icons.MaterialCommunityIcons
                                    name={'home'}
                                    size={20}
                                    color={color}
                                />
                            );
                        },
                    })}>
                    <BottomTabStack.Screen name="Offers" component={Offers} />
                    <BottomTabStack.Screen name="Stores" component={Stores} />
                    <BottomTabStack.Screen name="Department" component={Product} />
                    <BottomTabStack.Screen
                        name="My List"
                        options={{
                            headerRight: () => <Icons.AntDesign name={'delete'} />,
                        }}
                        component={Snips}
                    />
                    <BottomTabStack.Screen name="Profile" component={Profile} />

                </BottomTabStack.Navigator>
                <Overlay isVisible={showUserLocation}
                    height={600}
                >
                    <View style={{ width: '92%', }}>
                        {/* <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'position' : 'height'}
                            keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : 70}
                            enabled>
                            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}> */}
                        <View style={{ width: "92%", backgroundColor: '#ffffff', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20, height: 500 }}>
                            {!open && !areaOpen && <Text style={{ alignSelf: 'center', fontSize: 17, fontWeight: 'bold', width: '100%', marginBottom: 20, marginTop: 20 }}>Find sellers from specific country</Text>}
                            {!areaOpen && <DropDownPicker
                                placeholder='Select a Country'
                                schema={{
                                    label: 'countryName',
                                    value: 'countryId'
                                }}
                                maxHeight={240}
                                style={{ height: 45 }}
                                open={countryOpen}
                                value={countryId}
                                items={countryArr}
                                setOpen={setCountryOpen}
                                setValue={setCountryId}
                                onChangeValue={(item) => {
                                    let array = countryArr.filter(data => {
                                        console.log("data.countryId == item data :::", data.countryId, "::: Items ::", item)
                                        return data.countryId == item
                                    })
                                    console.log(array);
                                    getCountryVal(array[0]);
                                }}
                                setItems={setCountryArr}
                            />}
                            <Text style={{ alignSelf: 'center', fontSize: 17, fontWeight: 'bold', marginTop: 25, 'width': '100%' }}>{'Setting Location'}</Text>
                            <Text style={{ alignSelf: 'center', marginTop: 20, fontSize: 12 }}>Let you find stores from your city in Nearby section</Text>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold', marginTop: 15 }}>
                                {(valueCity && valueCity.cityName ? valueCity.cityName + ', ' : '') + (valueArea && valueArea.areaName ? valueArea.areaName : '')}</Text>
                            <View style={{ height: 20 }} />
                            <DropDownPicker
                                placeholder='Select a State/Province'
                                schema={{
                                    label: 'cityName',
                                    value: 'cityId'
                                }}
                                maxHeight={240}
                                style={{ height: 45, zIndex: 40 }}
                                open={open}
                                value={value}
                                items={cityArr}
                                setOpen={setOpen}
                                setValue={setValue}
                                onChangeValue={(item) => {
                                    let array = cityArr.filter(data => data.cityId == item)
                                    console.log(array);
                                    getCityVal(array[0])
                                }}
                                setItems={setCityArr}
                                searchable={true}
                                searchTextInputStyle={{
                                    borderWidth: 0,
                                    height: 30
                                }}
                                searchPlaceholder="Search state/province ...."
                                dropDownDirection="TOP"
                                searchTextInputProps={{
                                    autoCorrect: false,
                                    spellCheck: false,
                                    keyboardType: "visible-password",
                                    autoComplete: "off",
                                    textContentType: "none"
                                }}
                            />
                            {!open && (
                                <DropDownPicker
                                    placeholder='Select a City'

                                    schema={{
                                        label: 'areaName',
                                        value: 'areaId'
                                    }}
                                    style={{ height: 45, marginTop: 10, zIndex: 40 }}
                                    open={areaOpen}
                                    value={areaId}
                                    maxHeight={240}
                                    items={AreaArr}
                                    setOpen={setAreaOpen}
                                    setValue={setAreaId}
                                    setItems={setAreaArr}
                                    onChangeValue={(item) => {
                                        let array = AreaArr.filter(data => data.areaId == item)
                                        console.log(array);
                                        getAreaVal(array[0])
                                    }}
                                    searchable={true}
                                    searchTextInputStyle={{
                                        borderWidth: 0,
                                        height: 30
                                    }}
                                    searchPlaceholder="Search city ...."
                                    dropDownDirection="TOP"
                                    searchTextInputProps={{
                                        autoCorrect: false,
                                        spellCheck: false,
                                        keyboardType: "visible-password",
                                        autoComplete: "off",
                                        textContentType: "none"
                                    }}
                                />
                            )}
                            <ThemeButton
                                btnMainStyle={{ width: 280, marginTop: 50, marginBottom: 20, }}
                                btnDisable={!(areaId !== null && value !== null)}
                                onBtnPress={() => {
                                    setUserLocation(false);
                                    console.log('DATAAaaaay', value);
                                    if (valueCity.cityName != 'Select State/Province' && valueArea && valueArea.areaName != 'Select City') {
                                        setUserLocation(false);
                                        props.saveUserLocationArea(valueArea);
                                    }
                                }}
                                btnText={'Continue'} />
                        </View>
                        {/* </ScrollView>
                        </KeyboardAvoidingView> */}
                        {/* <TouchableOpacity style={{position:'absolute',margin:5}} onPress={()=> {
              toggleOverlay()
            }}>0
                    <Image source={close} style={{width:20,height:20}}>
            </Image>
            </TouchableOpacity> */}
                    </View>

                </Overlay>
            </SafeAreaView>
        </View >
    );
};

const mapStateToProps = (state) => ({
    favData: state.productTab.favourite,
    getLocation: state.location,
    userData: state.auth.userData
});

const mapDispatchToProps = (dispatch) => ({
    getCity: (userid) => dispatch(getCity(userid)),
    getArea: (userid, cityid) => dispatch(getArea(userid, cityid)),
    saveUserLocationCity: (data) => dispatch(saveUserLocationCity(data)),
    saveUserLocationArea: (data) => dispatch(saveUserLocationArea(data)),
    saveUserLocationCountry: (data) => dispatch(saveUserLocationCountry(data)),
    saveUserMarketPlace: (data) => dispatch(saveUserMarketPlace(data)),
    addToSearch: (data) => dispatch(addToSearch(data)),
    checkUserExist: (userId) => dispatch(checkUserExist(userId)),
    registerApi: (id, params) => dispatch(userLogin(id, params)),
});
export default connect(mapStateToProps, mapDispatchToProps)(BottomTab);
