
import React, { useState } from 'react'
import {
    View, Text, 
    TouchableOpacity,
    ActivityIndicator,
    Dimensions
} from 'react-native'
import { Overlay } from 'react-native-elements'
import ImageViewer from 'react-native-image-zoom-viewer'


interface headerProps {

    showAlert?: boolean,
    images:[],
 
}

//[{url:"https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",freeHeight:true,}]


export const ImageZoom = ({
    showAlert,
    images,

    ...props
}: headerProps) => {
    return (
        <Overlay isVisible={showAlert}>
            <View style={{width:Dimensions.get('window').width-100,height:Dimensions.get('window').height/2,alignItems:'center',justifyContent:'center'}}>
       <ImageViewer imageUrls={images}
       style={{
        height:Dimensions.get('window').height/2,
        width:Dimensions.get('window').width-100
       }}
    enableSwipeDown={true}/>

         
            </View>
            
        </Overlay>
    )
}

