import React from 'react'
import { View, Text, useWindowDimensions } from 'react-native'
import BottomTab from './BottomTabNavigation'
import { About } from '../Screens/About'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack'
import CustomDrawerContent from '../Components/DrawerContent/DrawerContent'

const Drawer = createDrawerNavigator()


const DrawerNavigation = () => {

  const dimensions = useWindowDimensions();

  const isLargeScreen = dimensions.width >= 768;

  return (
    <Drawer.Navigator openByDefault={false} drawerContent={props => <CustomDrawerContent {...props} />} initialRouteName="Home">
      <Drawer.Screen name="Home" component={BottomTab} />
      <Drawer.Screen name="About" component={About} />

    </Drawer.Navigator>
  )
}
export default DrawerNavigation