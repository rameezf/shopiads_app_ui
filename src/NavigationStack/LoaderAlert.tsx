
import React, { useState } from 'react'
import LottieLoader from 'react-native-lottie-loader';
import {
    LOADER,
  } from '../Constants/images';

import {
    View, Text, 
    TouchableOpacity,
    ActivityIndicator
} from 'react-native'
import { Overlay } from 'react-native-elements'


interface headerProps {

    showAlert?: boolean,
 
}

export const LoaderAlert = ({
    showAlert,
  
    ...props
}: headerProps) => {
    return (
        <LottieLoader visible={showAlert}
        source ={LOADER}
        speed={2}
        
        />

    )
}

