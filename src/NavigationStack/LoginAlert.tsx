
import React, { useState } from 'react'
import {
    View, Text, 
    TouchableOpacity
} from 'react-native'
import { Overlay } from 'react-native-elements'


interface headerProps {

    showAlert?: boolean,
    onLoginPress?: () => void,
    onCancelPress?: () => void,
}

export const LoginAlert = ({
    showAlert,
    onLoginPress,
    onCancelPress,
    ...props
}: headerProps) => {
    return (
        <Overlay isVisible={showAlert}>
            <Text style={{ marginTop: 10 }}>{'To Access this option you need to login first?'}</Text>
            <View style={{ flexDirection: 'row', marginTop: 20, }}>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    marginTop: 5, flex: 1, margin: 10, height: 35, backgroundColor: 'grey'
                }} onPress={onCancelPress}>
                    <Text style={{
                        paddingHorizontal: 10,
                        fontSize: 14,
                        fontWeight: 'bold', color: '#ffffff'
                    }}>{'Cancel'}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    marginTop: 5,
                    backgroundColor: '#1776d3', flex: 1, margin: 10, height: 35,
                }} onPress={onLoginPress}>
                    <Text style={{
                        paddingHorizontal: 10,
                        fontSize: 14,
                        fontWeight: 'bold', color: '#ffffff'
                    }}>{'Okay'}</Text>
                </TouchableOpacity>
            </View>
        </Overlay>
    )
}

