
import React, { useState } from 'react'
import {
    View, Text, 
    TouchableOpacity
} from 'react-native'
import { Overlay } from 'react-native-elements'


interface headerProps {

    showAlert?: boolean,
    marketPlace?: string,
    onLoginPress?: () => void,
    onCancelPress?: () => void,
}

export const MarketPlaceAlert = ({
    showAlert,
    marketPlace,
    onLoginPress,
    onCancelPress,
    ...props
}: headerProps) => {
    return (
        <Overlay isVisible={showAlert}>
            <Text style={{ marginTop: 10 }}>We're showing you stores that ship to <Text style={{fontWeight: 'bold' }}>{marketPlace}</Text>. To access all stores select view all</Text>
            <View style={{ flexDirection: 'row', marginTop: 20, }}>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    marginTop: 5, flex: 1, margin: 10, height: 35, backgroundColor: '#1776d3'
                }} onPress={onCancelPress}>
                    <Text style={{
                        paddingHorizontal: 10,
                        fontSize: 14,
                        fontWeight: 'bold', color: '#ffffff'
                    }}>{'Continue'}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                    marginTop: 5,
                    backgroundColor: 'grey', flex: 1, margin: 10, height: 35,
                }} onPress={onLoginPress}>
                    <Text style={{
                        paddingHorizontal: 10,
                        fontSize: 14,
                        fontWeight: 'bold', color: '#ffffff'
                    }}>{'View All'}</Text>
                </TouchableOpacity>
            </View>
        </Overlay>
    )
}

