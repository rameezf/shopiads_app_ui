import { SvgAst } from "react-native-svg";
import * as ACTION_TYPES from "../Constants/ActionTypes";

const initialState = {
    productKey: null,
    departmentKey: 0,
    profileImg: null,
    storeKey: null,
    offerId: null,
    storeType: null,
    snips: [],
    favourite: [],
    store: [],
    subCategoryId: null,
    followStore: [],
    viewAllProduct: { productType: false, product: [] },//  Product Type True  -> render Product else Banner,
    zoomImages: [],
    search:"",
    videoUrl:"",
    categories:[],
    subCategories:{}
}

export default (state = initialState, action) => {
    const { type, payload, error } = action;

    switch (type) {
        case ACTION_TYPES.SAVE_PRODUCT_KEY:
            return {
                ...state,
                productKey: payload
            }

        case ACTION_TYPES.SAVE_STORE_TYPE: {
            return {
                ...state,
                storeType: payload
            }
        }
        case ACTION_TYPES.SAVE_OFFER_ID: {
            return {
                ...state,
                offerId: payload
            }
        }
        case ACTION_TYPES.SAVE_DEPARTMENT_KEY:
            return {
                ...state,
                departmentKey: payload
            }

        case ACTION_TYPES.SAVE_SUBCATEGORY_ID:
            return {
                ...state,
                subCategoryId: payload
            }

        case ACTION_TYPES.SAVE_ALL_STORE:
            return {
                ...state,
                store: payload
            }
        case ACTION_TYPES.SAVE_STORE_KEY:
            return {
                ...state,
                storeKey: payload
            }
        case ACTION_TYPES.SAVE_PROFILE_IMAGE:
            return {
                ...state,
                profileImg: payload
            }


        case ACTION_TYPES.SNIPS_SELECTED:
            return {
                ...state,
                snips: [...state.snips, payload]
            }
        case ACTION_TYPES.SNIPS_DELETED:
            return {
                ...state,
                snips: payload
            }
        case ACTION_TYPES.ADD_TO_FAVOURITE:
            return {
                ...state,
                favourite: [...state.favourite, payload]
            }

        case ACTION_TYPES.ADD_TO_CATEGORIES:
            return {
                ...state,
                categories:payload
            }   
        case ACTION_TYPES.ADD_TO_SUB_CATEGORIES:{
            return {
                ...state,
                subCategories:payload
            }
        }     
        case ACTION_TYPES.DELETE_FOLLOW_STORE:
            return {
                ...state,
                followStore: payload
            }

        case ACTION_TYPES.CLEAR_TO_FAVOURITE:
            return {
                ...state,
                favourite: []
            }
        case ACTION_TYPES.DELETE_FAVOURITE:
            return {
                ...state,
                favourite: payload
            }
        case ACTION_TYPES.ADD_TO_FOLLOW:
            return {
                ...state,
                followStore: payload
            }

        case ACTION_TYPES.ADD_TO_VIDEO:
                return {
                    ...state,
                    videoUrl:payload
                }    
        case ACTION_TYPES.VIEW_ALL_PRODDUCT:
            return {
                ...state,
                viewAllProduct: payload
            }
        case ACTION_TYPES.ZOOM_IMAGES:
            return {
                ...state,
                zoomImages: payload
            }
        case ACTION_TYPES.ADD_TO_SNIPS:
            return {
                ...state,
                snips:payload    
            }    
        case ACTION_TYPES.ADD_TO_SEARCH:
            return {
                ...state,
                search:payload
            }    
        default:
            return state;
    }
}