import AsyncStorage from "@react-native-async-storage/async-storage";
import * as ACTION_TYPES from "../Constants/ActionTypes";

const initialState = {
    locationDataCity: null,
    locationDataArea: null,
    email:null,
    name:null,
    locationCountry:null,
    userMarketPlace:null,
}

export default (state = initialState, action) => {
    const { type, payload, error } = action;
    switch (type) {
        case ACTION_TYPES.SAVE_USER_LOCATION_CITY:
        return {
                ...state,
                locationDataCity: payload
            }
        case ACTION_TYPES.SAVE_USER_LOCATION_AREA:
           
        return {
                ...state,
                locationDataArea: payload
            }
        case ACTION_TYPES.SAVE_EMAIL:{
            return {
                ...state,
                email: payload
            }
        }    
        case ACTION_TYPES.SAVE_NAME:{
            return{
                ...state,
                name:payload
            }
        }
        case ACTION_TYPES.SAVE_USER_LOCATION_COUNTRY:
            return {
                ...state,
                locationCountry: payload
            }
        case ACTION_TYPES.SAVE_USER_MARKET_PLACE:
            return {
                ...state,
                userMarketPlace: payload
            }
        default:
            return state;
    }
}