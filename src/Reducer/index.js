import { combineReducers } from "redux";
//import { reducer as formReducer } from "redux-form";
import { reducer as modalReducer } from "redux-modal";
import productTab from './product'
import location from './location'
import auth from './auth'

export default combineReducers({
     modal: modalReducer,
     auth,
     productTab,
     location
})