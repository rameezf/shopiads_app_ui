import * as ACTION_TYPES from "../Constants/ActionTypes";

const initialState = {
    userData: null,

}

export default (state = initialState, action) => {
    const { type, payload, error } = action;
    switch (type) {
        case ACTION_TYPES.SAVE_USER_DATA:{
        return {
                ...state,
                userData: payload
            }
        }
        default:
            return state;
    }
}